//
//  HealthPointsViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 8/29/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import JTProgressHUD


class HealthPointsViewController: AccordionTableViewController,AccordionTableViewControllerDelegate{
  
    var hpModelDataSource:[HealthDailyLog] = [];
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    
    private lazy var viewModel:HealthPointViewModel = {
        return HealthPointViewModel();
    }();
    
    var StringHelp = "";
    private var loading:Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Health Points"
        
        JTProgressHUD.show()

        
        loading = true;
         viewModel.getHealthLog(self.appDelegate.user!, timestamp: "000000", completion: { (result, dailyTotals) in
            self.hpModelDataSource = result;
            self.sections.removeAll();
            for i in 0..<self.hpModelDataSource.count{
                self.sections.append(HealthPointSection.section(self.hpModelDataSource[i]));
                
            }
            
            if !dailyTotals.isEmpty{
                
                let lblTotal = UILabel(frame: CGRectMake(0,0,CGRectGetWidth(self.tableView!.frame),33));
                lblTotal.text = "Total Health Points: "+dailyTotals;
                lblTotal.textAlignment = .Center;
                lblTotal.backgroundColor = UIColor.geiaDarkGrayColor();
                lblTotal.textColor = UIColor.whiteColor();
                lblTotal.font = UIFont.geiaFontOfSize(18);
                self.tableView!.tableHeaderView = lblTotal;
            }
            
            self.oneSectionAlwaysOpen = true
            self.delegate = self
            JTProgressHUD.hide()
            self.loading = false;
            self.tableView.reloadData();
         });
        
        viewModel.getPrescriptionHelp { (result) in
            self.StringHelp = result;
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(HealthPointsViewController.dismissViewController));
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Help", style: .Plain, target: self, action: #selector(HealthPointsViewController.goToHelp));

    }

    override func viewWillAppear(animated: Bool) {
        if loading  == false{
            loading = true;
            self.sections.removeAll();
            self.tableView.reloadData();
            viewModel.getHealthLog(self.appDelegate.user!, timestamp: "000000", completion: { (result, dailyTotals) in
                self.hpModelDataSource = result;

                for i in 0..<self.hpModelDataSource.count{
                    self.sections.append(HealthPointSection.section(self.hpModelDataSource[i]));
                    
                }
                
                if !dailyTotals.isEmpty{
                    
                    let lblTotal = UILabel(frame: CGRectMake(0,0,CGRectGetWidth(self.tableView!.frame),33));
                    lblTotal.text = "Total Health Points: "+dailyTotals;
                    lblTotal.textAlignment = .Center;
                    lblTotal.backgroundColor = UIColor.geiaDarkGrayColor();
                    lblTotal.textColor = UIColor.whiteColor();
                    lblTotal.font = UIFont.geiaFontOfSize(18);
                    self.tableView!.tableHeaderView = lblTotal;
                }
                
                self.oneSectionAlwaysOpen = true
                self.delegate = self
                JTProgressHUD.hide()
                self.loading = false;
                self.tableView.reloadData();
            });
            
        }
    }
    
    func goToHelp(){
        //presentHelp
        self.performSegueWithIdentifier("goToHelp", sender: self);
    }
    
    func dismissViewController(){
        self.navigationController!.dismissViewControllerAnimated(true, completion: nil);
    }
    
    
    func accordionTableViewControllerSectionDidOpen(section: Section){
        
    }
    
    func accordionTableViewControllerSectionDidClose(section: Section){
        
    }
    
    
    func sectionForDate(date: NSDate) -> Section{
        
        let viewDay = NSBundle.mainBundle().loadNibNamed("HealthPointSectionHeader", owner: self, options: nil)![0]  as! UIView;
        let viewRay = NSBundle.mainBundle().loadNibNamed("HealthPointSectionHeader", owner: self, options: nil)![1] as! UIView;
        
        viewRay.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 320);
        viewDay.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 44);

        let lblTextDate = viewDay.viewWithTag(99) as! UILabel;
        let middleTextDate = viewDay.viewWithTag(98) as! UILabel;
        if date.isToday() {
            lblTextDate.text = "Today";
        }else{
            if date.isEqualToDate(NSDate().dateBySubtractingDays(1)!){
                lblTextDate.text = "Yesterday";
            }else{
                lblTextDate.text = date.getDayOfWeekString();
            }
        }
        
        middleTextDate.text = date.stringForDate();
        
        
        let section =  Section();
        section.view = viewRay;
        section.overlayView = viewDay;
        section.headerView = SectionHeaderView();
        section.headerView?.backgroundHeaderView = viewDay;
        section.backgroundColor = UIColor.geiaDarkGrayColor();
        
        return section;
    }
    

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return !StringHelp.isEmpty;
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let destinationController = (segue.destinationViewController as! UINavigationController).viewControllers.first as! HealthHelpViewController! ;
        
        destinationController.htmlStringHelp = StringHelp;
        
        
    }

}
