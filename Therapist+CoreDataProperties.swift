//
//  Therapist+CoreDataProperties.swift
//  Geia
//
//  Created by Sergio Solorzano on 10/3/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData

extension Therapist {

    @nonobjc public override class func fetchRequest() -> NSFetchRequest {
        return NSFetchRequest(entityName: "Therapist");
    }

    @NSManaged public var id: String?
    @NSManaged public var last_name: String?
    @NSManaged public var first_name: String?
    @NSManaged public var avatar: String?
    @NSManaged var user: User?

}
