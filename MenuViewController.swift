//
//  MenuViewController.swift
//  Geia
//
//  Created by zurce on 4/18/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class MenuViewController: GeiaViewController ,UITableViewDataSource,UITableViewDelegate{

    weak var delegate:MenuViewControllerDelegate?

    
    var menuData = [(name: String, icon: String, index: Int, subMenu: [ (name: String, type: cellType, index: Int) ])]();
    
    enum cellType: Int{
        case cellTypeDefault = 0
        case cellTypeSwitch
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuData = [
            
            (name: "My Profile", icon: "avatar", index: 50, subMenu: [
                (name: "Apple Health Settings", type: .cellTypeDefault, index: 51),
                (name: "My Survey", type: .cellTypeDefault, index: 92),
                (name: "DATA SYNCH", type: .cellTypeDefault, index: 53)
                ]),
            (name: "Settings", icon: "", index: 60, subMenu: [
                (name: "Add Custom Video", type: .cellTypeDefault, index: 61)
                ]),
            (name: "Devices", icon: "", index: 90, subMenu: [
                (name: "Fitbit", type: .cellTypeSwitch, index: 91)
                ]),
            (name: "Log out", icon: "closeMenuIcon", index: 70, subMenu: [])
        ]
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadTable(){
        
        tableView.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return menuData.count;

    }

    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 1));
        headerView.backgroundColor = UIColor.geiaLightGrayColor();
        return headerView;
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1;
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuData[section].subMenu.count+1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var identifier: String // = indexPath.row == 0 ? "cellHead" :  "cellText" ;
        
        if indexPath.row == 0 {
            identifier = "cellHead"
        } else {
            
            switch menuData[indexPath.section].subMenu[indexPath.row-1].type {
            case .cellTypeSwitch:
                identifier = "cellTextWithSwitch"
            default:
                identifier = "cellText"
            }
            
        }
        
        let cell =  tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath);


        
        
        if indexPath.row == 0 {
            let imgBadge = cell.viewWithTag(99) as! UIImageView;
            imgBadge.image = UIImage(named: menuData[indexPath.section].icon);
            let lblTitle = cell.viewWithTag(10) as! UILabel;
            lblTitle.text = menuData[indexPath.section].name;
        } else if menuData[indexPath.section].subMenu[indexPath.row-1].type == .cellTypeSwitch {
            
            let lblTitle = cell.viewWithTag(10) as! UILabel;
            lblTitle.text = "• " + menuData[indexPath.section].subMenu[indexPath.row-1].name;
            
            let switchView = cell.viewWithTag(20) as! UISwitch;
            
            if menuData[indexPath.section].subMenu[indexPath.row-1].name == "Fitbit" {
                
                let fitbitManager = FitbitManager.sharedInstance
                
                if fitbitManager.checkIsLogin() == true {
                    
                    switchView.on = true
                } else {
                    
                    switchView.on = false
                }
                
            }
            
        } else{
            //
            let lblTitle = cell.viewWithTag(10) as! UILabel;
            lblTitle.text = "• " + menuData[indexPath.section].subMenu[indexPath.row-1].name;
        }
        
        return cell;
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == 0 {
            self.delegate?.didSelectOption(menuData[indexPath.section].index);
        } else {
            
            if menuData[indexPath.section].subMenu[indexPath.row-1].type == .cellTypeSwitch {

                let cell = tableView.cellForRowAtIndexPath(indexPath);
                let switchView = cell!.viewWithTag(20) as! UISwitch;
                
                switchView.setOn(!switchView.on, animated: true)
                
            }
            
            self.delegate?.didSelectOption(menuData[indexPath.section].subMenu[indexPath.row-1].index);
        }
        
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true);
        
    }
    

}

protocol MenuViewControllerDelegate: class {
    func didSelectOption(selectedOption: NSInteger)
}
