//
//  NewGoalsViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import JTProgressHUD
import DZNEmptyDataSet

class NewGoalsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate{
    var dataSource:[GoalMeasure] = [];
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    
    private lazy var viewModel:NewGoalViewModel = {
        return NewGoalViewModel();
    }();
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Goals";
        self.tableView.emptyDataSetSource = self;
        self.tableView.emptyDataSetDelegate = self;
        self.tableView.tableFooterView = UIView();
        JTProgressHUD.show()
        viewModel.getGoalsMeasures(self.appDelegate.user!) { (result) in
            self.dataSource = result;
            JTProgressHUD.hide()
            self.tableView.reloadData();

        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(NewGoalsViewController.dismissViewController));

        

        
        // Do any additional setup after loading the view.
    }

    override func didMoveToParentViewController(parent: UIViewController?) {
        if (self.tableView.pullToRefreshView == nil) {
            self.tableView.addPullToRefreshWithActionHandler {
                self.viewModel.getGoalsMeasures(self.appDelegate.user!) { (result) in
                    self.dataSource = result;
                    self.tableView.reloadData();
                    self.tableView.pullToRefreshView.stopAnimating()
                    
                    
                }
            };
            
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let goal = self.dataSource[indexPath.row];
        let rate = CGFloat(70 * (goal.questions?.count)!);
        return 70 + rate + 55;
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:GoalTableViewCell = tableView.dequeueReusableCellWithIdentifier("goalCell", forIndexPath: indexPath) as! GoalTableViewCell;
        cell.setupWithGoal(self.appDelegate.managedObjectContext,goal: self.dataSource[indexPath.row]);
        cell.buttonSave.tag = indexPath.row;
        cell.buttonSave.addTarget(self, action: #selector(NewGoalsViewController.saveGoal(_:)), forControlEvents: UIControlEvents.TouchDown);
        return cell;
    }
    
    func saveGoal(sender: UIButton){
        JTProgressHUD.show()

        viewModel.updateGoalMeassure(dataSource[sender.tag]) { (result) in

            if result == true{
                print("success")
            }else{
                print("failed")
            }
            JTProgressHUD.hide()

        }
        
    }
    func dismissViewController(){
        self.navigationController!.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {

        return NSAttributedString(string: "No Goals", attributes: [NSFontAttributeName: UIFont.geiaFontOfSize(18),NSForegroundColorAttributeName:UIColor.whiteColor()]);
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "New Goal Measures are going to appear once your PT Reviews your tracking and performance", attributes: [NSFontAttributeName: UIFont.geiaFontOfSize(15),NSForegroundColorAttributeName:UIColor.lightGrayColor()]);
    }
    
}

