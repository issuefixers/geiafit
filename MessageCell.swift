//
//  MessageCell.swift
//  Geia
//
//  Created by Sergio Solorzano on 1/26/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var lblMessage: MessageLabel!
    @IBOutlet weak var lblSender: UILabel!
}


class MessageLabel: UILabel {
    
    
    let topInset = CGFloat(5.0), bottomInset = CGFloat(5.0), leftInset = CGFloat(5.0), rightInset = CGFloat(5.0)
    
    override func drawTextInRect(rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override func intrinsicContentSize() -> CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize()
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
}