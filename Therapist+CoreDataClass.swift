//
//  Therapist+CoreDataClass.swift
//  Geia
//
//  Created by Sergio Solorzano on 10/3/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData

@objc(Therapist)
public class Therapist: NSManagedObject {

    
    class func getTherapistWithId(moc: NSManagedObjectContext, therapistId: String) -> Therapist?{
        var therapistes: [Therapist]?
        
        let fetchRequest        = NSFetchRequest.init(entityName: "Therapist")
        
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "id = %@", therapistId)
        
        do {
            therapistes = (try moc.executeFetchRequest(fetchRequest)) as? [Therapist]
            if ( therapistes?.count  > 0){
                return therapistes![0];
            }
        } catch {
            print("Error Querying")
            return nil
        }
        return nil
        
    }
}
