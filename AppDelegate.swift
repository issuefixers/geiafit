//
//  AppDelegate.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 11/19/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import CoreData
import Google

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var blockRotation: Bool = false
    var user: User?
    var startedApp:Bool = false;

    // MARK: - App life cycle
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        print(applicationDocumentsDirectory.path)
        
        Fabric.with([Crashlytics.self])

        self.window?.tintColor = UIColor.geiaDarkBlueColor()

        UINavigationBar.appearance().backgroundColor = UIColor.blackColor();
        UINavigationBar.appearance().translucent = true;
        UINavigationBar.appearance().barTintColor = UIColor.blackColor();
        UIBarButtonItem.appearance().tintColor = UIColor.whiteColor();
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName : UIFont.geiaFontOfSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        var configureError:NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai.trackUncaughtExceptions = true  // report uncaught exceptions
        
        var GAILevel:GAILogLevel = GAILogLevel.None;
        if NSBundle.mainBundle().objectForInfoDictionaryKey("Analytics") as! Bool == true {
        // remove before app release
            GAILevel = GAILogLevel.Verbose
            gai.optOut = true;
        }
        gai.logger.logLevel = GAILevel;
        
        
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        startedApp = true;
        
        let paymentManager = PaymentManager();
        paymentManager.setReturnURLScheme()
        
        return true
    }

//    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
//        if (self.blockRotation) {
//            //print("supportedInterfaceOrientations - PORTRAIT")
//            return UIInterfaceOrientationMask.Portrait
//        } else {
//            //print("supportedInterfaceOrientations - ALL")
//            return UIInterfaceOrientationMask.All
//        }
//    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        let paymentManager = PaymentManager()
        
        if url.scheme!.localizedCaseInsensitiveCompare(paymentManager.getURLScheme()) == .OrderedSame {
            return paymentManager.handleOpenURL(url, sourceApplication:sourceApplication!)
        }
        return false
    }

    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        if (url.scheme == FitbitManager.sharedInstance.scheme ) {
            FitbitManager.sharedInstance.handleRedirectUrl(url)
        }
        return true
    }
    

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0;

    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    var _requestHeaders: Dictionary<String, String>?;
    
    // MARK: - Networking
    func requestHeaders()->Dictionary<String, String> {
        if((_requestHeaders == nil)){
            let sessionName: String = NSUserDefaults.standardUserDefaults().stringForKey(SESSION_NAME_KEY)!
            let sessionID: String   = NSUserDefaults.standardUserDefaults().stringForKey(SESSION_ID_KEY)!
            let token: String       = NSUserDefaults.standardUserDefaults().stringForKey(CSRF_TOKEN_KEY)!
             _requestHeaders = [
             sessionName:    sessionID,
             "X-CSRF-Token": token
             ]
        }
        
        return _requestHeaders!
   }
    
    //MARK - Push Notification
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        
//        print(userInfo);
        
      

    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
        if(application.applicationState == .Active){
            if  let category = userInfo["aps"]!["category"]{
                if category as! String == "MESSAGE"{
                    
                    NotifictionManager.showMessageNotification(title: "New Message from PT", message: userInfo["aps"]!["alert"] as!  String,screen: 10);
                }else if category as! String == "SNAPSHOT"{
                    NotifictionManager.showMessageNotification(title: "Your Snapshot Review is ready", message: userInfo["aps"]!["alert"] as!  String,screen: 23);
                    
                }

                }
        }else{
            if  let category = userInfo["aps"]!["category"]{
                if category as! String == "MESSAGE"{
                    NSUserDefaults.standardUserDefaults().setInteger(10, forKey: "initialScreen");
                }else if category as! String == "SNAPSHOT"{
                    NSUserDefaults.standardUserDefaults().setInteger(23, forKey: "initialScreen");

                }

            }
            NSUserDefaults.standardUserDefaults().setInteger(10, forKey: "initialScreen");
        }
        
        completionHandler(UIBackgroundFetchResult.NewData);

    }
    
    
    // implemented in your application delegate
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        let deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        
        
        
        print("Got token data! \(deviceTokenString)")
        

        NSUserDefaults.standardUserDefaults().setValue(deviceTokenString, forKey: "deviceToken");
        
        NSNotificationCenter.defaultCenter().postNotificationName("reportDevice", object: nil);

        
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Couldn't register: \(error)")
    }
    
    

    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "biz.wellnesslinks.Geia" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Geia", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Geia_3.sqlite")
		let options = [NSInferMappingModelAutomaticallyOption: true, NSMigratePersistentStoresAutomaticallyOption: true]
		
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options)
        } catch {
            // Report any error we got.
            var userInfo = [String: AnyObject]()
            userInfo[NSLocalizedDescriptionKey] = "There was an error creating or loading the application's saved data."
            userInfo[NSLocalizedFailureReasonErrorKey] = "There was an error creating or loading the application's saved data."
            
            userInfo[NSUnderlyingErrorKey] = error as! NSError
            let wrappedError = NSError(domain: "biz.wellnesslinks.GeiaFitApp", code: 1001, userInfo: userInfo)
            
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            let fm = NSFileManager.defaultManager()
            
            let URLPersistentStore = self.applicationStoresDirectory();
            
            if fm.fileExistsAtPath(URLPersistentStore.path!) {
                let nameIncompatibleStore = self.nameForIncompatibleStore()
                let URLCorruptPersistentStore = self.applicationIncompatibleStoresDirectory().URLByAppendingPathComponent(nameIncompatibleStore)
                
                do {
                    // Move Incompatible Store
                    try fm.moveItemAtURL(URLPersistentStore, toURL: URLCorruptPersistentStore!)
                    
                } catch {
                    let moveError = error as! NSError
                    print("\(moveError), \(moveError.userInfo)")
                }
            }
            do {
                // Declare Options
                let options = [ NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true ]
                
                // Add Persistent Store to Persistent Store Coordinator
                try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: URLPersistentStore, options: options)
                
            } catch {
                let storeError = error as! NSError
                print("\(storeError), \(storeError.userInfo)")
            }
        }
        
        return coordinator
    }()
    
    private func applicationStoresDirectory() -> NSURL {
        let fm = NSFileManager.defaultManager()
        
        // Fetch Application Support Directory
        let URLs = fm.URLsForDirectory(.ApplicationSupportDirectory, inDomains: .UserDomainMask)
        let applicationSupportDirectory = URLs[(URLs.count - 1)]
        
        // Create Application Stores Directory
        let URL = applicationSupportDirectory.URLByAppendingPathComponent("Stores")
        
        if !fm.fileExistsAtPath(URL!.path!) {
            do {
                // Create Directory for Stores
                try fm.createDirectoryAtURL(URL!, withIntermediateDirectories: true, attributes: nil)
                
            } catch {
                let createError = error as NSError
                print("\(createError), \(createError.userInfo)")
            }
        }
        
        return URL!
    }
    
    private func applicationIncompatibleStoresDirectory() -> NSURL {
        let fm = NSFileManager.defaultManager()
        
        // Create Application Incompatible Stores Directory
        let URL = applicationStoresDirectory().URLByAppendingPathComponent("Incompatible")
        
        if !fm.fileExistsAtPath(URL!.path!) {
            do {
                // Create Directory for Stores
                try fm.createDirectoryAtURL(URL!, withIntermediateDirectories: true, attributes: nil)
                
            } catch {
                let createError = error as NSError
                print("\(createError), \(createError.userInfo)")
            }
        }
        
        return URL!
    }
    
    
    private func nameForIncompatibleStore() -> String {
        // Initialize Date Formatter
        let dateFormatter = NSDateFormatter()
        
        // Configure Date Formatter
        dateFormatter.formatterBehavior = .Behavior10_4
        dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        
        return "\(dateFormatter.stringFromDate(NSDate())).sqlite"
    }
    
    

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}
