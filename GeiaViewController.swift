//
//  GeiaViewController.swift
//  Geia
//
//  Created by zurce on 5/2/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Google.Analytics
import Google.Core

class GeiaViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension NSObject {
    var className: String {
        return NSStringFromClass(self.dynamicType)
    }
}