//
//  FitbitManager.swift
//  Geia
//
//  Created by Eugene Budnik on 8/29/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import HealthKit
import UIKit

import SwiftDate
import SwiftyJSON
import CoreData
import Alamofire

@objc enum FitbitDataType: Int16 {
    case Steps, Calories, Distance
}


class FitbitManager {
    
    private let clientID = "227S9V"//"227S9V"  "2282FF"
    private let clientSecret = "196e97dde3070dc2b708b6e41db43a44"//"196e97dde3070dc2b708b6e41db43a44"   "7c8fdd96b83b483b57aa8612a6a58964"
    internal let scheme = "geiafitcallback"
    
    private var redirectUrl = ""
    
    private var code = ""
    private var headers : [String: String]?
    private var parameters : [String: AnyObject]?
    
    private var _completionHandler: (NSError? -> Void)?
    
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private var isLogin = false
    
    
    init () {
        
        
         redirectUrl = scheme + "://"
        
        
        let accessToken =  NSUserDefaults.standardUserDefaults().objectForKey("FitBit_access_token") as? String
        
        if accessToken != nil  {
            
            
            headers = [
                "Authorization": "Bearer " + accessToken!
            ]
            
            parameters = nil
            
            
            isLogin = true
            
        } else {
            
            isLogin = false
            
        }
        
    }
    
    class var sharedInstance: FitbitManager {
        struct Singleton {
            static let instance = FitbitManager()
        }
        return Singleton.instance
    }
    
    
    func handleRedirectUrl(url : NSURL) {
        
        debugPrint(url)
        
        let components = NSURLComponents(URL: url, resolvingAgainstBaseURL: false)
        guard components != nil else {
            return
        }
        
        code = components!.code!
        
        guard code.characters.count > 0 else {
            return
        }
        
        var authorizationCode : String { return convertToBase64("\(clientID):\(clientSecret)") }
        
        headers = [
            "Authorization": "Basic \(authorizationCode)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        parameters = [
            "grant_type":"authorization_code",
            "client_id":clientID,
            "redirect_uri":redirectUrl,
            "code":code
        ]
        
        getToken()
        
    }
    
    
    func login(completionHandler: (NSError? -> Void)?) {
        
        
        
        if isLogin  {
            
            
            if completionHandler != nil {
                completionHandler!(nil)
            }
            
        } else {
            
            let escapedCallbackURL = escapedString(redirectUrl)
            let authPath = "https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=\(clientID)&redirect_uri=\(escapedCallbackURL)&scope=activity%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&expires_in=604800"
            if let authURL:NSURL = NSURL(string: authPath) {
                UIApplication.sharedApplication().openURL(authURL)
            }
            
            _completionHandler = completionHandler
            
        }
        
    }
    
    func checkIsLogin() -> Bool {
        return isLogin
    }
    
    
    func logout (completionHandler: (NSError? -> Void)?){
        
        var authorizationCode : String { return convertToBase64("\(clientID):\(clientSecret)") }
        
        headers = [
            "Authorization": "Basic \(authorizationCode)"
        ]
        
        let accessToken =  NSUserDefaults.standardUserDefaults().objectForKey("FitBit_access_token") as? String
        
        parameters = [
            "token":accessToken!
        ]
        
        
        let accessTokenUrl =  "https://api.fitbit.com/oauth2/revoke"
        
        Alamofire.request(.POST, accessTokenUrl, parameters: parameters, headers: headers)
            .validate()
            .responseJSON { (response) in
                
                
                self.removeLoginDataAndHK({ (error) in
                    
                    if completionHandler != nil {
                        completionHandler!(error)
                    }
                    
                })
                
        }
        
    }
    
    func removeLoginData(){
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey("FitBit_access_token")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("FitBit_refresh_token")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("FitBit_expires_in")
        
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        self.isLogin = false
        
    }
    
    
    func removeLoginDataAndHK(completionHandler: (NSError? -> Void)?){
        
        
        let firstDateSteps = FitbitDateUpdates.getFirstDate(self.appDelegate.user!, dataType: FitbitDataType.Steps, moc: self.appDelegate.managedObjectContext)
        let firstDateCalories = FitbitDateUpdates.getFirstDate(self.appDelegate.user!, dataType: FitbitDataType.Calories, moc: self.appDelegate.managedObjectContext)
        let firstDateDistance = FitbitDateUpdates.getFirstDate(self.appDelegate.user!, dataType: FitbitDataType.Distance, moc: self.appDelegate.managedObjectContext)
        
        
        HealthManager.removeAllFitbitData(firstDateSteps, dataType: FitbitDataType.Steps, completition: { (success, error) in
            
            HealthManager.removeAllFitbitData(firstDateCalories, dataType: FitbitDataType.Calories, completition: { (success, error) in
                
                HealthManager.removeAllFitbitData(firstDateDistance, dataType: FitbitDataType.Distance, completition: { (success, error) in
                    
                    HealthDataPoint.purgeFitbitDataPointsInContext(self.appDelegate.managedObjectContext)
                    
                    FitbitDateUpdates.purgeData(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext)
                    
                    self.removeLoginData()
                    
                    if completionHandler != nil {
                        completionHandler!(error)
                    }
                    
                })
                
            })
            
        })
        
    }
    
    
    func getToken(){
        
        let accessTokenUrl =  "https://api.fitbit.com/oauth2/token"
        
        Alamofire.request(.POST, accessTokenUrl, parameters: parameters, headers: headers)
            .validate()
            .responseJSON { (response) in
                
                var requestError : NSError?
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        self.saveOAuthKeys(JSON(value))
                    }
                case .Failure(let error):
                    requestError = error
                }
                
                
//                self.getProfileData({ (error) in
                
                
                HealthDataPoint.purgeAllDataPointsInContext(self.appDelegate.managedObjectContext)
                
                    // Execute completion handler
                    if self._completionHandler != nil {
                        self._completionHandler!(requestError)
                    }
                    
//                })
                
        }
        
    }
    
    
    func getProfileData(completionHandler: (NSError? -> Void)?) {
        
        
//        let lastUpdate =  NSUserDefaults.standardUserDefaults().objectForKey("FitBit_last_update") as? NSDate
//        
//        if lastUpdate == nil  {
        
            
            let url = "https://api.fitbit.com/1/user/-/profile.json"
            
            getRequest(url) { (response) in
                
                if let value = response.result.value {
                    
                    
//                    let dateString = (value["user"] as! NSDictionary )["memberSince"] as! String
//                    
//                    NSUserDefaults.standardUserDefaults().setObject(dateString, forKey: "FitBit_memberSince")
//                    NSUserDefaults.standardUserDefaults().synchronize()
//                    
//                    
//                    let dateFormatter = NSDateFormatter()
//                    dateFormatter.dateFormat = "yyyy-MM-dd"
//                    
//                    var date = dateFormatter.dateFromString(dateString)!.dateAtStartOfDay()
//                    
//                    if date.isEarlierThanDate(NSDate().dateBySubtractingDays(30)!) == true {
//                        
//                        date = NSDate().dateBySubtractingDays(30)!
//                    }
//                    
//                    //                    self.resetStepsFromDate(date)
//                    
//                    self.getStepsCountForDate(date, completion: { (succes) in
//                        
//                    })
                    
                }
                
                if completionHandler != nil {
                    completionHandler!(nil)
                }
                
            }
            

        
        
    }
    
    
    private func saveOAuthKeys(json : JSON) {
        let accessToken = json["access_token"].string!
        let refreshToken = json["refresh_token"].string!
        let expiresIn = json["expires_in"].int!

        NSUserDefaults.standardUserDefaults().setObject(accessToken, forKey: "FitBit_access_token")
        NSUserDefaults.standardUserDefaults().setObject(refreshToken, forKey: "FitBit_refresh_token")
        NSUserDefaults.standardUserDefaults().setInteger(expiresIn, forKey: "FitBit_expires_in")
        
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        headers = [
            "Authorization": "Bearer " + accessToken
        ]
        
        parameters = nil
        
        isLogin = true
        
        
    }

    
    // MARK: Helper methods
    private func escapedString(str : String) -> String {
        return str.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
    }
    
    
    private func convertToBase64(str : String) -> String {
        let data = str.dataUsingEncoding(NSUTF8StringEncoding)
        return data!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    }
    
    
    ///--------------------------------------
    
    func getRequest(url: String, completion: (Response<AnyObject, NSError> -> Void)!){
        
        login { (error) in
            
            guard error == nil else {
                
                return
            }
            
            
            Alamofire.request(.GET, url, parameters: self.parameters, headers: self.headers)
                .validate()
                .responseJSON { (response) in
                    
                    if response.result.isFailure{
                        
                        if response.result.error != nil && response.result.error?.userInfo != nil && response.result.error?.userInfo["StatusCode"] != nil{
                            
                            let statusCode = response.result.error?.userInfo["StatusCode"] as! Int
                            
                            if statusCode == 401 {
                                
                                self.removeLoginData()
                                
                            }
                            
                        }
                        
                    }
                    
                    completion(response)
                    
                    
            }
            
        }

        
    }
    
    
    func getStepsCountForDate(date:NSDate, completion: (Bool -> Void)?) {
        
        getStepsCountForDate(date, dateEnd: NSDate().dateAtMidnight(), completion: completion)
        
    }
    
    func getStepsCountForDate(date:NSDate, dateEnd:NSDate, completion: (Bool -> Void)?) {

        
        debugPrint("getStepsCountForDate \(date)  ---------------------------------------------")
        
        checkIfLoadedData(date, dataType: FitbitDataType.Steps) { (loaded) in
            
            if loaded == true {
                
                self.checkIsNeedToContinueDownloadingData(date, dateEnd: dateEnd, dataType: FitbitDataType.Steps, completion: completion)
                
            } else {
                
                self.receiveFitbitDataForDate(date, dateEnd: dateEnd, dataType: FitbitDataType.Steps, completion: completion)
                
            }
            
        }
        
    }
    
    //------------------
    
    
    func getCaloriesCountForDate(date:NSDate, completion: (Bool -> Void)?) {
        
        getCaloriesCountForDate(date, dateEnd: NSDate().dateAtMidnight(), completion: completion)
    }
    
    
    func getCaloriesCountForDate(date:NSDate, dateEnd:NSDate, completion: (Bool -> Void)?) {
        
        
        debugPrint("getCaloriesCountForDate \(date)  ---------------------------------------------")
        
        checkIfLoadedData(date, dataType: FitbitDataType.Calories) { (loaded) in
            
            if loaded == true {
                
                self.checkIsNeedToContinueDownloadingData(date, dateEnd: dateEnd, dataType: FitbitDataType.Calories, completion: completion)
                
            } else {
                
                self.receiveFitbitDataForDate(date, dateEnd: dateEnd, dataType: FitbitDataType.Calories, completion: completion)
                
            }
            
        }
        
    }
    
    
    //------------------
    
    
    func getDistanceCountForDate(date:NSDate, completion: (Bool -> Void)?) {
        
        getDistanceCountForDate(date, dateEnd: NSDate().dateAtMidnight(), completion: completion)
    }
    
    
    func getDistanceCountForDate(date:NSDate, dateEnd:NSDate, completion: (Bool -> Void)?) {
        
        
        debugPrint("getDistanceCountForDate \(date)  ---------------------------------------------")
        
        checkIfLoadedData(date, dataType: FitbitDataType.Distance) { (loaded) in
            
            if loaded == true {
                
                self.checkIsNeedToContinueDownloadingData(date, dateEnd: dateEnd, dataType: FitbitDataType.Distance, completion: completion)
                
            } else {
                
                self.receiveFitbitDataForDate(date, dateEnd: dateEnd, dataType: FitbitDataType.Distance, completion: completion)
                
            }
            
        }
        
    }
    
    
    private func receiveFitbitDataForDate(date:NSDate, dateEnd:NSDate, dataType: FitbitDataType, completion: (Bool -> Void)?) {
    
        
        var url = "https://api.fitbit.com/1/user/-/activities/%@/date/"
        
        switch dataType {
        case FitbitDataType.Steps:
            
            url = NSString(format: url, "steps") as String
            
            break
        case FitbitDataType.Calories:
            
            url = NSString(format: url, "calories") as String
            
            break
        case FitbitDataType.Distance:
            
            url = NSString(format: url, "distance") as String
            
            break
        }
        
        
        url.appendContentsOf(NSString(format: "%@/1d.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
        
        debugPrint("\(url)")
        
        
        
        
        getRequest(url) { (response) in
        
            if let value = response.result.value {
                
                var key = "activities-%@-intraday"
                
                switch dataType {
                case FitbitDataType.Steps:
                    
                    key = NSString(format: key, "steps") as String
                    
                    break
                case FitbitDataType.Calories:
                    
                    key = NSString(format: key, "calories") as String
                    
                    break
                case FitbitDataType.Distance:
                    
                    key = NSString(format: key, "distance") as String
                    
                    break
                }
                
                
                if let activities_steps_intraday = value[key] {
                    
                    if activities_steps_intraday != nil {

        
                        
                        var count = 0
                        var value = 0.0
                        var dateNew = NSDate()
                        
                        let datasetCount = ((activities_steps_intraday as! NSDictionary ) ["dataset"] as! NSArray).count
                        
                        var data:[(value:Double, date:NSDate, duration:Int)] = []
                        
                        for i in 0 ..< datasetCount{
                            
                            let dataset = ((activities_steps_intraday as! NSDictionary ) ["dataset"] as! NSArray)[i]
                            
                            
                            
                            if count == 0 {
                                
                                let dateFormatter = NSDateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                let dateString = date.stringDateWithFormat("yyyy-MM-dd")
                                let timeString = dataset["time"] as! String
                                
                                dateNew = dateFormatter.dateFromString("\(dateString) \(timeString)")!
                            }
                            
                            if (dataType == FitbitDataType.Calories) {
                                
                                if (dataset["level"] as! Double) > 0 {
                                    value = value + (dataset["value"] as! Double)
                                }
                                
                            } else {
                                value = value + (dataset["value"] as! Double)
                            }
                            
                            
                            
                            if count == 4 || i == datasetCount - 1 {
                                
                                
                                if value > 0 {
                                    
//                                    debugPrint("getStepsCountForDate \(dateNew)  steps  \(steps)   duration \(count + 1)")

                                    data.append((value, dateNew, count + 1))
                                    
//                                    HealthManager.addFitbitValue(self.appDelegate.user!, value: value, date: dateNew, type: dataType, duration: count + 1, moc: self.appDelegate.managedObjectContext, completition: { (success, error) in
//                                        
//                                    })
                                    
                                    
                                }
                                
                                
                                count = 0
                                value = 0
                                
                            } else {
                                
                                count = count + 1
                            }
                            
                            //                    debugPrint("getStepsCountForDate \(date)    \(timeString)")
                            
                            
                            
                        }
                        
                        let dataCount = data.count
                        
                        if dataCount > 0 {
                            
                            func addFitbitValue () {
                                
                                let value = data.first?.value
                                let dateNow = data.first?.date
                                let duration = data.first?.duration
                                
                                HealthManager.addFitbitValue(self.appDelegate.user!, value: value!, date: dateNow!, type: dataType, duration: duration!, moc: self.appDelegate.managedObjectContext, completition: { (success, error) in
                                    
                                    data.removeFirst()
                                    
                                    if data.count <= 0 {
                                        
                                        FitbitDateUpdates.addDate(self.appDelegate.user!, date: dateNew.dateAtStartOfDay(), dataType: dataType, moc: self.appDelegate.managedObjectContext)
                                        
                                        self.checkIsNeedToContinueDownloadingData(date, dateEnd: dateEnd, dataType: dataType, completion: completion)
                                        
                                    } else {
                                        
                                        addFitbitValue()
                                    }
                                    
                                })
                            }
                            
                            addFitbitValue()
                            
                        } else {
                            
                            FitbitDateUpdates.addDate(self.appDelegate.user!, date: dateNew.dateAtStartOfDay(), dataType: dataType, moc: self.appDelegate.managedObjectContext)
                            
                            self.checkIsNeedToContinueDownloadingData(date, dateEnd: dateEnd, dataType: dataType, completion: completion)
                            
                        }
                        
                        
                        
                        
        
                        
                        
                        
                        //---
                    }
                }
                
                
            } else {

                
                if completion != nil {
                    completion!(false)
                }
                
                
            }
            
            
        }
    }
    
    private func checkIsNeedToContinueDownloadingData(date:NSDate, dateEnd:NSDate, dataType: FitbitDataType, completion: (Bool -> Void)?){
        
        if date < dateEnd.dateAtStartOfDay() {
            
            switch dataType {
            case FitbitDataType.Steps:
                
                self.getStepsCountForDate(date.dateByAddingDays(1)!, completion:completion)
                
                break
            case FitbitDataType.Calories:
                
                self.getCaloriesCountForDate(date.dateByAddingDays(1)!, completion: completion)
                
                break
            case FitbitDataType.Distance:
                
                self.getDistanceCountForDate(date.dateByAddingDays(1)!, completion: completion)
                
                break
            }
            
            
        } else {
            
            switch dataType {
            case FitbitDataType.Steps:
                
                debugPrint("getStepsCountForDate END !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                
                break
            case FitbitDataType.Calories:
                
                debugPrint("getCaloriesCountForDate END !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                
                break
            case FitbitDataType.Distance:
                
                debugPrint("getDistanceCountForDate END !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                
                break
            }
            
            
            
            if completion != nil {
                completion!(true)
            }
            
            
            
            
        }
        
    }
    
    
    func checkIfLoadedData(date:NSDate, dataType: FitbitDataType, completition: (Bool) -> Void){
        
        if FitbitDateUpdates.checkIsDateExist(self.appDelegate.user!, date: date, dataType: dataType, moc: self.appDelegate.managedObjectContext) == true {
            
            if FitbitDateUpdates.checkIfDateIsLast(self.appDelegate.user!, date: date, dataType: dataType, moc: self.appDelegate.managedObjectContext) == true {
                
                HealthManager.removeAllFitbitData(date.dateAtStartOfDay(), dataType: dataType, completition: { (success, error) in
                    
//                    HealthDataPoint.purgeDataPointsForDate(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext, date: date)
                    
                    completition(false)
                })
                
            } else {
                
                completition(true)
            }
            
        } else {
            
            let dateForUpdate = FitbitDateUpdates.getLastDataIfNeedUpdate(self.appDelegate.user!, date: date, dataType: dataType, moc: self.appDelegate.managedObjectContext)
            
            if dateForUpdate != nil {
                
                HealthManager.removeAllFitbitData(dateForUpdate!.dateAtStartOfDay(), dataType: dataType, completition: { (success, error) in
                    
                    FitbitDateUpdates.removeDate(self.appDelegate.user!, dataType: dataType, moc: self.appDelegate.managedObjectContext, date: dateForUpdate!)
                    
                    completition(false)
                })
                
            } else {
                
                completition(false)
                
            }
            
        }
        
        
    }
    
    
//    func checkAndLoadingStepsForDate(date: NSDate, completion: (Bool -> Void)?) {
//     
//        if checkIsLogin() == true {
//            
//            let dateUploaded = NSUserDefaults.standardUserDefaults().objectForKey("FitBit_dateUploaded") as! [String]
//            
//            var isNeedLoad = true
//            
//            for dateString in dateUploaded{
//                
//                if dateString == date.stringDateWithFormat("yyyy-MM-dd"){
//                    
//                    isNeedLoad = false
//                    
//                    break
//                }
//                
////                let dateFormatter = NSDateFormatter()
////                dateFormatter.dateFormat = "yyyy-MM-dd"
////                
////                let date = dateFormatter.dateFromString(dateString)!.dateAtStartOfDay()
//                
//            }
//            
//            if isNeedLoad == true {
//                
////                getStepsCountForDate(date, dateEnd: NSDate().dateAtMidnight(), completion: completion)
//                
//                getStepsCountForDate(date, dateEnd: date, completion: { (success) in
//                    
//                    if completion != nil {
//                        completion! (success)
//                    }
//                    
//                })
//                
//            }
//            
//            
//        } else {
//            
//            if completion != nil {
//                completion! (true)
//            }
//            
//        }
//        
//        
//    }
    
    
//    ///----------------------------------------------------
//    
//    func getUserStepsCountForDate(date:NSDate, dateRange: DateRange, completion: ((data:[(sum: Double, date: NSDate)], error:NSError!) -> Void)!) {
//        
//            
//            login({ (_error) in
//                
//                if (_error == nil) {
//                    
//                    var data:[(sum: Double, date: NSDate)] = []
//                    
////                    let date = date.dateAtStartOfDay()
//                    
//                    var url = "https://api.fitbit.com/1/user/-/activities/steps/date/"
//                    
//                    if dateRange == .Day {
//                        
//                        url.appendContentsOf(NSString(format: "%@/1d.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                        
//                    } else if dateRange == .Week {
//                        
//                        url.appendContentsOf(NSString(format: "%@/7d.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                        
//                    } else if dateRange == .Month {
//                        
//                        url.appendContentsOf(NSString(format: "%@/1m.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                        
//                    }
//                    
//                    FitbitSwift.client().URLRequestWithMethod(.GET, url: url, optionalHeaders: nil, parameters: nil) {
//                        (json, error) in
//                        
//                        
//                        guard error == nil else {
//                            debugPrint(error!)
//                            if (error?.userInfo["StatusCode"])?.integerValue == 400 {
//                                FitbitSwift.logOut()
//                            }
//                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                                completion(data: data, error:nil)
//                            })
//                            return
//                        }
//                        
//                        
//                        if dateRange == .Day {
//                            
//                            var dataTemp = [String : Double]()
//                            
//                            for (_,subJson):(String, JSON) in (json?["activities-steps-intraday"]["dataset"])! {
//                                
//                                let dateFormatter = NSDateFormatter()
//                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                                var dateNew = dateFormatter.dateFromString("\(date.stringDateWithFormat("yyyy-MM-dd")) \(subJson["time"].string!)")
//                                
//                                let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
//                                let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: dateNew!)
//                                components.second = 0
//                                components.minute = 0
//                                
//                                dateNew = NSCalendar.currentCalendar().dateFromComponents(components)!
//                                
//                                var sum = subJson["value"].doubleValue
//                                
//                                let key = dateNew?.stringDateWithFormat("yyyy-MM-dd HH:mm:ss")
//                                
//                                if (dataTemp[key!] != nil) {
//                                    
//                                    sum += dataTemp[key!]!
//                                    
//                                }
//                                
//                                dataTemp[key!] = sum
//                                
////                                data.append((sum:subJson["value"].double!, date: dateNew!))
//                                
//                            }
//                            
//                            
//                            var keys = Array(dataTemp.keys)
//                            keys = keys.sort({ $0 < $1 })
//                            
//                            for key in keys{
//                                
//                                let value = dataTemp[key]
//                                
//                                let dateFormatter = NSDateFormatter()
//                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                                let dateNew = dateFormatter.dateFromString(key)
//                                
//                                data.append((sum:value!, date: dateNew!))
//                            }
//                            
////                            for (key,value):(String, Double) in dataTemp{
////                                
////                                let dateFormatter = NSDateFormatter()
////                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
////                                let dateNew = dateFormatter.dateFromString(key)
////                                
////                                data.append((sum:value, date: dateNew!))
////                            }
//                            
//                            
//                        } else if dateRange == .Week {
//                            
//                            for (_,subJson):(String, JSON) in (json?["activities-steps"])! {
//                                
//                                let dateFormatter = NSDateFormatter()
//                                dateFormatter.dateFormat = "yyyy-MM-dd"
//                                var dateNew = dateFormatter.dateFromString(subJson["dateTime"].string!)
//                                dateNew = dateNew?.dateAtStartOfDay();
//                                
//                                data.append((sum:subJson["value"].doubleValue, date: dateNew!))
//                            }
//                            
//                        } else if dateRange == .Month {
//                            
//                            for (_,subJson):(String, JSON) in (json?["activities-steps"])! {
//                                
//                                let dateFormatter = NSDateFormatter()
//                                dateFormatter.dateFormat = "yyyy-MM-dd"
//                                var dateNew = dateFormatter.dateFromString(subJson["dateTime"].string!)
//                                dateNew = dateNew?.dateAtStartOfDay();
//                                
//                                data.append((sum:subJson["value"].doubleValue, date: dateNew!))
//                            }
//                        }
//                        
//                        
//                        
//                        
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            
//                            completion(data:data, error:nil)
//                            
//                        })
//                        
//                    }
//                    
//                }
//                
//            })
//
//    }
//    
//    
//    
//    
//    /**
//     Returns both Active and Resting calories burned sums for the specified date range
//     
//     - parameter dateRange:  .Day or .Week
//     - parameter completion: Callback that receives arrays for each type of burned calorie along with the date for each sum
//     */
//    func getCaloriesForDateRange(dateRange: DateRange, completion: ((activeCalories: [(sum: Double, date: NSDate)], restingCalories: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
//
//        let date = NSDate().dateAtStartOfDay()
//        
//        getCaloriesForDate(date, dateRange: dateRange, completion: completion)
//        
//    }
//    
//    
//    func getCaloriesForDate(date:NSDate, dateRange: DateRange, completion: ((activeCalories: [(sum: Double, date: NSDate)], restingCalories: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
//        
////        let date = date.dateAtStartOfDay()
//        
//        login({ (_error) in
//            
//            if (_error == nil) {
//                                
//                var activeCalories: [(sum: Double, date: NSDate)]  = []
//                var restingCalories: [(sum: Double, date: NSDate)]  = []
//                
//                var url = "https://api.fitbit.com/1/user/-/activities/calories/date/"
//
//                if dateRange == .Day {
//                    
//                    url.appendContentsOf(NSString(format: "%@/1d.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                    
//                } else if dateRange == .Week {
//                    
//                    url.appendContentsOf(NSString(format: "%@/7d.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                    
//                } else if dateRange == .Month {
//                    
//                    url.appendContentsOf(NSString(format: "%@/1m.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                    
//                }
//                
//                FitbitSwift.client().URLRequestWithMethod(.GET, url: url, optionalHeaders: nil, parameters: nil) {
//                    (json, error) in
//                    
//                    
//                    guard error == nil else {
//                        debugPrint(error!)
//                        if (error?.userInfo["StatusCode"])?.integerValue == 400 {
//                            FitbitSwift.logOut()
//                        }
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            completion(activeCalories: [], restingCalories: [], error: error)
//                        })
//                        return
//                    }
//                    
//                    
//                    
//                    
//                    if dateRange == .Day {
//                        
////                        debugPrint(json?["activities-calories-intraday"]["dataset"])
//                        
//                        var activeCaloriesTemp = [String : Double]()
//                        var restingCaloriesTemp = [String : Double]()
//                        
//                        for (_,subJson):(String, JSON) in (json?["activities-calories-intraday"]["dataset"])! {
//                            
//                            
//                            let dateFormatter = NSDateFormatter()
//                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                            var dateNew = dateFormatter.dateFromString("\(date.stringDateWithFormat("yyyy-MM-dd")) \(subJson["time"].string!)")
//                            
//                            let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
//                            let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: dateNew!)
//                            components.second = 0
//                            components.minute = 0
//                            
//                            dateNew = NSCalendar.currentCalendar().dateFromComponents(components)!
//                            
//                            
//                            let key = dateNew?.stringDateWithFormat("yyyy-MM-dd HH:mm:ss")
//                            
//                            if (subJson["level"].int > 0){
//                                
//                                var sum = subJson["value"].doubleValue
//                                
//                                if (activeCaloriesTemp[key!] != nil) {
//                                    
//                                    sum += activeCaloriesTemp[key!]!
//                                }
//                                
//                                activeCaloriesTemp[key!] = sum
//                                
//                            } else {
//                                
//                                var sum = subJson["value"].doubleValue
//                                
//                                if (restingCaloriesTemp[key!] != nil) {
//                                    
//                                    sum += restingCaloriesTemp[key!]!
//                                }
//                                
//                                restingCaloriesTemp[key!] = sum
//                                
//                            }
//                            
//                            
////                            if (subJson["level"].int > 0){
////                                activeCalories.append((sum:subJson["value"].double!, date: dateNew!))
////                            } else {
////                                restingCalories.append((sum:subJson["value"].double!, date: dateNew!))
////                            }
//                            
//                            
//                        }
//                        
//                        
//                        func sutupData(data: [String : Double]) -> [(sum: Double, date: NSDate)] {
//                            
//                            var resultData: [(sum: Double, date: NSDate)]  = []
//                            
//                            var keys = Array(data.keys)
//                            keys = keys.sort({ $0 < $1 })
//                            
//                            for key in keys{
//                                
//                                let value = data[key]
//                                
//                                let dateFormatter = NSDateFormatter()
//                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                                let dateNew = dateFormatter.dateFromString(key)
//                                
//                                resultData.append((sum:value!, date: dateNew!))
//                            }
//                            
//                            
//                            return resultData
//                        }
//                        
//                        activeCalories = sutupData(activeCaloriesTemp)
//                        restingCalories = sutupData(restingCaloriesTemp)
//                        
//                    } else if dateRange == .Week {
//                        
////                        debugPrint(json?["activities-calories"])
//                        
//                        for (_,subJson):(String, JSON) in (json?["activities-calories"])! {
//                            
//                            let dateFormatter = NSDateFormatter()
//                            dateFormatter.dateFormat = "yyyy-MM-dd"
//                            var dateNew = dateFormatter.dateFromString(subJson["dateTime"].string!)
//                            dateNew = dateNew?.dateAtStartOfDay();
//                            
//                            activeCalories.append((sum:subJson["value"].doubleValue, date: dateNew!))
//                            
//                        }
//                        
//                    } else if dateRange == .Month {
//                        
//                        for (_,subJson):(String, JSON) in (json?["activities-calories"])! {
//                            
//                            let dateFormatter = NSDateFormatter()
//                            dateFormatter.dateFormat = "yyyy-MM-dd"
//                            var dateNew = dateFormatter.dateFromString(subJson["dateTime"].string!)
//                            dateNew = dateNew?.dateAtStartOfDay();
//                            
//                            activeCalories.append((sum:subJson["value"].doubleValue, date: dateNew!))
//                            
//                            
//                        }
//                    }
//                    
//                    
//                    
//                    
//                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                        
//                        completion(activeCalories: activeCalories, restingCalories: restingCalories, error: nil)
//                        
//                    })
//                    
//                    
//                    
//                    
//                }
//                
//                
//            }
//            
//        })
//        
//        
//    }
//    
//    
//    /**
//     Gets the total distance walked or run by the user on the specified date
//     
//     - parameter date:       Date to be queried
//     - parameter completion: Callback that receives the totalDistance Double
//     */
//    func getDistanceForDate(date: NSDate, completion: ((totalDistance: Double, error: NSError!) -> Void)!) {
//        
////        let date = date.dateAtStartOfDay()
//        
//        login({ (_error) in
//            
//            if (_error == nil) {
//                
//                
//                var url = "https://api.fitbit.com/1/user/-/activities/distance/date/"
//
//                url.appendContentsOf(NSString(format: "%@/1d.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                    
//
//                
//                FitbitSwift.client().URLRequestWithMethod(.GET, url: url, optionalHeaders: nil, parameters: nil) {
//                    (json, error) in
//                    
//                    
//                    guard error == nil else {
//                        debugPrint(error!)
//                        if (error?.userInfo["StatusCode"])?.integerValue == 400 {
//                            FitbitSwift.logOut()
//                        }
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            completion(totalDistance: 0.0, error: error)
//                        })
//                        return
//                    }
//                    
//                    
//                    var sum: Double = 0
//                    
//                    for (_,subJson):(String, JSON) in (json?["activities-distance-intraday"]["dataset"])! {
//                        
//                        sum += subJson["value"].double!
//                    }
//                    
//                    
//                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                        
//                        completion(totalDistance: sum, error: nil)
//                        
//                    })
//                    
//                    
//                    
//                    
//                }
//                
//                
//            }
//            
//        })
//        
//        
//    }
//
//    
//    func getMilesForDate(dateRange: DateRange, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
//        
//        //        let date = date.dateAtStartOfDay()
//        
//        let date = NSDate().dateAtStartOfDay()
//        
//        getUserMilesForDate(date, dateRange: dateRange, completion: completion)
//        
//    }
//    
//    
//    func getUserMilesForDate(date:NSDate, dateRange: DateRange, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
//        
//        //        let date = date.dateAtStartOfDay()
//        
//        login({ (_error) in
//            
//            if (_error == nil) {
//                
//                var data: [(sum: Double, date: NSDate)]  = []
//                
//                
//                var url = "https://api.fitbit.com/1/user/-/activities/distance/date/"
//                
//                if dateRange == .Day {
//                    
//                    url.appendContentsOf(NSString(format: "%@/1d.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                    
//                } else if dateRange == .Week {
//                    
//                    url.appendContentsOf(NSString(format: "%@/7d.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                    
//                } else if dateRange == .Month {
//                    
//                    url.appendContentsOf(NSString(format: "%@/1m.json", date.toString(.Custom("yyyy-MM-dd"))! ) as String)
//                    
//                }
//                
//                FitbitSwift.client().URLRequestWithMethod(.GET, url: url, optionalHeaders: nil, parameters: nil) {
//                    (json, error) in
//                    
//                    
//                    guard error == nil else {
//                        debugPrint(error!)
//                        if (error?.userInfo["StatusCode"])?.integerValue == 400 {
//                            FitbitSwift.logOut()
//                        }
//                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                            completion(data: data, error:error)
//                        })
//                        return
//                    }
//                    
//                    
//                    
//                    
//                    if dateRange == .Day {
//                        
//                        var sum: Double = 0
//                        
//                        for (_,subJson):(String, JSON) in (json?["activities-distance-intraday"]["dataset"])! {
//                            
//                            sum += subJson["value"].double!
//                        }
//                        
//                        
//                        data.append((sum:sum, date: date))
//                        
//                        
//                    } else if dateRange == .Week {
//                        
//                        for (_,subJson):(String, JSON) in (json?["activities-distance"])! {
//                            
//                            let dateFormatter = NSDateFormatter()
//                            dateFormatter.dateFormat = "yyyy-MM-dd"
//                            var date = dateFormatter.dateFromString(subJson["dateTime"].string!)
//                            date = date?.dateAtStartOfDay();
//                            
//                            data.append((sum:subJson["value"].doubleValue, date: date!))
//                            
//                        }
//                        
//                    } else if dateRange == .Month {
//                        
//                        for (_,subJson):(String, JSON) in (json?["activities-distance"])! {
//                            
//                            let dateFormatter = NSDateFormatter()
//                            dateFormatter.dateFormat = "yyyy-MM-dd"
//                            var date = dateFormatter.dateFromString(subJson["dateTime"].string!)
//                            date = date?.dateAtStartOfDay();
//                            
//                            data.append((sum:subJson["value"].doubleValue, date: date!))
//                            
//                        }
//                    }
//                    
//                    
//                    
//                    
//                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                        
//                        completion(data: data, error: nil)
//                        
//                    })
//                    
//                    
//                    
//                    
//                }
//                
//                
//            }
//            
//        })
//        
//        
//    }
//
//    
    
    

    
}






extension NSURLComponents {
    var queryItemDictionary: [String : String] {
        var results = [String: String]()
        if queryItems != nil {
            for queryItem in queryItems! {
                results[queryItem.name.lowercaseString] = queryItem.value
            }
        }
        return results
    }
    
    var code: String? {
        if let _code = queryItemDictionary["code"] {
            return _code
        }
        return nil
    }
}







extension UIAlertController {
    
    func show() {
        present(true, completion: nil)
    }
    
    func present(animated: Bool, completion: (() -> Void)?) {
        if let rootVC = UIApplication.sharedApplication().keyWindow?.rootViewController {
            presentFromController(rootVC, animated: animated, completion: completion)
        }
    }
    
    private func presentFromController(controller: UIViewController, animated: Bool, completion: (() -> Void)?) {
        if let navVC = controller as? UINavigationController,
            let visibleVC = navVC.visibleViewController {
            presentFromController(visibleVC, animated: animated, completion: completion)
        } else
            if let tabVC = controller as? UITabBarController,
                let selectedVC = tabVC.selectedViewController {
                presentFromController(selectedVC, animated: animated, completion: completion)
            } else {
                controller.presentViewController(self, animated: animated, completion: completion);
        }
    }
}





















