//
//  NewMessageViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 2/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

class ChatViewController: JSQMessagesViewController,UITabBarDelegate{
    // MARK: - Outlets
    
    let outgoingBubble = JSQMessagesBubbleImageFactory(bubbleImage: UIImage(named: "bluePill"),  capInsets: UIEdgeInsetsZero).outgoingMessagesBubbleImageWithColor(UIColor(hexString: "#009cdb"))
    let incomingBubble = JSQMessagesBubbleImageFactory(bubbleImage: UIImage(named: "bluePill"), capInsets: UIEdgeInsetsZero).incomingMessagesBubbleImageWithColor(UIColor.blackColor());
    var outgoingAvatar:JSQMessagesAvatarImage? = nil;
    var incomingAvatar:JSQMessagesAvatarImage? = nil;

    var sendingMessage = false;
    var refreshTimer: NSTimer!
    var dataSource:[Message] = [];
    var userId:NSNumber      = 0;
    var lastSender:NSNumber  = 0;
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var messagesViewModel: MessagesViewModel = {
        return MessagesViewModel()
    }()
    
    let item1:UITabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "homeTabBar"), selectedImage: UIImage(named: "homeTabBarSel"))
    let item2:UITabBarItem = UITabBarItem(title: "Notifications", image: UIImage(named: "notifTabBar"), selectedImage: UIImage(named: "notifTabBarSel"))
    let item3:UITabBarItem =  UITabBarItem(title: "PT Chat", image: UIImage(named: "chatTabBar"), selectedImage: UIImage(named: "chatTabBarSel"))
    let item4:UITabBarItem = UITabBarItem(title: "Tracking", image: UIImage(named: "trackTabBar"), selectedImage: UIImage(named: "trackTabBarSel"))

    
    var kTabBar:UITabBar?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userId = NSNumber(longLong: (appDelegate.user?.userID)!);
        self.senderId = String(userId);
        self.senderDisplayName = "ME"
        
        
        
        
        SDWebImageDownloader.sharedDownloader().downloadImageWithURL(NSURL(string: self.appDelegate.user!.pictureURL!), options: SDWebImageDownloaderOptions.HighPriority, progress: nil) { (image, data, error, completed) in
            if image != nil{
                self.incomingAvatar = JSQMessagesAvatarImageFactory.avatarImageWithImage(image, diameter: 32);

            }else{
                self.incomingAvatar = JSQMessagesAvatarImageFactory.avatarImageWithImage(UIImage(named: "avatar"), diameter: 32);

            }

            
            SDWebImageDownloader.sharedDownloader().downloadImageWithURL(NSURL(string: (self.appDelegate.user!.therapist?.avatar!)!), options: SDWebImageDownloaderOptions.HighPriority, progress: nil) { (image, data, error, completed) in
                if image != nil{
                    self.outgoingAvatar = JSQMessagesAvatarImageFactory.avatarImageWithImage(image, diameter: 32);
                }else{
                    self.outgoingAvatar = JSQMessagesAvatarImageFactory.avatarImageWithImage(UIImage(named: "avatar"), diameter: 32);

                }

                dispatch_async(dispatch_get_main_queue(), {
                    self.collectionView.reloadData();
                });
            };
        };
        
        
        
//        
//    
//        self.outgoingAvatar = JSQMessagesAvatarImage(placeholder: UIImage(named: "avatar"));
//
//        self.incomingAvatar = JSQMessagesAvatarImage(placeholder: UIImage(named: "avatar"));
        dataSource = Message.getMessages(appDelegate.managedObjectContext)!;
        self.collectionView.backgroundColor = UIColor(hexString: "#121615");
        if(dataSource.count == 0){
            messagesViewModel.getMessagesWithTimeStamp(appDelegate.user!, timestamp: "000000000", completion: { (result) -> Void in
                self.dataSource = result;
                self.collectionView!.reloadData();
                
            })
        }
        //TabBar Setup
        item1.tag = 0;
        item2.tag = 10;
        item3.tag = 20;
        item4.tag = 30;
        kTabBar = UITabBar(frame: self.inputToolbar.frame);
        
        kTabBar?.delegate = self;
        kTabBar!.setItems([item1,item2,item3,item4], animated: true);        
        
        kTabBar!.selectedItem = item3;
        
        kTabBar!.barStyle = UIBarStyle.Black;
        kTabBar!.barTintColor = UIColor.blackColor();
        kTabBar!.translucent = true;

        
        self.inputToolbar!.contentView.textView.inputAccessoryView = kTabBar;
        self.inputToolbar!.contentView.backgroundColor = UIColor.blackColor();
        self.inputToolbar.contentView.textView.backgroundColor = UIColor(hexString: "#4a4a4a").colorWithAlphaComponent(0.60);
        self.inputToolbar.contentView.textView.textColor = UIColor.whiteColor();
        
        let gestureRecon:UITapGestureRecognizer = UITapGestureRecognizer(target: self.view,action: #selector(UIView.endEditing(_:)));
        gestureRecon.numberOfTapsRequired = 1;
        
        self.collectionView.addGestureRecognizer(gestureRecon);
        self.collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSizeMake(36,36);
        self.collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSizeMake(36,36);
        self.inputToolbar!.contentView!.leftBarButtonItem = nil;
        
        self.inputToolbar!.contentView!.rightBarButtonContainerView?.backgroundColor = UIColor.geiaYellowColor()
        self.inputToolbar!.contentView!.rightBarButtonContainerView?.layer.cornerRadius = CGRectGetHeight(self.inputToolbar!.contentView!.rightBarButtonItem!.frame)/2
        
        self.inputToolbar!.contentView!.rightBarButtonItem?.setTitleColor(UIColor(white: 1, alpha: 0.7), forState: .Disabled);
        self.inputToolbar!.contentView!.rightBarButtonItem?.setTitleColor(UIColor.whiteColor(), forState: .Normal);
        self.inputToolbar!.contentView!.rightBarButtonItem?.setTitle("SEND", forState: .Normal);
        self.inputToolbar!.contentView!.rightBarButtonItemWidth += 20
        self.inputToolbar!.contentView!.rightBarButtonItem?.titleLabel?.font = UIFont.geiaFontOfSize(16);
        
        self.collectionView?.registerNib(UINib(nibName: "GeiaMessagesCollectionViewCellIncoming", bundle: nil), forCellWithReuseIdentifier: "JSQIncoming")
        self.incomingCellIdentifier = "JSQIncoming"
        self.collectionView?.registerNib(UINib(nibName: "GeiaMessagesCollectionViewCellOutgoing", bundle: nil), forCellWithReuseIdentifier: "JSQOutgoing")
        self.outgoingCellIdentifier = "JSQOutgoing"


    }
    

    func endEdition(){
        self.view.endEditing(true);
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated);

    }
   
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        kTabBar!.selectedItem = item3;

        self.refreshTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(ChatViewController.updateMessages), userInfo: nil, repeats: true)

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.refreshTimer.invalidate()
        
        self.appDelegate.blockRotation = false
    }
    
    
    
    func updateMessages(){
        if(dataSource.count != 0){
            messagesViewModel.getMessagesWithTimeStamp(appDelegate.user!, timestamp: self.dataSource[self.dataSource.count - 1].timestamp!, completion: { (result) -> Void in
                if(result.count > 0){
                    self.dataSource.appendContentsOf(result);
                    self.finishReceivingMessage()
                }
                
            })
        }
    }
    
    
    
    
    
    //MARK: -JSQMessageViewController Methods
    
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        let data = self.dataSource[indexPath.row]
        return data
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell =  super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell;
        cell.textView!.linkTextAttributes = [
            NSForegroundColorAttributeName: UIColor.geiaDarkBlueColor(),
            NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue
        ]
        let data = dataSource[indexPath.row]
  
        cell.textView?.textColor = UIColor.whiteColor()

        if(data.sender == userId){
            cell.messageBubbleTopLabel?.textColor = UIColor.geiaMidBlueColor();
            cell.messageBubbleTopLabel?.text = data.senderName;
            cell.messageBubbleTopLabel?.textAlignment = NSTextAlignment.Left;
            cell.messageBubbleTopLabel?.textInsets = UIEdgeInsetsZero;
        }else{
            cell.messageBubbleTopLabel?.textColor = UIColor.geiaLightGrayColor();
            cell.messageBubbleTopLabel?.text = "PT"
            cell.messageBubbleTopLabel?.textAlignment = NSTextAlignment.Left;
            cell.messageBubbleTopLabel?.textInsets = UIEdgeInsetsZero;

        }
        return cell
    }
    
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = dataSource[indexPath.row]
        if(data.sender == userId){
            return  self.incomingBubble
        }else{
            return self.outgoingBubble
            
        }
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        let data = dataSource[indexPath.row]
        if(data.sender == userId){
            return  self.incomingAvatar
        }else{
            return self.outgoingAvatar
            
        }
    }
    
    
    
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        let newMessage = text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet());
   
        
        
        if(!self.sendingMessage &&  !newMessage.isEmpty){
            
            let messjNew = NSEntityDescription.insertNewObjectForEntityForName("Message", inManagedObjectContext: self.appDelegate.managedObjectContext) as? Message
            messjNew!.message = newMessage;
            messjNew!.id       = "timestamp_99999"
            messjNew!.sender   = self.userId
            messjNew!.receiver = 0009901
            messjNew!.senderName = "ME"
            messjNew!.timestamp = "\(Int(NSDate().timeIntervalSince1970))";
            self.dataSource.append(messjNew!);
            self.finishSendingMessage()
            
            messagesViewModel.sendMessage(string: newMessage) { (result) -> Void in
                
                if(result != nil){
                    
                    messjNew!.message = result!.message;
                    messjNew!.id       = result!.id;
                    self.appDelegate.saveContext();
                }else{
                    let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your data to our servers, please try again", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
            }
            
        }
        
    }
    

    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return 24;
    }
    
    
    //MARK -- Override UI
    
    // MARK: - TabBar Delegate Methods
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        self.view.endEditing(true);
        NSNotificationCenter.defaultCenter().postNotificationName("goToScreen", object: item.tag);
        
    }
}


