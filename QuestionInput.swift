//
//  QuestionInput.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/15/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class QuestionInput: QuestionView {

    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var txtAnswer: UITextField!
    
    class func viewWithQuestion(question: QuestionMeasure) -> QuestionInput{
        let view = QuestionInput.fromNib();
        view.lblQuestion.text = question.question;
        view.qMeasure = question;

        return view;
    }
    @IBAction func editingChangedOnTextfield(sender: AnyObject) {
        self.changed = true;
        self.qMeasure?.value = txtAnswer.text;
    }
}
