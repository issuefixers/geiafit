//
//  FitbitDateUpdates+CoreDataProperties.swift
//  Geia
//
//  Created by Eugene Budnik on 10/15/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData

extension FitbitDateUpdates {

    @NSManaged var date: NSDate?
    @NSManaged var userID: Int64
    @NSManaged var dataType: Int16

}
