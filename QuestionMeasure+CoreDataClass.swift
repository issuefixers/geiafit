//
//  QuestionMeasures+CoreDataClass.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

@objc(QuestionMeasure)
public class QuestionMeasure: NSManagedObject {

    func addQuestionMeasure(json: JSON,goal: GoalMeasure){
        self.question = json["question"].string;
        self.item_id = json["item_id"].numberValue;
        self.max = json["max"].numberValue;
        self.min = json["min"].numberValue;
        self.callback = json["callback"].string;
        self.type = json["type"].string;
        self.unit = json["unit"].string;
        self.icon = json["icon"].string;
        
        if self.max == nil {
            self.max = 0;
        }
        
        if self.min == nil{
            self.min = 0;
        }
        self.goal = goal;
    }
    
    
   class func getQuestionMeassureWithId(moc: NSManagedObjectContext, logId: NSNumber) -> QuestionMeasure?{
        var questions: [QuestionMeasure]?

        let fetchRequest        = NSFetchRequest.init(entityName: "QuestionMeasure")
        
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "item_id == %d", logId.intValue)
        
        do {
            questions = (try moc.executeFetchRequest(fetchRequest)) as? [QuestionMeasure];
            if ( questions?.count  > 0){
                return questions![0];
            }
        } catch {
            print("Error Querying")
            return nil
        }
        return nil
        
    }
    
    class func getQuestionsForGoalMeasure(moc: NSManagedObjectContext, goalId:NSString) -> [QuestionMeasure]?{
        var questions: [QuestionMeasure]?
        
        let fetchRequest        = NSFetchRequest.init(entityName: "QuestionMeasure")
        
//        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "goal.nid == %@", goalId)
        
        do {
            questions = (try moc.executeFetchRequest(fetchRequest)) as? [QuestionMeasure];
            if ( questions?.count  > 0){
                return questions!;
            }
        } catch {
            print("Error Querying")
            return nil
        }
        return nil
        
    }

}
