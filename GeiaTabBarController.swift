 //
//  GeiaSlidingPanelController.swift
//  Geia
//
//  Created by zurce on 4/25/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import MSSlidingPanelController
import KSToastView


class GeiaTabBarController: UITabBarController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    let item1:UITabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "homeTabBar"), selectedImage: UIImage(named: "homeTabBarSel"))
    let item2:UITabBarItem = UITabBarItem(title: "Notifications", image: UIImage(named: "notifTabBar"), selectedImage: UIImage(named: "notifTabBarSel"))
    let item3:UITabBarItem =  UITabBarItem(title: "PT Chat", image: UIImage(named: "chatTabBar"), selectedImage: UIImage(named: "chatTabBarSel"))
    let item4:UITabBarItem = UITabBarItem(title: "Tracking", image: UIImage(named: "trackTabBar"), selectedImage: UIImage(named: "trackTabBarSel"))
    let itemMore:UITabBarItem = UITabBarItem(title: "", image: UIImage(named:"moreTabBar")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named:"moreTabBarSel")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal))
    var kTabBar:UITabBar?
    var kMenuPanel:UICollectionView? = nil
    var selectedPrev:UITabBarItem? ;
    var menuImages = ["Snapshots","Health Points","Vitals","Exercises","Activity Levels","Goals","Account","Settings"];
    let viewMenuPane = UIView();
    let kDismissView = UIView();
    
    private let prescriptionsViewModel = PrescriptionsViewModel()
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad();
        item1.tag = 0;
        item2.tag = 10;
        item3.tag = 20;
        item4.tag = 30;
        
        let menuNib = UINib(nibName: "MenuPanelCell", bundle: nil)
        kMenuPanel = NSBundle.mainBundle().loadNibNamed("MenuPanel", owner: self, options: nil)![0] as? UICollectionView

        
        kMenuPanel!.registerNib(menuNib, forCellWithReuseIdentifier: "moreButton")
     
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GeiaTabBarController.updateBadgeForNotifications), name: "updateNotifications", object: nil);
        //TabBar Setup
        kTabBar = UITabBar(frame: self.tabBar.frame);

        kTabBar?.delegate = self;
        kTabBar!.setItems([item1,item2,itemMore,item3,item4], animated: true);
        itemMore.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        itemMore.titlePositionAdjustment = UIOffsetMake(0, 100);
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.geiaFontOfSize(11),
			NSForegroundColorAttributeName: UIColor.geiaMidBlueColor()], forState: .Selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.geiaFontOfSize(11),
            NSForegroundColorAttributeName: UIColor.geiaLightGrayColor()], forState: .Normal)
		//UITabBar.appearance().backgroundImage = UIImage(named: "tabbar-background");
        UITabBar.appearance().tintColor       = UIColor.geiaMidBlueColor();
        kTabBar!.barStyle = UIBarStyle.Black;
        kTabBar!.barTintColor = UIColor.blackColor();
        kTabBar!.translucent = true;
        kTabBar!.selectedItem = item1;
        self.selectedPrev = item1;
        let size  = (CGRectGetWidth(UIScreen.mainScreen().bounds)/4) * 2;
        kMenuPanel!.frame = CGRectMake(0, CGRectGetHeight(self.view.frame) - (size + CGRectGetHeight(kTabBar!.frame)), CGRectGetWidth(self.view.frame), size );
        viewMenuPane.frame = CGRectMake(0, CGRectGetHeight(self.view.frame) , CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - 44 );
        
        viewMenuPane.backgroundColor = UIColor.clearColor();
        
        kDismissView.frame = self.view.frame;
        kDismissView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4);
        kDismissView.alpha = 0;
        let tapGestureDismissPane = UITapGestureRecognizer(target: self, action: #selector(GeiaTabBarController.hideMenu))
        tapGestureDismissPane.numberOfTapsRequired = 1;
        kDismissView.addGestureRecognizer(tapGestureDismissPane);
        viewMenuPane.addSubview(kDismissView);
        viewMenuPane.addSubview(kMenuPanel!);
        self.view.addSubview(viewMenuPane);
        kMenuPanel?.delegate = self;
        kMenuPanel?.dataSource = self;
        self.view.addSubview(kTabBar!);
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.geiaYellowColor());
        self.updateBadgeForNotifications();
    }


 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setSelectedItem(item item:Int){
        switch item {
        case 1:
            self.kTabBar?.selectedItem = item1;
            break;
        case 2:
            self.kTabBar?.selectedItem = item2;
            break;
        case 4:
            self.kTabBar?.selectedItem = item3;
            break;
        case 5:
            self.kTabBar?.selectedItem = item4;
            break;
        default:
            break;
        }
    
        
        self.updateBadgeForNotifications();
    }
    
    func updateBadgeForNotifications(){
        item2.badgeValue = Notification.getBadgeOfNotifications(self.appDelegate.managedObjectContext);

    }

    

    // MARK: - TabBar Delegate Methods
    
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {

	        if item == itemMore {
            

            item.enabled = false;
                
            if kMenuPanel!.tag == 99{
                self.hideMenu();
            }else{
                self.showMenu();
            }
            
          
        }else{
            NSNotificationCenter.defaultCenter().postNotificationName("goToScreen", object: item.tag);
                self.selectedPrev = item;

                self.hideMenu();
        }
        
        
    }
    
    
    func showMenu(){
        var cRect  = self.viewMenuPane.frame
        cRect.origin.y = 0;

//        UIView.animateWithDuration(0.15, animations: {
//            self.viewMenuPane.frame = cRect;
//            }, completion: { (completed) in
//                self.itemMore.enabled = true;
//                self.kMenuPanel!.tag = 99;
//
//        });
        
        UIView.animateWithDuration(0.15, delay: 0, usingSpringWithDamping: 5, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.viewMenuPane.frame = cRect;
        }) { (completition) in
                self.itemMore.enabled = true;
                self.kMenuPanel!.tag = 99;
        }
        
        
        UIView.animateWithDuration(0.10, delay: 0.10, options: UIViewAnimationOptions.CurveEaseIn, animations: { 
            self.kDismissView.alpha  = 1
        }) { (animated) in
                
        };
    
        
    }
    
    func hideMenu(){
        var cRect  = self.viewMenuPane.frame
        cRect.origin.y =  CGRectGetHeight(self.view.frame);
        
        
        UIView.animateWithDuration(0.15, animations: {
            self.kDismissView.alpha = 0;
            
            }, completion: { (completed) in
                
        });
        
        UIView.animateWithDuration(0.10, delay: 0.10, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.viewMenuPane.frame = cRect;
        }) { (animated) in
            self.itemMore.enabled = true;
            self.kMenuPanel!.tag = 0;
            self.kTabBar?.selectedItem = self.selectedPrev;

        };
        
        
        
    }


    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        let size = CGRectGetWidth(UIScreen.mainScreen().bounds)/4;
        return CGSizeMake(size, size);
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0;
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSizeZero;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeZero;
    }
    
    
    //self.collectionView(self.collectionView, layout: self.collectionView.collectionViewLayout, sizeForItemAtIndexPath: indexPath)

    //MARK: CollectionView Delegate Methods
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        var screen  = 0;
        switch indexPath.row {
        case 0:
            screen = 42;
//            self.selectedPrev = item4;
        break;
        case 1:
            screen = 80;
//            self.selectedPrev = item2;
            break;
        case 2:
            screen = 32;
            self.selectedPrev = item4;
            break;
        case 3:
            screen = 41;
            break;
        case 4: screen = 52;
            break;
        case 5: screen = 40;
            break;
        case 6: screen = 50;
            break;
        case 7: screen = 71;
            break;
        default: break;
            
        }
        if screen  != 0 {
            NSNotificationCenter.defaultCenter().postNotificationName("goToScreen", object: screen);
        }

        self.hideMenu();
    }
    
    
    //MARK: CollectionView Datasource Methods
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8;
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1;
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("moreButton", forIndexPath: indexPath);
        
        if cell.tag != 99{
            cell.tag == 99;
            
            (cell.viewWithTag(10) as! UIImageView).image = UIImage(named: "more"+menuImages[indexPath.row]);
            (cell.viewWithTag(11) as! UILabel).text = menuImages[indexPath.row];
        }
        
        return cell;
    }
    

    
    
    
    
}





