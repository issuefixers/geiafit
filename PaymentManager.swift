//
//  PaymentManager.swift
//  Geia
//
//  Created by Eugene Budnik on 9/26/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData
import Braintree

class PaymentManager: ViewModel {
    
    private var URLScheme: String  {
        
//        #if DEBUG
//            //DEV
//            return "biz.wellnesslinks.GeiaFitApp.payments://test"
//        #else
//            //PRODUCTION
            return "biz.wellnesslinks.GeiaFitApp.payments"
//        #endif
    }
    
//    func checkPayment(completion: (paid: Bool) -> Void){
//    
//        completion(paid: false)
//        
//        return
//        
//        let url: String      = "\(SERVER_URL)\(BT_SUBSCRIPTION)"
//        print(url);
//        
//        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
//            .validate(statusCode: 200..<300)
//            .validate(contentType: ["application/json"])
//            .responseJSON { (response: Response) -> Void in
//                let result = response.result
//                if let jsonObject: AnyObject = result.value {
//                    let jsonDictionary:[String: JSON] = JSON(jsonObject).dictionaryValue
//                    
//                    if jsonDictionary["free"]?.boolValue == true {
//                        
//                        completion(paid: true)
//                        
//                    } else if jsonDictionary["expired"]?.boolValue == true {
//                        
//                        completion(paid: false)
//                        
//                    } else if jsonDictionary["block"]?.boolValue == true {
//                        
//                        completion(paid: false)
//                        
//                    } else {
//                        
//                        completion(paid: true)
//                    }
//                    
//                }
//        }
//        
//    }
    
    
    func checkPayment(completion: (paid: Bool, needInputPaymentInfo: Bool, message: String, client_name:String, plan:String, token:String) -> Void){
        
//        if NSProcessInfo.processInfo().environment["EBudnikDebug"] == "true" {
//            completion(paid: true, needInputPaymentInfo: false, message: "", client_name: "", plan: "")
//            return
//        }
        
        
        let url: String      = "\(SERVER_URL)\(BT_CHECK)"
        print(url);
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                
                switch response.result {
                case .Success:
                    
                    
                    let result = response.result
                    if let jsonObject: AnyObject = result.value {
                        let jsonDictionary:[String: JSON] = JSON(jsonObject).dictionaryValue
                        
                        if jsonDictionary["success"]?.boolValue == true {
                            
                            if jsonDictionary["allowed"]?.boolValue == true {
                                
                                
                                if let jsonObject: Bool = jsonDictionary["need_input_payment_info"]?.boolValue {
                                    
                                    if jsonObject == false {
                                        
                                        completion(paid: true, needInputPaymentInfo: false, message: "", client_name: "", plan: "", token:"")
                                        
                                        
                                    }   else {
                                        
                                        var plan = ""
                                        
                                        if (jsonDictionary["plan"] != nil){
                                            plan = (jsonDictionary["plan"]?.stringValue)!
                                        }
                                        
                                        var message = ""
                                        
                                        if (jsonDictionary["message"] != nil){
                                            message = (jsonDictionary["message"]?.stringValue)!
                                        }
                                        
                                        var client_name = ""
                                        
                                        if (jsonDictionary["client_name"] != nil){
                                            client_name = (jsonDictionary["client_name"]?.stringValue)!
                                        }
                                        
                                        var token : String = ""
                                        
                                        if (jsonDictionary["token"] != nil){
                                            token = (jsonDictionary["token"]?.stringValue)!
                                        }
                                        
                                        completion(paid: false, needInputPaymentInfo: true, message: message, client_name: client_name, plan: plan, token:token)
                                        
                                        
                                    }
                                    
                                    
                                } else {
                                    completion(paid: true, needInputPaymentInfo: false, message: "", client_name: "", plan: "", token:"")
                                }
                                
                                
                                
                                
                            }   else {
                                
                                if let jsonObject: Bool = jsonDictionary["need_input_payment_info"]?.boolValue {
                                    
                                    if jsonObject == false {
                                        
                                        var message = ""
                                        
                                        if (jsonDictionary["message"] != nil){
                                            message = (jsonDictionary["message"]?.stringValue)!
                                        }
                                        
                                        
                                        completion(paid: false, needInputPaymentInfo: false, message: message, client_name: "", plan: "", token:"")
                                        
                                        
                                    }   else {
                                        
                                        var plan = ""
                                        
                                        if (jsonDictionary["plan"] != nil){
                                            plan = (jsonDictionary["plan"]?.stringValue)!
                                        }
                                        
                                        var message = ""
                                        
                                        if (jsonDictionary["message"] != nil){
                                            message = (jsonDictionary["message"]?.stringValue)!
                                        }
                                        
                                        var client_name = ""
                                        
                                        if (jsonDictionary["client_name"] != nil){
                                            client_name = (jsonDictionary["client_name"]?.stringValue)!
                                        }
                                        
                                        var token : String = ""
                                        
                                        if (jsonDictionary["token"] != nil){
                                            token = (jsonDictionary["token"]?.stringValue)!
                                        }
                                        
                                        completion(paid: false, needInputPaymentInfo: true, message: message, client_name: client_name, plan: plan, token:token)
                                        
                                        
                                    }
                                    
                                    
                                } else {
                                    
                                    var message = ""
                                    
                                    if (jsonDictionary["message"] != nil){
                                        message = (jsonDictionary["message"]?.stringValue)!
                                    }
                                    
                                    
                                    completion(paid: false, needInputPaymentInfo: false, message: message, client_name: "", plan: "", token:"")
                                    
                                }
                            }
                            
                            
                        }   else {
                            
                            
                            var message = ""
                            
                            if (jsonDictionary["message"] != nil){
                                message = (jsonDictionary["message"]?.stringValue)!
                            }

                            
                            completion(paid: false, needInputPaymentInfo: false, message: message, client_name: "", plan: "", token:"")
                            
                            
                        }
                        
                                                
                    }
                    
                    
                case .Failure( _):
                    
                        completion(paid: true, needInputPaymentInfo: false, message: "", client_name: "", plan: "", token:"")
                    
                }
                
                
                    
                    
                    
            
        }
        
    }
    
    
    func getBraintreeClient(completion: (braintreeClient: BTAPIClient) -> Void){
        
//        #if DEBUG
//            //DEV
//            
//            let clientToken = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJkMzAzNjRmMWZiMzM5OTNlMDA2M2ZhMjMzNWMzZDQ1N2NlM2U5YmI1YzY5YzQ1ZDNkMTk0ODZlNTNlOTVkMzBifGNyZWF0ZWRfYXQ9MjAxNi0xMC0xOVQxODowNjoyMS40MzE3NTE2MTYrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJjb2luYmFzZUVuYWJsZWQiOmZhbHNlLCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0="
//            
//            let braintreeClient = BTAPIClient(authorization: clientToken)
//            
//            completion(braintreeClient: braintreeClient!)
//            
//        #else
            //PRODUCTION
        
            let clientTokenURL = NSURL(string: "https://braintree-sample-merchant.herokuapp.com/client_token")!
            let clientTokenRequest = NSMutableURLRequest(URL: clientTokenURL)
            clientTokenRequest.setValue("text/plain", forHTTPHeaderField: "Accept")
            
            NSURLSession.sharedSession().dataTaskWithRequest(clientTokenRequest) { (data, response, error) -> Void in
                // TODO: Handle errors
                let clientToken = String(data: data!, encoding: NSUTF8StringEncoding)
                
                let braintreeClient = BTAPIClient(authorization: clientToken!)
                // As an example, you may wish to present our Drop-in UI at this point.
                // Continue to the next section to learn more...
                
                completion(braintreeClient: braintreeClient!)
                
                }.resume()

            
//        #endif
    
        
    }
    
    func setReturnURLScheme() {
        
        BTAppSwitch.setReturnURLScheme(URLScheme)
        
    }
    
    func getURLScheme() -> String {
        
        return URLScheme
    }
    
    func handleOpenURL(url: NSURL, sourceApplication: String) -> Bool{
        
        return BTAppSwitch.handleOpenURL(url, sourceApplication:sourceApplication)
    }
    
    
    func postNonceToServer(nonce: String, completion: (paid: Bool, message: String) -> Void) {
        
        //http://geiafit.dev.cpcs.ws/api/bt/payment/<plan_id>/<nonceFromTheClient>/?discount=<discount_code>
        
        
        let url: String      = "\(SERVER_URL)\(BT_SUBSCRIBE)/\(nonce)"
        print(url);
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                if let jsonObject: AnyObject = result.value {
                    let jsonDictionary:[String: JSON] = JSON(jsonObject).dictionaryValue
                    
                    if jsonDictionary["success"]?.boolValue == true {
                        
                        completion(paid: true, message: (jsonDictionary["message"]?.string)!)
                        
                    } else {
                        
                        completion(paid: false, message: (jsonDictionary["message"]?.string)!)
                    }
                    
                } else {
                    
                    completion(paid: false, message: "An unexpected error occurred, please try again later.")
                }
        
        }

    }
    
}
