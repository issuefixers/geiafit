//
//  GoalTableViewCell.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import CoreData

class GoalTableViewCell: UITableViewCell {

    
    @IBOutlet weak var kContentView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    var buttonSave:UIButton = UIButton();
    
//    @IBOutlet weak var questionViewHolder: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupWithGoal(moc: NSManagedObjectContext,goal: GoalMeasure){
        self.lblTitle.text = goal.title;
        
        let questions  = QuestionMeasure.getQuestionsForGoalMeasure(moc, goalId: goal.nid!);
        
        var constY:CGFloat = 70;

        dispatch_async(dispatch_get_main_queue()) { 
            for question in questions!{
                
                if question.type == "slider"{
                    let view = QuestionSlider.viewWithQuestion(question);
                    view.frame = CGRectMake(0, constY, CGRectGetWidth(self.kContentView.frame) - 16, 70);
                    constY = constY + 70;
                    self.kContentView.addSubview(view);
                }else{
                    let view = QuestionInput.viewWithQuestion(question);
                    view.frame = CGRectMake(0, constY, CGRectGetWidth(self.kContentView.frame) - 16, 70);
                    constY = constY + 70;
                    self.kContentView.addSubview(view);

                    
                }
  
            }
            
            self.buttonSave.frame = CGRectMake(CGRectGetWidth(self.kContentView.frame) - 70,constY + 5 ,55,30);
            self.buttonSave.backgroundColor = UIColor.geiaYellowColor();
            self.buttonSave.setTitleColor(UIColor.whiteColor(), forState: .Normal);
            self.buttonSave.setTitle("SAVE", forState: .Normal);
            self.buttonSave.titleLabel?.font = UIFont.geiaFontOfSize(14);
            self.buttonSave.layer.cornerRadius = 15;
            
        
            self.kContentView.addSubview( self.buttonSave);
            self.setNeedsDisplay();
            self.setNeedsLayout();
        }
    
    
    }
     
    
   
}
