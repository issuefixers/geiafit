//
//  OnboardingViewModel.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/8/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

class OnboardingViewModel: ViewModel {

    func getProfileForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = PROFILE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"

        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
            let result = response.result

            if result.isFailure {
                //Epic fail
                self.dumpDebugData(response)
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                return
            }

            if let jsonObject: AnyObject = result.value {
                let json = JSON(jsonObject)
                user.addProfileWithData(json,moc: self.appDelegate.managedObjectContext)
                //debugPrint(json)

                
                
                completion(result: ["success": true, "user":user])
            } else {
                completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
            }
        }
    }

    func updateUserInformationForUser(user: User, password: String, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String              = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String            = PROFILE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String                 = "\(SERVER_URL)\(endpoint)"
        var params: [String: AnyObject] = [
            "email":      user.email!,
            "first_name": user.firstName!,
            "last_name":  user.lastName!,
            "gender":     NSNumber(short: user.gender),
            "dob":        user.birthday,
            "field_onboarded": 1
        ]

        if password.characters.count > 0 {
            params.updateValue(password, forKey: "password")
        }

        //debugPrint(params)

        request(.PUT, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
            let result = response.result

            if result.isFailure {
                //Epic fail
                self.dumpDebugData(response)
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                return
            }

            completion(result: ["success": true])
        }
    }

    func updateProfileImageForUser(user: User, imageData: NSData, completion: (result: Dictionary<String, AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = PROFILE_PIC_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"
        let base64String     = imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.init(rawValue: 0))

        let params: [String: AnyObject] = [
            "image_name": "\(user.firstName!)_\(user.lastName!).png",
            "image":      base64String
        ]

        request(.POST, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
            let result = response.result

            if result.isFailure {
                //Epic fail
                self.dumpDebugData(response)
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                return
            }

            completion(result: ["success": true])
        }
    }

    func getCharacteristicsForUser(user: User, completion: (result: Dictionary<String, AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = CHARACTERISTICS_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"

        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
            let result = response.result

            if result.isFailure {
                //Epic fail
                self.dumpDebugData(response)
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                return
            }

            if let jsonObject: AnyObject = result.value {
                let json = JSON(jsonObject).arrayValue

                for data: JSON in json {
                    user.addCharacteristicsWithData(data, moc: self.appDelegate.managedObjectContext)
                }

                completion(result: ["success": true])
            } else {
                completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
            }
        }
    }

    func updateCharacteristics(characteristics: Characteristics, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        guard let user: User = characteristics.user else {
            completion(result: ["success": false, "errorMessage":"No user object"])
            return
        }

        let userID: String              = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String            = CHARACTERISTICS_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String                 = "\(SERVER_URL)\(endpoint)"
		
		var params = Dictionary<String,AnyObject>()
		
		if(characteristics.weight != -1){
			params["weight"] = characteristics.weight!
		}
		
		if(characteristics.height != -1){
			params["height"] = characteristics.height!
		}
		
		if(characteristics.bodyFat != -1){
			params["body_fat"] = characteristics.bodyFat!
		}
		
		if(characteristics.bmi != -1){
			params["bmi"] = characteristics.bmi!
		}
		
		if(characteristics.restingHeartRate != -1){
			params["resting_heart_rate"] = characteristics.restingHeartRate!
		}
		
		if(characteristics.maxHeartRate != -1){
			params["max_heart_rate"] = characteristics.maxHeartRate!
		}
		
		
		if(characteristics.bloodPressureSys != -1){
			params["blood_pressure_sys"] = characteristics.bloodPressureSys!
		}
		
		if(characteristics.bloodPressureDia != -1){
			params["blood_pressure_dia"] = characteristics.bloodPressureDia!
		}
		
		if(characteristics.emotion != -1){
			params["emotion"] = characteristics.emotion!
		}
			
		params["record_date"] = characteristics.getRecordDateAsInteger()
			
		params["date_created"] = characteristics.getCreatedDateAsInteger()
			

        //print("Sending: \(params)")
        request(.PUT, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
            let result = response.result

            if result.isFailure {
                //Epic fail
                self.dumpDebugData(response)
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                return
            } else {

            }

            completion(result: ["success": true])
        }
    }

    func updateGoalsForUser(user: User, goals: [String], completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String            = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String          = PROFILE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String               = "\(SERVER_URL)\(endpoint)"
        let params: [String:[String]] = ["goals": goals]

        request(.PUT, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
            let result = response.result

            if result.isFailure {
                //Epic fail
                self.dumpDebugData(response)
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                return
            }

            completion(result: ["success": true])
        }
    }

    func getRiskAnalysisSurveyForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let endpoint: String = RISK_ANALYSIS_SURVEY_ENDPOINT
        let url: String      = "\(SERVER_URL)\(endpoint)"

        request(.GET, url)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
            let result = response.result

            if result.isFailure {
                //Epic fail
                self.dumpDebugData(response)
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                return
            }

            if let jsonObject: AnyObject = result.value {
                let json       = JSON(jsonObject)
                let survey     = Survey.addSurveyWithData(json, moc: self.appDelegate.managedObjectContext)
                let userSurvey = UserSurvey.getSurveyForUser(user, survey: survey!, moc: self.appDelegate.managedObjectContext)

                print("Survey \(json)")
                completion(result: ["success": true, "userSurvey": userSurvey!])
            } else {
                completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
            }
        }
    }

    func registerAnswerForUser(user: User, question: Question, points: Int) {
        UserAnswer.addAnswerForUser(user, question: question, points: points, moc: self.appDelegate.managedObjectContext)
    }

    func sendRiskAnalysisAnswersForUser(user: User, survey: Survey, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = SURVEY_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"
        let userSurvey       = UserSurvey.getSurveyForUser(user, survey: survey, moc: self.appDelegate.managedObjectContext)
        var answers: [[String:AnyObject]] = []

        for question:Question in survey.getQuestions() {
            let userAnswer = UserAnswer.getAnswerForUser(user, question: question, moc: self.appDelegate.managedObjectContext)
            let questionID = Int(question.questionID)
            let points     = Int((userAnswer?.points)!)
            answers.append(["question_id": questionID, "points": points])
        }

        let params: [String: AnyObject] = [
            "uid":      Int(user.userID),
            "answers":  answers
        ]

        request(.PUT, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result

                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }

                //Mark as complete and submitted
                userSurvey?.isComplete    = true
                userSurvey?.submittedDate = NSDate().timeIntervalSince1970

                completion(result: ["success": true])
        }

    }
}
