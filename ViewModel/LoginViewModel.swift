//
//  LoginViewModel.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/4/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class LoginViewModel: ViewModel {

    class func loginToAPIWithUsername(username: String, password: String, completion:(result: Dictionary<String,AnyObject>) -> Void) {
        let params: [String:AnyObject] = ["username": username, "password": password]
        let url                        = "\(SERVER_URL)\(LOGIN_ENDPOINT)"
        
        
        
        let rheaders: [String:String] = ["X-CSRF-Token": "","Cookie": ""]
        
        request(.POST, url, parameters: params, headers: rheaders).responseJSON { (response: Response) -> Void in

            let result = response.result

            if result.isFailure {
                //Epic fail
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                return
            }

            if let jsonObject: AnyObject = result.value {
                let json        = JSON(jsonObject)
                let sessionID   = json["sessid"]
                let sessionName = json["session_name"]
                let token       = json["token"]
                let userJSON    = json["user"]
                var userID: Int?

                if userJSON != nil {
                   userID = userJSON["uid"].intValue
                }

                if sessionID != nil && sessionName != nil && token != nil && userJSON != nil {
                    NSUserDefaults.standardUserDefaults().setObject(sessionName.stringValue, forKey: SESSION_NAME_KEY)
                    NSUserDefaults.standardUserDefaults().setObject(sessionID.stringValue, forKey: SESSION_ID_KEY)
                    NSUserDefaults.standardUserDefaults().setObject(token.stringValue, forKey: CSRF_TOKEN_KEY)
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: LOGGED_IN_KEY)
                    NSUserDefaults.standardUserDefaults().setInteger(userID!, forKey: USER_ID_KEY)

                    print("Saving...\nCookie = \(sessionName.stringValue)=\(sessionID.stringValue)\nToken = \(token.stringValue)\nUserID = \(userID!)")
                    NSUserDefaults.standardUserDefaults().synchronize()

                    //Get User Object from Model
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    let user: User? = User.getUserWithData(userJSON, moc: appDelegate.managedObjectContext)
                    appDelegate.saveContext()

                    print("User object: \(user)")
                    if user != nil {
                        completion(result: ["success": true, "user":user!])
                    } else {
                        completion(result: ["success": false, "errorMessage":"couldn't create the user object"])
                    }
                } else {
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                }
            }
        }
    }
    
    

    class func checkSurveysCompleteForUser(user: User, moc: NSManagedObjectContext) -> Bool {
        let surveys = user.surveys?.allObjects as! [UserSurvey]

        if surveys.count == 0 {
            //Hasn't been assigned a survey, it means user didn't 
            //complete the onboarding process prior to the survey
            return false
        }

        for userSurvey:UserSurvey in surveys {
            if userSurvey.isComplete == false {
                //has an incomplete survey
                return false
            }
        }

        //Good to go
        return true
    }

    /**
     Logs the user out in the server, killing all the user's sessions

     - parameter user:       the user object
     - parameter completion: callback, receives a success flag
     */
    func logoutUser(user: User, completion:(result: Dictionary<String,AnyObject>) -> Void) {
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let endpoint: String         = LOGOUT_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: "\(user.userID)")
        let url: String              = "\(SERVER_URL)\(endpoint)"

        request(.POST, url, encoding: .JSON, headers: appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                //print(result.value)

                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                } else {
                    completion(result: ["success": true])
                }
        }
    }

    /**
     Sends a password reset request for the specified email address

     - parameter email:      The email address to lookup
     - parameter completion: callback, receives a success flag
     */
    func requestPasswordResetForEmail(email: String, completion:(result: Dictionary<String,AnyObject>) -> Void) {
        let url: String = "\(SERVER_URL)\(PASSWORD_RESET_ENDPOINT)"
        let parameters  = ["email": email]

        request(.POST, url, parameters: parameters, encoding: .JSON)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                print(result.value)

                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                } else {
                    if let jsonObject: AnyObject = result.value {
                        let json    = JSON(jsonObject)
                        let message = json["Message"].stringValue

                        completion(result: ["success": true, "message":message])
                    } else {
                        completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
                    }
                }
        }
    }

}
