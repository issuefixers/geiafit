//
//  GemsViewModel.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/27/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Alamofire
import SwiftyJSON

class GemsViewModel: ViewModel {
    
    func getDailyChallengeFromServer(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = DAILY_CHALLENGE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                
                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }
                
                if let jsonObject: AnyObject = result.value {
                    let json = JSON(jsonObject).arrayValue
                    if json.count > 0 {
                        if let challenge = json.first?.stringValue {
                            completion(result: ["success": true, "challenge": challenge])
                        } else {
                            completion(result: ["success": false, "errorMessage":"Couldn't get the daily challenge"])
                        }
                    } else {
                        completion(result: ["success": false, "errorMessage":"Couldn't get the daily challenge"])
                    }
                } else {
                    completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
                }
        }
    }
    
    func updateGemsForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String              = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String            = PROFILE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String                 = "\(SERVER_URL)\(endpoint)"
        let params: [String: AnyObject] = ["gems": 1]
        debugPrint(params)
        
        request(.PUT, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                
                print(response)
                
                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }
                
                completion(result: ["success": true])
        }
    }

}