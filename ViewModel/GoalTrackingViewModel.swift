//
//  GoalTrackingViewModel.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData
import HealthKit

class GoalTrackingViewModel: ViewModel {

    /**
     Returns the Thresholds that are used to determine the effort level of a HK datapoint

     - parameter user:       app's current user
     - parameter completion: callback function
     */
    func getThresholdsForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = THRESHOLDS_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"

        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result

                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }

                if let jsonObject: AnyObject = result.value {
                    let json = JSON(jsonObject)
                    let success = json["success"].boolValue

                    if success {
                        let data = json["data"].object

                        completion(result: ["success": true, "data":data])
                    } else {
                        let message = json["message"].stringValue
                        completion(result: ["success": false, "errorMessage":message])
                    }
                } else {
                    completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
                }
        }
    }

    /**
     Gets the step data from HealthKit and stores it in the local CoreData DB

     - parameter dateRange:  Either .Day (latest values) or .Week (last 7 entries)
     - parameter completion: Callback function, receives a success flag
     */
    
    func updateStepsFromHealthKitForDayOrWeek(date: NSDate, dateRange: DateRange, completion: (success: Bool) -> Void) {
        let healthManager = HealthManager()

        healthManager.getStepsForDate(date, dateRange: dateRange) { (data, error) -> Void in
            
            if error != nil {
                print("Error reading HealthKit Store: \(error.localizedDescription)")
                completion(success: false)
                return
            }
            
            // Save data points to DB
            var healthDataPointObjects: [HealthDataPoint] = []
            
            for stepSample in data {
                // Get sample data
                let steps       = stepSample.quantity.doubleValueForUnit(HKUnit.countUnit())
                let sampleStart = stepSample.startDate
                var timeDiff = stepSample.endDate.timeIntervalSince1970 - stepSample.startDate.timeIntervalSince1970
                timeDiff = (Int(timeDiff) > 0) ? timeDiff : 1.0
                
                
                var deviceType = HealthDataDeviceType.HealthKit
                
                if let metadata = stepSample.metadata {
                    
                    if let deviceName = metadata["HealthDataDeviceStringType"] {
                        
                        if deviceName as! String == HealthDataDeviceStringType.Fitbit.rawValue {
                            
                            deviceType = HealthDataDeviceType.Fitbit
                            
                        } else if deviceName as! String == HealthDataDeviceStringType.Manually.rawValue {
                            
                            deviceType = HealthDataDeviceType.Manually
                            
                        }
                        
                    }
                    
                }
                
                
                
                if dateRange == .Day {
                
                    let dateStart = date.dateAtStartOfDay()
                    if (sampleStart.compare(dateStart) == .OrderedDescending) {
                        
                        if let obj = HealthDataPoint.addDataPointForUser(self.appDelegate.user!, count: steps, timestamp: sampleStart, type: .Steps, duration: timeDiff, moc: self.appDelegate.managedObjectContext, deviceType: deviceType) {
                            healthDataPointObjects.append(obj)
                        }
                    }
                } else {
                    
                    if let obj = HealthDataPoint.addDataPointForUser(self.appDelegate.user!, count: steps, timestamp: sampleStart, type: .Steps, duration: timeDiff, moc: self.appDelegate.managedObjectContext, deviceType: deviceType) {
                        healthDataPointObjects.append(obj)
                    }
                }
            }
            
            completion(success: true)
        }
    }

    func updateStepsFromHealthKitForMonth(date: NSDate, completion: (success: Bool, healthDicts: [Int : [HealthViewPoint]]?) -> Void) {
        let healthManager = HealthManager()
        healthManager.getStepsForDate(date, dateRange: .Month) { (data, error) -> Void in
            
            if error != nil {
                //print("Error reading HealthKit Store: \(error.localizedDescription)")
                completion(success: false, healthDicts: nil)
                return
            }
        
            var healthMonthDataDict = Dictionary<Int, Array<HealthViewPoint>>()
            
            for stepSample in data {
                // Get sample data
                let steps       = stepSample.quantity.doubleValueForUnit(HKUnit.countUnit())
                let sampleStart = stepSample.startDate
                var timeDiff = stepSample.endDate.timeIntervalSince1970 - stepSample.startDate.timeIntervalSince1970
                timeDiff = (Int(timeDiff) > 0) ? timeDiff : 1.0
                
                let obj = HealthViewPoint()
                obj.count = steps
                obj.timestamp = sampleStart
                obj.duration = timeDiff
                obj.type = .Steps
                obj.user = self.appDelegate.user!
                
                let sampleDay = sampleStart.day()
                
                if healthMonthDataDict[sampleDay] == nil {
                    healthMonthDataDict[sampleDay] = [obj]
                } else {
                
                    if var arr = healthMonthDataDict[sampleDay] {
                        arr.append(obj)
                        healthMonthDataDict[sampleDay] = arr
                    }
                }

            }
            
            completion(success: true, healthDicts: healthMonthDataDict)
        }
    }
    
	func updateStepsFromHealthKitForDateRange(dateRange: DateRange, completion: (success: Bool) -> Void) {
        let healthManager = HealthManager()
        healthManager.getStepsForDateRange(dateRange) { (data, error) -> Void in
            if error != nil {
                print("Error reading HealthKit Store: \(error.localizedDescription)")
                completion(success: false)
                return
            }

            let periodLength     = 2//minutes per datapoint
            let duration         = Double(periodLength) * 60.0
            var currentStepCount = 0.0
            var currentRangeStart: NSDate?

            // Save data points to DB
            var healthDataPointObjects: [HealthDataPoint] = []

            //debugPrint("Got \(data.count) samples")
            for stepSample in data {
                
                let metadata = stepSample.metadata
                let deviceName = metadata != nil ? metadata!["HealthDataDeviceStringType"] : nil
                
                if deviceName == nil {
                    
                    
                    // Get sample data
                    let steps       = stepSample.quantity.doubleValueForUnit(HKUnit.countUnit())
                    let sampleStart = stepSample.startDate.dateIgnoringSeconds()
                    
                    if #available(iOS 9.0, *) {
                        //print("\(steps),\(stepSample.startDate),\(stepSample.endDate),\(stepSample.device?.name),\(stepSample.device?.manufacturer),\(stepSample.device?.model)")
                    } else {
                        // Fallback on earlier versions
                    }
                    
                    if currentRangeStart == nil {
                        currentRangeStart = sampleStart
                        currentStepCount  = steps
                    } else if sampleStart.isInDateRange(currentRangeStart!, rangeDurationInMinutes: periodLength) {
                        currentStepCount += steps
                    } else {
                        //save current datapoint
                        if let obj = HealthDataPoint.addDataPointForUser(self.appDelegate.user!, count: currentStepCount, timestamp: currentRangeStart!, type: .Steps, duration: duration, moc: self.appDelegate.managedObjectContext, deviceType: HealthDataDeviceType.HealthKit) {
                            //print("HDP: \(currentStepCount) steps at \(currentRangeStart!)")
                            healthDataPointObjects.append(obj)
                        }
                        
                        //start new period
                        currentRangeStart = sampleStart
                        currentStepCount  = steps
                    }

                    
                    
                }
                
            }

            //After the loop, insert the last datapoint
            if currentRangeStart != nil && currentStepCount > 0 {
                let duration = Double(periodLength) * 60.0
                if let obj = HealthDataPoint.addDataPointForUser(self.appDelegate.user!, count: currentStepCount, timestamp: currentRangeStart!, type: .Steps, duration: duration, moc: self.appDelegate.managedObjectContext, deviceType: HealthDataDeviceType.HealthKit) {
                    //print("HDP: \(currentStepCount) steps at \(currentRangeStart!)")
                    healthDataPointObjects.append(obj)
                }
            }

            completion(success: true)
        }
    }

    /**
     Uses the HealthManager to get the total sum of steps for today or the past week and stores the values in the HealthDailyTotal model

     - parameter user:       App's user object
     - parameter dateRange:  .Day or .Week
     - parameter completion: Block called with success flag when finished
     */

    /**
     Processes the HealthDataPoint objects queried from the CoreData DB and prepares the dictionary for the ViewControllers

     - parameter user:       app user's object
     - parameter dateRange:  Either .Day (latest values) or .Week (last 7 entries)
     - parameter completion: Callback function, receives a dictionary with all the data needed for the different graphs and labels
     */
    func getStepsForUser(user: User, dateRange: DateRange, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        if dateRange == .Day {
            //debugPrint("GTVM - getting step data for today")
            let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(user, date: NSDate(), type: .Steps, moc: self.appDelegate.managedObjectContext)
            var dataPoints: [Dictionary<String,AnyObject>] = []
            var lightMin    = 0.0
            var moderateMin = 0.0
            var vigorousMin = 0.0
            var lightMinGoal: Int
            var moderateMinGoal: Int
            var vigorousMinGoal: Int
            var stepGoal: Int
            var totalMinGoal: Int
            var thresholdLow: Double
            var thresholdHigh: Double

            if let thresholds = user.thresholds {
                thresholdLow  = thresholds.stepsLow
                thresholdHigh = thresholds.stepsHigh
            } else {
                //Default thresholds
                thresholdLow  = 20.0
                thresholdHigh = 50.0
            }

            //debugPrint("GTVM - getting goals")
            if let goals = ActivityGoals.getLatestGoalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext) {
                lightMinGoal    = Int(goals.lightTime)
                moderateMinGoal = Int(goals.moderateTime)
                vigorousMinGoal = Int(goals.vigorousTime)
                stepGoal        = Int(goals.totalSteps)
                totalMinGoal    = lightMinGoal + moderateMinGoal + vigorousMinGoal
            } else {
                lightMinGoal    = 0
                moderateMinGoal = 0
                vigorousMinGoal = 0
                stepGoal        = 0
                totalMinGoal    = 0
            }
            //debugPrint("GTVM - parsed goals: \(lightMinGoal), \(moderateMinGoal), \(vigorousMinGoal), \(stepGoal)")

            //debugPrint("GTVM - parsing \(healthDataPointObjects.count) objects")
            for hdp: HealthDataPoint in healthDataPointObjects {
                // Calculate avg steps per min for data point
                let avgStepsPerSecond = hdp.count / hdp.duration
                let avgStepsPerMinute = avgStepsPerSecond * 60.0

                // Determine effort level
                switch avgStepsPerMinute {
                case 0.0 ..< 5.0:
                    hdp.level = .None
                    lightMin += (hdp.duration / 60.0)
                case 5.0 ..< thresholdLow:
                    hdp.level = .Light
                    lightMin += (hdp.duration / 60.0)
                case thresholdLow ..< thresholdHigh:
                    hdp.level = .Moderate
                    moderateMin += (hdp.duration / 60.0)
                default:
                    hdp.level = .Vigorous
                    vigorousMin += (hdp.duration / 60.0)
                }

                //debugPrint("steps: \(hdp.count), duration:\(hdp.duration), avg:\(avgStepsPerMinute), level: \(hdp.level.rawValue)")
                if hdp.level != .None {
                    // Add data point if it has an effort level
                    dataPoints.append(["value": avgStepsPerMinute, "timestamp": hdp.timestamp])
                }
            }

            // Calculate total activity minutes
            let totalMin = Int(lightMin + moderateMin + vigorousMin)

            // Calculate progress twoards goals
            var lightProgress    = 0.0
            var moderateProgress = 0.0
            var vigorousProgress = 0.0

            if lightMinGoal > 0 {
                lightProgress = lightMin / Double(lightMinGoal)
            }

            if moderateMinGoal > 0 {
                moderateProgress = moderateMin / Double(moderateMinGoal)
            }

            if vigorousMinGoal > 0 {
                vigorousProgress = vigorousMin / Double(vigorousMinGoal)
            }

            //add padding for the datapoints
            //print("GTVM - datapoint count: \(dataPoints.count)")
            if dataPoints.count > 0 {
                let first     = dataPoints.first!
                let firstDate = first["timestamp"] as! NSDate
                let firstHour = firstDate.hour()
                let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
                let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: firstDate)

                if firstHour > 0 && firstHour < 24 {
                    for h in 0..<firstHour {
                        components.hour   = h
                        components.minute = 0
                        components.second = 0
                        let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                        dataPoints.insert(["value": 0.0, "timestamp": timestamp], atIndex: h)
                    }
                }

                let last     = dataPoints.last!
                let lastDate = last["timestamp"] as! NSDate
                let lastHour = lastDate.hour()

                if lastHour >= 0 && lastHour < 23 {
                    for h in lastHour+1...23 {
                        components.hour   = h
                        components.minute = 0
                        components.second = 0
                        let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                        dataPoints.append(["value": 0.0, "timestamp": timestamp])
                    }
                }
            } else {
                let date = NSDate().dateAtStartOfDay()
                let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
                let components =    NSCalendar.currentCalendar().components(componentFlags, fromDate: date)

                for h in 0..<24 {
                    components.hour   = h
                    components.minute = 0
                    components.second = 0
                    let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                    dataPoints.append(["value": 0.0, "timestamp": timestamp])
                }
            }

            //debugPrint("GTVM - done")
            completion(result: ["success": true, "data": dataPoints,"stepGoal": stepGoal, "lightMin": Int(lightMin), "moderateMin": Int(moderateMin), "vigorousMin": Int(vigorousMin), "lightMinGoal": lightMinGoal, "moderateMinGoal": moderateMinGoal, "vigorousMinGoal": vigorousMinGoal, "totalMin": totalMin, "totalMinGoal": totalMinGoal, "lightProgress": lightProgress, "moderateProgress": moderateProgress, "vigorousProgress": vigorousProgress, "lowThreshold": thresholdLow, "highThreshold": thresholdHigh])

        } else if dateRange == .Week {

            var lightDataPoints: [Dictionary<String,AnyObject>]    = []
            var moderateDataPoints: [Dictionary<String,AnyObject>] = []
            var vigorousDataPoints: [Dictionary<String,AnyObject>] = []
            var stepDataPoints: [Dictionary<String,AnyObject>]     = []

            var thresholdLow: Double
            var thresholdHigh: Double

            if let thresholds = user.thresholds {
                thresholdLow  = thresholds.stepsLow
                thresholdHigh = thresholds.stepsHigh
            } else {
                //Default thresholds
                thresholdLow  = 20.0
                thresholdHigh = 50.0
            }

            let today = NSDate()
            for dayDiff in 0...6 {
                let nDays = 6 - dayDiff
                let date  = today.dateBySubtractingDays(nDays)!

                let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(user, date: date, type: .Steps, moc: self.appDelegate.managedObjectContext)

                var lightMin    = 0.0
                var moderateMin = 0.0
                var vigorousMin = 0.0
                var stepCount   = 0

                var lightMinGoal: Int
                var moderateMinGoal: Int
                var vigorousMinGoal: Int
                var stepGoal: Int

                if let goals = ActivityGoals.getGoalsForUser(user, date: date, moc: self.appDelegate.managedObjectContext) {
                    lightMinGoal    = Int(goals.lightTime)
                    moderateMinGoal = Int(goals.moderateTime)
                    vigorousMinGoal = Int(goals.vigorousTime)
                    stepGoal        = Int(goals.totalSteps)
                } else {
                    lightMinGoal    = 0
                    moderateMinGoal = 0
                    vigorousMinGoal = 0
                    stepGoal        = 0
                }

                for hdp: HealthDataPoint in healthDataPointObjects {
                    // Acumulate steps for the day
                    stepCount += Int(hdp.count)

                    // Calculate avg steps per min for data point
                    let avgStepsPerSecond = hdp.count / hdp.duration
                    let avgStepsPerMinute = avgStepsPerSecond * 60.0

                    // Determine effort level
                    switch avgStepsPerMinute {
                    case 0.0 ..< 5.0:
                        hdp.level = .None
						lightMin += (hdp.duration / 60.0)
                    case 5.0 ..< thresholdLow:
                        hdp.level = .Light
                        lightMin += (hdp.duration / 60.0)
                    case thresholdLow ..< thresholdHigh:
                        hdp.level = .Moderate
                        moderateMin += (hdp.duration / 60.0)
                    default:
                        hdp.level = .Vigorous
                        vigorousMin += (hdp.duration / 60.0)
                    }
                }

                //save data for this day
                lightDataPoints.append(   ["value": lightMin,    "goal":lightMinGoal,    "timestamp": date])
                moderateDataPoints.append(["value": moderateMin, "goal":moderateMinGoal, "timestamp": date])
                vigorousDataPoints.append(["value": vigorousMin, "goal":vigorousMinGoal, "timestamp": date])
                stepDataPoints.append(    ["value": stepCount,   "goal":stepGoal,        "timestamp": date])
				
            }

            completion(result: ["success": true, "lightData": lightDataPoints, "moderateData": moderateDataPoints, "vigorousData": vigorousDataPoints, "stepData": stepDataPoints])
		} else if dateRange == .Month {
			var lightDataPoints: [Dictionary<String,AnyObject>]    = []
			var moderateDataPoints: [Dictionary<String,AnyObject>] = []
			var vigorousDataPoints: [Dictionary<String,AnyObject>] = []
			var stepDataPoints: [Dictionary<String,AnyObject>]     = []
			
			var thresholdLow: Double
			var thresholdHigh: Double
			
			if let thresholds = user.thresholds {
				thresholdLow  = thresholds.stepsLow
				thresholdHigh = thresholds.stepsHigh
			} else {
				//Default thresholds
				thresholdLow  = 20.0
				thresholdHigh = 50.0
			}
			
			let today = NSDate()
			let calender : NSCalendar = NSCalendar.currentCalendar()
			
			let range : NSRange = calender.rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: today)
			
			debugPrint(range.length)
			for dayDiff in 1...today.day() {
				let nDays = today.day() - dayDiff
				let date  = today.dateBySubtractingDays(nDays)!
				
				let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(user, date: date, type: .Steps, moc: self.appDelegate.managedObjectContext)
				
				var lightMin    = 0.0
				var moderateMin = 0.0
				var vigorousMin = 0.0
				var stepCount   = 0
				
				var lightMinGoal: Int
				var moderateMinGoal: Int
				var vigorousMinGoal: Int
				var stepGoal: Int
				
				if let goals = ActivityGoals.getGoalsForUser(user, date: date, moc: self.appDelegate.managedObjectContext) {
					lightMinGoal    = Int(goals.lightTime)
					moderateMinGoal = Int(goals.moderateTime)
					vigorousMinGoal = Int(goals.vigorousTime)
					stepGoal        = Int(goals.totalSteps)
				} else {
					lightMinGoal    = 0
					moderateMinGoal = 0
					vigorousMinGoal = 0
					stepGoal        = 0
				}
				
				for hdp: HealthDataPoint in healthDataPointObjects {
					// Acumulate steps for the day
					stepCount += Int(hdp.count)
					
					// Calculate avg steps per min for data point
					let avgStepsPerSecond = hdp.count / hdp.duration
					let avgStepsPerMinute = avgStepsPerSecond * 60.0
					
					// Determine effort level
					switch avgStepsPerMinute {
					case 0.0 ..< 5.0:
						hdp.level = .None
						lightMin += (hdp.duration / 60.0)
					case 5.0 ..< thresholdLow:
						hdp.level = .Light
						lightMin += (hdp.duration / 60.0)
					case thresholdLow ..< thresholdHigh:
						hdp.level = .Moderate
						moderateMin += (hdp.duration / 60.0)
					default:
						hdp.level = .Vigorous
						vigorousMin += (hdp.duration / 60.0)
					}
				}
				
				//save data for this day
				lightDataPoints.append(   ["value": lightMin,    "goal":lightMinGoal,    "timestamp": date])
				moderateDataPoints.append(["value": moderateMin, "goal":moderateMinGoal, "timestamp": date])
				vigorousDataPoints.append(["value": vigorousMin, "goal":vigorousMinGoal, "timestamp": date])
				stepDataPoints.append(    ["value": stepCount,   "goal":stepGoal,        "timestamp": date])
			}
			
			for dayDiff in 1 ... range.length - today.day() {
				let nDays = dayDiff
				let date  = today.dateByAddingDays(nDays)!
				
				let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(user, date: date, type: .Steps, moc: self.appDelegate.managedObjectContext)
				
				var lightMin    = 0.0
				var moderateMin = 0.0
				var vigorousMin = 0.0
				var stepCount   = 0
				
				var lightMinGoal: Int
				var moderateMinGoal: Int
				var vigorousMinGoal: Int
				var stepGoal: Int
				
				if let goals = ActivityGoals.getGoalsForUser(user, date: date, moc: self.appDelegate.managedObjectContext) {
					lightMinGoal    = Int(goals.lightTime)
					moderateMinGoal = Int(goals.moderateTime)
					vigorousMinGoal = Int(goals.vigorousTime)
					stepGoal        = Int(goals.totalSteps)
				} else {
					lightMinGoal    = 0
					moderateMinGoal = 0
					vigorousMinGoal = 0
					stepGoal        = 0
				}
				
				for hdp: HealthDataPoint in healthDataPointObjects {
					// Acumulate steps for the day
					stepCount += Int(hdp.count)
					
					// Calculate avg steps per min for data point
					let avgStepsPerSecond = hdp.count / hdp.duration
					let avgStepsPerMinute = avgStepsPerSecond * 60.0
					
					// Determine effort level
					switch avgStepsPerMinute {
					case 0.0 ..< 5.0:
						hdp.level = .None
					case 5.0 ..< thresholdLow:
						hdp.level = .Light
						lightMin += (hdp.duration / 60.0)
					case thresholdLow ..< thresholdHigh:
						hdp.level = .Moderate
						moderateMin += (hdp.duration / 60.0)
					default:
						hdp.level = .Vigorous
						vigorousMin += (hdp.duration / 60.0)
					}
				}
				
				//save data for this day
				lightDataPoints.append(   ["value": lightMin,    "goal":lightMinGoal,    "timestamp": date])
				moderateDataPoints.append(["value": moderateMin, "goal":moderateMinGoal, "timestamp": date])
				vigorousDataPoints.append(["value": vigorousMin, "goal":vigorousMinGoal, "timestamp": date])
				stepDataPoints.append(    ["value": stepCount,   "goal":stepGoal,        "timestamp": date])
			}
			completion(result: ["success": true, "lightData": lightDataPoints, "moderateData": moderateDataPoints, "vigorousData": vigorousDataPoints, "stepData": stepDataPoints])
		
		}
    }

    func getStepsForUserByDay(startDate: NSDate,user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {

        let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(user, date: startDate, type: .Steps, moc: self.appDelegate.managedObjectContext)
        var dataPoints: [Dictionary<String,AnyObject>] = []
        var dataActPoints: [Dictionary<String,AnyObject>] = []
        var lightMin    = 0.0
        var moderateMin = 0.0
        var vigorousMin = 0.0
        var lightMinGoal: Int
        var moderateMinGoal: Int
        var vigorousMinGoal: Int
        var stepGoal: Int
        var totalMinGoal: Int
        var thresholdMin: Double
        var thresholdLow: Double
        var thresholdHigh: Double
        
        if let thresholds = user.thresholds {
            thresholdMin  = thresholds.stepsMin
            thresholdLow  = thresholds.stepsLow
            thresholdHigh = thresholds.stepsHigh
        } else {
            //Default thresholds
            thresholdMin  = 5.0
            thresholdLow  = 20.0
            thresholdHigh = 50.0
        }
        
        //debugPrint("GTVM - getting goals")
        if let goals = ActivityGoals.getLatestGoalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext) {
            lightMinGoal    = Int(goals.lightTime)
            moderateMinGoal = Int(goals.moderateTime)
            vigorousMinGoal = Int(goals.vigorousTime)
            stepGoal        = Int(goals.totalSteps)
            totalMinGoal    = lightMinGoal + moderateMinGoal + vigorousMinGoal
        } else {
            lightMinGoal    = 0
            moderateMinGoal = 0
            vigorousMinGoal = 0
            stepGoal        = 0
            totalMinGoal    = 0
        }
        //debugPrint("GTVM - parsed goals: \(lightMinGoal), \(moderateMinGoal), \(vigorousMinGoal), \(stepGoal)")
        
        //debugPrint("GTVM - parsing \(healthDataPointObjects.count) objects")
        for hdp: HealthDataPoint in healthDataPointObjects {
            // Calculate avg steps per min for data point
            let avgStepsPerSecond = hdp.count / hdp.duration
            let avgStepsPerMinute = avgStepsPerSecond * 60.0
            
            // Determine effort level
            var level = 1
            switch avgStepsPerMinute {
            case 0.0 ..< thresholdMin:
                hdp.level = .None
                lightMin += hdp.duration
            case thresholdMin ..< thresholdLow:
                hdp.level = .Light
                lightMin += hdp.duration
            case thresholdLow ..< thresholdHigh:
                hdp.level = .Moderate
                level = 2
                moderateMin += hdp.duration
            default:
                hdp.level = .Vigorous
                level = 3
                vigorousMin += hdp.duration
            }
            
            //debugPrint("steps: \(hdp.count), duration:\(hdp.duration), avg:\(avgStepsPerMinute), level: \(hdp.level.rawValue)")
            //if hdp.level != .None {
            // Add data point if it has an effort level
            dataPoints.append(["value": avgStepsPerMinute, "timestamp": hdp.timestamp])
            //}
            
            dataActPoints.append(["value": hdp.count, "level": level, "timestamp": hdp.timestamp, "duration": hdp.duration])
            
        }
        
        // Calculate total activity minutes
        lightMin = round(lightMin / 60.0)
        moderateMin = round(moderateMin / 60.0)
        vigorousMin = round(vigorousMin / 60.0)
        
        let totalMin = Int(lightMin) + Int(moderateMin) + Int(vigorousMin)
        
        // Calculate progress twoards goals
        var lightProgress    = 0.0
        var moderateProgress = 0.0
        var vigorousProgress = 0.0
        
        if lightMinGoal > 0 {
            lightProgress = lightMin / Double(lightMinGoal)
        }
        
        if moderateMinGoal > 0 {
            moderateProgress = moderateMin / Double(moderateMinGoal)
        }
        
        if vigorousMinGoal > 0 {
            vigorousProgress = vigorousMin / Double(vigorousMinGoal)
        }
        
        //add padding for the datapoints
        //print("GTVM - datapoint count: \(dataPoints.count)")
        var realDataPoints = dataPoints
        if dataPoints.count > 0 {
            
            let first     = dataPoints.first!
            let firstDate = first["timestamp"] as! NSDate
            let firstHour = firstDate.hour()
            let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
            let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: firstDate)
            
            if dataPoints.count > 1 {
                
                for dayDiff in 0 ... dataPoints.count-2 {
                    let datePoint = dataPoints[dayDiff]
                    let pointDate = datePoint["timestamp"] as! NSDate
                    let pointHour = pointDate.hour()
                    
                    let nextDatePoint = dataPoints[dayDiff+1]
                    let nextPointDate = nextDatePoint["timestamp"] as! NSDate
                    let nextPointHour = nextPointDate.hour()
                    
                    if (nextPointHour - pointHour) > 1 {
                        var i = 1
                        for h in pointHour+1...nextPointHour-1 {
                            
                            components.hour   = h
                            components.minute = 0
                            components.second = 0
                            let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                            realDataPoints.insert(["value": 0.0, "timestamp": timestamp], atIndex: dayDiff+i)
                            i = i + 1
                        }
                    }
                }
            }
            
            
            if firstHour > 0 && firstHour < 24 {
                for h in 0..<firstHour {
                    components.hour   = h
                    components.minute = 0
                    components.second = 0
                    let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                    realDataPoints.insert(["value": 0.0, "timestamp": timestamp], atIndex: h)
                }
            }
            
            
            let last     = dataPoints.last!
            let lastDate = last["timestamp"] as! NSDate
            let lastHour = lastDate.hour()
            
            if lastHour >= 0 && lastHour < 24 {
                for h in lastHour+1...24 {
                    components.hour   = h
                    components.minute = 0
                    components.second = 0
                    let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                    realDataPoints.append(["value": 0.0, "timestamp": timestamp])
   
                }
            }
            
        } else {
            let date = startDate.dateAtStartOfDay()
            let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
            let components =    NSCalendar.currentCalendar().components(componentFlags, fromDate: date)
            
            for h in 0..<25 {
                components.hour   = h
                components.minute = 0
                components.second = 0
                let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                realDataPoints.append(["value": 0.0, "timestamp": timestamp])
  
            }
        }
        
        let todayExercises = Exercise.getExercisesWithDate(startDate, moc: self.appDelegate.managedObjectContext)! as [Exercise]
        
        var didCount: Int = 0
        var totalCount: Int = 0
        if (todayExercises.count > 0) {
            for data:Exercise in todayExercises {
                
                didCount += data.realized
                totalCount += data.dailyTotal
            }
        }
        
        let daily:Int = NSUserDefaults.standardUserDefaults().integerForKey(LAST_CHALLENGE_DATE_KEY + startDate.stringForDate());

        
        //debugPrint("GTVM - done")
        completion(result: ["success": true, "data": realDataPoints,"dataAct": dataActPoints, "stepGoal": stepGoal, "lightMin": Int(lightMin), "moderateMin": Int(moderateMin), "vigorousMin": Int(vigorousMin), "lightMinGoal": lightMinGoal, "moderateMinGoal": moderateMinGoal, "vigorousMinGoal": vigorousMinGoal, "totalMin": totalMin, "totalMinGoal": totalMinGoal, "lightProgress": lightProgress, "moderateProgress": moderateProgress, "vigorousProgress": vigorousProgress, "lowThreshold": thresholdLow, "highThreshold": thresholdHigh,"total_exercise":didCount,"total_exercise_goal":totalCount,"daily_challenge":daily]);
    }
    
    func getStepsForUserByWeek(startDate: NSDate,user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        
        var weekPoints : [Dictionary<String,AnyObject>] = []
        var lightDataPoints: [Dictionary<String,AnyObject>]    = []
        var moderateDataPoints: [Dictionary<String,AnyObject>] = []
        var vigorousDataPoints: [Dictionary<String,AnyObject>] = []
        var stepDataPoints: [Dictionary<String,AnyObject>]     = []
        
        var thresholdMin: Double
        var thresholdLow: Double
        var thresholdHigh: Double
        
        if let thresholds = user.thresholds {
            thresholdMin  = thresholds.stepsMin
            thresholdLow  = thresholds.stepsLow
            thresholdHigh = thresholds.stepsHigh
        } else {
            //Default thresholds
            thresholdMin  = 5.0
            thresholdLow  = 20.0
            thresholdHigh = 50.0
        }
        
        let today = startDate
        for dayDiff in 0...6 {
            let nDays = 6 - dayDiff
            let date  = today.dateBySubtractingDays(nDays)!
            
            let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(user, date: date, type: .Steps, moc: self.appDelegate.managedObjectContext)
            
            var lightMin    = 0.0
            var moderateMin = 0.0
            var vigorousMin = 0.0
            var stepCount   = 0
            
            var lightMinGoal: Int
            var moderateMinGoal: Int
            var vigorousMinGoal: Int
            var stepGoal: Int
            
            if let goals = ActivityGoals.getGoalsForUser(user, date: date, moc: self.appDelegate.managedObjectContext) {
                lightMinGoal    = Int(goals.lightTime)
                moderateMinGoal = Int(goals.moderateTime)
                vigorousMinGoal = Int(goals.vigorousTime)
                stepGoal        = Int(goals.totalSteps)
            } else {
                lightMinGoal    = 0
                moderateMinGoal = 0
                vigorousMinGoal = 0
                stepGoal        = 0
            }
            
            for hdp: HealthDataPoint in healthDataPointObjects {
                // Acumulate steps for the day
                stepCount += Int(hdp.count)
                
                // Calculate avg steps per min for data point
                let avgStepsPerSecond = hdp.count / hdp.duration
                let avgStepsPerMinute = avgStepsPerSecond * 60.0
                
                // Determine effort level
                switch avgStepsPerMinute {
                case 0.0 ..< thresholdMin:
                    hdp.level = .None
                    lightMin += hdp.duration
                case thresholdMin ..< thresholdLow:
                    hdp.level = .Light
                    lightMin += hdp.duration
                case thresholdLow ..< thresholdHigh:
                    hdp.level = .Moderate
                    moderateMin += hdp.duration
                default:
                    hdp.level = .Vigorous
                    vigorousMin += hdp.duration
                }
            }
            
            //save data for this day
            lightDataPoints.append(   ["value": Int(lightMin/60.0),    "goal":lightMinGoal,    "timestamp": date])
            moderateDataPoints.append(["value": Int(moderateMin/60.0), "goal":moderateMinGoal, "timestamp": date])
            vigorousDataPoints.append(["value": Int(vigorousMin/60.0), "goal":vigorousMinGoal, "timestamp": date])
            stepDataPoints.append(    ["value": stepCount,   "goal":stepGoal,        "timestamp": date])
            
       
            let todayExercises = Exercise.getExercisesWithDate(date, moc: self.appDelegate.managedObjectContext)! as [Exercise]

            var didCount: Int = 0
            var totalCount: Int = 0
            if (todayExercises.count > 0) {
                for data:Exercise in todayExercises {
                    
                    didCount += data.realized
                    totalCount += data.dailyTotal
                }
            }

            let daily:Int = NSUserDefaults.standardUserDefaults().integerForKey(LAST_CHALLENGE_DATE_KEY + date.stringForDate());

            weekPoints.append(["stepGoal": stepGoal, "lightMin": Int(lightMin/60.0), "moderateMin": Int(moderateMin/60.0), "vigorousMin": Int(vigorousMin/60.0), "lightMinGoal": lightMinGoal, "moderateMinGoal": moderateMinGoal, "vigorousMinGoal": vigorousMinGoal,"totalSteps":stepCount,"timestamp": date, "total_exercise":didCount,"total_exercise_goal":totalCount,"daily_challenge":daily]);
        }
        
        completion(result: ["success": true, "lightData": lightDataPoints, "moderateData": moderateDataPoints, "vigorousData": vigorousDataPoints, "stepData": stepDataPoints,"weekPoints":weekPoints])
    }
    
    func getStepsForUserByMonth(startDate: NSDate,user: User, healthDicts: [Int : [HealthViewPoint]]?,completion: (result: Dictionary<String,AnyObject>) -> Void) {
        
        var lightDataPoints: [Dictionary<String,AnyObject>]    = []
        var moderateDataPoints: [Dictionary<String,AnyObject>] = []
        var vigorousDataPoints: [Dictionary<String,AnyObject>] = []
        
        var thresholdMin: Double
        var thresholdLow: Double
        var thresholdHigh: Double
        
        if let thresholds = user.thresholds {
            thresholdMin  = thresholds.stepsMin
            thresholdLow  = thresholds.stepsLow
            thresholdHigh = thresholds.stepsHigh
        } else {
            //Default thresholds
            thresholdMin  = 5.0
            thresholdLow  = 20.0
            thresholdHigh = 50.0
        }
        
        var lightMinGoal: Int
        var moderateMinGoal: Int
        var vigorousMinGoal: Int
        
        if let goals = ActivityGoals.getGoalsForUser(user, date: startDate, moc: self.appDelegate.managedObjectContext) {
            lightMinGoal    = Int(goals.lightTime)
            moderateMinGoal = Int(goals.moderateTime)
            vigorousMinGoal = Int(goals.vigorousTime)
        } else {
            lightMinGoal    = 0
            moderateMinGoal = 0
            vigorousMinGoal = 0
        }
        
        if healthDicts != nil {
            
            for (_, points) in healthDicts! {
                
                var lightMin    = 0.0
                var moderateMin = 0.0
                var vigorousMin = 0.0
                
                let date  = (points[0] as HealthViewPoint).timestamp
                
                for hdp: HealthViewPoint in points {
                    // Calculate avg steps per min for data point
                    let avgStepsPerSecond = hdp.count / hdp.duration
                    let avgStepsPerMinute = avgStepsPerSecond * 60.0
                    
                    // Determine effort level
                    switch avgStepsPerMinute {
                    case 0.0 ..< thresholdMin:
                        hdp.level = .None
                        lightMin += hdp.duration
                    case thresholdMin ..< thresholdLow:
                        hdp.level = .Light
                        lightMin += hdp.duration
                    case thresholdLow ..< thresholdHigh:
                        hdp.level = .Moderate
                        moderateMin += hdp.duration
                    default:
                        hdp.level = .Vigorous
                        vigorousMin += hdp.duration
                    }
                }
                
                //save data for this day
                lightDataPoints.append(   ["value": Int(lightMin/60.0),    "goal":lightMinGoal,    "timestamp": date])
                moderateDataPoints.append(["value": Int(moderateMin/60.0), "goal":moderateMinGoal, "timestamp": date])
                vigorousDataPoints.append(["value": Int(vigorousMin/60.0), "goal":vigorousMinGoal, "timestamp": date])
                
            }
            //                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
            //                            HealthDataPoint.addDataPointForUser(self.appDelegate.user!, count: hdp.count, timestamp: hdp.timestamp, type: .Steps, duration: hdp.duration, moc: self.appDelegate.managedObjectContext)
            //                        })
            
        } else {
        
        let today = startDate
        let calender: NSCalendar = NSCalendar.currentCalendar()
        let range: NSRange = calender.rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: today)
        
        for dayDiff in 1...today.day() {
            let nDays = today.day() - dayDiff
            let date  = today.dateBySubtractingDays(nDays)!
            
            let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(user, date: date, type: .Steps, moc: self.appDelegate.managedObjectContext)
            
            var lightMin    = 0.0
            var moderateMin = 0.0
            var vigorousMin = 0.0
            
            for hdp: HealthDataPoint in healthDataPointObjects {
                
                // Calculate avg steps per min for data point
                let avgStepsPerSecond = hdp.count / hdp.duration
                let avgStepsPerMinute = avgStepsPerSecond * 60.0
                
                // Determine effort level
                switch avgStepsPerMinute {
                case 0.0 ..< thresholdMin:
                    hdp.level = .None
                    lightMin += hdp.duration
                case thresholdMin ..< thresholdLow:
                    hdp.level = .Light
                    lightMin += hdp.duration
                case thresholdLow ..< thresholdHigh:
                    hdp.level = .Moderate
                    moderateMin += hdp.duration
                default:
                    hdp.level = .Vigorous
                    vigorousMin += hdp.duration
                }
            }
            
            //save data for this day
            lightDataPoints.append(   ["value": Int(lightMin/60.0),    "goal":lightMinGoal,    "timestamp": date])
            moderateDataPoints.append(["value": Int(moderateMin/60.0), "goal":moderateMinGoal, "timestamp": date])
            vigorousDataPoints.append(["value": Int(vigorousMin/60.0), "goal":vigorousMinGoal, "timestamp": date])
        }
        
            if (range.length - today.day()) >= 1 {
            
               for dayDiff in 1 ... range.length - today.day() {
                let nDays = dayDiff
                let date  = today.dateByAddingDays(nDays)!
                
                let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(user, date: date, type: .Steps, moc: self.appDelegate.managedObjectContext)
                
                var lightMin    = 0.0
                var moderateMin = 0.0
                var vigorousMin = 0.0
                
                for hdp: HealthDataPoint in healthDataPointObjects {
                    
                    // Calculate avg steps per min for data point
                    let avgStepsPerSecond = hdp.count / hdp.duration
                    let avgStepsPerMinute = avgStepsPerSecond * 60.0
                    
                    // Determine effort level
                    switch avgStepsPerMinute {
                    case 0.0 ..< thresholdMin:
                        hdp.level = .None
                    case thresholdMin ..< thresholdLow:
                        hdp.level = .Light
                        lightMin += hdp.duration
                    case thresholdLow ..< thresholdHigh:
                        hdp.level = .Moderate
                        moderateMin += hdp.duration
                    default:
                        hdp.level = .Vigorous
                        vigorousMin += hdp.duration
                    }
                }
                
                //save data for this day
                lightDataPoints.append(   ["value": Int(lightMin/60.0),    "goal":lightMinGoal,    "timestamp": date])
                moderateDataPoints.append(["value": Int(moderateMin/60.0), "goal":moderateMinGoal, "timestamp": date])
                vigorousDataPoints.append(["value": Int(vigorousMin/60.0), "goal":vigorousMinGoal, "timestamp": date])
                
                }
            }
        }
        
        completion(result: ["success": true, "lightData": lightDataPoints, "moderateData": moderateDataPoints, "vigorousData": vigorousDataPoints])
        
    }

    /**
     Gets the Nutrition Goals from the endpoint

     - parameter user:       app's user
     - parameter completion: callback function
     */
    func getNutritionGoalsForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = NUTRITION_GOALS_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"

        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result

                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }

                if let jsonObject: AnyObject = result.value {
                    let json = JSON(jsonObject)
                    let success = json["success"].boolValue

                    if success {
                        let data = json["data"].object

                        completion(result: ["success": true, "data":data])
                    } else {
                        let message = json["message"].stringValue
                        completion(result: ["success": false, "errorMessage":message])
                    }
                } else {
                    completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
                }
        }
    }

    /**
     Uses HealthManager to update the Nutrition Data from HealthKit and store it in CoreData

     - parameter dateRange:  .Day or .Week
     - parameter completion: Sends a success flag
     */
    func updateNutritionDataFromHealthKitForDateRange(dateRange: DateRange, completion: (success: Bool) -> Void) {
        let healthManager = HealthManager()

        //debugPrint("GTVM - getting nutrition for today")
        healthManager.getNutritionForDateRange(dateRange) { (success) -> Void in
            self.appDelegate.saveContext()
            //debugPrint("GTVM - saved nutrition")

            completion(success: success)
        }
    }

    /**
     Returns the data needed for the Nutrition Goal Tracking Views. It returns different data depending on the *dateRange* parameter which can be either .Day or .Week

     - parameter user:       App's user
     - parameter dateRange:  Either .Day (latest values) or .Week (last 7 entries)
     - parameter completion: Callback function
     */
    func getNutritionDataForUser(user: User, dateRange: DateRange, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        if dateRange == .Day {

            //debugPrint("GTVM - getting nutrition goals")
            let nutritionGoals = NutritionGoals.getLatestGoalsForUser(user, moc: self.appDelegate.managedObjectContext)

            //debugPrint("GTVM - getting totals")
            guard let dailyTotals = HealthDailyTotal.getTodaysTotalsForUser(user, moc: self.appDelegate.managedObjectContext) else {
                debugPrint("GTVM - totals nil")
                completion(result: ["success": false])
                return
            }
            //debugPrint("GTVM - got totals")

            var totalCaloriesGoal: Double
            var fatPercentGoal: Double
            var carbsPercentGoal: Double
            var proteinPercentGoal: Double
            
            if nutritionGoals == nil {
                totalCaloriesGoal  = 0.0
                fatPercentGoal     = 0.0
                carbsPercentGoal   = 0.0
                proteinPercentGoal = 0.0
            } else {
                totalCaloriesGoal  = Double(nutritionGoals!.totalCalories)
                fatPercentGoal     = nutritionGoals!.fatPercent
                carbsPercentGoal   = nutritionGoals!.carbsPercent
                proteinPercentGoal = nutritionGoals!.proteinPercent
            }
            
            let data = [
                "calorieGoal": totalCaloriesGoal, "fatGoal": fatPercentGoal, "carbsGoal": carbsPercentGoal, "proteinGoal": proteinPercentGoal,
                "totalCalories": Double(dailyTotals.caloriesConsumed), "totalFat": dailyTotals.fat, "totalCarbs": dailyTotals.carbs, "totalProtein": dailyTotals.protein
             ]

            completion(result: ["success": true, "data": data])
            //debugPrint("GTVM - done")

        } else if dateRange == .Week {

            let today             = NSDate()
            let lastWeek          = today.dateAtStartOfDay().dateBySubtractingDays(7)!
            guard let dailyTotals = HealthDailyTotal.getTotalsForUser(user, startDate: lastWeek, endDate: today, moc: self.appDelegate.managedObjectContext) else {
                completion(result: ["success": false])
                return
            }

            guard let goals = NutritionGoals.getGoalsForUser(user, startDate: lastWeek, endDate: today, moc: self.appDelegate.managedObjectContext) else {
                completion(result: ["success": false])
                return
            }

            var fatDataPoints: [Dictionary<String,AnyObject>]     = []
            var carbsDataPoints: [Dictionary<String,AnyObject>]   = []
            var proteinDataPoints: [Dictionary<String,AnyObject>] = []
            var currentGoal: NutritionGoals?

            for dailyTotal: HealthDailyTotal in dailyTotals {
                //debugPrint("\(dailyTotal.fat), \(dailyTotal.carbs), \(dailyTotal.protein) on \(dailyTotal.date)")

                // Check the date
                let dailyTotalDate = dailyTotal.date.dateAtStartOfDay()

                for goal in goals {
                    let goalDate = goal.date.dateAtStartOfDay()

                    if goalDate.isEqualToDateIgnoringTime(dailyTotalDate) || goalDate.isEarlierThanDate(dailyTotalDate) {
                        currentGoal = goal
                        break
                    }
                }

                if currentGoal != nil {
                    fatDataPoints.append(    ["value": dailyTotal.fat,     "goal": currentGoal!.fatPercent,     "timestamp": dailyTotalDate])
                    carbsDataPoints.append(  ["value": dailyTotal.carbs,   "goal": currentGoal!.carbsPercent,   "timestamp": dailyTotalDate])
                    proteinDataPoints.append(["value": dailyTotal.protein, "goal": currentGoal!.proteinPercent, "timestamp": dailyTotalDate])
                } else {
                    fatDataPoints.append(    ["value": dailyTotal.fat,     "goal": 0.0, "timestamp": dailyTotalDate])
                    carbsDataPoints.append(  ["value": dailyTotal.carbs,   "goal": 0.0, "timestamp": dailyTotalDate])
                    proteinDataPoints.append(["value": dailyTotal.protein, "goal": 0.0, "timestamp": dailyTotalDate])
                }
            }

            completion(result: ["success": true, "fatData": fatDataPoints, "carbsData": carbsDataPoints, "proteinData": proteinDataPoints])
        }
    }

    /**
     Returns the data needed for the Vitals Goal Tracking Views. It returns different data depending on the *dateRange* parameter which can be either .Day or .Week

     - parameter user:       App's user
     - parameter dateRange:  Either .Day (latest values) or .Week (last 7 entries)
     - parameter completion: Callback function
     */
    func getVitalsForUser(user: User, dateRange: DateRange, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        if dateRange == .Day {

            guard let obj = Characteristics.getLatestCharacteristicsForUser(user, moc: self.appDelegate.managedObjectContext) else {
                completion(result: ["success": false])
                return
            }

            let data = ["weight": obj.weight!, "rhr": obj.restingHeartRate!, "minHR": obj.bloodPressureDia!, "maxHR": obj.maxHeartRate!, "bmi": obj.bmi!, "emotion": obj.emotion!]
            completion(result: ["success": true, "data": data])

        } else if dateRange == .Week {
            
            let today         = NSDate()
            let lastWeek      = today.dateAtStartOfDay().dateBySubtractingDays(7)!
            guard let objects = Characteristics.getCharacteristicsForUser(user, startDate: lastWeek, endDate: today, moc: self.appDelegate.managedObjectContext) else {
                completion(result: ["success": false])
                return
            }

            var dataPoints: [Dictionary<String,AnyObject>] = []
            
            for charsObj in objects {
                let emotion   = charsObj.emotion!.doubleValue * 100.00
                let dataPoint = ["weight": charsObj.weight!,        "rhr": charsObj.restingHeartRate!,
                                  "maxHR": charsObj.maxHeartRate!,  "bmi": charsObj.bmi!,
                                "emotion": emotion,           "timestamp": charsObj.createdDate!]
                //print(dataPoint)
                dataPoints.append(dataPoint)
            }
            
            completion(result: ["success": true, "data": dataPoints])
        } else if dateRange == .Month {
            
            let today         = NSDate()
            
            let lastMonth      = today.dateAtStartOfDay().dateBySubtractingDays(today.day())!
            guard let objects = Characteristics.getCharacteristicsForUser(user, startDate: lastMonth, endDate: today, moc: self.appDelegate.managedObjectContext) else {
                completion(result: ["success": false])
                return
            }
            
            var dataPoints: [Dictionary<String,AnyObject>] = []
            
            var days = [Int]()
            
            ///
            var dataPointArr: [Dictionary<String,AnyObject>] = []
            
            for charsObj in objects {
                let emotion   = charsObj.emotion!.doubleValue * 100.00
                let dataPoint = ["weight": charsObj.weight!,        "rhr": charsObj.restingHeartRate!,
                                 "maxHR": charsObj.maxHeartRate!,  "bmi": charsObj.bmi!,
                                 "emotion": emotion,           "timestamp": charsObj.createdDate!]
                //print(dataPoint)
                dataPointArr.append(dataPoint)
                days.append((charsObj.createdDate?.day())!)
                
            }
            
            for dataPoint in dataPointArr.reverse() {
                dataPoints.append(dataPoint)
            }
 
            
            
            
            if dataPoints.count > 0 {
                let first     = dataPoints.first!
                let firstDate = first["timestamp"] as! NSDate
                let firstDay = firstDate.day()
                
                let last     = dataPoints.last!
                let lastDate = last["timestamp"] as! NSDate
                let lastDay = lastDate.day()
                
                let componentFlags: NSCalendarUnit = [.Year, .Day, .Month]
                let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: firstDate)
        
                
                for dayDiff in 0 ... lastDay - firstDay-1 {
                    let datePoint = dataPoints[dayDiff]
                    let pointDate = datePoint["timestamp"] as! NSDate
                    let pointHour = pointDate.day()
                    
                    let nextDatePoint = dataPoints[dayDiff+1]
                    let nextPointDate = nextDatePoint["timestamp"] as! NSDate
                    let nextPointHour = nextPointDate.day()
                    
                    if (nextPointHour - pointHour) > 1 {
                        var i = 1
                        for h in pointHour+1...nextPointHour {
                            
                            components.day   = h
                            components.month = firstDate.month()
                            components.year = firstDate.year()
                            
                             let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                            
                            let dataPoint = ["weight": 0,        "rhr": 0,
                                             "maxHR": 0,  "bmi": 0,
                                             "emotion": 0,           "timestamp": timestamp]
                            
                            dataPoints.insert(dataPoint, atIndex: dayDiff+i)
                        
                            i = i + 1
                        }
                    }
                }
                
                if firstDay > 1 {
                    for d in 0..<firstDay-1 {
                        components.day   = d+1
                        components.month = firstDate.month()
                        components.year = firstDate.year()
                        let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
                        
                        let dataPoint = ["weight": 0,        "rhr": 0,
                                         "maxHR": 0,  "bmi": 0,
                                         "emotion": 0,           "timestamp": timestamp]
                        
                        dataPoints.insert(dataPoint, atIndex: d)
                        
                    }
                }
                
                for dayDiff in 1 ... Common.getDays(today) - lastDay {
                    let nDays = dayDiff
                    let date  = today.dateByAddingDays(nDays)!
                    
                    let dataPoint = ["weight": 0,        "rhr": 0,
                                     "maxHR": 0,  "bmi": 0,
                                     "emotion": 0,           "timestamp": date]
                    dataPoints.append(dataPoint)
                    
                }
            
            }

            
            completion(result: ["success": true, "data": dataPoints])
        }
    }

    func getEmotionImageForValue(value: Float) -> UIImage? {
        var filteredValue = value

        if filteredValue < 0 {
            filteredValue = 0.0
        }

        var imageName: String
        switch filteredValue {
        case 0.00 ... 0.10: imageName = "emotion-1"
        case 0.10 ... 0.20: imageName = "emotion-2"
        case 0.20 ... 0.30: imageName = "emotion-3"
        case 0.30 ... 0.40: imageName = "emotion-4"
        case 0.40 ... 0.50: imageName = "emotion-5"
        case 0.50 ... 0.60: imageName = "emotion-6"
        case 0.60 ... 0.70: imageName = "emotion-7"
        case 0.70 ... 0.80: imageName = "emotion-8"
        case 0.80 ... 0.90: imageName = "emotion-9"
        default:            imageName = "emotion-10"
        }

        let image = UIImage(named: imageName)
        return image
    }

    func getTodaysCharacteristicsObject() -> Characteristics? {
        let obj = Characteristics.getTodaysCharacteristicsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext)

        return obj
    }
    
    func sendActivityDataToServerForUser(date: NSDate, user: User, data: [String: AnyObject], completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = LOG_ACTIVITY_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"
        
        /*
 "total_exercise":didCount,"total_exercise_goal":totalCount,"daily_challenge":daily]);
            */
        
        
        let params: [String: AnyObject] = [
            "date":                    date.timeIntervalSince1970,
            "total_steps":             data["totalSteps"]!,
            "total_steps_goal":        data["stepGoal"]!,
            "time_active_high":        data["vigorousMin"]!,
            "time_active_high_goal":   data["vigorousMinGoal"]!,
            "time_active_medium":      data["moderateMin"]!,
            "time_active_medium_goal": data["moderateMinGoal"]!,
            "time_active_low":         data["lightMin"]!,
            "time_active_low_goal":    data["lightMinGoal"]!,
            "total_exercise":          data["total_exercise"]!,
            "total_exercise_goal":     data["total_exercise_goal"]!,
            "daily_challenge":         data["daily_challenge"]!
        
        ]

        request(.PUT, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                //debugPrint(response)
                let result = response.result
                
                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }
                
                completion(result: ["success": true])
        }
    }
    
	
	func sendWeeklyActivityDataToServerForUser(user: User, data: [Dictionary<String,AnyObject>], completion: (result: Dictionary<String,AnyObject>) -> Void) {
		let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
		let endpoint: String = LOG_ACTIVITY_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
		let url: String      = "\(SERVER_URL)\(endpoint)"

        
        //total_exercise, total_exercise_goal, daily_challenge
		var array = [AnyObject]()
		
		for dataPoint in data {
			
			let param: [String: AnyObject] = [
				"date":                    dataPoint["timestamp"]!.timeIntervalSince1970,
				"total_steps":             dataPoint["totalSteps"]!,
				"total_steps_goal":        dataPoint["stepGoal"]!,
				"time_active_high":        dataPoint["vigorousMin"]!,
				"time_active_high_goal":   dataPoint["vigorousMinGoal"]!,
				"time_active_medium":      dataPoint["moderateMin"]!,
				"time_active_medium_goal": dataPoint["moderateMinGoal"]!,
				"time_active_low":         dataPoint["lightMin"]!,
				"time_active_low_goal":    dataPoint["lightMinGoal"]!,
                "total_exercise":          dataPoint["total_exercise"]!,
                "total_exercise_goal":     dataPoint["total_exercise_goal"]!,
                "daily_challenge":         dataPoint["daily_challenge"]!
			]
			
			array.append(param)
		}
		
//		print(array)
		let params : [String:AnyObject] = ["activePoints": array]
		
//		print(params)
		
		request(.PUT, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
			.validate(statusCode: 200..<300)
			.validate(contentType: ["application/json"])
			.responseJSON { (response: Response) -> Void in
				//debugPrint(response)
				let result = response.result
				
				if result.isFailure {
					//Epic fail
					self.dumpDebugData(response)
					completion(result: ["success": false, "errorMessage":result.debugDescription])
					return
				}
				
				completion(result: ["success": true])
		}
	}
	
    func sendNutritionDataToServerForUser(user: User, data: [String: AnyObject], completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = LOG_NUTRITION_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"
        
        let params: [String: AnyObject] = [
            "date":               NSDate().timeIntervalSince1970,
            "calories":           data["totalCalories"]!,
            "calories_goal":      data["calorieGoal"]!,
            "carbohydrates":      data["totalCarbs"]!,
            "carbohydrates_goal": data["carbsGoal"]!,
            "fat":                data["totalFat"]!,
            "fat_goal":           data["fatGoal"]!,
            "protein":            data["totalProtein"]!,
            "protein_goal":       data["proteinGoal"]!
        ]
        
        request(.PUT, url, parameters: params, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                //debugPrint(response)
                let result = response.result
                
                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }
                
                completion(result: ["success": true])
        }
    }
	
	func getVitalsForUserByDate(date:NSDate, user: User, dateRange: DateRange, completion: (result: Dictionary<String,AnyObject>) -> Void) {
		if dateRange == .Day {
			
			let today         = date
			let lastday      = today.dateAtStartOfDay().dateBySubtractingDays(1)!
			guard let objects = Characteristics.getCharacteristicsForUser(user, startDate: lastday, endDate: today, moc: self.appDelegate.managedObjectContext) else {
				completion(result: ["success": false])
				return
			}
			
			var dataPoints: [Dictionary<String,AnyObject>] = []
				
			let tempDay : NSDate = today.dateAtStartOfDay()
				
			let dataPoint = ["weight": -1,        "rhr": -1,
				                 "maxHR": -1,  "bmi": -1,
				                 "emotion": -1, "timestamp":tempDay]
			dataPoints.append(dataPoint)
			
			for charsObj in objects {
			
				let point = dataPoints[0]
					
				let datePointDate = point["timestamp"] as! NSDate
				let charsObjPointDate = charsObj.createdDate
				if datePointDate.isEqualToDateIgnoringTime(charsObjPointDate!) {
						
					var changePoint = point
//					let emotion   = charsObj.emotion!.doubleValue * 100.00
					
					changePoint["weight"] = charsObj.weight!
					changePoint["rhr"] = charsObj.restingHeartRate!
					changePoint["maxHR"]=charsObj.maxHeartRate!
					changePoint["emotion"] = charsObj.emotion!.doubleValue
					changePoint["bmi"] = charsObj.bmi!
					changePoint["timestamp"] = charsObj.createdDate!
					let range :Range = Range(0...0)
					dataPoints.replaceRange(range, with: [changePoint])
					
//					print(charsObj.weight,charsObj.restingHeartRate,charsObj.maxHeartRate,charsObj.emotion!.doubleValue,charsObj.bmi, charsObj.createdDate)
				}
			}
			
			completion(result: ["success": true, "data": dataPoints])
			
		} else if dateRange == .Week {
			
			let today         = date
			let lastWeek      = today.dateAtStartOfDay().dateBySubtractingDays(7)!
			guard let objects = Characteristics.getCharacteristicsForUser(user, startDate: lastWeek, endDate: today, moc: self.appDelegate.managedObjectContext) else {
				completion(result: ["success": false])
				return
			}
			
			var dataPoints: [Dictionary<String,AnyObject>] = []
			
			for index in 0...6 {
			
				let tempDay : NSDate = today.dateAtStartOfDay().dateBySubtractingDays(index)!
			
				let dataPoint = ["weight": -1,        "rhr": -1,
				                 "maxHR": -1,  "bmi": -1,
				                 "emotion": -1, "timestamp":tempDay]
				debugPrint(dataPoint)
				dataPoints.append(dataPoint)
			
			}
			for charsObj in objects {
				var i : Int = 0
				for point in dataPoints {
				
					let datePointDate = point["timestamp"] as! NSDate
					let charsObjPointDate = charsObj.createdDate
					if datePointDate.isEqualToDateIgnoringTime(charsObjPointDate!) {
					
						var changePoint = point
						let emotion   = charsObj.emotion!.doubleValue
						
						changePoint["weight"] = charsObj.weight!
						changePoint["rhr"] = charsObj.restingHeartRate!
						changePoint["maxHR"]=charsObj.maxHeartRate!
						changePoint["emotion"] = emotion
						changePoint["bmi"] = charsObj.bmi!
						changePoint["timestamp"] = charsObj.createdDate!
						let range :Range = Range(i...i)
						dataPoints.replaceRange(range, with: [changePoint])
						
					}
					i = i+1
				}
			}
			
			completion(result: ["success": true, "data": dataPoints])
		}else if dateRange == .Month {
			
			let today         = date
			let month      = today.dateAtStartOfDay().dateBySubtractingDays(today.day())!
			guard let objects = Characteristics.getCharacteristicsForUserForDate(user, startDate: month, endDate: today, moc: self.appDelegate.managedObjectContext) else {
				completion(result: ["success": false])
				return
			}
			
			var dataPoints: [Dictionary<String,AnyObject>] = []
			
			for index in 0...Common.getDays(today)-1 {
				
				let componentFlags: NSCalendarUnit = [.Year, .Day, .Month]
				let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: today.dateAtStartOfDay())
				
				components.day   = index + 1
				components.month = today.month()
				components.year = today.year()
			
				let tempDay : NSDate =  NSCalendar.currentCalendar().dateFromComponents(components)!
				
				let dataPoint = ["weight": -1,        "rhr": -1,
				                 "maxHR": -1,  "bmi": -1,
				                 "emotion": -1, "timestamp":tempDay]
//				print(tempDay)
				dataPoints.append(dataPoint)
				
			}
			for charsObj in objects {
				var i : Int = 0
				for point in dataPoints {
					
					let datePointDate = point["timestamp"] as! NSDate
					let charsObjPointDate = charsObj.createdDate
					if datePointDate.isEqualToDateIgnoringTime(charsObjPointDate!) {
						
						var changePoint = point
						let emotion   = charsObj.emotion!.doubleValue
						
						changePoint["weight"] = charsObj.weight!
						changePoint["rhr"] = charsObj.restingHeartRate!
						changePoint["maxHR"]=charsObj.maxHeartRate!
						changePoint["emotion"] = emotion
						changePoint["bmi"] = charsObj.bmi!
						changePoint["timestamp"] = charsObj.createdDate!
						let range :Range = Range(i...i)
						dataPoints.replaceRange(range, with: [changePoint])
					}
					
					i = i+1
				}
			}

			
			completion(result: ["success": true, "data": dataPoints])
		}
	}
	
	func getEmotionImageForValueByDate(value: Float) -> UIImage? {
		var filteredValue = value
		
		if filteredValue < 0 {
			filteredValue = 0.0
		}
		
		var imageName: String
		switch filteredValue {
		case 0.00 ... 0.10: imageName = "emotion-1"
		case 0.10 ... 0.20: imageName = "emotion-2"
		case 0.20 ... 0.30: imageName = "emotion-3"
		case 0.30 ... 0.40: imageName = "emotion-4"
		case 0.40 ... 0.50: imageName = "emotion-5"
		case 0.50 ... 0.60: imageName = "emotion-6"
		case 0.60 ... 0.70: imageName = "emotion-7"
		case 0.70 ... 0.80: imageName = "emotion-8"
		case 0.80 ... 0.90: imageName = "emotion-9"
		default:            imageName = "emotion-10"
		}
		
		let image = UIImage(named: imageName)
		return image
	}

}
