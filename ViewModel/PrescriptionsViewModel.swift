//
//  PrescriptionsViewModel.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/23/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData
import AVKit
import AVFoundation


class PrescriptionsViewModel: ViewModel {

    func getFitnessGoalsForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = FITNESS_GOALS_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                
                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }
                
                if let jsonObject: AnyObject = result.value {
                    let json = JSON(jsonObject)
                    let success = json["success"].boolValue
                    
                    if success {
                        let data = json["data"].object
                        
                        if data.isKindOfClass(NSNull) {
                            completion(result: ["success": false, "errorMessage":"No Activity Goals for user"])
                        } else {
                            completion(result: ["success": true, "data":data])
                        }
                    } else {
                        let message = json["message"].stringValue
                        completion(result: ["success": false, "errorMessage":message])
                    }
                } else {
                    completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
                }
        }
    }

    func getPostureReportForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = POSTURE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"

        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result

                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }

                if let jsonObject: AnyObject = result.value {
                    let json = JSON(jsonObject)
                    //debugPrint(json)
                    let success = json["success"].boolValue

                    if success {
                        let url = json["url"].stringValue
                        completion(result: ["success": true, "url":url])
                    } else {
                        let message = json["message"].stringValue
                        completion(result: ["success": false, "errorMessage":message])
                    }
                } else {
                    completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
                }
        }
    }

    func getExercisesForUser(user: User, completion: (result: [Exercise]) -> Void){
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = WEBEX_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"

        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result

                if result.isFailure {
                    //Epic fail
                    self.dumpDebugData(response)
                    completion(result: []);
                    return
                }

                if let jsonObject: AnyObject = result.value {
                    let json = JSON(jsonObject)
                    //debugPrint(json)
                    
                    if (json["exercises"] != nil && json["exercises"].array?.count != 0) {
                        var exercises:[Exercise] = [];
                        for exercisesJSON:JSON in json["exercises"].array!  {
                            exercises.append(Exercise.addExerciseWithData(exercisesJSON, moc: self.appDelegate.managedObjectContext)!);
                            
                        }
                        
                        completion(result: exercises);
                    } else {
                        completion(result: []);
                    }
                    
//                    
//                        completion(result: ["success": true, "data": exercisesJSON])
//                    } else {
//                        completion(result: ["success": true, "data": []])
//                    }

                } else {
                    completion(result: []);
                }
        }
    }
    
    func getPrescriptionHelp(completion:(result: String)->Void){
        
        //health_points
        let url: String      = "\(SERVER_URL)\(PRESCRIPTIONS_HELP)"
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                if let jsonObject: AnyObject = result.value {
                    
                    let  dailyTotals = JSON(jsonObject)["content"].stringValue
                    completion(result:dailyTotals);
                    
                }
        }
        
    }

}
