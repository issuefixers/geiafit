//
//  ViewModel.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/27/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Alamofire

class ViewModel {
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    lazy var numberFormatter: NSNumberFormatter = {
        let nf          = NSNumberFormatter()
        nf.allowsFloats = false
        return nf
    }()
    
    func dumpDebugData(response: Response<AnyObject, NSError>) {
        print("\nERROR [\((response.result.error?.code)!)] on \((response.request?.URLString)!)\n")
        if let data = response.request?.HTTPBody {
            print("Request body: \((Toolchain.stringFromNSData(data))!)\n")
        }
        print("Headers: \((response.request?.allHTTPHeaderFields)!)")
        print("Epic Fail: \((response.result.error?.description)!)")
    }
}

class HealthViewPoint {
    var count:     Double = 0.0
    var timestamp: NSDate = NSDate()
    var type:      HealthDataType = .Steps
    var duration:  Double = 0.0
    var level:     EffortLevel = .None
    var user:      User?
}