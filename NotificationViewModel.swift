//
//  NotificationViewModel.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/4/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class NotificationViewModel: ViewModel {
    func getNotifications(user: User, timestamp:String, completion: (result: [Notification]) -> Void){
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = (LOG_NOTIFICATION_ENDPOINT.stringByReplacingOccurrencesOfString("<timeStamp>", withString: timestamp)).stringByReplacingOccurrencesOfString("<userID>", withString: userID);
        
        let url: String      = "\(SERVER_URL)\(endpoint)"
        print(url);
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                var listNotif:[Notification] = [];
                if let jsonObject: AnyObject = result.value {
                    
                    let jsonArray:[JSON] = JSON(jsonObject)["notifications"].arrayValue
                    for jsonMessage:JSON in jsonArray{
                        
                        let notifId = jsonMessage["id"].stringValue
                        var newNotif = Notification.getNotificationWithId(self.appDelegate.managedObjectContext, notificationId:notifId )
                        
                        if(newNotif == nil){
                            newNotif = NSEntityDescription.insertNewObjectForEntityForName("Notification", inManagedObjectContext: self.appDelegate.managedObjectContext) as? Notification
                        }
                        newNotif!.newNotificationWithData(jsonMessage);
                        
                        
                        
                        listNotif.append(newNotif!);
                    }
                    self.appDelegate.saveContext();
                }
                
                1
                
                completion(result: listNotif);
        }
    }

    
    func setNotificationAsRead(notificationId: String, completion: (result: Bool) -> Void){
        let endpoint: String = LOG_NOTIFICATION_ENDPOINT
        
        let url: String      = ("\(SERVER_URL)\(endpoint)" + notificationId).stringByReplacingOccurrencesOfString("<userID>", withString: "");
        print(url);
        request(.PUT, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
             
                if result.isSuccess{
                    print(result.description)
                    completion(result: true);
                }else{
                    completion(result: false);

                }
        }

    
    }
    
    func deleteNotification(notification: Notification){
        let endpoint: String = LOG_NOTIFICATION_ENDPOINT
        
        let url: String      = ("\(SERVER_URL)\(endpoint)" + notification.id!).stringByReplacingOccurrencesOfString("<userID>", withString: "");
        print(url);
        request(.DELETE, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                
                if result.isSuccess{
                    print(result.description)
                    self.appDelegate.managedObjectContext.deleteObject(notification);
                }else{
                    print("couldn't be deleted");
                }
        }
    }
}
