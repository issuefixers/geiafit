//
//  NSMutableAttributedString+Utilities.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/28/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
extension NSMutableAttributedString {
    func bold(text:String) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont(name: "Lato-Bold", size: 14)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.appendAttributedString(boldString)
        return self
    }
    
    func normal(text:String)->NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont(name: "Lato-Light", size: 14)!]

        let normal =  NSAttributedString(string: text, attributes:attrs);
        self.appendAttributedString(normal)
        return self
    }
}
