//
//  NotificationTableViewCell.swift
//  Geia
//
//  Created by Sergio Solorzano on 8/25/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class NotificationTableViewCell: MGSwipeTableCell {

    @IBOutlet weak var notificationBadge: UIImageView!
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var notificationDesc: UILabel!
    @IBOutlet weak var notificationDate: UILabel!
    
    var loaded:Bool = false;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initializeCellWithDict(dict: Notification){
        self.notificationTitle.text = dict.title!
        self.notificationDesc.text = dict.message!
        self.notificationDate.text = "";
        
        var imageNamed = "moreNotifications";
        switch dict.category! {
        case "messages":
            imageNamed = "moreAccount"
            break
        case "snapshot":
            imageNamed = "moreSnapshots"
            break;
        case "prescription":
            imageNamed = "moreVitals"
            break;
        case "activity":
            imageNamed = "moreGoals"
            break;
        default:
            break;
        }
        
        if dict.read?.integerValue == 0 {
            self.backgroundColor = UIColor.geiaDarkColor();
        }else{
            self.backgroundColor = UIColor.clearColor();
        }
        self.notificationBadge.image = UIImage(named: imageNamed);
        
        
       
        if loaded == false{
            self.rightButtons = [MGSwipeButton.init(title: "Delete", backgroundColor: UIColor.redColor()) { (cell) -> Bool in
                NSNotificationCenter.defaultCenter().postNotificationName("refreshNotifications", object: self);
                return true;
                }]
            loaded = true;
        }
      
        
    }
    
    
    func setAsRead(notificationId: Int){
        
    }

}
