//
//  WeeklyVitalsGoalTrackingView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 2/1/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class WeeklyVitalsGoalTrackingView: UIView, SChartDatasource {
    
    // MARK: - Outlets
    @IBOutlet weak var weightGraphView: UIView!
    @IBOutlet weak var rhrGraphView: UIView!
    @IBOutlet weak var maxHRGraphView: UIView!
    @IBOutlet weak var bmiGraphView: UIView!
    @IBOutlet weak var emotionGraphView: UIView!
    
    // MARK: Local variables
    private var datasource: [[String: AnyObject]]  = []
    private var weightChart: ShinobiChart?
    private var rhrChart: ShinobiChart?
    private var maxHRChart: ShinobiChart?
    private var bmiChart: ShinobiChart?
    private var emotionChart: ShinobiChart?
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    // MARK: - Actions
    func updateNutrition() {
        self.goalTrackingViewModel.updateNutritionDataFromHealthKitForDateRange(.Week) { (success) -> Void in
            if success {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //save changes
                    self.appDelegate.saveContext()
                    
                    self.loadData()
                })
            }
        }
    }
    
    func loadData() {
        self.loaderView.show()
        self.goalTrackingViewModel.getVitalsForUser(self.appDelegate.user!, dateRange: .Week) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool
            
            if success {
                if let data = result["data"] {
                    self.datasource = data as! [[String: AnyObject]]
                }

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.setupGraphs()
                })
            }
        }
    }
    
    func setupGraphs() {
        let padding = CGFloat(10.0)
        let frame   = CGRect(x: padding, y: padding, width: self.weightGraphView.bounds.width - 2 * padding, height: self.weightGraphView.bounds.height - 2 * padding)
        self.weightChart                             = ShinobiChart(frame: frame)
        self.weightChart?.clearsContextBeforeDrawing = true
        self.weightChart?.title                      = "Weight"
        self.weightChart?.titleLabel.font            = .geiaFontOfSize(15.0)
        self.weightChart?.autoresizingMask           = .None
        self.weightChart?.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.weightChart?.backgroundColor            = .clearColor()
        self.weightChart?.canvasAreaBackgroundColor  = .clearColor()
        self.weightChart?.plotAreaBackgroundColor    = .whiteColor()
        self.weightChart?.plotAreaBorderThickness    = 1.0
        self.weightChart?.plotAreaBorderColor        = .lightGrayColor()
        
        self.rhrChart                             = ShinobiChart(frame: frame)
        self.rhrChart?.clearsContextBeforeDrawing = true
        self.rhrChart?.title                      = "Resting Heart Rate"
        self.rhrChart?.titleLabel.font            = .geiaFontOfSize(15.0)
        self.rhrChart?.autoresizingMask           = .None
        self.rhrChart?.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.rhrChart?.backgroundColor            = .clearColor()
        self.rhrChart?.canvasAreaBackgroundColor  = .clearColor()
        self.rhrChart?.plotAreaBackgroundColor    = .whiteColor()
        self.rhrChart?.plotAreaBorderThickness    = 1.0
        self.rhrChart?.plotAreaBorderColor        = .lightGrayColor()
        
        self.maxHRChart                             = ShinobiChart(frame: frame)
        self.maxHRChart?.clearsContextBeforeDrawing = true
        self.maxHRChart?.title                      = "Max Heart Rate"
        self.maxHRChart?.titleLabel.font            = .geiaFontOfSize(15.0)
        self.maxHRChart?.autoresizingMask           = .None
        self.maxHRChart?.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.maxHRChart?.backgroundColor            = .clearColor()
        self.maxHRChart?.canvasAreaBackgroundColor  = .clearColor()
        self.maxHRChart?.plotAreaBackgroundColor    = .whiteColor()
        self.maxHRChart?.plotAreaBorderThickness    = 1.0
        self.maxHRChart?.plotAreaBorderColor        = .lightGrayColor()
        
        self.bmiChart                             = ShinobiChart(frame: frame)
        self.bmiChart?.clearsContextBeforeDrawing = true
        self.bmiChart?.title                      = "Body Mass Index"
        self.bmiChart?.titleLabel.font            = .geiaFontOfSize(15.0)
        self.bmiChart?.autoresizingMask           = .None
        self.bmiChart?.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.bmiChart?.backgroundColor            = .clearColor()
        self.bmiChart?.canvasAreaBackgroundColor  = .clearColor()
        self.bmiChart?.plotAreaBackgroundColor    = .whiteColor()
        self.bmiChart?.plotAreaBorderThickness    = 1.0
        self.bmiChart?.plotAreaBorderColor        = .lightGrayColor()
        
        self.emotionChart                             = ShinobiChart(frame: frame)
        self.emotionChart?.clearsContextBeforeDrawing = true
        self.emotionChart?.title                      = "Emotion"
        self.emotionChart?.titleLabel.font            = .geiaFontOfSize(15.0)
        self.emotionChart?.autoresizingMask           = .None
        self.emotionChart?.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.emotionChart?.backgroundColor            = .clearColor()
        self.emotionChart?.canvasAreaBackgroundColor  = .clearColor()
        self.emotionChart?.plotAreaBackgroundColor    = .whiteColor()
        self.emotionChart?.plotAreaBorderThickness    = 1.0
        self.emotionChart?.plotAreaBorderColor        = .lightGrayColor()

        //Axes
        self.weightChart?.xAxis  = self.setupXAxis()
        self.rhrChart?.xAxis     = self.setupXAxis()
        self.maxHRChart?.xAxis   = self.setupXAxis()
        self.bmiChart?.xAxis     = self.setupXAxis()
        self.emotionChart?.xAxis = self.setupXAxis()
        
        self.weightChart?.yAxis  = self.setupYAxis()
        self.rhrChart?.yAxis     = self.setupYAxis()
        self.maxHRChart?.yAxis   = self.setupYAxis()
        self.bmiChart?.yAxis     = self.setupYAxis()
        self.emotionChart?.yAxis = self.setupYAxis()
        
        self.weightGraphView.addSubview(self.weightChart!)
        self.rhrGraphView.addSubview(self.rhrChart!)
        self.maxHRGraphView.addSubview(self.maxHRChart!)
        self.bmiGraphView.addSubview(self.bmiChart!)
        self.emotionGraphView.addSubview(self.emotionChart!)
        
        self.weightChart?.datasource  = self
        self.rhrChart?.datasource     = self
        self.maxHRChart?.datasource   = self
        self.bmiChart?.datasource     = self
        self.emotionChart?.datasource = self
    }
    
    func setupXAxis() -> SChartDateTimeAxis {
        let xAxis = SChartDateTimeAxis()
        xAxis.majorTickFrequency             = SChartDateFrequency(day: 1)
        xAxis.rangePaddingHigh               = SChartDateFrequency(hour: 12)
        xAxis.rangePaddingLow                = SChartDateFrequency(hour: 12)
        xAxis.enableGesturePanning           = false
        xAxis.enableGestureZooming           = false
        xAxis.enableMomentumPanning          = false
        xAxis.enableMomentumZooming          = false
        xAxis.axisPositionValue              = 0
        xAxis.style.lineColor                = .clearColor()
        xAxis.style.majorTickStyle.labelFont = UIFont.systemFontOfSize(12.0)
        xAxis.style.majorTickStyle.showTicks = false
        xAxis.style.majorGridLineStyle.showMajorGridLines = true
        
        let df                       = NSDateFormatter()
        df.dateFormat                = "MM/dd"
        let tickLabelFormatter       = SChartTickLabelFormatter()
        tickLabelFormatter.formatter = df
        xAxis.labelFormatter         = tickLabelFormatter
        
        return xAxis
    }
    
    func setupYAxis() -> SChartNumberAxis {
        let yAxis = SChartNumberAxis()
        yAxis.rangePaddingHigh                = 50.0
        yAxis.enableGesturePanning            = false
        yAxis.enableGestureZooming            = false
        yAxis.enableMomentumPanning           = false
        yAxis.enableMomentumZooming           = false
        yAxis.axisPositionValue               = 0
        yAxis.style.lineColor                 = .clearColor()
        yAxis.style.majorTickStyle.showTicks  = true
        yAxis.style.majorTickStyle.showLabels = true
        yAxis.style.majorGridLineStyle.showMajorGridLines = true
        
        return yAxis
    }
    
    // MARK: - SChart Datasource
    func numberOfSeriesInSChart(chart: ShinobiChart!) -> Int {
        return 1
    }
    
    func sChart(chart: ShinobiChart!, seriesAtIndex index: Int) -> SChartSeries! {
        let scatterSeries = SChartScatterSeries()
        scatterSeries.style().useSeriesSymbols         = true
        scatterSeries.crosshairEnabled                 = true
        scatterSeries.style().pointStyle().color       = .geiaLightBlueColor()
        scatterSeries.style().pointStyle().innerColor  = .geiaLightBlueColor()
        scatterSeries.style().pointStyle().radius      = 10.0
        scatterSeries.style().pointStyle().innerRadius = 1.0

        return scatterSeries
    }
    
    func sChart(chart: ShinobiChart!, numberOfDataPointsForSeriesAtIndex seriesIndex: Int) -> Int {
        let count = self.datasource.count
        
        return count
    }
    
    func sChart(chart: ShinobiChart!, dataPointAtIndex dataIndex: Int, forSeriesAtIndex seriesIndex: Int) -> SChartData! {
        let scDatapoint = SChartDataPoint()
        let datapoint   = self.datasource[dataIndex]
        let value: NSNumber = {
            switch chart {
                case self.weightChart!: return datapoint["weight"]  as! NSNumber
                case self.rhrChart!: 	return datapoint["rhr"]     as! NSNumber
                case self.maxHRChart!:  return datapoint["maxHR"]   as! NSNumber
                case self.bmiChart!:    return datapoint["bmi"]     as! NSNumber
                default:                return datapoint["emotion"] as! NSNumber
            }
        }()
        
        let xValue = datapoint["timestamp"] as! NSDate
        scDatapoint.xValue = xValue
        
        let yValue = value.doubleValue
        scDatapoint.yValue = yValue
        
        return scDatapoint
    }
}