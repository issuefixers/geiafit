//
//  MainNavigationBar.swift
//  Geia
//
//  Created by Sergio Solorzano on 4/21/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {

   
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController);

//        rootViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "navbarHamburger"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(MainNavigationController.hamburgerPress))
        
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func hamburgerPress()  {
        NSNotificationCenter.defaultCenter().postNotificationName("openMenu", object: self);
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);


    }


    
}
