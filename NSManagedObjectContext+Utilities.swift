//
//  NSManagedObjectContext+Utilities.swift
//  Geia
//
//  Created by Sergio Solorzano on 4/22/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
//
//  Created by Collin Donnell on 7/22/15.
//  Copyright (c) 2015 Collin Donnell. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    
    convenience init(parentContext parent: NSManagedObjectContext, concurrencyType: NSManagedObjectContextConcurrencyType) {
        self.init(concurrencyType: concurrencyType)
        parentContext = parent
    }
    
    func deleteAllObjects(error: NSErrorPointer) {
        
        if let entitesByName:[String:NSEntityDescription] = (persistentStoreCoordinator?.managedObjectModel.entitiesByName)!{
            for (name, _) in entitesByName {
                clearCoreData(name)
                
                // If there's a problem, bail on the whole operation.
                if error.memory != nil {
                    return
                }
            }
        }
    }
            
      
    
    
     func clearCoreData(entity:String) {
        let fetchRequest = NSFetchRequest()
        fetchRequest.entity = NSEntityDescription.entityForName(entity, inManagedObjectContext: self)
        fetchRequest.includesPropertyValues = false
        do {
            if let results = try self.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
                for result in results {
                    self.deleteObject(result)
                }
                
                try self.save()
            }
        } catch {
            print("failed to clear core data")
        }
    }
    
}