//
//  QuestionMeasures+CoreDataProperties.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData


extension QuestionMeasure {

    @nonobjc public override class func fetchRequest() -> NSFetchRequest {
        return NSFetchRequest(entityName: "QuestionMeasure");
    }

    @NSManaged public var question: String?
    @NSManaged public var item_id: NSNumber?
    @NSManaged public var type: String?
    @NSManaged public var unit: String?
    @NSManaged public var min: NSNumber?
    @NSManaged public var max: NSNumber?
    @NSManaged public var callback: String?
    @NSManaged public var goal: GoalMeasure?
    @NSManaged public var icon: String?
    @NSManaged public var value:String?

}
