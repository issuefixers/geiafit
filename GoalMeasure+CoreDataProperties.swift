//
//  GoalMeasures+CoreDataProperties.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData


extension GoalMeasure {

    @nonobjc public override class func fetchRequest() -> NSFetchRequest {
        return NSFetchRequest(entityName: "GoalMeasure");
    }

    @NSManaged public var title: String?
    @NSManaged public var nid: String?
    @NSManaged public var questions: [QuestionMeasure]?

}
