//
//  Exercise+CoreDataProperties.swift
//  Geia
//
//  Created by zurce on 6/12/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Exercise {

    @NSManaged var code: String?
    @NSManaged var comment: String?
    @NSManaged var realized: Int
    @NSManaged var completed: Bool
    @NSManaged var reps: String?
    @NSManaged var rest: String?
    @NSManaged var sets: NSNumber?
    @NSManaged var thumbUrl1: String?
    @NSManaged var thumbUrl2: String?
    @NSManaged var title: String?
    @NSManaged var videoURL: String?
    @NSManaged var daily: Bool
    @NSManaged var dailyTotal: Int
    @NSManaged var kSun: Bool
    @NSManaged var kSat: Bool
    @NSManaged var kMon: Bool
    @NSManaged var kThu: Bool
    @NSManaged var kWed: Bool
    @NSManaged var kTue: Bool
    @NSManaged var kFri: Bool

}
