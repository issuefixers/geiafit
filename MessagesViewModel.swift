//
//  LoginViewModel.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/4/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class MessagesViewModel: ViewModel {
    
    
    func getMessagesWithTimeStamp(user: User, timestamp:String, completion: (result: [Message]) -> Void){
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = (MESSAGE_ENDPOINT.stringByReplacingOccurrencesOfString("<timeStamp>", withString: timestamp)).stringByReplacingOccurrencesOfString("<userID>", withString: userID);

        let url: String      = "\(SERVER_URL)\(endpoint)"
        print(url);
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                var listMessage:[Message] = [];
                if let jsonObject: AnyObject = result.value {
                    let jsonArray:[JSON] = JSON(jsonObject).arrayValue
                    for jsonMessage:JSON in jsonArray{
                        
                        let messageId = jsonMessage["message_id"].stringValue
                        var newMessage = Message.getMessageWithId(self.appDelegate.managedObjectContext, messageId:messageId )
                        
                        if(newMessage == nil){
                            newMessage = NSEntityDescription.insertNewObjectForEntityForName("Message", inManagedObjectContext: self.appDelegate.managedObjectContext) as? Message
                        }
                        newMessage!.addMessageWithData(jsonMessage);
                        
                        if(Int64((newMessage!.sender?.intValue)!) == self.appDelegate.user?.userID){
                            newMessage!.senderName = "ME"
                        }else{
                            newMessage!.senderName = "PT"

                        }
                        
                        listMessage.append(newMessage!);
                    }
                    self.appDelegate.saveContext();
                }
                
                1
             
                completion(result: listMessage);
            }
        }
    
    
    func sendMessage(string message: String,  completion: (result: Message?) -> Void){
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: self.appDelegate.user!.userID))!
        let endpoint: String = (MESSAGE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)).stringByReplacingOccurrencesOfString("/<timeStamp>", withString: "");
        let encodedText:NSString =  NSString(data: message.dataUsingEncoding(NSNonLossyASCIIStringEncoding, allowLossyConversion: true)!,encoding: NSUTF8StringEncoding)!
        let url: String      = "\(SERVER_URL)\(endpoint)"
//        let params: [String: AnyObject] = ["message": 1]
        request(.PUT, url, parameters: ["message":encodedText], encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
                    .validate(contentType: ["application/json"])
                    .responseJSON { (response: Response) -> Void in
                let result = response.result
                
                if let jsonMessage:AnyObject = result.value  {
                    let jsonValue:JSON = JSON(jsonMessage)
                    if(jsonValue["success"].int != 0){
                        let newMessage = NSEntityDescription.insertNewObjectForEntityForName("Message", inManagedObjectContext: self.appDelegate.managedObjectContext) as! Message
                        newMessage.addMessageWithData(jsonValue);
                        newMessage.message = message;
                        completion(result: newMessage);
                    }else{
                        completion(result: nil);
                    }
                 
                    
                }
        }
    }

    }
 


