//
//  LineChartBaseView.swift
//  Geia
//
//  Created by haley on 6/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts

class LineChartBaseView: LineChartView {
	func setupLineChartView() {
		self.descriptionText = "";
		self.noDataTextDescription = "You need to provide data for the chart.";
		
		self.drawGridBackgroundEnabled = false;
		self.dragEnabled = false;
		self.setScaleEnabled(false)
		self.legend.enabled = false;
		self.pinchZoomEnabled = false;
		
		let xAxis : ChartXAxis = self.xAxis;
		xAxis.labelFont = UIFont.systemFontOfSize(10);
		xAxis.labelTextColor = UIColor.whiteColor();
		xAxis.drawGridLinesEnabled = false;
		xAxis.drawAxisLineEnabled = true;
		xAxis.labelPosition = .Bottom
		
		let leftAxis : ChartYAxis  = self.leftAxis;
		leftAxis.labelTextColor = UIColor.clearColor()
		leftAxis.axisMaxValue = 200.0;
		leftAxis.axisMinValue = 0.0;
		leftAxis.drawGridLinesEnabled = true;
		leftAxis.drawZeroLineEnabled = false;
		leftAxis.granularityEnabled = true;
		leftAxis.labelPosition = .OutsideChart
		leftAxis.labelCount = 2
		leftAxis.stretchOffsetValue = 0
		leftAxis.labelTextColor = UIColor.clearColor()
		leftAxis.gridColor = UIColor.clearColor()
		leftAxis.axisLineColor = UIColor.clearColor()
		let rightAxis : ChartYAxis  = self.rightAxis;
		rightAxis.enabled = false
		self.animate(yAxisDuration: 0.5)
	}
	
	class func setupDate(dateRange: DateRange) -> LineChartData {
		
		let today         = NSDate()
		var xVals : [String] = [String]()
		var yVals : [ChartDataEntry] = [ChartDataEntry]()
		var totalDay = 6
		
		let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
		let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: today)
		
		if dateRange == .Day {
			
			
			
			for index in 0...24 {
				
				components.hour   = index
				components.minute = 0
				components.second = 0
				let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
				
				let df        = NSDateFormatter()
				
				df.dateFormat = "h"
				
				let category  = df.stringFromDate(timestamp)
				
				let yValue : Double = 0
				
				yVals.append(BarChartDataEntry(value: yValue, xIndex:index))
				
				xVals.append(("\(category)"))
			}
		} else if dateRange == .Week {
			
			
			for index in 0...totalDay{
				
				let df        = NSDateFormatter()
				let xValue    = today.dateAtStartOfDay().dateBySubtractingDays(totalDay - index)!
				
				if index == 0 {
					df.dateFormat = "MMM d"
				} else {
					df.dateFormat = "d"
				}
				
				let category  = df.stringFromDate(xValue)
				
				let yValue : Double = 0
				
				yVals.append(BarChartDataEntry(value: yValue, xIndex:index))
				
				xVals.append(("\(category)"))
			}
			
			
		} else if dateRange == .Month {
			
			totalDay = Common.getDays(today)
			
			for index in 1...totalDay {
				components.day = index
				components.hour   = 0
				components.minute = 0
				components.second = 0
				
				let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
				
				let df        = NSDateFormatter()
				
				if index == 1 {
					df.dateFormat = "MMM d"
				} else {
					df.dateFormat = "d"
				}
				
				let category  = df.stringFromDate(timestamp)
				
				let yValue : Double = 0
				
				yVals.append(BarChartDataEntry(value: yValue, xIndex:index))
				
				xVals.append(("\(category)"))
			}
			
		}
		var set1 :LineChartDataSet
		set1 = LineChartDataSet(yVals: yVals, label: "DataSet")
		set1.drawValuesEnabled = false;
		
		set1.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
		
		var dataSets = [IChartDataSet]()
		dataSets.append(set1)
		let data = LineChartData(xVals: xVals, dataSets: dataSets)
		return data
	}
}