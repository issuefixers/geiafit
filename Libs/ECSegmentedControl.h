#import <UIKit/UIKit.h>

@class ECSegmentedControl;
@protocol ECSegmentedControlDelegate <NSObject>

@optional
- (void)selectedSegmentChanged:(id)sender withIndex:(NSUInteger)index;
@end

@interface ECSegmentedControlSegment : NSObject {
    UIImage *backgroundImage;
    UIImage *highlightedImage;
    NSString *textValue;
    NSUInteger size; // width or height depending on orientation (horizontal/vertical)
                     // size usually implies 2-d but i can't think of a better generic word :p
}

@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, strong) UIImage *highlightedImage;
@property (nonatomic, copy) NSString *textValue;
@property (nonatomic, assign) NSUInteger size; // height or width depending on orientation (horizontal/vertical)

@end

@interface ECSegmentedControl : UIView {
    NSMutableArray *buttons;
    id<ECSegmentedControlDelegate> __weak delegate;
    NSUInteger selectedIndex;
}
@property (nonatomic, weak) IBOutlet id<ECSegmentedControlDelegate> delegate;


- (void)setupWithSegmentArray:(NSArray *)segments
              selectedSegment:(NSUInteger)selected
                  controlSize:(NSUInteger)controlSize
                 dividerImage:(UIImage *)divider
          verticalOrientation:(BOOL)isVertical;

- (void)setupWithSegmentArray:(NSArray *)segments
              selectedSegment:(NSUInteger)selected
                  controlSize:(NSUInteger)controlSize
                 dividerImage:(UIImage *)divider;

- (void)setButtonSelected:(UIButton*)selectedButton;
- (void)selectButtonAtIndex:(NSUInteger)selectedIndex;
- (void)setButtonAtIndex:(NSUInteger)index enabled:(BOOL)enabled;
- (void)clearSelectedButton;

- (NSUInteger)indexOfSelectedButton;
@end