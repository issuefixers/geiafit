//
//  Common.swift
//  Geia
//
//  Created by haley on 6/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

class Common: NSObject {
	
	class func getTextRectSize(text:NSString,font:UIFont,size:CGSize) -> CGRect {
		let attributes = [NSFontAttributeName: font]
		let option = NSStringDrawingOptions.UsesLineFragmentOrigin
		let rect:CGRect = text.boundingRectWithSize(size, options: option, attributes: attributes, context: nil)
		return rect;
	}
	
	class func stretchableImageWithLeftCapWidth(leftCapWidth: Int, topCapHeight: Int, image:UIImage) -> UIImage{
		
		let textbgimg = image.stretchableImageWithLeftCapWidth(leftCapWidth, topCapHeight: topCapHeight)
		return textbgimg
	}
	
	class func getDays(date:NSDate)-> (Int) {
	
		let calender : NSCalendar = NSCalendar.currentCalendar()
		
		let range : NSRange = calender.rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: date)
		
		//		let day : NSInteger = dateComponents!.day;
		return range.length
		
	}
	class func getFirstAndLastDateOfMonth(date: NSDate) -> (fistDateOfMonth: NSDate, lastDateOfMonth: NSDate) {
		
		let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
		
		
		let calendarUnits = [NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day,NSCalendarUnit.Hour,NSCalendarUnit.Minute,NSCalendarUnit.Second]as NSCalendarUnit
		
		let dateComponents = calendar?.components(calendarUnits, fromDate: date)
		let calender : NSCalendar = NSCalendar.currentCalendar()
	
		dateComponents?.day = Common.getDays(date)
		
		let lastDateOfMonth : NSDate = calender.dateFromComponents(dateComponents!)!.dateAtStartOfDay()
		
		let fistDateOfMonth = self.returnedDateForMonth(dateComponents!.month, year: dateComponents!.year, day: 1)

		
		return (fistDateOfMonth!,lastDateOfMonth)
	}
	
	
	class func earlyDays(smallDay:NSDate) -> Int {
		
		
		let D_DAY = 86400
		
		let interval: NSTimeInterval = NSDate().timeIntervalSinceDate(smallDay)
		let days = (Int(interval)) / D_DAY
		
		return days
	
	}
	class func returnedDateForMonth(month: NSInteger, year: NSInteger, day: NSInteger) -> NSDate? {
		let components = NSDateComponents()
		components.day = day
		components.month = month
		components.year = year
		
		let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
		
		return gregorian!.dateFromComponents(components)
	}
	
	class func getNextMonth(date:NSDate) -> NSDate {
	
		
		let calendar = NSCalendar.currentCalendar()
		
		let calendarUnits = [NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day,NSCalendarUnit.Hour,NSCalendarUnit.Minute,NSCalendarUnit.Second]as NSCalendarUnit
		
		let dateComponents = calendar.components(calendarUnits, fromDate: date)

		dateComponents.month = dateComponents.month + 1
		dateComponents.day = 1
		let today = calendar.dateFromComponents(dateComponents)
		let lastday = Common.getFirstAndLastDateOfMonth(today!).lastDateOfMonth
		return lastday
	}
	
	
	class func getPreviousMonth(date:NSDate) -> NSDate {
		
		let calendar = NSCalendar.currentCalendar()
		
		let calendarUnits = [NSCalendarUnit.Year,NSCalendarUnit.Month,NSCalendarUnit.Day,NSCalendarUnit.Hour,NSCalendarUnit.Minute,NSCalendarUnit.Second]as NSCalendarUnit
		
		let dateComponents = calendar.components(calendarUnits, fromDate: date)
		if (dateComponents.month <= 1)
		{
			dateComponents.year = dateComponents.year - 1;
			dateComponents.month = 12;
		}
		else
		{
			dateComponents.month = dateComponents.month - 1
		}
		
		dateComponents.day = 1
		let today = calendar.dateFromComponents(dateComponents)
		
		let lastday = Common.getFirstAndLastDateOfMonth(today!).lastDateOfMonth
		return lastday
	}
	
	class func fixOrientation(originalImage:UIImage) -> UIImage {
		if (originalImage.imageOrientation == .Up)
		{
			return originalImage
		}
		var transform = CGAffineTransformIdentity
		switch (originalImage.imageOrientation) {
		case .Down, .DownMirrored:
			transform = CGAffineTransformTranslate(transform, originalImage.size.width, originalImage.size.height)
			transform = CGAffineTransformRotate(transform, CGFloat(M_PI))
			break
		case .Left, .LeftMirrored:
			transform = CGAffineTransformTranslate(transform, originalImage.size.width, 0)
			transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2))
			break
		case .Right, .RightMirrored:
			transform = CGAffineTransformTranslate(transform, 0, originalImage.size.height)
			transform = CGAffineTransformRotate(transform, CGFloat(-M_PI_2))
			break
		default:
			break
		}
		switch (originalImage.imageOrientation) {
		case .UpMirrored, .DownMirrored:
			transform = CGAffineTransformTranslate(transform, originalImage.size.width, 0)
			transform = CGAffineTransformScale(transform, -1, 1)
			break
		case .LeftMirrored, .RightMirrored:
			transform = CGAffineTransformTranslate(transform, originalImage.size.height, 0)
			transform = CGAffineTransformScale(transform, -1, 1)
			break
		default:
			break
		}
		let ctx = CGBitmapContextCreate(nil, Int(originalImage.size.width),
		                                Int(originalImage.size.height),    CGImageGetBitsPerComponent(originalImage.CGImage!), 0,    CGImageGetColorSpace(originalImage.CGImage!)!,    CGImageGetBitmapInfo(originalImage.CGImage!).rawValue)
		CGContextConcatCTM(ctx!, transform)
		switch (originalImage.imageOrientation) {
		case .Left,
		     .LeftMirrored,
		     .Right,
		     .RightMirrored:
			CGContextDrawImage(ctx!, CGRectMake(0,0,originalImage.size.height,originalImage.size.width), originalImage.CGImage!)
			break
		default:
			CGContextDrawImage(ctx!, CGRectMake(0,0,originalImage.size.width,originalImage.size.height), originalImage.CGImage!)
			break
		}
		
		let cgimg = CGBitmapContextCreateImage(ctx!)
		return UIImage(CGImage: cgimg!)
		
    }
    
    class func getStrFromProgress(progress: Double) -> String {
    
        if (progress == 0 || progress*100 < 0.01) {
            return "0%"
        }
        
        var centerText: String
        if (progress*100 > 1.0 || progress*100 == 1.0) {
            centerText = "\(Int(progress*100))%"
        } else {
            let pro = Double(String(format: "%.2f", progress*100))
            centerText = "\(String(format: "%g", pro!))%"
        }
        
        return centerText
    }
    
    class func isLargerThanToday(day: NSDate) -> Bool {
    
        let components = NSDateComponents()
        components.day = 1
        components.second = -1
    
        let calendar = NSCalendar.currentCalendar()
        let startOfDay = calendar.startOfDayForDate(NSDate())
        let endOfDay = calendar.dateByAddingComponents(components, toDate: startOfDay, options: NSCalendarOptions())
        
        if day.compare(endOfDay!) == .OrderedAscending {
           return false
        }
        
        return true
    }
    
    class func isEqualThisMonth(day: NSDate) -> Bool {
       
        let calendar = NSCalendar.currentCalendar()
        let calendarUnits = [.Year, .Month, .Day] as NSCalendarUnit
        let currentComponents = calendar.components(calendarUnits, fromDate: day)
        let todayComponents = calendar.components(calendarUnits, fromDate: NSDate())
        
        if (currentComponents.year == todayComponents.year) && (currentComponents.month == todayComponents.month) {
            return true
        }
        
        return false
    }
    
    class func isLargerThanMonth(day: NSDate) -> Bool {
        
        let calendar = NSCalendar.currentCalendar()
        let calendarUnits = [.Year, .Month, .Day] as NSCalendarUnit
        let currentComponents = calendar.components(calendarUnits, fromDate: day)
        let todayComponents = calendar.components(calendarUnits, fromDate: NSDate())
        
        if (currentComponents.year > todayComponents.year)  {
            return true
        } else {
            if currentComponents.year == todayComponents.year && currentComponents.month > todayComponents.month {
                return true
            }
        }
        
        return false
    }
    
    class func isEarlyThisMonth(day: NSDate) -> Bool {
        
        let calendar = NSCalendar.currentCalendar()
        let calendarUnits = [.Year, .Month, .Day] as NSCalendarUnit
        let currentComponents = calendar.components(calendarUnits, fromDate: day)
        let todayComponents = calendar.components(calendarUnits, fromDate: NSDate())
        
        if (currentComponents.year < todayComponents.year) {
            return true
        }

        if (currentComponents.month < todayComponents.month) && (currentComponents.year == todayComponents.year) {
            return true
        }
        
        return false
    }
    
    class func dateAtStartOfHour(date: NSDate) -> NSDate {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: date)
        components.minute = 0
        components.second = 0
        
        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }
    
    class func isBetweenDate(date: NSDate, beginDate: NSDate) -> Bool {
    
        if (date.compare(beginDate) == .OrderedAscending) {
            return false
        }
        
        let endDate = beginDate.dateByAddingMinutes(10)
        if (date.compare(endDate!) == .OrderedDescending) {
            return false
        }
        
        return true
    }
    
    class func dateByAddingSeconds(date: NSDate, sec: Int) -> NSDate {
        let dateComponents    = NSDateComponents()
        dateComponents.second = sec
        let newDate: NSDate   = NSCalendar.currentCalendar().dateByAddingComponents(dateComponents, toDate: date, options: NSCalendarOptions(rawValue: 0))!
        
        return newDate
    }
    
    class func dateToGMT(date: NSDate) -> NSDate {
    
        let zone = NSTimeZone.systemTimeZone()
        let interval = zone.secondsFromGMTForDate(date)
        
        return date.dateByAddingTimeInterval(Double(interval))
    }
    
}
