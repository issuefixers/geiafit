//
//  CircleView.h
//  testSwift1
//
//  Created by Cassie on 6/7/16.
//  Copyright © 2016 Objectiva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleView : UIView

@property (nonatomic, strong) UIColor *aboveColor;
@property (nonatomic, strong) UIColor *underlineColor; //default is 0.2*aboveColor.alpha
@property (nonatomic, strong) NSString *centerText;
@property (nonatomic) BOOL isAttributed;
@property (nonatomic) double progress;

- (void)updateChartView;
- (void)resetView;

@end
