//
//  ThreeCircleView.h
//  testSwift
//
//  Created by Cassie on 6/6/16.
//  Copyright © 2016 Objectiva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThreeCircleView : UIView

- (void)updateChartData:(NSArray *)rangeArr;

@end
