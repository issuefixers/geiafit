//
//  CircleView.m
//  testSwift1
//
//  Created by Cassie on 6/7/16.
//  Copyright © 2016 Objectiva. All rights reserved.
//

#import "CircleView.h"
#import <Charts/Charts.h>
#import "GeiaFit-Swift.h"

@interface CircleView ()

@property (nonatomic, strong) PieChartView *chartView;

@end

@implementation CircleView

- (void)awakeFromNib {

    [self initData];
    [self setupPieChartView:self.chartView];
    [self addSubview:self.chartView];
    
    [self updateChartView];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}

- (void)initData {
    
    self.backgroundColor = [UIColor clearColor];
    self.chartView = [[PieChartView alloc] initWithFrame: CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    self.aboveColor = [UIColor colorWithRed:0 green:163/255.0 blue:222/255.0 alpha:1.0];
    self.underlineColor = [UIColor colorWithRed:0 green:163/255.0 blue:222/255.0 alpha:0.2];
    self.isAttributed = NO;
    self.centerText = @"0%";
    self.progress = 0.0;
    
}

- (void)resetView {
    self.aboveColor = [UIColor colorWithRed:0 green:163/255.0 blue:222/255.0 alpha:1.0];
    self.centerText = @"0%";
    self.progress = 0.0;
    [self updateChartView];
}

- (void)setupPieChartView:(PieChartView *)chartView
{

    chartView.drawCenterTextEnabled = YES;
    chartView.holeColor = [UIColor colorWithRed:32/255.0 green:35/255.0 blue:37/255.0 alpha:1.0];
    chartView.holeRadiusPercent = 0.85;
    chartView.transparentCircleColor = [UIColor clearColor];
    chartView.descriptionText = @"";
    chartView.drawHoleEnabled = YES;
    chartView.rotationAngle = 270.0;
    chartView.rotationEnabled = NO;
    chartView.highlightPerTapEnabled = NO;
    chartView.legend.enabled = NO;
    
}

- (void)updateChartView {
    
    if (self.chartView.data != nil) {
      self.chartView.data = nil;
    }
    NSMutableAttributedString *centerText = [[NSMutableAttributedString alloc] initWithString:self.centerText];
    
    if (self.isAttributed) {

        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"\n"];
        NSRange range = [self.centerText rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            NSLog(@"Can't find Enter symbol");
            return;
        }
        
        NSRange preRange = NSMakeRange(0, range.location);
        NSRange laRange =  NSMakeRange(range.location + 1, centerText.length - range.location - 1);
        [centerText setAttributes:@{
                                    NSFontAttributeName: [UIFont systemFontOfSize:18.0],
                                    NSForegroundColorAttributeName: self.aboveColor
                                    } range:preRange];
        [centerText addAttributes:@{
                                    NSFontAttributeName: [UIFont systemFontOfSize:13.0],
                                    NSForegroundColorAttributeName: [UIColor colorWithRed:139/255.0 green:139/255.0 blue:139/255.0 alpha:1.0]
                                    } range:laRange];
        NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.alignment = NSTextAlignmentCenter;
        [centerText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:preRange];
        [centerText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:laRange];
        
    } else {
        [centerText addAttributes:@{
                                    NSFontAttributeName: [UIFont systemFontOfSize:18.0],
                                    NSForegroundColorAttributeName: self.aboveColor
                                    } range:NSMakeRange(0, centerText.length)];
        NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.alignment = NSTextAlignmentCenter;
        [centerText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, centerText.length)];
    }
    
    self.chartView.centerAttributedText = centerText;
    [self setData:self.progress];
    
    [self.chartView animateWithXAxisDuration:0.5 easingOption:ChartEasingOptionLinear];
}

- (void)setData:(double)range
{
    double newRange = range;
    if (range > 1.0) {
        newRange = 1.0;
    } else if (range < 0) {
        newRange = 0;
    }
    
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    [yVals addObject:[[BarChartDataEntry alloc] initWithValue:newRange xIndex:0]];
    [yVals addObject:[[BarChartDataEntry alloc] initWithValue:1-newRange xIndex:1]];
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals label:@""];
    dataSet.valueTextColor = [UIColor clearColor];
    
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    [colors addObject:self.aboveColor];
    [colors addObject:self.underlineColor];
    
    dataSet.colors = colors;
    PieChartData *data = [[PieChartData alloc] initWithXVals:[NSMutableArray arrayWithObjects:@"",@"", nil] dataSet:dataSet];
    self.chartView.data = data;
    
}

@end
