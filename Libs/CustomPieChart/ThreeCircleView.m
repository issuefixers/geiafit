//
//  ThreeCircleView.m
//  testSwift
//
//  Created by Cassie on 6/6/16.
//  Copyright © 2016 Objectiva. All rights reserved.
//

#import "ThreeCircleView.h"
#import <Charts/Charts.h>
#import "GeiaFit-Swift.h"

@interface ThreeCircleView ()
    
    @property (nonatomic, strong) IBOutlet PieChartView *chartView1;
    @property (nonatomic, strong) IBOutlet PieChartView *chartView2;
    @property (nonatomic, strong) IBOutlet PieChartView *chartView3;
    
    @end

@implementation ThreeCircleView
    
- (void)awakeFromNib {
    
    [self setupPieChartView:self.chartView1];
    [self setupPieChartView:self.chartView2];
    [self setupPieChartView:self.chartView3];
    
    [self updateChartData:[NSArray arrayWithObjects:@"0.0",@"0.0",@"0.0", nil]];
}
    
- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
}
    
- (void)setupPieChartView:(PieChartView *)chartView
    {
        
        chartView.holeColor = [UIColor colorWithRed:32/255.0 green:35/255.0 blue:37/255.0 alpha:1.0];
        chartView.holeRadiusPercent = 0.5;
        chartView.transparentCircleColor = [UIColor clearColor];
        chartView.descriptionText = @"";
        chartView.drawCenterTextEnabled = NO;
        chartView.drawHoleEnabled = YES;
        chartView.rotationAngle = 270.0;
        chartView.rotationEnabled = NO;
        chartView.highlightPerTapEnabled = NO;
        chartView.legend.enabled = NO;
        
    }
    
    
- (void)updateChartData:(NSArray *)rangeArr
    {
        [self setChartData:rangeArr];
    }
    
- (void)setChartData:(NSArray *)rangeArr
    {
        double range1 = [rangeArr[0] doubleValue];
        double range2 = [rangeArr[1] doubleValue];
        double range3 = [rangeArr[2] doubleValue];
        
        double newRange1 = range1;
        if (range1 > 1.0) {
            newRange1 = 1.0;
        } else if (range1 < 0) {
            newRange1 = 0;
        }
        
        double newRange2 = range2;
        if (range2 > 1.0) {
            newRange2 = 1.0;
        } else if (range2 < 0) {
            newRange2 = 0;
        }
        
        double newRange3 = range3;
        if (range3 > 1.0) {
            newRange3 = 1.0;
        } else if (range3 < 0) {
            newRange3 = 0;
        }
        
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:newRange1 xIndex:0]];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:1-newRange1 xIndex:1]];
        
        NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
        [yVals2 addObject:[[BarChartDataEntry alloc] initWithValue:newRange2 xIndex:0]];
        [yVals2 addObject:[[BarChartDataEntry alloc] initWithValue:1-newRange2 xIndex:1]];
        
        NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
        [yVals3 addObject:[[BarChartDataEntry alloc] initWithValue:newRange3 xIndex:0]];
        [yVals3 addObject:[[BarChartDataEntry alloc] initWithValue:1-newRange3 xIndex:1]];
        
        PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals1 label:@""];
        dataSet.valueTextColor = [UIColor clearColor];
        
        PieChartDataSet *dataSet1 = [[PieChartDataSet alloc] initWithYVals:yVals2 label:@""];
        dataSet1.valueTextColor = [UIColor clearColor];
        
        PieChartDataSet *dataSet2 = [[PieChartDataSet alloc] initWithYVals:yVals3 label:@""];
        dataSet2.valueTextColor = [UIColor clearColor];
        
        NSMutableArray *colors = [[NSMutableArray alloc] init];
        [colors addObject:[UIColor colorWithRed:66/255.f green:153/255.f blue:209/255.f alpha:1.f]];
        //[colors addObject:[UIColor colorWithRed:58/255.f green:140/255.f blue:198/255.f alpha:1.f]];
        [colors addObject:[UIColor colorWithRed:34/255.f green:51/255.f blue:68/255.f alpha:1.f]];
        
        
        NSMutableArray *colors1 = [[NSMutableArray alloc] init];
        [colors1 addObject:[UIColor colorWithRed:0/255.f green:203/255.f blue:239/255.f alpha:1.f]];
        //[colors1 addObject:[UIColor colorWithRed:0 green:191/255.f blue:234/255.f alpha:1.f]];
        [colors1 addObject:[UIColor colorWithRed:24/255.f green:58/255.f blue:71/255.f alpha:1.f]];
        
        NSMutableArray *colors2 = [[NSMutableArray alloc] init];
        [colors2 addObject:[UIColor colorWithRed: 221/255.f green: 246/255.f blue: 188/255.f alpha: 1.f]]; /* #ddf6bc data*/
        [colors2 addObject:[UIColor colorWithRed:179/255.f green:231/255.f blue:1.f alpha:1.f]]; /* #ddf6bc background*/
        //[colors2 addObject:[UIColor colorWithRed:62/255.f green:74/255.f blue:81/255.f alpha:1.f]];
        
        
        dataSet.colors = colors2;
        dataSet1.colors = colors1;
        dataSet2.colors = colors;
        
        NSMutableArray *xVals = [NSMutableArray arrayWithObjects:@"",@"", nil];
        PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
        PieChartData *data1 = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet1];
        PieChartData *data2 = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet2];
        
        //self.chartView1.data = data;
        //self.chartView2.data = data1;
        //self.chartView3.data = data2;
        
        //switch circles per objectiva
        self.chartView1.data = data2;
        self.chartView2.data = data1;
        self.chartView3.data = data;
        
    }
    
    @end
