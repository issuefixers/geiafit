


#import "ECSegmentedControl.h"

@implementation ECSegmentedControlSegment
@synthesize backgroundImage;
@synthesize highlightedImage;
@synthesize textValue;
@synthesize size;


@end

@implementation ECSegmentedControl
@synthesize delegate;

- (void)setupWithSegmentArray:(NSArray *)segments
              selectedSegment:(NSUInteger)selected
                  controlSize:(NSUInteger)controlSize
                 dividerImage:(UIImage *)divider
          verticalOrientation:(BOOL)isVertical {
    // clear up old buttons
    buttons = [[NSMutableArray alloc] init];
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    selectedIndex = selected;
    
    NSUInteger offset = 0;
    // create buttons for all the segments
    for (NSUInteger i = 0; i < [segments count]; i++) {
        ECSegmentedControlSegment *segment = [segments objectAtIndex:i];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (isVertical) {
            [button setFrame:CGRectMake(0, offset, controlSize, segment.size)];
            offset += segment.size;
        }
        else {
            [button setFrame:CGRectMake(offset, 0, segment.size, controlSize)];
            offset += segment.size;
        }
        
        if (segment.textValue) {
            [button setTitle:segment.textValue forState:UIControlStateNormal];
//            if (labelStyle) {
//                [button.titleLabel applyStyle:labelStyle];
//            }
        }
        
        if (segment.backgroundImage) {
            [button setBackgroundImage:segment.backgroundImage forState:UIControlStateNormal];
        }
        
        if (segment.highlightedImage) {
            [button setBackgroundImage:segment.highlightedImage forState:UIControlStateHighlighted];
            [button setBackgroundImage:segment.highlightedImage forState:UIControlStateSelected];
        }
        
        button.adjustsImageWhenHighlighted = NO;
        
        if (i == selectedIndex) {
            button.selected = YES;
        }
        
        // grab all the events to respond correctly
        [button addTarget:self action:@selector(touchUpInsideAction:) forControlEvents:UIControlEventTouchDown];
        [button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchDragOutside];
        [button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchDragInside];
        
        [self addSubview:button];
        [buttons addObject:button];
        
        // add the divider image if we're not at the last button
        if (divider && i != ([segments count] - 1)) {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:divider];
            
            if (isVertical) {
                imageView.frame = CGRectMake(0, offset, divider.size.width, divider.size.height);
                offset += divider.size.height;
            }
            else {
                imageView.frame = CGRectMake(offset, 0, divider.size.width, divider.size.height);
                offset += divider.size.width;
            }
            
            [self addSubview:imageView];
        }
    }
    
}

- (void)setupWithSegmentArray:(NSArray *)segments
              selectedSegment:(NSUInteger)selected
                  controlSize:(NSUInteger)controlSize
                 dividerImage:(UIImage *)divider {
    [self setupWithSegmentArray:segments selectedSegment:selected controlSize:controlSize dividerImage:divider verticalOrientation:NO];
}

- (void)setButtonSelected:(UIButton*)selectedButton {
    for (UIButton* button in buttons) {
        if (button == selectedButton) {
            button.selected = YES;
            button.highlighted = NO;
        } else {
            button.selected = NO;
            button.highlighted = NO;
        }
    }
}

- (void)selectButtonAtIndex:(NSUInteger)index {
    selectedIndex = index;
    UIButton *btn = [buttons objectAtIndex:selectedIndex];
    if (btn.enabled) {
        [self setButtonSelected:btn];
    }
}

- (void)setButtonAtIndex:(NSUInteger)index enabled:(BOOL)enabled {
    [(UIButton *)[buttons objectAtIndex:index] setEnabled:enabled];
}

- (NSUInteger)indexOfSelectedButton {
    return selectedIndex;
}

- (void)touchUpInsideAction:(UIButton *)button {
    [self setButtonSelected:button];
    
    NSUInteger newIndex = [buttons indexOfObject:button];
    if (selectedIndex != newIndex) {
        selectedIndex = newIndex;
        
        if (delegate && [delegate respondsToSelector:@selector(selectedSegmentChanged:withIndex:)]) {
            /*
             change the first parameter from button to self, we can get the button from the index
             in case we use more than one segmentedControl in one page, we need distinguish them
             */
            [delegate selectedSegmentChanged:self withIndex:selectedIndex];
        }
    }
}


- (void)otherTouchesAction:(UIButton *)button {
    [self setButtonSelected:button];
}

- (void)clearSelectedButton {
    for (UIButton* button in buttons) {
        button.selected = NO;
        button.highlighted = NO;
    }
    selectedIndex = NSUIntegerMax;
}



@end