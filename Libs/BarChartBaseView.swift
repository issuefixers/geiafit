//
//  BarChartBaseView.swift
//  Geia
//
//  Created by haley on 6/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts

class BarChartBaseView: BarChartView {

	func setupBarChartView() {
		self.drawBarShadowEnabled = false
		self.drawValueAboveBarEnabled = false
        self.highlightPerTapEnabled = false
        self.doubleTapToZoomEnabled = false
        self.userInteractionEnabled = false
        
		self.legend.enabled = false
		self.descriptionText = ""
		self.maxVisibleValueCount = 750;
		self.noDataText = ""
		self.noDataTextDescription = ""
		let xAxis : ChartXAxis = self.xAxis;
		xAxis.labelPosition = ChartXAxis.LabelPosition.Bottom;
		
		xAxis.labelFont = UIFont.systemFontOfSize(10);
		xAxis.labelTextColor = UIColor.whiteColor()
		xAxis.drawGridLinesEnabled = false
		xAxis.spaceBetweenLabels = 2;
		
		let leftAxis : ChartYAxis = self.leftAxis;
		leftAxis.labelFont = UIFont.systemFontOfSize(10);
		leftAxis.labelCount = 3;
		leftAxis.labelTextColor = UIColor.whiteColor()
		leftAxis.valueFormatter = NSNumberFormatter.init();
		leftAxis.valueFormatter!.maximumFractionDigits = 1;
		leftAxis.axisLineColor = UIColor.clearColor()
		leftAxis.labelPosition = ChartYAxis.LabelPosition.OutsideChart;
		leftAxis.spaceTop = 0.25;
		leftAxis.axisMinValue = 0.0; // this replaces startAtZero = YES
		
		let rightAxis : ChartYAxis = self.rightAxis;
		rightAxis.enabled = false;
		
		self.animate(yAxisDuration:1.0)
	}
	
	class func setupDataByDate(date:NSDate, dateRange: DateRange) -> BarChartData {
		
		
		let today         = date
		var xVals : [String] = [String]()
		var yVals : [ChartDataEntry] = [ChartDataEntry]()
		var totalDay = 6
		let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
		let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: today)
		if dateRange == .Day {
			
			for index in 0...24 {
				
				components.hour   = index
				components.minute = 0
				components.second = 0
				let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
				
				let df        = NSDateFormatter()

					df.dateFormat = "h"
				
				let category  = df.stringFromDate(timestamp)
				
				let yValue : Double = 0
				
				yVals.append(BarChartDataEntry(value: yValue, xIndex:index))
				
				xVals.append(("\(category)"))
			}
			} else if dateRange == .Week {
			
			
			for index in 0...totalDay{
				
				let df        = NSDateFormatter()
				let xValue    = today.dateAtStartOfDay().dateBySubtractingDays(totalDay - index)!
				
				if index == 0 {
					df.dateFormat = "MMM d"
				} else {
					df.dateFormat = "d"
				}
				
				let category  = df.stringFromDate(xValue)
				
				let yValue : Double = 0
				
				yVals.append(BarChartDataEntry(value: yValue, xIndex:index))
				
				xVals.append(("\(category)"))
			}
		
		
		} else if dateRange == .Month {
			
				totalDay = Common.getDays(today)
			
			for index in 1...totalDay {
				
				components.day = index
				components.hour   = 0
				components.minute = 0
				components.second = 0
				
				let timestamp = NSCalendar.currentCalendar().dateFromComponents(components)!
				
				let df        = NSDateFormatter()
				
				if index == 1 {
					df.dateFormat = "MMM d"
				} else {
					df.dateFormat = "d"
				}
				
				let category  = df.stringFromDate(timestamp)
				
				let yValue : Double = 0
				
				yVals.append(BarChartDataEntry(value: yValue, xIndex:index))
				
				xVals.append(("\(category)"))
			}
		
		}
		
		
		
		var set1 :BarChartDataSet
		set1 = BarChartDataSet(yVals: yVals, label: "DataSet")
		set1.drawValuesEnabled = false;
		set1.barShadowColor = NSUIColor(red:74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 0.5)
		set1.colors = [UIColor(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
		
		var dataSets = [IChartDataSet]()
		dataSets.append(set1)
		let data = BarChartData(xVals: xVals, dataSets: dataSets)
		return data
	}
	
    func setupActDayBarChartView() {
		
        self.setupBarChartView()
		
        self.xAxis.setLabelsToSkip(20)
        //self.leftAxis.valueFormatter?.positiveSuffix = " min";
		
    }
	
    func setupActWeekBarChartView() {
		
        self.setupBarChartView()
		
        self.leftAxis.axisLineColor = UIColor.clearColor()
        self.leftAxis.gridColor = UIColor.clearColor()
        self.leftAxis.labelTextColor = UIColor.clearColor()
        self.leftAxis.valueFormatter?.positiveSuffix = " min";
        
    }
    
    func setupActMonthBarChartView() {
        
        self.setupActWeekBarChartView()
        self.xAxis.axisLineColor = UIColor.whiteColor()
        self.xAxis.setLabelsToSkip(13)
        
    }

}

class PieChartBaseView: PieChartView {

    var aboveColor: UIColor = UIColor(red:0, green: 163.0/255.0, blue: 222.0/255.0, alpha: 1.0)
    var underlineColor: UIColor = UIColor(red:0, green: 163.0/255.0, blue: 222.0/255.0, alpha: 0.2)  //default is 0.2*aboveColor.alpha
    var isAttributed: Bool = false
    var centerShowText: String = "0%"
    var progress: Double = 0.0
    
    func resetView() {
        
        self.aboveColor = UIColor(red:0, green: 163.0/255.0, blue: 222.0/255.0, alpha: 1.0)
        self.centerShowText = "0%"
        self.progress = 0.0
        self.updateChartView()
        
    }
    
    func setupPieChartView() {
    
        self.drawCenterTextEnabled = true
        self.holeColor = UIColor(red:32.0/255.0, green: 35.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        self.holeRadiusPercent = 0.85
        self.transparentCircleColor = UIColor.clearColor()
        self.descriptionText = "";
        self.drawHoleEnabled = true
        self.rotationAngle = 270.0
        self.rotationEnabled = false
        self.highlightPerTapEnabled = false
        self.legend.enabled = false
    
    }
    
    func updateChartView() {
    
        if (self.data != nil) {
            self.data = nil;
        }
        
        let cText = NSMutableAttributedString(string: self.centerShowText)
        let text = self.centerShowText as NSString
        
        let paragraphStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.alignment = .Center
        
        if self.isAttributed {
            
            let cSet = NSCharacterSet(charactersInString: "\n")
            let range: NSRange = text.rangeOfCharacterFromSet(cSet)
            if range.location == NSNotFound {
                print("Can't find Enter symbol")
                return
            }

            let preRange = NSMakeRange(0, range.location)
            let laRange = NSMakeRange(range.location + 1, text.length - range.location - 1)
            cText.setAttributes([NSFontAttributeName : UIFont.systemFontOfSize(18.0), NSForegroundColorAttributeName : self.aboveColor], range: preRange)
            cText.setAttributes([NSFontAttributeName : UIFont.systemFontOfSize(13.0), NSForegroundColorAttributeName : UIColor(red:139.0/255.0, green: 139.0/255.0, blue: 139.0/255.0, alpha: 1.0)], range: laRange)

            cText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: preRange)
            cText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: laRange)
        } else {
            cText.setAttributes([NSFontAttributeName : UIFont.systemFontOfSize(18.0), NSForegroundColorAttributeName : self.aboveColor], range: NSMakeRange(0, text.length))
            cText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, text.length))
        }
    
        self.centerAttributedText = cText
        self.setData(self.progress)
    
        self.animate(xAxisDuration: 0.5, easingOption: .Linear)
    }
    
    func setData(range: Double) {
      
        var newRange: Double = range
        
        if range > 1.0 {
           newRange = 1.0
        } else if range < 0 {
           newRange = 0
        }
    
        var yVals = [BarChartDataEntry]()
        yVals.append(BarChartDataEntry(value: newRange, xIndex: 0))
        yVals.append(BarChartDataEntry(value: 1-newRange, xIndex: 1))
        
        let dataSet = PieChartDataSet(yVals: yVals, label: "")
        dataSet.valueTextColor = UIColor.clearColor()
        dataSet.colors = [self.aboveColor, self.underlineColor]
        
        let data = PieChartData(xVals: ["",""], dataSet: dataSet)
        self.data = data
    
    }
    
}
