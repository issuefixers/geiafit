//
//  ExerciseDetailViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 5/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import BIZPopupView
import AVKit
import AVFoundation

class ExerciseDetailViewController: GeiaViewController {
    var exertData:Exercise?

    @IBOutlet weak var imgExe2: UIImageView!
    @IBOutlet weak var imgExe1: UIImageView!
    @IBOutlet weak var btnIDidIt: UIButton!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var imgPlayVideo: UIImageView!
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblFrec: UILabel!
    @IBOutlet weak var lblNumRep: UILabel!
    @IBOutlet weak var lbltimesPerDay: UILabel!
    @IBOutlet weak var lblSets: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        

//        self.hidesBottomBarWhenPushed = true;
      

        self.title = "Exercise Details"
        if exertData != nil {
            self.imgExe1.sd_setImageWithURL(NSURL(string: exertData!.thumbUrl1!));
            
            if (exertData!.thumbUrl2 != nil) {
                self.imgExe2.sd_setImageWithURL(NSURL(string: exertData!.thumbUrl2!));
                self.imgPlayVideo.sd_setImageWithURL(NSURL(string: exertData!.thumbUrl2!));
            }
            
            self.lblTitle.text = exertData!.title;
            self.lblFrec.text = exertData!.rest;
            self.lblContent.text = exertData!.comment;
            self.lblNumRep.text = exertData!.reps;
            self.lblSets.text = String(exertData!.sets!);

            
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value:"Exercise Detail View")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated);
        NSNotificationCenter.defaultCenter().postNotificationName("enablePan", object: nil);

    }
    
   override func viewDidAppear(animated: Bool) {
    super.viewWillDisappear(animated);
        NSNotificationCenter.defaultCenter().postNotificationName("disablePan", object: nil);

    }
    @IBAction func showModalDid(sender: AnyObject) {
        
        let modalView:UIViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("popLogExersise"))!;
        let vcModalContainer = BIZPopupViewController(contentViewController: modalView, contentSize: CGSizeMake(CGRectGetWidth(self.view.frame) - 20, CGRectGetWidth(self.view.frame) - 80))
        
        self.presentViewController(vcModalContainer, animated: false, completion: nil);
    }
    
    
   
    @IBAction func playVideoBtnTouchDown(sender: AnyObject) {
        
        self.performSegueWithIdentifier("playVideo", sender: NSURL(string:exertData!.videoURL!));

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "playVideo" {
            let url                  = sender as! NSURL
            let vc                   = segue.destinationViewController as! AVPlayerViewController
            vc.showsPlaybackControls = true
            vc.player                = AVPlayer(URL: url)
            vc.player?.play()
        }else if segue.identifier == "showExersise"{
            
            let vc                   = segue.destinationViewController as! ExerciseDetailViewController;
            vc.exertData = sender as? Exercise;
            
            
        }
    }
}
