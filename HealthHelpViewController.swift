//
//  HealthHelpViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 10/12/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class HealthHelpViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    var htmlStringHelp:String = "";

    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.attributedText = htmlStringHelp.utf8Data?.attributedString;

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(HealthHelpViewController.dismissViewController));
        
       }
    
    
        func dismissViewController(){
            self.navigationController!.dismissViewControllerAnimated(true, completion: nil);
        }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
