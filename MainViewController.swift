
//
//  MainViewController.swift
//  Geia
//
//  Created by zurce on 4/18/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import CoreData
import MSSlidingPanelController
import SwiftyJSON
import Alamofire
import ZWIntroductionViewController
import Braintree

class MainViewController: GeiaViewController,MenuViewControllerDelegate,EmotionNotificationViewDelegate, BTDropInViewControllerDelegate{
    
    var mainSliderPanel: MSSlidingPanelController?
    var mainNavigationController:MainNavigationController?
    var prescriptionController:PrescriptionsViewController?
    var dashboardController:DashboardViewController?
    var blankController:UIViewController?
    var messageController:MessagesViewController?
    var notificationController:NotificationViewController?
    var goalTrackController:GoalTrackingViewController?
    var profileController:ProfileViewController?
    var geiaTabBarController:GeiaTabBarController?
    var riskAnalysisSurveyController:RiskAnalysisViewController?
//    var healthPointsViewController:HealthPointsViewController?
    @IBOutlet weak var emotionNotificationView: EmotionNotificationView!
    
    weak var emotionNotificationDelegate: EmotionNotificationViewDelegate?
    
    private var braintreeClient: BTAPIClient?
    
    // MARK: - Local variables
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
	private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
		return GoalTrackingViewModel()
	}()
	
	private lazy var loaderView: RPLoadingAnimationView = {
		return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
	}()
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
        profileController = UIStoryboard.getProfileStoryboard().instantiateViewControllerWithIdentifier("ProfileViewController") as?  ProfileViewController;
        profileController?.automaticallyAdjustsScrollViewInsets = false;
        
        profileController!.getDataFromServer();

        var storedDate = NSUserDefaults.standardUserDefaults().objectForKey("lastTimeOpened") as? NSDate;
        
        if storedDate == nil {
            storedDate = NSDate();
            
        }
        
        if (storedDate!.isToday()) {
            print("today");
            NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: "lastTimeOpened");
        }else{
            print("nottoday");
            
            
            
            for exer in Exercise.getAllExercises(self.appDelegate.managedObjectContext)!{
                exer.completed = false;
                exer.realized = 0;
                self.appDelegate.managedObjectContext.deleteObject(exer)
            }
            
            
            self.appDelegate.saveContext();
            NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: "lastTimeOpened");

        }
        
        
        let thView = self.storyboard?.instantiateViewControllerWithIdentifier("leftMenu") as! MenuViewController;
        thView.delegate = self;
        
        blankController = UIStoryboard.getMainStoryboard().instantiateViewControllerWithIdentifier("blankViewController");
        
        dashboardController = UIStoryboard.getDashboardStoryboard().instantiateViewControllerWithIdentifier("DashboardViewController") as? DashboardViewController
        
        
        self.emotionNotificationDelegate = dashboardController
        
        //and vice-versa
        dashboardController!.emotionNotificationDelegate = self
        
        geiaTabBarController = GeiaTabBarController();
        
        mainNavigationController = MainNavigationController(rootViewController: dashboardController!);
        geiaTabBarController?.viewControllers = [mainNavigationController!];
        geiaTabBarController?.setSelectedItem(item: 1);
        mainSliderPanel?.centerViewController = mainNavigationController;
        mainSliderPanel = MSSlidingPanelController(centerViewController: geiaTabBarController, andRightPanelController: thView);
        self.addChildViewController(mainSliderPanel!);
        self.view.addSubview((mainSliderPanel?.view)!);
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.openMenu), name: "openMenu", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.disablePan), name: "disablePan", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.enablePan), name: "enablePan", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.goToScreen(_:)), name: "goToScreen", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.dismissSurvey(_:)), name: "dismissSurvey", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainViewController.reportTokenToApi), name: "reportDevice", object: nil);
        
        
        self.emotionNotificationView.hidden   = true
        self.emotionNotificationView.delegate = self
        
        reportTokenToApi();
        
        
        if let initialScreen:Int = NSUserDefaults.standardUserDefaults().integerForKey("initialScreen"){
            self.didSelectOption(initialScreen);
            NSUserDefaults.standardUserDefaults().removeObjectForKey("initialScreen");
        }
        self.disablePan();

        
        //------------
        
//        if NSProcessInfo.processInfo().environment["EBudnikDebug"] == "true" {
//            return
//        }
        
        
        dispatch_async(dispatch_get_main_queue(), {
        
            self.checkForPayment()
            
        })

        
    }
    
    func checkForPayment() -> Void {
        
        
        let loaderView = RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
        loaderView.show()
        
        
        let paymentManager = PaymentManager()
        
        paymentManager.checkPayment { (paid, needInputPaymentInfo, message, client_name, plan, token) in
            
            loaderView.hide(animated: true)
            
            if  (paid){
                
                
                
            } else {
                
                
                
                if needInputPaymentInfo == true {
                    
                            
                            
                            let alert: UIAlertController = UIAlertController(title: "Hey there!", message: "GeiaFit demo mode is over now...\nFor further usage of the app you need to pay according to your therapist's plan.\n\nYour current Therapist:\n\(client_name)\n\nYour current plan is:\n$\(plan) / month", preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "Proceed", style: .Default, handler: { (action) -> Void in
                                
                                
                                dispatch_async(dispatch_get_main_queue(), {
                                    
                                    self.showBTDrop(token)
                                    
                                })
                                
                                
                            }))
                            
                            alert.addAction(UIAlertAction(title: "Unsubscribe", style: .Default, handler: { (action) -> Void in
                                
                                self.changeViewController(70)
                                
                                
                            }))
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                
                                self.presentViewController(alert, animated: true, completion: nil)
                                
                            })
                            
                    
                    
                    
                } else {
                    
                    let alert: UIAlertController = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in
                    
                        self.changeViewController(70)
                    
                    }))
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                    })
                    
                }
                
                
                
            }
            
        }
        
    }
	
	
	override func viewWillAppear(animated: Bool) {
		
		super.viewWillAppear(animated)
        
		let tracker = GAI.sharedInstance().defaultTracker
		tracker.set(kGAIScreenName, value:"RM")
		
		let builder = GAIDictionaryBuilder.createScreenView()
		tracker.send(builder.build() as [NSObject : AnyObject])
        self.disablePan();
	}
	
    func goToScreen(notification:NSNotification) {
        
        print(notification.object);
        
        self.didSelectOption(notification.object as! NSInteger)
        
    }
    
    
    func dismissSurvey(notification:NSNotification) {
        
        riskAnalysisSurveyController?.presentingViewController?.dismissViewControllerAnimated(true, completion: {
            self.riskAnalysisSurveyController?.dismissViewControllerAnimated(true, completion: nil);
        })
        
    }
    func openMenu() {
        if mainSliderPanel?.sideDisplayed == MSSPSideDisplayed.None{
            mainSliderPanel?.openRightPanel();
            
        }else{
            mainSliderPanel?.closePanel();
        }
        
    }
	
	func showLoadingView() {
		self.loaderView.show()
		UIApplication.sharedApplication().beginIgnoringInteractionEvents()
	}
	
	func hideLoadingView() {
		self.loaderView.hide(animated: true)
		UIApplication.sharedApplication().endIgnoringInteractionEvents()
	}

	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    func reportTokenToApi() {
        
        var deviceTokenString = NSUserDefaults.standardUserDefaults().stringForKey("deviceToken") as String?
        let endpoint: String = DEVICE_TOKEN_ENDPOINT
        let enpointurl: String      = "\(SERVER_URL)\(endpoint)"
        
        
        if (deviceTokenString == nil){
            deviceTokenString = "N/A";
        }
        let payload: [String: AnyObject] = [
            "type":                    "ios",
            "token":             deviceTokenString!,
            ]
        
        print(payload)
        
        
        request(.POST, enpointurl, parameters: payload, encoding: .JSON, headers: appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                
                print(result)
                
                if result.isFailure {
                    //Epic fail
                    //self.dumpDebugData(response)
                    //completion(result: ["success": false, "errorMessage":result.debugDescription])
                    return
                }
                
                //completion(result: ["success": true])
        }
        
        
    }
    
    
    
    func didSelectOption(selectedOption: NSInteger) {
        print("Selected \(selectedOption)");
        
        self.changeViewController(selectedOption);
    }
    
    func changeViewController(selectedOption: NSInteger){

        var section = 0;
        
        switch selectedOption {
        case 0:
            self.setMainNavigation(self.dashboardController!, index: 1);
            break
        case 10:
            if notificationController == nil{
                notificationController = UIStoryboard.getNotificationStoryboard().instantiateViewControllerWithIdentifier("NotificationViewController") as? NotificationViewController
            }
            self.setMainNavigation(self.notificationController!, index: 2);
        case 20:
            if messageController == nil {
                messageController = UIStoryboard.getMessagesStoryboard().instantiateViewControllerWithIdentifier("MessagesViewController") as? MessagesViewController
            }
            self.setMainNavigation(self.messageController!, index: 3);
            break;
        
            
        case  30,31,32: section = selectedOption - 30;
        if mainNavigationController?.viewControllers.first == goalTrackController {
            goalTrackController!.currentSection = GoalTrackingSection(rawValue: section)!
            goalTrackController?.automaticallyAdjustsScrollViewInsets = false;
            goalTrackController!.changeTabs();
            
        }else{
            if goalTrackController == nil {
                goalTrackController = UIStoryboard.getGoalTrackingStoryboard().instantiateViewControllerWithIdentifier("GoalTrackingViewController") as? GoalTrackingViewController
                goalTrackController?.automaticallyAdjustsScrollViewInsets = false;
                
            }
            
            goalTrackController!.currentSection = GoalTrackingSection(rawValue: section)!
            self.setMainNavigation(self.goalTrackController!, index: 5);

        }
        break;
        
            
        case 40:
            let newGoals = UIStoryboard.getGoalTrackingStoryboard().instantiateViewControllerWithIdentifier("NewGoalsViewController") as! NewGoalsViewController;
            self.presentViewController(UINavigationController(rootViewController:newGoals), animated: true, completion: nil);

            break;
        case 41:
                let exercisesTableView = UIStoryboard.getPrescriptionStoryboard().instantiateViewControllerWithIdentifier("ExercisesTableViewController") as! ExercisesTableViewController;
                self.presentViewController(UINavigationController(rootViewController:exercisesTableView), animated: true, completion: nil);
        case 42:
                let postureView = PostureListViewController.instanceFromStoryBord();
                self.presentViewController(UINavigationController(rootViewController:postureView), animated: true, completion: nil);
                break;
        case 50:
            //My Profile
            
            if profileController == nil {
                profileController = UIStoryboard.getProfileStoryboard().instantiateViewControllerWithIdentifier("ProfileViewController") as?  ProfileViewController;
                profileController?.automaticallyAdjustsScrollViewInsets = false;
                
            }
            self.presentViewController(UINavigationController(rootViewController:profileController!)
                , animated: true, completion: nil);
            break;
        case 51:
            //Apple Health
            self.authHealthKit();
            break;
        case 52:
            //Prescription
            let fitnessViewController = UIStoryboard.getPrescriptionStoryboard().instantiateViewControllerWithIdentifier("FitnessViewController") as! FitnessViewController;
            self.presentViewController(UINavigationController(rootViewController:fitnessViewController), animated: true, completion: nil);
            break;
        case 53:
            self.synchActivityDate()
            mainSliderPanel?.closePanel();
            //DATA SYNCH
            break;
        case 60:
        //Settings
            self.openMenu();
            break;
        case 61:
            //add custom videos
            let videoNavController: UINavigationController = UIStoryboard.getVideoCaptureStoryboard().instantiateViewControllerWithIdentifier("VideoNavController") as! UINavigationController
            self.presentViewController(videoNavController, animated: true, completion: nil);
            break;
        case 70:
            self.logout()
            mainSliderPanel?.closePanel();
            break
        case 71:
            self.openMenu();
            break
        case 80:
            //HealthPointsViewController
            let healthPointsViewController = UIStoryboard.getNotificationStoryboard().instantiateViewControllerWithIdentifier("HealthPointsViewController") as? HealthPointsViewController
                self.presentViewController(UINavigationController(rootViewController:healthPointsViewController!),animated: true,completion: nil);
                
            
            break;
        case 90:
            //Devices
            self.openMenu();
            break;
        case 91:
            
            // Fitbit
            
            showLoadingView()
            
            
            
            let fitbitManager = FitbitManager.sharedInstance

            if fitbitManager.checkIsLogin() == true {
                
                fitbitManager.logout({ (error) in
                    
                    self.hideLoadingView()
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("updateActivityData", object: nil);
                        
                })
                
            } else {
            
                fitbitManager.login({ (error) in
                                        
                        self.hideLoadingView()
                        
                        NSNotificationCenter.defaultCenter().postNotificationName("updateActivityData", object: nil);
                        
//                    })
                    
                })
                
//                dispatch_async(dispatch_get_main_queue(), {
//                    
//                    fitbitManager.getStepsCountForDate(NSDate().dateBySubtractingDays(10)!, completion: { (success) in
//                        
//                        dispatch_async(dispatch_get_main_queue(), {
//                            
//                            self.hideLoadingView()
//                            
//                            NSNotificationCenter.defaultCenter().postNotificationName("updateActivityData", object: nil);
//                            
//                        })
//                    })
//                    
//                    
//                })
                
                
                
            }
            
        
            break;
        case 92:
            self.riskAnalysisSurveyController = UIStoryboard.getOnboardingStoryboard().instantiateViewControllerWithIdentifier("RiskAnalysisViewController") as? RiskAnalysisViewController;
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isFromMenu");
            self.riskAnalysisSurveyController?.isFromMenu = true;
            self.presentViewController(UINavigationController(rootViewController:self.riskAnalysisSurveyController!)
                , animated: true, completion: nil);
            
            
            
            //Survey

            break;
        default:
            self.setMainNavigation(self.blankController!, index: 2);
            break;
        }
        
    }
    
    
    func setMainNavigation(controller:UIViewController, index:Int){
        mainNavigationController = MainNavigationController(rootViewController:controller);
        geiaTabBarController?.viewControllers = [mainNavigationController!]
        geiaTabBarController?.setSelectedItem(item:index);
        mainSliderPanel?.centerViewController = geiaTabBarController;
    }
    
    func synchActivityDate() {
		
		self.showLoadingView()
		self.goalTrackingViewModel.updateStepsFromHealthKitForDateRange(.Week) { (success) -> Void in
			if success {
				
				self.appDelegate.saveContext()
				
				self.goalTrackingViewModel.getStepsForUserByWeek(NSDate(), user: self.appDelegate.user!, completion:  { (result) -> Void in
					
					let success = result["success"] as! Bool
					
					if success {
						
						let data           = result
						let weekPoints = data["weekPoints"] as! [Dictionary<String,AnyObject>]
						
//						print(weekPoints)
						//Post updated data to server
										self.goalTrackingViewModel.sendWeeklyActivityDataToServerForUser(self.appDelegate.user!, data: weekPoints, completion: { (result) -> Void in
											
											self.hideLoadingView()
											//Parse response
										})
					} else {
					
						self.hideLoadingView()
					}
				})
			} else {
			
				self.hideLoadingView()
			}
		}

		
		
	}
	
    func logout() {
        let viewModel = LoginViewModel()
        viewModel.logoutUser(self.appDelegate.user!) { (result) -> Void in
            let success = result["success"] as! Bool
			
            if success {
       
//                var error:NSError?;
//                self.appDelegate.managedObjectContext.deleteAllObjects(&error);
				

                self.deleteToken();
                self.performSegueWithIdentifier("logout", sender: nil)
            } else {
                let alert: UIAlertController = UIAlertController(title: "Couldn't logout", message: "There was a problem loging out from our server, please try again", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                self.presentViewController(alert, animated: true, completion: nil)
                print("Couldn't logout")
            }
        }
    }
    
    func deleteToken(){
        let lastTimeEmotionNotificationWasShown = NSUserDefaults.standardUserDefaults().objectForKey(LAST_EMOTION_DATE_KEY) as! NSDate?
        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(NSBundle.mainBundle().bundleIdentifier!);
        NSUserDefaults.standardUserDefaults().removeObjectForKey(SESSION_NAME_KEY);
        NSUserDefaults.standardUserDefaults().removeObjectForKey(SESSION_ID_KEY);
        NSUserDefaults.standardUserDefaults().removeObjectForKey(CSRF_TOKEN_KEY);
        NSUserDefaults.standardUserDefaults().removeObjectForKey(LOGGED_IN_KEY);
        NSUserDefaults.standardUserDefaults().removeObjectForKey(USER_ID_KEY);
        self.appDelegate._requestHeaders = nil;
        
        if lastTimeEmotionNotificationWasShown != nil || lastTimeEmotionNotificationWasShown!.isEqualToDateIgnoringTime(NSDate()) {
            NSUserDefaults.standardUserDefaults().setObject(lastTimeEmotionNotificationWasShown, forKey: LAST_EMOTION_DATE_KEY)
        }
        NSUserDefaults.standardUserDefaults().synchronize()

    }
    
    func deleteMessages() {
        let fetchRequest = NSFetchRequest(entityName: "Message")
        if #available(iOS 9.0, *) {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
                try appDelegate.persistentStoreCoordinator.executeRequest(deleteRequest, withContext: appDelegate.managedObjectContext)
            } catch let error as NSError {
                print(error.localizedDescription)
            } catch {
                print("")
            }
        } else {
            // Fallback on earlier versions
            let fetchRequest:[Message] =  Message.getMessages(self.appDelegate.managedObjectContext)!;
            
            for storedMessage in fetchRequest {
                self.appDelegate.managedObjectContext.deleteObject(storedMessage)
            }
        }
    }
    
    
    // MARK: - Emotion Notification View Delegate
    func emotionValueDidChange() {
        self.emotionNotificationDelegate?.emotionValueDidChange()
    }
    
    func showEmotionalNotification() {
        self.emotionNotificationView.setupView()
        self.emotionNotificationView.hidden = false
        
        
        self.view.bringSubviewToFront(self.emotionNotificationView);
    }
    
    // MARK: - Actions
    func authHealthKit() {
        // Example 1
        let coverImageNames =  ["help1","help2", "help3"]
        let backgroundImageNames = ["bg_onboard2","bg_onboard2", "bg_onboard2"]
        let enterButton = UIButton();
        enterButton.setTitle("I Understand", forState: .Normal);
        enterButton.backgroundColor = UIColor.geiaYellowColor();
        enterButton.layer.cornerRadius = 3;
        
        let introductionView = ZWIntroductionViewController(coverImageNames: coverImageNames, backgroundImageNames: backgroundImageNames, button: enterButton);
        
        
        introductionView.enterButton.tintColor = UIColor.geiaDarkGrayColor();
    
        introductionView!.didSelectedEnter = {
            introductionView.dismissViewControllerAnimated(true, completion: nil);
        }
        
        
        self.presentViewController(introductionView, animated: true, completion: nil);
    }
    
    
    
    //MARK - Pan Gestures
    
    func disablePan(){
        self.mainSliderPanel!.rightPanelOpenGestureMode = .None;
    }
    
    func enablePan(){
        
        self.mainSliderPanel!.rightPanelOpenGestureMode = .PanContent;
        
    }
    
    
    
    //------------------------
    
    
    func showBTDrop(token : String) {
        
//        let loaderView = RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
//        loaderView.show()
        
        let braintreeClient = BTAPIClient(authorization: token)
        
        self.braintreeClient = braintreeClient;
        
//        let paymentManager = PaymentManager()
//        
//        paymentManager.getBraintreeClient { (braintreeClient) in
//            
//            loaderView.hide(animated: true)
//            
//            self.braintreeClient = braintreeClient;
        
            
            // Create a BTDropInViewController
            let dropInViewController = BTDropInViewController(APIClient: self.braintreeClient!)
            dropInViewController.delegate = self
            
            // This is where you might want to customize your view controller (see below)
            
            // The way you present your BTDropInViewController instance is up to you.
            // In this example, we wrap it in a new, modally-presented navigation controller:
            dropInViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: UIBarButtonSystemItem.Cancel,
                target: self, action: #selector(self.userDidCancelPayment))
            let navigationController = UINavigationController(rootViewController: dropInViewController)
            
            
            dispatch_async(dispatch_get_main_queue(), {
                self.presentViewController(navigationController, animated: true, completion: nil)
            })
//        }
        
    }
    
    func userDidCancelPayment() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: BTDropInViewControllerDelegate
    func dropInViewController(viewController: BTDropInViewController, didSucceedWithTokenization paymentMethodNonce: BTPaymentMethodNonce){
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        let loaderView = RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
        loaderView.show()
        
        let paymentManager = PaymentManager()
        
        
        paymentManager.postNonceToServer(paymentMethodNonce.nonce) { (paid, message) in
            
            loaderView.hide(animated: true)
            
            if (paid == true) {
                
                
                
            } else {
                
                let alert: UIAlertController = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in
                    
                    self.changeViewController(70)
                
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            
            
            
            
            
        }
        
    }
    
    func dropInViewControllerDidCancel(viewController: BTDropInViewController){
        
        dismissViewControllerAnimated(true, completion: nil)
    }

}



