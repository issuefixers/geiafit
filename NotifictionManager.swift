//
//  NotifictionManager.swift
//  Geia
//
//  Created by Sergio Solorzano on 5/22/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class NotifictionManager: NSObject {

    
    class func showMessageNotification(title title:String,message:String,screen:Int){
        HDNotificationView.showNotificationViewWithImage( UIImage(named: "menu-chat") , title: title, message: message, isAutoHide: true) {
            NSNotificationCenter.defaultCenter().postNotificationName("goToScreen", object: screen);
        }
    }
}
