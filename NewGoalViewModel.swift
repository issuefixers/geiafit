//
//  NewGoalViewModel.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class NewGoalViewModel: ViewModel {

    func getGoalsMeasures(user:User, completion: (result: [GoalMeasure]) -> Void){
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = (OUTCOME_MEASURES_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID));
        
        let url: String      = "\(SERVER_URL)\(endpoint)"
        print(url);
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                var listMeasures:[GoalMeasure] = [];
                if let jsonObject: AnyObject = result.value {
                    let jsonArray:[JSON] = JSON(jsonObject)["goals"].arrayValue
                    for jsonMessage:JSON in jsonArray{
                        
                        let nId = jsonMessage["nid"].stringValue
                        var newMeasure = GoalMeasure.getMeassureWithId(self.appDelegate.managedObjectContext, nId:nId )
                        
                        if(newMeasure == nil){
                            newMeasure = NSEntityDescription.insertNewObjectForEntityForName("GoalMeasure", inManagedObjectContext: self.appDelegate.managedObjectContext) as? GoalMeasure
                        }
                        newMeasure!.addGoalMeassureWithData(jsonMessage)
                        
                        
                        
                        listMeasures.append(newMeasure!);
                    }
                    self.appDelegate.saveContext();
                }
                
                1
                
                completion(result: listMeasures);
        }
    }
    
    
    func updateGoalMeassure(goalMeassre: GoalMeasure, completition: (result:Bool)->Void){
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: self.appDelegate.user!.userID))!
        let endpoint: String = (OUTCOME_MEASURES_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID));
        
        let url: String      = "\(SERVER_URL)\(endpoint)"
        print(url);
        
        var params: [Dictionary<String, String>] = []
        let date = NSDate().epochTimestamp()
        let questions  = QuestionMeasure.getQuestionsForGoalMeasure(self.appDelegate.managedObjectContext, goalId: goalMeassre.nid!);

        for qmeasure in questions!{

            if (qmeasure.value != nil && qmeasure.value?.isEmpty == false){
                params.append(["nid":goalMeassre.nid!,"item_id":(qmeasure.item_id?.stringValue)!,"value":qmeasure.value!,"outcome_date":date]);
            }
        
        }
        
        if params.count == 0{
               completition(result: true);
        }else{
            print(params);
            for dict in params{
                request(.PUT, url, parameters: dict, encoding: .JSON, headers: self.appDelegate.requestHeaders())
                    .validate(statusCode: 200..<300)
                    .validate(contentType: ["application/json"])
                    .responseJSON { (response: Response) -> Void in
                        //debugPrint(response)
                        let result = response.result
                        
                        if result.isFailure {
                            //Epic fail
                            print("Could not report measure data");
                            completition(result: false);
                            return;
                            
                            
                        }else{
                            print("updated measure data ");
                            if dict == params.last!{
                                completition(result: true);
                            }
                        }
                }
            }
        }
       
        
    }
}
