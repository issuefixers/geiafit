//
//  NotificationViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 8/25/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import JTProgressHUD
import DZNEmptyDataSet

class NotificationViewController: GeiaViewController,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    var fetching:Bool = false;
    var shouldUpdate = false;
    var items:[Notification] = [];
    private lazy var viewModel:NotificationViewModel = {
        return NotificationViewModel();
    }();
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    
    override func viewDidLoad() {
        JTProgressHUD.show()

        super.viewDidLoad()
        self.tableView.emptyDataSetSource = self;
        self.tableView.emptyDataSetDelegate = self;

        self.title = "Notifications";
        self.tableView.tableFooterView = UIView();

        self.fetchForNew();
   
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NotificationViewController.deleteCell(_:)), name: "refreshNotifications", object: nil);
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData();
        self.fetchForNew();

    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        if (self.tableView.pullToRefreshView == nil) {
            self.tableView.addPullToRefreshWithActionHandler {
                self.viewModel.getNotifications(self.appDelegate.user!, timestamp: "0000000") { (result) in
                    self.items = result;
                    self.tableView.reloadData();
                    self.tableView.pullToRefreshView.stopAnimating()
                }
            };
            
        }
        
    }
    
    func fetchForNew(){
        if fetching == false {
            fetching = true;
            self.viewModel.getNotifications(self.appDelegate.user!, timestamp: "0000000") { (result) in
                self.items = result;
                self.fetching = false;
                self.shouldUpdate = true;
                self.tableView.reloadData();
                NSNotificationCenter.defaultCenter().postNotificationName("updateNotifications", object: nil);
                JTProgressHUD.hide();
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:NotificationTableViewCell = tableView.dequeueReusableCellWithIdentifier("notificationCell", forIndexPath: indexPath) as! NotificationTableViewCell;
        cell.initializeCellWithDict(items[indexPath.row]);
        cell.tag = indexPath.row;
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let kNotification = items[indexPath.row];
        
        switch (kNotification.category!){
        case "messages":
            self.goToScreen(20)
            break;
            
        case "exercises":
            self.goToScreen(41)
            break;
        case "snapshot":
            self.goToScreen(42)
            break;
        case "steps":
            self.goToScreen(31)
            break;
        case "prescription":
            self.goToScreen(52)
            break;
        case "activity":
            self.goToScreen(30)
            break;
        default:  break;
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true);
        

        if kNotification.read == 0 {
            viewModel.setNotificationAsRead(kNotification.id!, completion: { (result) in
                if result == true{
                    kNotification.read = 1;
                    tableView.cellForRowAtIndexPath(indexPath)?.tag = 0;
                }else{
                    
                }
            })
        }
        
        self.appDelegate.saveContext();
        
    }

    func goToScreen(screen: Int){
        NSNotificationCenter.defaultCenter().postNotificationName("goToScreen", object: screen);
    }

    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No Notifications", attributes: [NSFontAttributeName: UIFont.geiaFontOfSize(18),NSForegroundColorAttributeName:UIColor.whiteColor()]);
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
         return NSAttributedString(string: "Start exercising and tracking your goals to receive notifications from your PT", attributes: [NSFontAttributeName: UIFont.geiaFontOfSize(15),NSForegroundColorAttributeName:UIColor.lightGrayColor()]);
    }
    
    func deleteCell(notif :NSNotification){
        let cell = notif.object as! UITableViewCell;
        viewModel.deleteNotification(self.items[cell.tag]);

        self.items.removeAtIndex(cell.tag);
        self.tableView.deleteRowsAtIndexPaths([NSIndexPath.init(forRow: cell.tag, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Left);
        self.tableView.reloadData();
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
