//
//  HealthPointSection.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/4/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class HealthPointSection: NSObject {
    
    var open = false
    var view: UIView?
    var overlayView: UIView?
    var headerView: SectionHeaderView?
    var title: String?
    var backgroundColor: UIColor?
    var sectionIndex: Int?
    var appearance = Appearance()

    
    class func section (point:HealthDailyLog)->Section{

        
        let viewDay = NSBundle.mainBundle().loadNibNamed("HealthPointSectionHeader", owner: nil, options: nil)![0]  as! UIView;
        let viewRay = NSBundle.mainBundle().loadNibNamed("HealthPointSectionHeader", owner: nil, options: nil)![1] as! UIView;
        
        viewRay.frame = CGRectMake(0, 0, CGRectGetWidth(UIScreen.mainScreen().bounds), 320);
        viewDay.frame = CGRectMake(0, 0, CGRectGetWidth(UIScreen.mainScreen().bounds), 44);
        
        
        // set header
        let lblTextDate = viewDay.viewWithTag(99) as! UILabel;
        let middleTextDate = viewDay.viewWithTag(98) as! UILabel;
        let totalPoints = viewDay.viewWithTag(97) as! UILabel;
//        let numberOfPoints = Int(point.activity!) + Int(point.exercise!) + Int(point.daily_challenge!) + Int(point.goals!)
        totalPoints.text = "\(point.daily_points!)"
//
        let date = point.pt_date!
        
        
        //set info
        
        let lblActivity = viewRay.viewWithTag(99) as! UILabel;
        let lblExercise = viewRay.viewWithTag(98) as! UILabel;
        let lblDailyChallenge = viewRay.viewWithTag(97) as! UILabel;
        let lblGoals = viewRay.viewWithTag(96) as! UILabel;
        
        lblActivity.text = String(point.activity!);
        lblExercise.text = String(point.exercise!);
        lblDailyChallenge.text = String(point.daily_challenge!);
        lblGoals.text = String(point.goals);
        
        
        if date.isToday() {
            lblTextDate.text = "Today";
        }else{
            if date.isEqualToDate(NSDate().dateBySubtractingDays(1)!){
                lblTextDate.text = "Yesterday";
            }else{
                lblTextDate.text = date.getDayOfWeekString();
            }
        }
        
        middleTextDate.text = date.stringForDate();
        let section = Section();
        section.view = viewRay;
        section.overlayView = viewDay;
        section.headerView = SectionHeaderView();
        section.headerView?.backgroundHeaderView = viewDay;
        section.backgroundColor = UIColor.geiaDarkGrayColor();
        
        return section;
        
    }

}
