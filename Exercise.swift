//
//  Exercise.swift
//  Geia
//
//  Created by Sergio Solorzano on 5/30/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

@objc(Exercise)
class Exercise: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    class func addExerciseWithData(data: JSON, moc: NSManagedObjectContext) -> Exercise? {
        var exercise: Exercise
        
        let title     = data["title"].string
        let code     = data["peid"].string
        
        let comments  = data["comments"].string
        let sets      = data["sets"].intValue
        let videoURL = data["mp4"].string
        let reps      = data["reps"].string
        let rest      = data["rest"].string
        let thumb1    = data["thumb1"].string
        let thumb2    = data["thumb2"].string
        let dailyTotals = data["daily"].intValue;
        let fetchRequest        = NSFetchRequest.init(entityName: "Exercise")
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "code = %@", code!)
        var exercises: [Exercise]   = []
        
        do {
            exercises = (try moc.executeFetchRequest(fetchRequest)) as! [Exercise]
        } catch {
            print("Error Querying")
            return nil
        }
        
        if exercises.count > 0 {
            print("Found surveys: \(exercises)")
            exercise = exercises.first! as Exercise
        } else {
            exercise          = NSEntityDescription.insertNewObjectForEntityForName("Exercise", inManagedObjectContext: moc) as! Exercise
            exercise.code = code;
            exercise.realized = 0;
            exercise.completed = false;
            print("Inserted survey: \(exercise)")
        }
        
        exercise.daily = false;
        
        if let weekly:Dictionary = data["weekly"].dictionary {
            exercise.kSat = (weekly["sat"]?.boolValue)!;
            exercise.kSun = (weekly["sun"]?.boolValue)!;
            exercise.kMon = (weekly["mon"]?.boolValue)!;
            exercise.kTue = (weekly["tue"]?.boolValue)!;
            exercise.kWed = (weekly["wed"]?.boolValue)!;
            exercise.kThu = (weekly["thu"]?.boolValue)!;
            exercise.kFri = (weekly["fri"]?.boolValue)!;
            
            
            if !exercise.kSat && !exercise.kSun && !exercise.kMon && !exercise.kThu && !exercise.kWed && !exercise.kTue && !exercise.kFri{
                exercise.daily = true;
                
            }
            
        }else{
            exercise.daily = true;
        }
        
        exercise.title = title;
        exercise.comment = comments;
        exercise.sets = sets;
        exercise.videoURL = videoURL;
        exercise.reps = reps;
        exercise.rest = rest;
        exercise.thumbUrl1 = thumb1;
        exercise.thumbUrl2 = thumb2;
        
        if dailyTotals > 1 {
            exercise.dailyTotal = dailyTotals;
        }else{
            exercise.dailyTotal = 1;
        }

        
        
        return exercise
    }
    
    class func getExercisesWithDate(date: NSDate, moc: NSManagedObjectContext) -> [Exercise]?{
        var exercises: [Exercise]?
        
        let queryDay = "k"+date.getDayOfWeekString()! + " = %i";
        //        let queryDay = "kTue = %i";
        
        print(queryDay);
        let fetchRequest        = NSFetchRequest.init(entityName: "Exercise")
        
        let predicate1:NSPredicate = NSPredicate(format: queryDay, 1)
        let predicate2:NSPredicate = NSPredicate(format: "completed = %i",0);
        let predicate:NSPredicate  = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2] )
        //        let resultPredicate:NSPredicate  = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate,predicate3] )
        //        fetchRequest.predicate = resultPredicate;
        fetchRequest.predicate = predicate;
        
        
        
        do {
            exercises = (try moc.executeFetchRequest(fetchRequest)) as? [Exercise]
        } catch {
            print("Error Querying")
            return nil
        }
        
        
        return exercises
    }
    
    
    class func getTodayExercises(moc: NSManagedObjectContext) -> [Exercise]?{
        var exercises: [Exercise]?
        
        let queryDay = "k"+NSDate().getDayOfWeekString()! + " = %i";
        //        let queryDay = "kTue = %i";
        
        print(queryDay);
        let fetchRequest        = NSFetchRequest.init(entityName: "Exercise")
        
        let predicate1:NSPredicate = NSPredicate(format: queryDay, 1)
        let predicate2:NSPredicate = NSPredicate(format: "completed = %i",0);
        let predicate:NSPredicate  = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2] )
        //        let resultPredicate:NSPredicate  = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate,predicate3] )
        //        fetchRequest.predicate = resultPredicate;
        fetchRequest.predicate = predicate;
        
        
        
        do {
            exercises = (try moc.executeFetchRequest(fetchRequest)) as? [Exercise]
        } catch {
            print("Error Querying")
            return nil
        }
        
        
        return exercises
    }
    
    class func getTodayAllExercises(moc: NSManagedObjectContext) -> [Exercise]?{
        var exercises: [Exercise]?
        
        let queryDay = "k"+NSDate().getDayOfWeekString()! + " = %i";
        
        let fetchRequest        = NSFetchRequest.init(entityName: "Exercise")
        
        let predicate1:NSPredicate = NSPredicate(format: queryDay, 1)
        fetchRequest.predicate = predicate1;
        
        do {
            exercises = (try moc.executeFetchRequest(fetchRequest)) as? [Exercise]
        } catch {
            print("Error Querying")
            return nil
        }
        
        return exercises
    }
    
    class func getCompletedExercises(moc: NSManagedObjectContext) -> [Exercise]?{
        var exercises: [Exercise]?
        
        
        let fetchRequest        = NSFetchRequest.init(entityName: "Exercise")
        let resultPredicate1 = NSPredicate(format: "completed = %i", 1);
        fetchRequest.predicate = resultPredicate1;
        
        do {
            exercises = (try moc.executeFetchRequest(fetchRequest)) as? [Exercise]
        } catch {
            print("Error Querying")
            return nil
        }
        
        
        return exercises
    }

    
    class func getAllExercises(moc: NSManagedObjectContext) -> [Exercise]?{
        var exercises: [Exercise]?
        
        
        let fetchRequest        = NSFetchRequest.init(entityName: "Exercise")
        
        do {
            exercises = (try moc.executeFetchRequest(fetchRequest)) as? [Exercise]
        } catch {
            print("Error Querying")
            return nil
        }
        
        
        return exercises
    }

    class func getPendingExercises(moc: NSManagedObjectContext) -> [Exercise]?{
        var exercises: [Exercise]?
        let queryDay = "k"+NSDate().getDayOfWeekString()! + " = %i";
        let fetchRequest        = NSFetchRequest.init(entityName: "Exercise")
        
        let predicate1:NSPredicate = NSPredicate(format: queryDay, 0)
        let predicate2:NSPredicate = NSPredicate(format: "completed = %i",0);
        let predicate:NSPredicate  = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2] )
        fetchRequest.predicate = predicate;
        
        do {
            exercises = (try moc.executeFetchRequest(fetchRequest)) as? [Exercise]
        } catch {
            print("Error Querying")
            return nil
        }
        return exercises
    }
    
}
