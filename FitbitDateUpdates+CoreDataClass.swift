//
//  FitbitDateUpdates+CoreDataClass.swift
//  Geia
//
//  Created by Eugene Budnik on 10/15/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData


public class FitbitDateUpdates: NSManagedObject {

    
    private static var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    class func addDate(user: User, date: NSDate, dataType: FitbitDataType, moc: NSManagedObjectContext) -> FitbitDateUpdates? {
        
        var obj: FitbitDateUpdates
        
        
        let fetchRequest               = NSFetchRequest.init(entityName: "FitbitDateUpdates")
        fetchRequest.fetchLimit        = 1
        fetchRequest.predicate         = NSPredicate(format: "userID = %@ AND date = %@  AND dataType = %@ ", NSNumber(longLong: user.userID), date.dateAtStartOfDay(), NSNumber(short: dataType.rawValue) )
        var objects: [FitbitDateUpdates] = []
        
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [FitbitDateUpdates]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return nil
        }
        
        
        if objects.count > 0 {
            print("Found data points: \(objects)")
            obj = objects.first! as FitbitDateUpdates
        } else {
            obj             = NSEntityDescription.insertNewObjectForEntityForName("FitbitDateUpdates", inManagedObjectContext: moc) as! FitbitDateUpdates
            obj.userID      = user.userID
            obj.date        = date
            obj.dataType    = dataType.rawValue
        }
        
        dispatch_async(dispatch_get_main_queue(), {
        
            self.appDelegate.saveContext()
            
            })
        
        return obj
        
    }
    
    
    class func checkIsDateExist(user: User, date: NSDate, dataType: FitbitDataType, moc: NSManagedObjectContext) -> Bool {
        
        let fetchRequest               = NSFetchRequest.init(entityName: "FitbitDateUpdates")
        fetchRequest.fetchLimit        = 1
        fetchRequest.predicate         = NSPredicate(format: "userID = %@ AND date = %@  AND dataType = %@ ", NSNumber(longLong: user.userID), date.dateAtStartOfDay(), NSNumber(short: dataType.rawValue) )
        var objects: [FitbitDateUpdates] = []
        
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [FitbitDateUpdates]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return false
        }
        
        if objects.count > 0 {
            print("Found data points: \(objects)")
            
            return true
        } else {
            
            return false
        }
        
    }
    
    class func getLastDataIfNeedUpdate(user: User, date: NSDate, dataType: FitbitDataType, moc: NSManagedObjectContext) -> NSDate? {
        
        let fetchRequest               = NSFetchRequest.init(entityName: "FitbitDateUpdates")
        fetchRequest.fetchLimit        = 1
        fetchRequest.predicate         = NSPredicate(format: "userID = %@  AND dataType = %@ ", NSNumber(longLong: user.userID), NSNumber(short: dataType.rawValue) )
        
        let sectionSortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        var objects: [FitbitDateUpdates] = []
        
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [FitbitDateUpdates]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return nil
        }
        
        if objects.count > 0 {
            print("Found data points: \(objects)")
            
            let obj = objects.first! as FitbitDateUpdates
            
            if obj.date <= date.dateAtStartOfDay() {
                
                return obj.date
            } else {
                
                return nil
            }
            
            
        } else {
            
            return nil
        }
        
    }
    
    
    
    class func checkIfDateIsLast(user: User, date: NSDate, dataType: FitbitDataType, moc: NSManagedObjectContext) -> Bool {
        
        let fetchRequest               = NSFetchRequest.init(entityName: "FitbitDateUpdates")
        fetchRequest.fetchLimit        = 1
        fetchRequest.predicate         = NSPredicate(format: "userID = %@ AND dataType = %@ ", NSNumber(longLong: user.userID), NSNumber(short: dataType.rawValue) )
        
        let sectionSortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        var objects: [FitbitDateUpdates] = []
        
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [FitbitDateUpdates]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return false
        }
        
        if objects.count > 0 {
            print("Found data points: \(objects)")
            
            let obj = objects.first! as FitbitDateUpdates
            
            if obj.date == date.dateAtStartOfDay() {
                
                return true
            } else {
                
                return false
            }
            
            
        } else {
            
            return false
        }
        
    }
    
    
    class func getFirstDate(user: User, dataType: FitbitDataType, moc: NSManagedObjectContext) -> NSDate {
        
        let fetchRequest               = NSFetchRequest.init(entityName: "FitbitDateUpdates")
        fetchRequest.fetchLimit        = 1
        fetchRequest.predicate         = NSPredicate(format: "userID = %@ AND dataType = %@ ", NSNumber(longLong: user.userID), NSNumber(short: dataType.rawValue) )
        
        let sectionSortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        var objects: [FitbitDateUpdates] = []
        
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [FitbitDateUpdates]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return NSDate()
        }
        
        if objects.count > 0 {
            print("Found data points: \(objects)")
            
            let obj = objects.first! as FitbitDateUpdates
            
            return obj.date!;
            
            
        } else {
            
            return NSDate()
        }
        
    }
    
    class func removeDate(user: User, dataType: FitbitDataType, moc: NSManagedObjectContext, date: NSDate){
        
        let fetchRequest = NSFetchRequest(entityName: "FitbitDateUpdates")
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate         = NSPredicate(format: "userID = %@ AND date = %@ AND dataType = %@ ", NSNumber(longLong: user.userID), date.dateAtStartOfDay(), NSNumber(short: dataType.rawValue) )
        
        do
        {
            let results = try moc.executeFetchRequest(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                moc.deleteObject(managedObjectData)
            }
        } catch let error as NSError {
            print("Detele all data in FitbitDateUpdates error : \(error) \(error.userInfo)")
        }
    }
    
    
    class func purgeData(user: User, moc: NSManagedObjectContext){
        
        let fetchRequest = NSFetchRequest(entityName: "FitbitDateUpdates")
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate         = NSPredicate(format: "userID = %@ ", NSNumber(longLong: user.userID) )
        
        do
        {
            let results = try moc.executeFetchRequest(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                moc.deleteObject(managedObjectData)
            }
        } catch let error as NSError {
            print("Detele all data in FitbitDateUpdates error : \(error) \(error.userInfo)")
        }
    }

    
    
}
