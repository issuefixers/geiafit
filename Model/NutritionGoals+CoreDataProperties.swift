//
//  NutritionGoals+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/19/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension NutritionGoals {

    @NSManaged var totalCalories: Int32
    @NSManaged var fatPercent: Double
    @NSManaged var carbsPercent: Double
    @NSManaged var proteinPercent: Double
    @NSManaged var weight: Double
    @NSManaged var instructions: String?
    @NSManaged var date: NSDate
    @NSManaged var user: User?

}
