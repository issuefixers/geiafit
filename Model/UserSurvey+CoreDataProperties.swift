//
//  UserSurvey+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/22/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserSurvey {

    @NSManaged var createdDate: NSTimeInterval
    @NSManaged var submittedDate: NSTimeInterval
    @NSManaged var isComplete: Bool
    @NSManaged var survey: Survey?
    @NSManaged var user: User?

}
