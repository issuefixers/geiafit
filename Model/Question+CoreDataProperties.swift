//
//  Question+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/21/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Question {

    @NSManaged var questionID: Int32
    @NSManaged var text: String?
    @NSManaged var answers: NSSet?
    @NSManaged var userAnswers: NSSet?
    @NSManaged var survey: Survey?

}
