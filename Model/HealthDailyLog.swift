//
//  HealthDailyLog.swift
//  
//
//  Created by Sergio Solorzano on 9/4/16.
//
//

import Foundation
import CoreData
import SwiftyJSON


class HealthDailyLog: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    func addDailyLogWithData(json: JSON){
        
        
        self.activity = json["activity"].numberValue;
        
        if self.activity == nil{
            self.activity = 0;
        }
        self.goals = json["goals"].numberValue;
        if self.goals == nil{
            self.goals = 0;
        }
        
        self.daily_points = json["daily_points"].numberValue;
        if self.daily_points == nil{
            self.daily_points = 0;
        }
        
        
        self.daily_challenge = json["daily_challenge"].numberValue;
        if self.daily_challenge == nil{
            self.daily_challenge = 0;
        }
        
        
        self.exercise = json["exercises"].numberValue;
        
        if self.exercise == nil{
            self.daily_challenge = 0;
        }
        
        self.id = json["id"].string;
        self.uid = json["uid"].number;
        self.created_date = NSDate(timeIntervalSince1970: json["created"].doubleValue);
        self.updated_date = NSDate(timeIntervalSince1970: json["updated"].doubleValue);
        print("date del challenge \(json["id"].string)  = \(json["pt_date"].string)");
        self.pt_date = NSDate.dateWithEpochTimestamp(json["pt_date"].doubleValue);
    }
    
    class func getLogWithId(moc: NSManagedObjectContext, logId: String) -> HealthDailyLog?{
        var logs: [HealthDailyLog]?
        
        let fetchRequest        = NSFetchRequest.init(entityName: "HealthDailyLog")
        
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "id = %@", logId)
        
        do {
            logs = (try moc.executeFetchRequest(fetchRequest)) as? [HealthDailyLog];
            if ( logs?.count  > 0){
                return logs![0];
            }
        } catch {
            print("Error Querying")
            return nil
        }
        return nil
        
    }

    
}
