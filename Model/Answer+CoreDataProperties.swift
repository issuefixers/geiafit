//
//  Answer+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/16/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Answer {

    @NSManaged var answerID: Int32
    @NSManaged var points: Int16
    @NSManaged var text: String?
    @NSManaged var question: Question?

}
