//
//  ActivityGoals+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/8/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ActivityGoals {

    @NSManaged var date: NSDate
    @NSManaged var totalSteps: Int64
    @NSManaged var lightTime: Int16
    @NSManaged var moderateTime: Int16
    @NSManaged var vigorousTime: Int16
    @NSManaged var instructions: String?
    @NSManaged var user: User?

}
