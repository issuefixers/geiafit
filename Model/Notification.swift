//
//  Notification.swift
//  
//
//  Created by Sergio Solorzano on 9/4/16.
//
//

import Foundation
import CoreData
import SwiftyJSON

class Notification: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    

    func newNotificationWithData(json: JSON){
        self.id = json["id"].stringValue;
        self.title = json["title"].stringValue;
        self.message = json["message"].stringValue;
        self.category = json["category"].stringValue;
        self.read = json["read"].numberValue;
        self.image_url = json["image_url"].stringValue;
        self.updated_at = NSDate.dateWithEpochTimestamp(json["updated"].doubleValue);
        self.uid = json["uid"].numberValue;
        self.created_at = NSDate.dateWithEpochTimestamp(json["created"].doubleValue);
    }
    
    class func getNotificationWithId(moc: NSManagedObjectContext, notificationId: String) -> Notification?{
        var notifications: [Notification]?
        
        let fetchRequest        = NSFetchRequest.init(entityName: "Notification")
        
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "id = %@", notificationId)
        
        do {
            notifications = (try moc.executeFetchRequest(fetchRequest)) as? [Notification];
            if (notifications?.count  > 0){
                let newNotification:Notification = notifications![0];
                newNotification.read = NSNumber(int: 0);
                return newNotification;
            }
        } catch {
            print("Error Querying")
            return nil
        }
        return nil
        
    }
    
    
    class func getBadgeOfNotifications(moc: NSManagedObjectContext) -> String?{
        var notifications: [Notification]?
        
        let fetchRequest        = NSFetchRequest.init(entityName: "Notification")
        
        fetchRequest.predicate  = NSPredicate(format: "read == 0")
        
        do {
            notifications = (try moc.executeFetchRequest(fetchRequest)) as? [Notification];
          
            if notifications?.count == 0  {
                return nil;
            }
            return "\(notifications!.count)";
        } catch {
            print("Error Querying")
            return nil;
        }
    }
}
