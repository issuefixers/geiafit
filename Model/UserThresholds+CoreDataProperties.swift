//
//  UserThresholds+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserThresholds {

    @NSManaged var stepsMin: Double
    @NSManaged var stepsLow: Double
    @NSManaged var stepsHigh: Double
    @NSManaged var heartRateLow: Double
    @NSManaged var heartRateHigh: Double
    @NSManaged var user: User?

}
