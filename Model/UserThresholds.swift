//
//  UserThresholds.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class UserThresholds: NSManagedObject {

    class func addThresholdsForUser(user: User, data: [String: AnyObject], moc: NSManagedObjectContext) -> UserThresholds? {
        var obj: UserThresholds?

        let stepsMin  = data["steps_min"]?.doubleValue
        let stepsLow  = data["steps_low"]?.doubleValue
        let stepsHigh = data["steps_high"]?.doubleValue
        let bpmLow    = data["hr_low"]?.doubleValue
        let bpmHigh   = data["hr_high"]?.doubleValue

        let fetchRequest           = NSFetchRequest.init(entityName: "UserThresholds")
        fetchRequest.fetchLimit    = 1
        fetchRequest.predicate     = NSPredicate(format: "user = %@", user)
        var objs: [UserThresholds] = []

        do {
            objs = (try moc.executeFetchRequest(fetchRequest)) as! [UserThresholds]
        } catch {
            print("Error Querying")
            return nil
        }

        if objs.count > 0 {
            //print("Found thresholds: \(objs)")
            obj = objs.first as UserThresholds?
        } else {
            obj              = NSEntityDescription.insertNewObjectForEntityForName("UserThresholds", inManagedObjectContext: moc) as? UserThresholds
            obj?.user        = user
            //print("Inserted thresholds: \(obj)")
        }

        if stepsMin != nil {
            obj?.stepsMin = stepsMin!
        }
        
        if stepsLow != nil {
            obj?.stepsLow = stepsLow!
        }

        if stepsHigh != nil {
            obj?.stepsHigh = stepsHigh!
        }

        if bpmLow != nil {
            obj?.heartRateLow = bpmLow!
        }

        if bpmHigh != nil {
            obj?.heartRateHigh = bpmHigh!
        }

        return obj
    }

}
