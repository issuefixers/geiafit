//
//  UserSurvey.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/16/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Foundation
import CoreData


class UserSurvey: NSManagedObject {

    class func getSurveyForUser(user: User, survey: Survey, moc: NSManagedObjectContext) -> UserSurvey? {
        let fetchRequest          = NSFetchRequest.init(entityName: "UserSurvey")
        fetchRequest.fetchLimit   = 1
        fetchRequest.predicate    = NSPredicate(format: "user = %@ AND survey = %@", user, survey)
        var objects: [UserSurvey] = []

        do {
            objects = (try moc.executeFetchRequest(fetchRequest)) as! [UserSurvey]
        } catch {
            print("Error Querying")
            return nil
        }

        let userSurvey: UserSurvey
        if objects.count > 0 {
            //print("Found surveys: \(surveys)")
            userSurvey = objects.first! as UserSurvey
        } else {
            userSurvey = NSEntityDescription.insertNewObjectForEntityForName("UserSurvey", inManagedObjectContext: moc) as! UserSurvey
            //print("Inserted survey: \(survey)")
        }

        userSurvey.user   = user
        userSurvey.survey = survey

        return userSurvey
    }
}
