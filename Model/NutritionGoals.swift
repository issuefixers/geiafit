//
//  NutritionGoals.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/19/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData


class NutritionGoals: NSManagedObject {


    class func addGoalsForUser(user: User, data: [String:AnyObject], moc: NSManagedObjectContext) -> NutritionGoals? {
        var goals: NutritionGoals?

        let calValue     = data["total_calories"]?.integerValue
        let fatValue     = data["fat_percent"]?.doubleValue
        let carbsValue   = data["carbs_percent"]?.doubleValue
        let proteinValue = data["protein_percent"]?.doubleValue
        let weightValue  = data["weight"]?.doubleValue
        let dateValue    = data["goal_date"]?.longLongValue
        let instructions = data["instructions"] as? String

        var goalDate: NSDate?
        if dateValue != nil {
            let aDate = Double(dateValue!)
            goalDate  = NSDate(timeIntervalSince1970: aDate)
        } else {
            goalDate = NSDate().dateAtStartOfDay()
        }

        let fetchRequest           = NSFetchRequest.init(entityName: "NutritionGoals")
        fetchRequest.fetchLimit    = 1
        fetchRequest.predicate     = NSPredicate(format: "user = %@ AND date = %@", user, goalDate!)
        var objs: [NutritionGoals] = []

        do {
            objs = try moc.executeFetchRequest(fetchRequest) as! [NutritionGoals]
        } catch {
            print("Error Querying")
            return nil
        }

        if objs.count > 0 {
            //print("Found goals: \(objs)")
            goals = objs.first as NutritionGoals?
        } else {
            goals       = NSEntityDescription.insertNewObjectForEntityForName("NutritionGoals", inManagedObjectContext: moc) as? NutritionGoals
            goals?.user = user
            goals?.date = goalDate!
            //print("Inserted goals: \(goals)")
        }

        if calValue != nil {
            goals?.totalCalories = Int32(calValue!)
        }

        if fatValue != nil {
            goals?.fatPercent = fatValue!
        }

        if carbsValue != nil {
            goals?.carbsPercent = carbsValue!
        }

        if proteinValue != nil {
            goals?.proteinPercent = proteinValue!
        }

        if weightValue != nil {
            goals?.weight = weightValue!
        }

        if instructions != nil {
            goals?.instructions = instructions!
        }

        return goals
    }

    class func getGoalsForUser(user: User, startDate: NSDate, endDate: NSDate, moc: NSManagedObjectContext) -> [NutritionGoals]? {
        let fetchRequest             = NSFetchRequest.init(entityName: "NutritionGoals")
        fetchRequest.fetchLimit      = 7
        fetchRequest.predicate       = NSPredicate(format: "user = %@", user)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        var goals: [NutritionGoals]?

        do {
            goals = try moc.executeFetchRequest(fetchRequest) as? [NutritionGoals]
        } catch {
            print("Error Querying")
            return nil
        }

        return goals
    }

    class func getLatestGoalsForUser(user: User, moc: NSManagedObjectContext) -> NutritionGoals? {
        var goals: NutritionGoals?

        let fetchRequest             = NSFetchRequest.init(entityName: "NutritionGoals")
        fetchRequest.fetchLimit      = 1
        fetchRequest.predicate       = NSPredicate(format: "user = %@", user)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        var objs: [NutritionGoals]    = []

        do {
            objs = try moc.executeFetchRequest(fetchRequest) as! [NutritionGoals]
        } catch {
            print("Error Querying")
            return nil
        }

        if objs.count > 0 {
            //print("Found goals: \(objs.first)")
            goals = objs.first as NutritionGoals?
        }

        //debugPrint("NG goals: \(goals)")
        return goals
    }

}
