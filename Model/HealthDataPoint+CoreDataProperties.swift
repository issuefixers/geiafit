//
//  HealthDataPoint+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/8/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension HealthDataPoint {

    @NSManaged var count:       Double
    @NSManaged var timestamp:   NSDate
    @NSManaged var type:        HealthDataType
    @NSManaged var deviceType:  HealthDataDeviceType
    @NSManaged var duration:    Double
    @NSManaged var level:       EffortLevel
    @NSManaged var user:        User?

}
