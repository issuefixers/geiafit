//
//  HealthDailyTotal.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData


class HealthDailyTotal: NSManagedObject {

    class func addDailyTotalsForUser(user: User, date: NSDate, moc: NSManagedObjectContext) -> HealthDailyTotal? {
        var obj: HealthDailyTotal

        let fetchRequest                = NSFetchRequest.init(entityName: "HealthDailyTotal")
        fetchRequest.fetchLimit         = 1
        fetchRequest.predicate          = NSPredicate(format: "user = %@ AND date = %@", user, date)
        var objects: [HealthDailyTotal] = []

        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDailyTotal]
        } catch {
            print("Error Querying")
            return nil
        }

        if objects.count > 0 {
            obj = objects.first! as HealthDailyTotal
            //print("Found HDT for date: \(obj.date)")
        } else {
            obj      = NSEntityDescription.insertNewObjectForEntityForName("HealthDailyTotal", inManagedObjectContext: moc) as! HealthDailyTotal
            obj.user = user
            obj.date = date
            //print("Inserted HDT for date \(obj.date)")
        }

        return obj
    }

    class func getTotalsForUser(user: User, startDate: NSDate, endDate: NSDate, moc: NSManagedObjectContext) -> [HealthDailyTotal]? {
        let fetchRequest             = NSFetchRequest.init(entityName: "HealthDailyTotal")
        fetchRequest.fetchLimit      = 7
        fetchRequest.predicate       = NSPredicate(format: "user = %@", user)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        var totals: [HealthDailyTotal]?

        do {
            totals = try moc.executeFetchRequest(fetchRequest) as? [HealthDailyTotal]
        } catch {
            print("Error Querying")
            return nil
        }

        return totals
    }

    class func getTodaysTotalsForUser(user: User, moc: NSManagedObjectContext) -> HealthDailyTotal? {
        var obj: HealthDailyTotal?

        let today                       = NSDate().dateAtStartOfDay()
        let fetchRequest                = NSFetchRequest.init(entityName: "HealthDailyTotal")
        fetchRequest.fetchLimit         = 1
        fetchRequest.predicate          = NSPredicate(format: "user = %@ AND date = %@", user, today)
        var objects: [HealthDailyTotal] = []

        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDailyTotal]
        } catch {
            print("Error Querying")
            return nil
        }

        if objects.count > 0 {
            //print("Found daily totals: \(objects)")
            obj = objects.first! as HealthDailyTotal
        } else {
            print("Didn't find it, inserting new one")
            obj = HealthDailyTotal.addDailyTotalsForUser(user, date: today, moc: moc)
        }

        return obj
    }

	
	class func getTotalsForUserByDate(date: NSDate, user: User, moc: NSManagedObjectContext) -> HealthDailyTotal? {
		var obj: HealthDailyTotal?
		
		let today                       = date.dateAtStartOfDay()
		let fetchRequest                = NSFetchRequest.init(entityName: "HealthDailyTotal")
		fetchRequest.fetchLimit         = 1
		fetchRequest.predicate          = NSPredicate(format: "user = %@ AND date = %@", user, today)
		var objects: [HealthDailyTotal] = []
		
		do {
			objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDailyTotal]
		} catch {
			print("Error Querying")
			return nil
		}
		
		if objects.count > 0 {
			//print("Found daily totals: \(objects)")
			obj = objects.first! as HealthDailyTotal
		} else {
			print("Didn't find it, inserting new one")
			obj = HealthDailyTotal.addDailyTotalsForUser(user, date: today, moc: moc)
		}
		
		return obj
	}
	
    func getTotalCaloriesBurned() -> Double {
        let total = self.activeCaloriesBurned + self.restingCaloriesBurned

        return total
    }
}
