//
//  Message+CoreDataProperties.swift
//  Geia
//
//  Created by Sergio Solorzano on 2/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Message {

    @NSManaged var id: String?
    @NSManaged var message: String?
    @NSManaged var receiver: NSNumber?
    @NSManaged var sender: NSNumber?
    @NSManaged var timestamp: String?
    @NSManaged var senderName: String?

}
