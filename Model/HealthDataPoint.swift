//
//  HealthDataPoint.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData

@objc enum HealthDataType: Int16 {
    case Steps, Calories, BPM
}

@objc enum EffortLevel: Int16 {
    case None, Light, Moderate, Vigorous
}

@objc enum HealthDataDeviceType: Int16 {
    case HealthKit, Manually, Fitbit
}

enum HealthDataDeviceStringType: String {
    case Manually = "Manually", Fitbit = "Fitbit"
}

class HealthDataPoint: NSManagedObject {

    /**
     Adds new HealthDataPoint for the user

     - parameter user:          User object
     - parameter count:         Double, number of steps/calories/beats in period
     - parameter timestamp:     Timestamp to be used to chart the datapoint
     - parameter type:          .Steps, .Calories, .BPM
     - parameter duration:      Double, duration of the period in seconds
     - parameter moc:           Managed Object Context
     - parameter deviceType:    .HealthKit, .Manually, .Fitbit
     
     - returns: Optional HealthDataPoint object, either found in the database based on user, timestamp and type or just inserted
     */
    
    class func addDataPointForUser(user: User, count: Double, timestamp: NSDate, type: HealthDataType, duration: Double, moc: NSManagedObjectContext, deviceType: HealthDataDeviceType) -> HealthDataPoint? {
        var obj: HealthDataPoint
        
        let fetchRequest               = NSFetchRequest.init(entityName: "HealthDataPoint")
        fetchRequest.fetchLimit        = 1
        fetchRequest.predicate         = NSPredicate(format: "user = %@ AND timestamp = %@ AND type = %@ AND deviceType = %@", user, timestamp, NSNumber(short: type.rawValue), NSNumber(short: deviceType.rawValue))
        var objects: [HealthDataPoint] = []

        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDataPoint]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return nil
        }

        if objects.count > 0 {
            print("Found data points: \(objects)")
            obj = objects.first! as HealthDataPoint
        } else {
            obj             = NSEntityDescription.insertNewObjectForEntityForName("HealthDataPoint", inManagedObjectContext: moc) as! HealthDataPoint
            obj.user        = user
            obj.timestamp   = timestamp
            obj.type        = type
            obj.deviceType  = deviceType
//            debugPrint("HDP - Inserted HealthDataPoint: userID:\(user.userID), type:\(type.rawValue), timestamp:\(obj.timestamp), steps:\(count)")
        }

        //update the count and duration
        obj.count    = count
        obj.duration = duration

        return obj
    }

    /**
     Queries the health data from the local CoreData DB. Returns all the data points for the date specified

     - parameter user: app user's object
     - parameter date: date to be queried
     - parameter type: HealthDataType value (Steps, Calories, BPM)
     - parameter moc:  ManagedObjectContext to be queried

     - returns: Array of HealthDataPoint objects, if no results are found it returns an empty array
     */
    class func getDataPointsForUser(user: User, date: NSDate, type: HealthDataType, moc: NSManagedObjectContext) -> [HealthDataPoint] {
        var objects: [HealthDataPoint] = []

        let timestampStart           = date.dateAtStartOfDay()
        let timestampEnd             = date.dateAtMidnight()
        let fetchRequest             = NSFetchRequest(entityName: "HealthDataPoint")
        fetchRequest.predicate       = NSPredicate(format: "user = %@ AND (timestamp >= %@ AND timestamp <= %@) AND type = %@", user, timestampStart, timestampEnd, NSNumber(short: type.rawValue))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: true)]
        //debugPrint("HDP - Querying datapoints. User:\(user.userID), Start:\(timestampStart), End:\(timestampEnd), type:\(type.rawValue)")

        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDataPoint]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return []
        }

        //debugPrint("HDP - found \(objects.count) datapoints")
        return objects
    }

    /**
     Deletes objects older than one week since the app doesn't use them.

     - parameter moc:  ManagedObjectContext to be purged
     */
    class func purgeOldDataPointsInContext(moc: NSManagedObjectContext) {
        let timestamp          = NSDate().dateAtStartOfDay().dateBySubtractingDays(7)
        let fetchRequest       = NSFetchRequest(entityName: "HealthDataPoint")
        fetchRequest.predicate = NSPredicate(format: "timestamp <= %@", timestamp!)

        var objects: [HealthDataPoint] = []
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDataPoint]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return
        }

        if objects.count > 0 {
            print("purging \(objects.count) HealtDataPoints")
            for object: HealthDataPoint in objects {
                moc.deleteObject(object)
            }
        }
    }
	
	
	class func purgeTodayDataPointsInContext(user: User,moc: NSManagedObjectContext,type: HealthDataType) {
		
		
		let timestampStart           = NSDate().dateAtStartOfDay()
		let timestampEnd             = NSDate().dateAtMidnight()
		let fetchRequest             = NSFetchRequest(entityName: "HealthDataPoint")
		fetchRequest.predicate       = NSPredicate(format: "user = %@ AND (timestamp >= %@ AND timestamp <= %@) AND type = %@", user, timestampStart, timestampEnd, NSNumber(short: type.rawValue))
		
		var objects: [HealthDataPoint] = []
		do {
			objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDataPoint]
		} catch let error as NSError {
			print("Error Querying: \(error)")
			return
		}
		
		if objects.count > 0 {
			print("purging \(objects.count) HealtDataPoints")
			for object: HealthDataPoint in objects {
				moc.deleteObject(object)
			}
		}
	}
    
    class func purgeDataPointsForDate(user: User,moc: NSManagedObjectContext,date:NSDate) {
        
        
        let timestampStart           = date.dateAtStartOfDay()
        let timestampEnd             = date.dateAtMidnight()
        let fetchRequest             = NSFetchRequest(entityName: "HealthDataPoint")
        fetchRequest.predicate       = NSPredicate(format: "user = %@ AND (timestamp >= %@ AND timestamp <= %@)", user, timestampStart, timestampEnd)
        
        var objects: [HealthDataPoint] = []
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDataPoint]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return
        }
        
        if objects.count > 0 {
            print("purging \(objects.count) HealtDataPoints")
            for object: HealthDataPoint in objects {
                moc.deleteObject(object)
            }
        }
    }
    
    /**
     Deletes Fitbit data.
     
     - parameter moc:  ManagedObjectContext to be purged
     */
    class func purgeFitbitDataPointsInContext(moc: NSManagedObjectContext) {

        let fetchRequest       = NSFetchRequest(entityName: "HealthDataPoint")
        fetchRequest.predicate = NSPredicate(format: "deviceType = %@", NSNumber(short: HealthDataDeviceType.Fitbit.rawValue))
        
        var objects: [HealthDataPoint] = []
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDataPoint]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return
        }
        
        if objects.count > 0 {
            print("purging \(objects.count) HealtDataPoints")
            for object: HealthDataPoint in objects {
                moc.deleteObject(object)
            }
        }
    }
    
    class func purgeAllDataPointsInContext(moc: NSManagedObjectContext) {
        
        let fetchRequest       = NSFetchRequest(entityName: "HealthDataPoint")
        
        var objects: [HealthDataPoint] = []
        do {
            objects = try moc.executeFetchRequest(fetchRequest) as! [HealthDataPoint]
        } catch let error as NSError {
            print("Error Querying: \(error)")
            return
        }
        
        if objects.count > 0 {
            print("purging \(objects.count) HealtDataPoints")
            for object: HealthDataPoint in objects {
                moc.deleteObject(object)
            }
        }
    }

}
