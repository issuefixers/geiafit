//
//  Posture.swift
//  Geia
//
//  Created by haley on 5/31/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class Posture: NSManagedObject {
 
	// Insert code here to add functionality to your managed object subclass
	
	class func addPostureWithData(dic:NSDictionary,moc: NSManagedObjectContext){
		
		let fetchRequest          = NSFetchRequest.init(entityName: "Posture")
		fetchRequest.fetchLimit   = 1
		fetchRequest.predicate    = NSPredicate(format:"postureCreateDate = %@", dic.objectForKey("createDate") as! NSString as String)
		
		var postureTemp:[Posture] = []
		
		do {
			postureTemp = (try moc.executeFetchRequest(fetchRequest)) as! [Posture]
		} catch {
			
		}
		
		if postureTemp.count > 0 {
			
		} else {
			
			let posture : Posture!
			posture         = NSEntityDescription.insertNewObjectForEntityForName("Posture", inManagedObjectContext: moc) as? Posture
			
			posture.userID = (dic.objectForKey("userId")?.longLongValue)!
			posture.postureCreateDate = dic.objectForKey("createDate") as! NSString as String
			try! moc.save()
		}
	}
	
	class func queryPostureData(userId:Int64,moc: NSManagedObjectContext) -> NSMutableArray? {
		
		let fetchRequest          = NSFetchRequest.init(entityName: "Posture")
		
		fetchRequest.predicate    = NSPredicate(format: "userID = %lld", userId)
		
		var postureTemp:[Posture] = []
		
		do {
			postureTemp = (try moc.executeFetchRequest(fetchRequest)) as! [Posture]
		} catch {
			
		}
		
		if postureTemp.count > 0 {
			let array : NSMutableArray = NSMutableArray()
			for item in postureTemp {
				let posture : PostureReportInfo = PostureReportInfo.init()
				posture.report_created = item.postureCreateDate
				array.addObject(posture)
			}
			
			return array
		} else {
			return nil
		}
		
		
		
	}
}
