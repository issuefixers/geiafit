//
//  ActivityGoals.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/8/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData

class ActivityGoals: NSManagedObject {

    class func addGoalsForUser(user: User, data: [String:AnyObject], moc: NSManagedObjectContext) -> ActivityGoals? {
        var goals: ActivityGoals?

        let stepsValue   = data["total_steps"]?.integerValue
        let highValue    = data["time_active_high"]?.integerValue
        let midValue     = data["time_active_medium"]?.integerValue
        let lowValue     = data["time_active_low"]?.integerValue
        let dateValue    = data["goal_date"]?.longLongValue
        let instructions = data["instructions"] as? String

        var goalDate: NSDate?
        if dateValue != nil {
            let aDate = Double(dateValue!)
            goalDate  = NSDate(timeIntervalSince1970: aDate)
        } else {
            goalDate = NSDate().dateAtStartOfDay()
        }

        let fetchRequest           = NSFetchRequest.init(entityName: "ActivityGoals")
        fetchRequest.fetchLimit    = 1
        fetchRequest.predicate     = NSPredicate(format: "user = %@ AND date = %@", user, goalDate!)
        var objs: [ActivityGoals] = []

        do {
            objs = (try moc.executeFetchRequest(fetchRequest)) as! [ActivityGoals]
        } catch {
            print("Error Querying")
            return nil
        }

        if objs.count > 0 {
            //print("Found goals: \(objs)")
            goals = objs.first as ActivityGoals?
        } else {
            goals       = NSEntityDescription.insertNewObjectForEntityForName("ActivityGoals", inManagedObjectContext: moc) as? ActivityGoals
            goals?.user = user
            goals?.date = goalDate!
            //print("Inserted goals: \(goals)")
        }

        if stepsValue != nil {
            goals?.totalSteps = Int64(stepsValue!)
        }

        if highValue != nil {
            goals?.vigorousTime = Int16(highValue!)
        }

        if midValue != nil {
            goals?.moderateTime = Int16(midValue!)
        }

        if lowValue != nil {
            goals?.lightTime = Int16(lowValue!)
        }

        if instructions != nil {
            goals?.instructions = instructions
        }

        return goals
    }

    class func getGoalsForUser(user: User, date: NSDate, moc: NSManagedObjectContext) -> ActivityGoals? {
        var goal: ActivityGoals?

        let timestampEnd             = date.dateAtMidnight()
        let fetchRequest             = NSFetchRequest.init(entityName: "ActivityGoals")
        fetchRequest.fetchLimit      = 1
        fetchRequest.predicate       = NSPredicate(format: "user = %@ AND date <= %@", user, timestampEnd)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        var objs: [ActivityGoals]    = []

        do {
            objs = (try moc.executeFetchRequest(fetchRequest)) as! [ActivityGoals]
        } catch {
            print("Error Querying")
            return nil
        }

        if objs.count > 0 {
            goal = objs.first as ActivityGoals?
        }

        return goal
    }

    class func getLatestGoalsForUser(user: User, moc: NSManagedObjectContext) -> ActivityGoals? {
        var goals: ActivityGoals?
        //debugPrint("AG getting goals")
        let fetchRequest             = NSFetchRequest.init(entityName: "ActivityGoals")
        fetchRequest.fetchLimit      = 1
        fetchRequest.predicate       = NSPredicate(format: "user = %@", user)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        var objs: [ActivityGoals]    = []

        do {
            objs = (try moc.executeFetchRequest(fetchRequest)) as! [ActivityGoals]
        } catch {
            print("Error Querying")
            return nil
        }

        if objs.count > 0 {
            //print("Found goals: \(objs)")
            goals = objs.first as ActivityGoals?
        }

        //debugPrint("AG goals: \(goals)")
        return goals
    }
}
