//
//  Notification+CoreDataProperties.swift
//  
//
//  Created by Sergio Solorzano on 9/4/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Notification {

    @NSManaged var id: String?
    @NSManaged var created_at: NSDate?
    @NSManaged var updated_at: NSDate?
    @NSManaged var read: NSNumber?
    @NSManaged var category: String?
    @NSManaged var message: String?
    @NSManaged var uid: NSNumber?
    @NSManaged var title: String?
    @NSManaged var image_url: String?

}
