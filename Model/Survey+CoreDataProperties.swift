//
//  Survey+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/21/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Survey {

    @NSManaged var disclaimer: String?
    @NSManaged var surveyID: Int32
    @NSManaged var text: String?
    @NSManaged var title: String?
    @NSManaged var users: NSSet?
    @NSManaged var theQuestions: NSSet?

}
