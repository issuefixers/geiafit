//
//  Question.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/16/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class Question: NSManagedObject {

    class func addQuestionForSurvey(survey: Survey, data: JSON, moc: NSManagedObjectContext) {
        var question: Question

        let questionID = data["question_id"].int32Value
        let text       = data["question"].stringValue
        let answers    = data["answers"].arrayValue

        let fetchRequest          = NSFetchRequest.init(entityName: "Question")
        fetchRequest.fetchLimit   = 1
        fetchRequest.predicate    = NSPredicate(format: "survey = %@ AND questionID = %d", survey, questionID)
        var questions: [Question] = []

        do {
            questions = (try moc.executeFetchRequest(fetchRequest)) as! [Question]
        } catch {
            print("Error Querying")
            return
        }

        if questions.count > 0 {
            //print("Found questions: \(questions)")
            question = questions.first! as Question
        } else {
            question            = NSEntityDescription.insertNewObjectForEntityForName("Question", inManagedObjectContext: moc) as! Question
            question.survey  = survey
            question.questionID = questionID
            //print("Inserted question: \(question)")
        }

        question.text  = text

        //delete all current answers for question
        let allAnswers = question.answers?.allObjects
        for object in allAnswers! {
            let answer = object as! Answer
            moc.deleteObject(answer)
        }

        //add new ones
        for answerJSON: JSON in answers {
            Answer.addAnswerWithData(answerJSON, question: question, moc: moc)
        }

        return
    }

}
