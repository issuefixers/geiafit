//
//  Survey.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/16/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class Survey: NSManagedObject {

    class func addSurveyWithData(data: JSON, moc: NSManagedObjectContext) -> Survey? {
        var survey: Survey

        let surveyID    = data["nid"].int32Value
        let title       = data["title"].stringValue
        let description = data["description"].stringValue
        let disclaimer  = data["disclaimer"].stringValue
        let questions   = data["questions"].arrayValue

        let fetchRequest        = NSFetchRequest.init(entityName: "Survey")
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "surveyID = %d", surveyID)
        var surveys: [Survey]   = []

        do {
            surveys = (try moc.executeFetchRequest(fetchRequest)) as! [Survey]
        } catch {
            print("Error Querying")
            return nil
        }

        if surveys.count > 0 {
            print("Found surveys: \(surveys)")
            survey = surveys.first! as Survey
        } else {
            survey          = NSEntityDescription.insertNewObjectForEntityForName("Survey", inManagedObjectContext: moc) as! Survey
            survey.surveyID = surveyID
            print("Inserted survey: \(survey)")
        }

        survey.title      = title
        survey.text       = description
        survey.disclaimer = disclaimer

        for questionData: JSON in questions {
            Question.addQuestionForSurvey(survey, data: questionData, moc: moc)
        }

        return survey
    }

    func getQuestions() -> [Question] {
        let questions: [Question] = self.theQuestions?.sortedArrayUsingDescriptors([NSSortDescriptor.init(key: "questionID", ascending: true)]) as! [Question]

        return questions
    }

}
