//
//  Posture+CoreDataProperties.swift
//  Geia
//
//  Created by haley on 5/31/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Posture {
	
	@NSManaged var postureCreateDate: String?
	@NSManaged var userID: Int64
	
}
