//
//  Answer.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/16/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class Answer: NSManagedObject {

    class func addAnswerWithData(data: JSON, question: Question, moc: NSManagedObjectContext) {
        var answer: Answer

        let points   = data["points"].int16Value
        let text     = data["answer"].stringValue

        answer          = NSEntityDescription.insertNewObjectForEntityForName("Answer", inManagedObjectContext: moc) as! Answer
        answer.points   = points
        answer.text     = text
        answer.question = question

        return
    }

}
