//
//  HealthDailyLog+CoreDataProperties.swift
//  
//
//  Created by Sergio Solorzano on 9/4/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension HealthDailyLog {

    @NSManaged var activity: NSNumber!
    @NSManaged var created_date: NSDate?
    @NSManaged var daily_challenge: NSNumber!
    @NSManaged var daily_points: NSNumber!
    @NSManaged var updated_date: NSDate?
    @NSManaged var uid: NSNumber?
    @NSManaged var pt_date: NSDate?
    @NSManaged var id: String?
    @NSManaged var goals: NSNumber!;
    @NSManaged var exercise: NSNumber!;

}
