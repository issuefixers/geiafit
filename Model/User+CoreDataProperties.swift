//
//  User+CoreDataProperties.swift
//  Geia
//
//  Created by zurce on 6/24/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var birthday: NSTimeInterval
    @NSManaged var email: String?
    @NSManaged var firstName: String?
    @NSManaged var gender: Int16
    @NSManaged var goals: Int16
    @NSManaged var lastName: String?
    @NSManaged var lastPicURLDownloaded: String?
    @NSManaged var picture: NSData?
    @NSManaged var pictureURL: String?
    @NSManaged var userID: Int64
    @NSManaged var username: String?
    @NSManaged var gems: Int16
    @NSManaged var onboarded: NSNumber?
    @NSManaged var activityGoals: NSSet?
    @NSManaged var characteristics: NSSet?
    @NSManaged var healthDailyTotals: NSSet?
    @NSManaged var healthDataPoints: NSSet?
    @NSManaged var nutritionGoals: NSSet?
    @NSManaged var surveys: NSSet?
    @NSManaged var thresholds: UserThresholds?
    @NSManaged var userAnswers: NSSet?
    @NSManaged var therapist: Therapist?


}
