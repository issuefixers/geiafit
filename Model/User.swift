//
//  User.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/7/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import Alamofire
import SwiftyJSON
import UIKit

struct UserGoals: OptionSetType {
    let rawValue: Int16

    static let None                 = UserGoals(rawValue: 0)
    static let Weightloss           = UserGoals(rawValue: 1 << 0)
    static let EatHealthy           = UserGoals(rawValue: 1 << 1)
    static let PainRelief           = UserGoals(rawValue: 1 << 2)
    static let ImprovePosture       = UserGoals(rawValue: 1 << 3)
    static let ImproveOverallHealth = UserGoals(rawValue: 1 << 4)
    static let ManageRecovery       = UserGoals(rawValue: 1 << 5)
}

struct Gender: OptionSetType {
    let rawValue: Int16

    static let NotSet = Gender(rawValue: 0)
    static let Female = Gender(rawValue: 1 << 0)
    static let Male   = Gender(rawValue: 1 << 1)
}

class User: NSManagedObject {

    class func getUserWithData(data: JSON, moc: NSManagedObjectContext) -> User? {
        var user: User?

        let userID   = data["uid"].int64Value
        let username = data["name"].stringValue
        let email    = data["mail"].stringValue
        let boolOnboarded = data["field_onboarded"].dictionary!["und"]?.array?.first?.dictionary!["value"]?.numberValue;
        print(boolOnboarded);
        let fetchRequest        = NSFetchRequest.init(entityName: "User")
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "userID = %d", userID)
        var users: [User]       = []

        do {
            users = (try moc.executeFetchRequest(fetchRequest)) as! [User]
        } catch {
            print("Error Querying")
            return nil
        }

        if users.count > 0 {
            //print("Found users: \(users)")
            user = users.first as User?
        } else {
            user         = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: moc) as? User
            user?.userID = userID
            user?.goals  = UserGoals.None.rawValue
            user?.gender = Gender.NotSet.rawValue
            //print("Inserted user: \(user)")
        }

        user?.username = username
        user?.email    = email

        if boolOnboarded != nil {
            user?.onboarded = boolOnboarded;
        }else{
            user?.onboarded = 0;
        }
        return user
    }

    class func getUserWithID(userID: Int, moc: NSManagedObjectContext) -> User? {
        var user: User?

        let fetchRequest        = NSFetchRequest.init(entityName: "User")
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate  = NSPredicate(format: "userID = %d", userID)
        var users: [User]       = []

        do {
            users = (try moc.executeFetchRequest(fetchRequest)) as! [User]
        } catch {
            print("Error Querying")
        }

        if users.count > 0 {
            user = users.first as User?
        }

        return user
    }

    func addProfileWithData(json: JSON,moc: NSManagedObjectContext ) {
        let firstName = json["first_name"]
        let lastName  = json["last_name"]
        let birthday  = json["dob"]
        let gender    = json["gender"]
        let picURL    = json["image"]
        let goals     = json["goals"].arrayValue
        let gems      = json["gems"]

        //Therapist 
        let therapist_name = json["therapist_first_name"];
        let therapist_last_name = json["therapist_last_name"];
        let therapist_uid = json["therapist_uid"];
        let therapist_image = json["therapist_image"];
        
        self.therapist = Therapist.getTherapistWithId(moc, therapistId: therapist_uid.stringValue)
        
        if self.therapist == nil {
            self.therapist = NSEntityDescription.insertNewObjectForEntityForName("Therapist", inManagedObjectContext: moc) as? Therapist

        }
        
        self.therapist?.id = therapist_uid.stringValue;
        self.therapist?.first_name = therapist_name.stringValue;
        self.therapist?.last_name = therapist_last_name.stringValue;
        self.therapist?.avatar = therapist_image.stringValue;
        
        
        
        
        if firstName != nil {
            self.firstName = firstName.stringValue
        }

        if lastName != nil {
            self.lastName = lastName.stringValue
        }

        if birthday != nil {
            self.birthday = birthday.doubleValue
//            print("dob: \(self.birthday)")
        }

        if gender != nil {
            self.gender = gender.int16Value
//            print("is dude = \(self.gender == Gender.Male.rawValue), is lady = \(self.gender == Gender.Female.rawValue)")
        }

        if picURL != nil {
            self.pictureURL = picURL.stringValue.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet());
        }

        
        
        
        var goalsValue = UserGoals.None.rawValue
        for goal: JSON in goals {

            if goal.stringValue == WEIGHTLOSS {
                goalsValue += UserGoals.Weightloss.rawValue
                continue
            }

//            if goal.stringValue == EAT_HEALTHY {
//                goalsValue += UserGoals.EatHealthy.rawValue
//                continue
//            }

            if goal.stringValue == PAIN_RELIEF {
                goalsValue += UserGoals.PainRelief.rawValue
                continue
            }

            if goal.stringValue == IMPROVE_POSTURE {
                goalsValue += UserGoals.ImprovePosture.rawValue
                continue
            }

            if goal.stringValue == IMPROVE_OVERALL_HEALTH {
                goalsValue += UserGoals.ImproveOverallHealth.rawValue
                continue
            }

            if goal.stringValue == MANAGE_RECOVERY {
                goalsValue += UserGoals.ManageRecovery.rawValue
                continue
            }
        }
        self.goals = goalsValue
        
        if gems != nil {
            self.gems = gems.int16Value
        }
    }

    func addCharacteristicsWithData(data: JSON, moc: NSManagedObjectContext) {
        let characteristics: Characteristics = Characteristics.getCharacteristicsForUser(self, data: data, moc: moc)!
        characteristics.user = self
    }

    func getHeight() -> Float {
        if let characteristics = Characteristics.getLatestCharacteristicsForUser(self, moc: self.managedObjectContext!) {
            return (characteristics.height?.floatValue)!
        }
        return 0
    }

    func getWeight() -> Float {
        if let characteristics = Characteristics.getLatestCharacteristicsForUser(self, moc: self.managedObjectContext!) {
            return (characteristics.weight?.floatValue)!
        }
        return 0.0
    }

    func downloadPicture(completion: () -> Void) {
        if self.pictureURL != nil || self.pictureURL?.characters.count == 0 || self.pictureURL == self.lastPicURLDownloaded {
//            request(.GET, self.pictureURL!).responseData({ (response) -> Void in
//
//            })
            
            print(self.pictureURL);
            
            let stringForDownload = self.pictureURL?.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.controlCharacterSet())
            ;
            request(.GET, stringForDownload!).responseData(completionHandler: { (response) -> Void in
                                //print(response.request)
                                //print(response.response)
                                //debugPrint(response.result)
                
                                if response.response?.statusCode == 200 {
                                    if let imageData = response.result.value {
                                        self.picture              = imageData
                                        self.lastPicURLDownloaded = self.pictureURL
                
                                        completion()
                                    }
                                }
            })
        }
    }

    func pictureAsImage() -> UIImage? {
        var img: UIImage?

        if self.picture != nil {
            img = UIImage(data: self.picture!)
        }

        return img
    }
}
