//
//  Characteristics+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/11/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Characteristics {

    @NSManaged var bloodPressureDia: NSNumber?
    @NSManaged var bloodPressureSys: NSNumber?
    @NSManaged var bmi: NSNumber?
    @NSManaged var bodyFat: NSNumber?
    @NSManaged var createdDate: NSDate?
    @NSManaged var height: NSNumber?
    @NSManaged var recordDate: NSDate?
    @NSManaged var restingHeartRate: NSNumber?
    @NSManaged var maxHeartRate: NSNumber?
    @NSManaged var weight: NSNumber?
    @NSManaged var emotion: NSNumber?
    @NSManaged var user: User?

}
