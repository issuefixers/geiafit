//
//  HealthDailyTotal+CoreDataProperties.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension HealthDailyTotal {

    @NSManaged var steps: Double
    @NSManaged var customSteps: Double
    @NSManaged var activeCaloriesBurned: Double
    @NSManaged var restingCaloriesBurned: Double
    @NSManaged var caloriesConsumed: Double
    @NSManaged var carbs: Double
    @NSManaged var fat: Double
    @NSManaged var protein: Double
    @NSManaged var date: NSDate
    @NSManaged var user: User?

}
