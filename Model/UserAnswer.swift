//
//  UserAnswer.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/16/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Foundation
import CoreData


class UserAnswer: NSManagedObject {

    class func addAnswerForUser(user: User, question: Question, points: Int, moc: NSManagedObjectContext) {
        var obj: UserAnswer

        let fetchRequest          = NSFetchRequest.init(entityName: "UserAnswer")
        fetchRequest.fetchLimit   = 1
        fetchRequest.predicate    = NSPredicate(format: "user = %@ AND question = %@", user, question)
        var objects: [UserAnswer] = []

        do {
            objects = (try moc.executeFetchRequest(fetchRequest)) as! [UserAnswer]
        } catch {
            print("Error Querying")
            return
        }

        if objects.count > 0 {
            //print("Found user-answers: \(objects)")
            obj = objects.first! as UserAnswer
        } else {
            obj          = NSEntityDescription.insertNewObjectForEntityForName("UserAnswer", inManagedObjectContext: moc) as! UserAnswer
            obj.user     = user
            obj.question = question
            //print("Inserted user-answer: \(obj)")
        }

        obj.points = Int16.init(points)

        return
    }

    class func getAnswerForUser(user: User, question: Question, moc: NSManagedObjectContext) -> UserAnswer? {
        var obj: UserAnswer?

        let fetchRequest          = NSFetchRequest.init(entityName: "UserAnswer")
        fetchRequest.fetchLimit   = 1
        fetchRequest.predicate    = NSPredicate(format: "user = %@ AND question = %@", user, question)
        var objects: [UserAnswer] = []

        do {
            objects = (try moc.executeFetchRequest(fetchRequest)) as! [UserAnswer]
        } catch {
            print("Error Querying")
            return nil
        }

        if objects.count > 0 {
            //print("Found user-answers: \(objects)")
            obj = objects.first! as UserAnswer
        }

        return obj
    }
}
