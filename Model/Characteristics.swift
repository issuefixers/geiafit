//
//  Characteristics.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/11/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class Characteristics: NSManagedObject {

    class func getCharacteristicsForUser(user: User, data: JSON, moc: NSManagedObjectContext) -> Characteristics? {
        var char: Characteristics?

		
		var height      = -1
		var weight  :Float    = -1
		var bpm         = -1
		var maxBPM      = -1
		var bmi         = -1
		var bodyFat     = -1
		var bpSys       = -1
		var bpDia       = -1
		var emotion :Float  = -1
		
		if (data["height"] != nil)  {
			height = data["height"].intValue
		}
		
		if (data["weight"] != nil)  {
			weight = data["weight"].floatValue
		}
		
		if (data["resting_heart_rate"] != nil)  {
			bpm  = data["resting_heart_rate"].intValue
		}
		
		if (data["max_heart_rate"] != nil)  {
			maxBPM  = data["max_heart_rate"].intValue
		}
		
		
		if (data["bmi"] != nil)  {
			bmi  = data["bmi"].intValue
		}
		
		if (data["body_fat"] != nil)  {
			bodyFat  = data["body_fat"].intValue
		}
		
		if (data["blood_pressure_sys"] != nil)  {
			bpSys  = data["blood_pressure_sys"].intValue
		}

		if (data["blood_pressure_dia"] != nil)  {
			bpDia  = data["blood_pressure_dia"].intValue
		}

		if (data["emotion"] != nil) {
			emotion = data["emotion"].floatValue
		}
		
		
        let createdDate = NSDate(timeIntervalSince1970: data["date_created"].doubleValue)
        let recordDate  = NSDate(timeIntervalSince1970: data["record_date"].doubleValue)

		
//		print(data["record_date"],emotion,bmi,maxBPM,height,weight,recordDate,createdDate)
		
        let fetchRequest             = NSFetchRequest.init(entityName: "Characteristics")
        fetchRequest.fetchLimit      = 1
        fetchRequest.predicate       = NSPredicate(format: "user = %@ AND createdDate = %@", user, createdDate)
        var chars: [Characteristics] = []

        do {
            chars = (try moc.executeFetchRequest(fetchRequest)) as! [Characteristics]
        } catch {
            print("Error Querying")
            return nil
        }

        if chars.count > 0 {
            //print("Found characteristics: \(chars)")
            char = chars.first as Characteristics?
        } else {
            char              = NSEntityDescription.insertNewObjectForEntityForName("Characteristics", inManagedObjectContext: moc) as? Characteristics
            char?.user        = user
            char?.createdDate = createdDate
            //print("Inserted characteristics: \(chars)")
        }

        char?.height           = height
        char?.weight           = weight
        char?.restingHeartRate = bpm
        char?.maxHeartRate     = maxBPM
        char?.bmi              = bmi
        char?.bodyFat          = bodyFat
        char?.bloodPressureSys = bpSys
        char?.bloodPressureDia = bpDia
        char?.recordDate       = recordDate
		char?.emotion = emotion
        return char
    }

    class func getLatestCharacteristicsForUser(user: User, moc: NSManagedObjectContext) -> Characteristics? {
        var char: Characteristics?

        let fetchRequest             = NSFetchRequest.init(entityName: "Characteristics")
        fetchRequest.fetchLimit      = 1
        fetchRequest.predicate       = NSPredicate(format: "user = %@", user)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "createdDate", ascending: false)]
        var chars: [Characteristics] = []

        do {
            chars = (try moc.executeFetchRequest(fetchRequest)) as! [Characteristics]
        } catch {
            print("Error Querying")
            return nil
        }

        if chars.count > 0 {
            //print("Found characteristics: \(chars)")
            char = chars.first as Characteristics?
        }

        return char
    }

    class func getCharacteristicsForUser(user: User, startDate: NSDate, endDate: NSDate, moc: NSManagedObjectContext) -> [Characteristics]? {
        let fetchRequest             = NSFetchRequest.init(entityName: "Characteristics")
        fetchRequest.fetchLimit      = 7
        fetchRequest.predicate       = NSPredicate(format: "user = %@", user)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "createdDate", ascending: false)]
        var chars: [Characteristics]?
        
        do {
            chars = try moc.executeFetchRequest(fetchRequest) as? [Characteristics]
        } catch {
            print("Error Querying")
            return nil
        }
        
        return chars
    }

	class func getCharacteristicsForUserForDate(user: User, startDate: NSDate, endDate: NSDate, moc: NSManagedObjectContext) -> [Characteristics]? {
		
		let limit = Common.earlyDays(startDate)
		
		let fetchRequest             = NSFetchRequest.init(entityName: "Characteristics")
		fetchRequest.fetchLimit      = limit
		fetchRequest.predicate       = NSPredicate(format: "user = %@", user)
		fetchRequest.sortDescriptors = [NSSortDescriptor(key: "createdDate", ascending: false)]
		var chars: [Characteristics]?
		
		do {
			chars = try moc.executeFetchRequest(fetchRequest) as? [Characteristics]
		} catch {
			print("Error Querying")
			return nil
		}
		
		return chars
	}
	
    class func getTodaysCharacteristicsForUser(user: User, moc: NSManagedObjectContext) -> Characteristics? {
        let latest = Characteristics.getLatestCharacteristicsForUser(user, moc: moc)

        //check if it's today's
        if latest != nil && latest!.createdDate!.isEqualToDateIgnoringTime(NSDate()) {
            return latest
        }

        //there wasn't any data or it wasn't today's... create one
        let char: Characteristics?
        char              = NSEntityDescription.insertNewObjectForEntityForName("Characteristics", inManagedObjectContext: moc) as? Characteristics
        char?.user        = user
        char?.createdDate = NSDate().dateAtStartOfDay()

        //inherit data from the latest 
        if latest != nil {
            char?.height           = latest!.height
            char?.weight           = latest!.weight
            char?.bloodPressureDia = latest!.bloodPressureDia
            char?.bloodPressureSys = latest!.bloodPressureSys
            char?.bmi              = latest!.bmi
            char?.bodyFat          = latest!.bodyFat
            char?.bloodPressureDia = latest!.bloodPressureDia
            char?.restingHeartRate = latest!.restingHeartRate
            char?.maxHeartRate     = latest!.maxHeartRate
            char?.emotion          = latest!.emotion
		} else {
			char?.height           = 0
			char?.weight           = -1
			char?.bloodPressureDia = 0
			char?.bloodPressureSys = -1
			char?.bmi              = -1
			char?.bodyFat          = 0
			char?.bloodPressureDia = 0
			char?.restingHeartRate = -1
			char?.maxHeartRate     = -1
			char?.emotion          = -1
		}

        return char
    }

    func getRecordDateAsInteger() -> Int {
        let number = Int((self.recordDate?.timeIntervalSince1970)!)

        return number
    }

    func getCreatedDateAsInteger() -> Int {
        let number = Int((self.createdDate?.timeIntervalSince1970)!)

        return number
    }
}
