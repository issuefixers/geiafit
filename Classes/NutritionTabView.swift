//
//  NutritionTabView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/24/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class NutritionTabView: UIView {

    // MARK: - Properties
    @IBOutlet weak var calories: UILabel!
    @IBOutlet weak var fat:      UILabel!
    @IBOutlet weak var carbs:    UILabel!
    @IBOutlet weak var protein:  UILabel!
    @IBOutlet weak var weight:   UILabel!
    @IBOutlet weak var instructions: UILabel!
    
    weak var alertViewDelegate: AlertViewDelegate?
    private let prescriptionsViewModel = PrescriptionsViewModel()
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    private lazy var dateFormatter:NSDateFormatter = {
        let df       = NSDateFormatter()
        df.locale    = NSLocale.currentLocale()
        df.timeStyle = .NoStyle
        df.dateStyle = .MediumStyle
        
        return df
    }()
    
    private lazy var numberFormatter:NSNumberFormatter = {
        let nf         = NSNumberFormatter()
        nf.locale      = NSLocale.currentLocale()
        nf.numberStyle = .DecimalStyle
        
        return nf
    }()
    
    // MARK: - Methods
    func getContentFromServerForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        self.loaderView.show()
        let goalTrackingViewModel = GoalTrackingViewModel()
        goalTrackingViewModel.getNutritionGoalsForUser(user) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool
            
            if success {
                guard let data = result["data"] as? [[String:AnyObject]] else {
                    print("couldn't parse the data in the response:\n\(result)")
                    self.calories.text = "N/A"
                    self.fat.text      = "N/A"
                    self.carbs.text    = "N/A"
                    self.protein.text  = "N/A"
                    self.weight.text   = "N/A"
                    self.instructions.text   = "N/A"

                    return
                }

                for nutritionData in data {
                    NutritionGoals.addGoalsForUser(user, data: nutritionData, moc: self.appDelegate.managedObjectContext)
                }

                //save changes
                self.appDelegate.saveContext()

                if let nutritionGoals = NutritionGoals.getLatestGoalsForUser(user, moc: self.appDelegate.managedObjectContext) {
                    //debugPrint(nutritionGoals)

                    let calories = Int(nutritionGoals.totalCalories)
                    if calories > 0 {
                        self.calories.text = "\((self.numberFormatter.stringFromNumber(calories))!) cal"
                    } else {
                        self.calories.text = "N/A"
                    }

                    let fat = nutritionGoals.fatPercent
                    if fat > 0.0 {
                        self.fat.text = "\((self.numberFormatter.stringFromNumber(fat))!)g"
                    } else {
                        self.fat.text = "N/A"
                    }

                    let carbs = nutritionGoals.carbsPercent
                    if carbs > 0.0 {
                        self.carbs.text = "\((self.numberFormatter.stringFromNumber(carbs))!)g"
                    } else {
                        self.carbs.text = "N/A"
                    }

                    let protein = nutritionGoals.proteinPercent
                    if protein > 0.0 {
                        self.protein.text = "\((self.numberFormatter.stringFromNumber(protein))!)g"
                    } else {
                        self.protein.text = "N/A"
                    }

                    let weight = nutritionGoals.weight
                    if weight > 0.0 {
                        self.weight.text = "\((self.numberFormatter.stringFromNumber(weight))!) lbs"
                    } else {
                        self.weight.text = "N/A"
                    }

                    if let instructions = nutritionGoals.instructions {
                        self.instructions.text = instructions
                    } else {
                        self.instructions.text = "N/A"
                    }
                    
                    let formattedDate = self.dateFormatter.stringFromDate(nutritionGoals.date)
                    completion(result: ["success":true, "date":formattedDate])
                }
            } else {
                let message = result["errorMessage"] as! String
                let alert: UIAlertController = UIAlertController(title: "Posture", message: message, preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                self.alertViewDelegate?.showAlert(alert, sender: self)
            }
        }
    }
    
}
