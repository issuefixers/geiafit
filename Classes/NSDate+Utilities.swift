//
//  NSDate+Utilities.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/9/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

extension NSDate {

    // MARK: - Class methods
    class func dateWithEpochTimestamp(epochTimestamp: Double) -> NSDate {
        let theDate = NSDate(timeIntervalSince1970: epochTimestamp)

        return theDate
    }

    class func dateWithDaysFromNow(days: Int) -> NSDate {
        return NSDate().dateByAddingDays(days)!
    }
    
    
    class func dateWithDaysBeforeNow(days: Int) -> NSDate {
        return NSDate().dateBySubtractingDays(days)!
    }

    class func dateTomorrow() -> NSDate {
        return NSDate.dateWithDaysFromNow(1)
    }

    class func dateYesterday() -> NSDate {
        return NSDate.dateWithDaysBeforeNow(1)
    }

    class func dateWithDay(day: Int, month: Int, year: Int) -> NSDate {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components                     = NSCalendar.currentCalendar().components(componentFlags, fromDate: NSDate())

        components.day    = day
        components.month  = month
        components.year   = year
        components.hour   = 0
        components.minute = 0
        components.second = 0

        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }

    // MARK: - Instance methods
    func epochTimestamp() -> String {
        let ts    = self.timeIntervalSince1970
        let value = ts < 1 ? "" : "\(ts)"

        return value
    }

    func isEqualToDateIgnoringTime(aDate: NSDate) -> Bool {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month]

        let components1 = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)
        let components2 = NSCalendar.currentCalendar().components(componentFlags, fromDate: aDate)

        let isEqual = ((components1.year == components2.year) &&
                       (components1.month == components2.month) &&
                       (components1.day == components2.day))

        return isEqual
    }

    func isToday() -> Bool {
        return self.isEqualToDateIgnoringTime(NSDate())
    }

    func dateAtStartOfDay() -> NSDate {
        let calendar  = NSCalendar.currentCalendar()
        let startDate = calendar.startOfDayForDate(self)

        return startDate
    }

    func dateAtMidnight() -> NSDate {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)
        components.hour   = 23
        components.minute = 59
        components.second = 59

        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }

    func dateIgnoringSeconds() -> NSDate {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)
        components.second = 0

        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }

    func dateByAddingDays(days: Int) -> NSDate? {
        let dateComponents  = NSDateComponents()
        dateComponents.day  = days
        let newDate: NSDate = NSCalendar.currentCalendar().dateByAddingComponents(dateComponents, toDate: self, options: NSCalendarOptions(rawValue: 0))!

        return newDate
    }

    func dateByAddingMinutes(min: Int) -> NSDate? {
        let dateComponents    = NSDateComponents()
        dateComponents.minute = min
        let newDate: NSDate   = NSCalendar.currentCalendar().dateByAddingComponents(dateComponents, toDate: self, options: NSCalendarOptions(rawValue: 0))!

        return newDate
    }

    func dateBySubtractingDays(days: Int) -> NSDate? {
        return self.dateByAddingDays(-days)
    }

    func isEarlierThanDate(aDate: NSDate) -> Bool {
        return self.compare(aDate) == .OrderedAscending
    }

    func isLaterThanDate(aDate: NSDate) -> Bool {
        return self.compare(aDate) == .OrderedDescending
    }

    func isInDateRange(rangeStart: NSDate, rangeDurationInMinutes: Int) -> Bool {
        guard let rangeEnd = rangeStart.dateByAddingMinutes(rangeDurationInMinutes) else {
            return false
        }

        var isInRange: Bool

        if rangeStart.isEqualToDate(self) || (rangeStart.isEarlierThanDate(self) && rangeEnd.isLaterThanDate(self)) {
            //we're removing seconds so to enter here we need the date to be either 
            //equal to the range's start (same minute) or between the range's start and end.
            //if it's equal tothe rangeEnd, then it should go in the next segment.
            isInRange = true
        } else {
            isInRange = false
        }

        return isInRange
    }

    // MARK: - Decomposing Dates
    func nearestHour() -> Int {
        let interval                       = NSDate().timeIntervalSince1970 + 60 * 30
        let newDate                        = NSDate(timeIntervalSince1970: interval)
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components                     = NSCalendar.currentCalendar().components(componentFlags, fromDate: newDate)

        return components.hour
    }

    func hour() -> Int {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components                     = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)

        return components.hour
    }

    func minute() -> Int {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components                     = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)

        return components.minute
    }

    func seconds() -> Int {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components                     = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)

        return components.second
    }

    func day() -> Int {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components                     = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)

        return components.day
    }

    func month() -> Int {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components                     = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)

        return components.month
    }

    func year() -> Int {
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components                     = NSCalendar.currentCalendar().components(componentFlags, fromDate: self)

        return components.year
    }
    func dayOfWeek() -> Int? {
        if
            let cal: NSCalendar = NSCalendar.currentCalendar(),
            let comp: NSDateComponents = cal.components(.Weekday, fromDate: self) {
            return comp.weekday
        } else {
            return nil
        }
    }
    
    func getDayOfWeekString()->String? {
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.Weekday, fromDate: self)
        let weekDay = myComponents.weekday
        switch weekDay {
        case 1:
            return "Sun"
        case 2:
            return "Mon"
        case 3:
            return "Tue"
        case 4:
            return "Wed"
        case 5:
            return "Thu"
        case 6:
            return "Fri"
        case 7:
            return "Sat"
        default:
            print("Error fetching days")
            return "Day"
        }
  
    }
    
    func stringDateWithFormat(format: String) ->String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        
        
        return dateFormatter.stringFromDate(self);
    }
    
    func stringForDate() -> String{
        return self.stringDateWithFormat("MM/dd/yyyy");
    }
    


}
