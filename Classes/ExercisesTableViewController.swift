//
//  ExercisesTabView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/24/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol ExercisesTabViewDelegate: class {
    func moveNavBar(offset: CGFloat);
}

class ExercisesTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var tableView:                 UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var visible = true,fetching = false;
    weak var alertViewDelegate:          AlertViewDelegate?
    weak var delegate:ExercisesTabViewDelegate?
    private let prescriptionsViewModel = PrescriptionsViewModel()
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    
    lazy var numberFormatter: NSNumberFormatter = {
        let nf          = NSNumberFormatter()
        nf.allowsFloats = false
        return nf
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    private var todayExercises: [Exercise] = []
    private var completedExercises: [Exercise] = []
    private var dataSource: [Exercise] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.getContentFromServerForUser(appDelegate.user!);
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(ExercisesTableViewController.dismissViewController));
        self.title = "Exercises";
        

    }
    

    
    // MARK: - Methods
    func getContentFromServerForUser(user: User) {
        self.loaderView.show()
        
    
        if fetching == false{
            fetching = true;
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "completedCell", object: nil);

            self.prescriptionsViewModel.getExercisesForUser(user) { (result) -> Void in
                self.loaderView.hide(animated: true)
                self.todayExercises = Exercise.getTodayExercises(self.appDelegate.managedObjectContext)!;
                self.completedExercises = Exercise.getCompletedExercises(self.appDelegate.managedObjectContext)!;
                self.dataSource = Exercise.getPendingExercises(self.appDelegate.managedObjectContext)!;
                self.tableView.reloadData()
                self.fetching = false;
            }
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ExercisesTableViewController.updateCells(_:)), name: "completedCell", object: nil)

        }
        
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.appDelegate.blockRotation = true

        super.viewWillAppear(true);
        
        self.getContentFromServerForUser(self.appDelegate.user!);
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ExercisesTableViewController.playVideoAtURL(_:)), name: "playVid", object: nil);
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "playVid", object: nil);
        
        appDelegate.blockRotation = false
        self.moveNavBar(0);

    }

    
    
    func playVideoAtURL(notif: NSNotification) {
        let url:NSURL = notif.object as! NSURL;
        self.performSegueWithIdentifier("playVideo", sender: url)
    }
    
    
    // MARK: - TableView Delegate & Datasource
    
    func tableView(tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
        case 0:
            if todayExercises.count > 0 {
                return 22;
            }
        case 1:
            if dataSource.count > 0 {
                return 22;
            }
        case 2:
            if completedExercises.count > 0 {
                return 22;
            }
            
        default: break;
            
        }
        return 0;
        
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 380;
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch section {
        case 0:
            return todayExercises.count;
        case 1:
            return dataSource.count;
        case 2:
            return completedExercises.count;
        default: return 0;
        }
        
        
    }
    
 
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.bounds.size.width, 22))
        var lblHeader:UILabel? =  headerView.viewWithTag(99) as? UILabel;
        headerView.backgroundColor = UIColor.geiaDarkGrayColor()
        ;
        if (lblHeader == nil){
            lblHeader = UILabel(frame: CGRectMake(5, 8, tableView.bounds.size.width, 22))
            lblHeader?.textColor = UIColor.geiaYellowColor();
            lblHeader?.tag = 99;
            lblHeader?.font = UIFont.geiaBoldFontOfSize(20);
            headerView.addSubview(lblHeader!);
            
        }
        
        
        
        switch section {
        case 0:
            lblHeader?.text =  "Today's Exercises"
        case 1:
            lblHeader?.text = "All Exercises"
        case 2:
            lblHeader?.text = "Completed Exercises"
        default: break;
        }
        
        
        return headerView
    }
    
    
    
    


    
    func moveNavBar(offset: CGFloat) {
        self.navigationController?.navigationBar.lt_setTranslationY(-44 * offset);
        self.navigationController?.navigationBar.lt_setElementsAlpha(1-offset);
        self.tableView?.transform = CGAffineTransformMakeTranslation(0, -44 * offset);

    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cardExersise") as! ExercisesTableViewCell
        var object:Exercise;
        cell.todayExercise = false;
        
        switch indexPath.section {
        case 0:
            object = self.todayExercises[indexPath.row];
            cell.todayExercise = true;
        case 1:
            object = self.dataSource[indexPath.row];
        default:
            object = self.completedExercises[indexPath.row];
        }
        
        cell.layoutContentWithData(object)
        cell.lblHeader.text = object.title;
        cell.selectionStyle = UITableViewCellSelectionStyle.None;
        cell.tag = indexPath.row;
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var object:Exercise;
        switch indexPath.section {
        case 0:
            object = self.todayExercises[indexPath.row];
        case 1:
            object = self.dataSource[indexPath.row];
        default:
            object = self.completedExercises[indexPath.row];
        }
        self.visible = false;

        
        let secondVC = self.storyboard?.instantiateViewControllerWithIdentifier("ExerciseDetailViewController") as? ExerciseDetailViewController;
        secondVC?.exertData = object;
//        self.moveNavBar(0);
        
        self.navigationController?.pushViewController(secondVC!, animated: true);
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true);
        
    }
    
    
    func updateCells(notification: NSNotification){
        let cell = notification.object as! ExercisesTableViewCell;
        //        self.tableView.beginUpdates();
        
        if cell.todayExercise {
            todayExercises.removeAtIndex(cell.tag);
        }else{
            dataSource.removeAtIndex(cell.tag);
        }
        let newIndexPath = self.completedExercises.count;
        completedExercises.insert(cell.exertData!, atIndex: newIndexPath)

        self.sendIDitIt(cell.exertData!);


    }

    func sendIDitIt(exertData: Exercise){
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: appDelegate.user!.userID))!
        let peID: String  = exertData.code!;
        let endpoint: String = IDIDIT_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>",
            withString: userID).stringByReplacingOccurrencesOfString("<peid>", withString: peID).stringByAppendingString("/\(Int(NSDate().timeIntervalSince1970))")
        
        let url: String      = "\(SERVER_URL)\(endpoint)"

        request(.POST, url, parameters: nil, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                //debugPrint(response)
                let result = response.result
                
                if result.isFailure {
                    //Epic fail
                    print("Could not report did it data");
                    self.tableView.reloadData();

                }else{
                    
                    //        self.tableView.moveRowAtIndexPath(NSIndexPath(forRow: cell.tag, inSection: 0), toIndexPath: NSIndexPath(forRow:  newIndexPath, inSection: 1));
                    //        self.tableView.reloadSections(NSIndexSet(index:0), withRowAnimation: .None);
                    self.tableView.reloadData();
                    //        self.tableView.endUpdates();
                }
        }
    }
    
    
    func dismissViewController(){
        self.navigationController!.dismissViewControllerAnimated(true, completion: nil);
    }
    
    
    
}
