//
//  EmotionNotificationViewDelegate.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 2/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

@objc protocol EmotionNotificationViewDelegate: class {
    func emotionValueDidChange()
    optional func showEmotionalNotification()
}
