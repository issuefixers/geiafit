//
//  DailyActivityGoalTrackingView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class DailyActivityGoalTrackingView: UIView, SChartDatasource {

    // MARK: - Outlets
    @IBOutlet weak var totalDistance: UILabel!
    @IBOutlet weak var totalTime: UILabel!
    @IBOutlet weak var totalCalories: UILabel!
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var lightProgressView: CircleProgressView!
    @IBOutlet weak var lightTime: UILabel!
    @IBOutlet weak var moderateProgressView: CircleProgressView!
    @IBOutlet weak var moderateTime: UILabel!
    @IBOutlet weak var vigorousProgressView: CircleProgressView!
    @IBOutlet weak var vigorousTime: UILabel!

    // MARK: Local variables
    private var datasource: [[String: AnyObject]] = []
    private var lowThreshold: Double  = 0.0
    private var highThreshold: Double = 0.0

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()

    // MARK: - Actions
    func updateSteps() {
        self.goalTrackingViewModel.updateStepsFromHealthKitForDateRange(.Day) { (success) -> Void in
            if success {
                //save changes
                self.appDelegate.saveContext()

                self.loadData()
            }
        }
    }

    func loadData() {
        self.lightProgressView.setProgress(0, animated: true)
        self.moderateProgressView.setProgress(0, animated: true)
        self.vigorousProgressView.setProgress(0, animated: true)
        self.lightTime.text     = "0/0 min"
        self.moderateTime.text  = "0/0 min"
        self.vigorousTime.text  = "0/0 min"
        self.totalDistance.attributedText = self.getAttributedStringFor(value: "0", unit: "MILES")
        self.totalTime.attributedText     = self.getAttributedStringFor(value: "0 of 30", unit: "MIN")
        self.totalCalories.attributedText = self.getAttributedStringFor(value: "0", unit: "CAL")

        self.goalTrackingViewModel.getStepsForUser(self.appDelegate.user!, dateRange: .Day) { (result) -> Void in
            let success = result["success"] as! Bool

            if success {
                if let data = result["data"] {
                    //debugPrint(result)

                    self.datasource = data as! [[String: AnyObject]]
                    self.setupGraph()
                }

                let lightMin         = result["lightMin"] as! Int
                let lightMinGoal     = result["lightMinGoal"] as! Int
                let moderateMin      = result["moderateMin"] as! Int
                let moderateMinGoal  = result["moderateMinGoal"] as! Int
                let vigorousMin      = result["vigorousMin"] as! Int
                let vigorousMinGoal  = result["vigorousMinGoal"] as! Int
                let totalMin         = result["totalMin"] as! Int
                let totalMinGoal     = result["totalMinGoal"] as! Int
                let lightProgress    = result["lightProgress"] as! Double
                let moderateProgress = result["moderateProgress"] as! Double
                let vigorousProgress = result["vigorousProgress"] as! Double
                self.lowThreshold    = result["lowThreshold"] as! Double
                self.highThreshold   = result["highThreshold"] as! Double

                //Update UI
                self.totalTime.attributedText    = self.getAttributedStringFor(value: "\(totalMin) of \(totalMinGoal)", unit: "MIN")
                self.lightTime.text    = "\(lightMin)/\(lightMinGoal) min"
                self.moderateTime.text = "\(moderateMin)/\(moderateMinGoal) min"
                self.vigorousTime.text = "\(vigorousMin)/\(vigorousMinGoal) min"
                self.lightProgressView.setProgress(lightProgress, animated: true)
                self.moderateProgressView.setProgress(moderateProgress, animated: true)
                self.vigorousProgressView.setProgress(vigorousProgress, animated: true)

                //add totalSteps to result dictionary
                let dailyTotals    = HealthDailyTotal.getTodaysTotalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext)
                let totalSteps     = (dailyTotals?.steps)!
                var data           = result
                data["totalSteps"] = totalSteps

                //Post updated data to server
                self.goalTrackingViewModel.sendActivityDataToServerForUser(NSDate(), user: self.appDelegate.user!, data: data, completion: { (result) -> Void in
                    //Parse response
                })
            }
        }

        //Calories
        let healthManager = HealthManager()
        healthManager.getCaloriesForDateRange(.Day, completion: { (activeCalories, restingCalories, error) -> Void in
            if error != nil {
                print("Error reading HealthKit Store: \(error.localizedDescription), \(error.code)")
                return
            }

            //commit new data
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.appDelegate.saveContext()
            })

            var totalCalories = 0.0

            if let totalsForToday = HealthDailyTotal.getTodaysTotalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext) {
                totalCalories = totalsForToday.getTotalCaloriesBurned()
            }

            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let nf                   = NSNumberFormatter()
                nf.maximumFractionDigits = 0
                nf.usesGroupingSeparator = true

                self.totalCalories.attributedText = self.getAttributedStringFor(value: (nf.stringFromNumber(totalCalories))!, unit: "CAL")
            })
        })

        //Distance
        healthManager.getDistanceForDate(NSDate(), completion: { (totalDistance, error) -> Void in
            if error != nil {
                print("Error reading HealthKit Store: \(error.localizedDescription)")
                return
            }

            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let nf                   = NSNumberFormatter()
                nf.maximumFractionDigits = 2

                let distance = nf.stringFromNumber(totalDistance)!
                let unit     = totalDistance > 1 ? "MILE" : "MILES"
                self.totalDistance.attributedText = self.getAttributedStringFor(value: "\(distance)", unit: unit)

            })
        })
    }

    func setupGraph() {
        //print("DAGTV - \(self.datasource)")

        let padding = CGFloat(10.0)
        let frame   = CGRect(x: padding, y: padding, width: self.graphView.bounds.width - 2 * padding, height: self.graphView.bounds.height - 2 * padding)
        let chart                        = ShinobiChart(frame: frame)
        chart.clearsContextBeforeDrawing = true
        chart.autoresizingMask           = .None
        chart.licenseKey                 = SHINOBI_CHARTS_LICENSE
        chart.backgroundColor            = .clearColor()
        chart.canvasAreaBackgroundColor  = .clearColor()
        chart.plotAreaBackgroundColor    = .geiaDarkBlueColor()
        chart.plotAreaBorderThickness    = 0.0
        chart.plotAreaBorderColor        = .clearColor()

        //Axes
        let xAxis = SChartDateTimeAxis()
        xAxis.majorTickFrequency             = SChartDateFrequency(hour: 6)
        xAxis.minorTickFrequency             = SChartDateFrequency(minute: 30)
        xAxis.enableGesturePanning           = false
        xAxis.enableGestureZooming           = false
        xAxis.enableMomentumPanning          = false
        xAxis.enableMomentumZooming          = false
        xAxis.axisPositionValue              = 0
        xAxis.style.lineColor                = .geiaDarkBlueColor()
        xAxis.style.majorTickStyle.labelFont = UIFont.geiaFontOfSize(8.0)
        xAxis.style.majorTickStyle.showTicks = false
  
        let df                       = NSDateFormatter()
        df.dateFormat                = "h"
        let tickLabelFormatter       = SChartTickLabelFormatter()
        tickLabelFormatter.formatter = df
        xAxis.labelFormatter         = tickLabelFormatter
        chart.xAxis = xAxis

        let yAxis = SChartNumberAxis()
        yAxis.rangePaddingHigh                = 25.0
        yAxis.enableGesturePanning            = false
        yAxis.enableGestureZooming            = false
        yAxis.enableMomentumPanning           = false
        yAxis.enableMomentumZooming           = false
        yAxis.axisPositionValue               = 0
        yAxis.style.lineColor                 = .clearColor()
        yAxis.style.majorTickStyle.showTicks  = true
        yAxis.style.majorTickStyle.showLabels = false
        chart.yAxis = yAxis

        self.graphView.addSubview(chart)

        chart.datasource = self
    }

    // MARK: - SChart Datasource
    func numberOfSeriesInSChart(chart: ShinobiChart!) -> Int {
        return 3
    }

    func sChart(chart: ShinobiChart!, seriesAtIndex index: Int) -> SChartSeries! {
        var series: SChartSeries?

        switch index {
            case 0:
                let lineSeries                      = SChartLineSeries()
                let color: UIColor                  = .geiaLightBlueColor()
                lineSeries.style().lineColor        = color
                lineSeries.style().areaColor        = color
                lineSeries.style().fillWithGradient = false
                lineSeries.style().showFill         = true
                series = lineSeries
            case 1:
                let bandSeries                      = SChartBandSeries()
                let color: UIColor                  = .geiaMidBlueColor()
                bandSeries.style().lineColorHigh    = color
                bandSeries.style().lineColorLow     = color
                bandSeries.style().areaColorNormal  = color
                bandSeries.style().showFill         = true
                series = bandSeries
            case 2:
                let lineSeries               = SChartLineSeries()
                lineSeries.style().lineColor = .whiteColor()
                lineSeries.style().lineWidth = 2.0
                lineSeries.crosshairEnabled  = true
                series = lineSeries
            default:
                series = nil
        }

        return series
    }

    func sChart(chart: ShinobiChart!, numberOfDataPointsForSeriesAtIndex seriesIndex: Int) -> Int {
        return self.datasource.count
    }

    func sChart(chart: ShinobiChart!, dataPointAtIndex dataIndex: Int, forSeriesAtIndex seriesIndex: Int) -> SChartData! {
        let data: SChartData

        let datapoint = self.datasource[dataIndex]
        let xValue    = datapoint["timestamp"]

        switch seriesIndex {
            case 0:
                let dataPoint    = SChartDataPoint()
                dataPoint.xValue = xValue
                dataPoint.yValue = self.lowThreshold
                data             = dataPoint
            case 1:
                let bandDataPoint     = SChartMultiYDataPoint()
                bandDataPoint.xValue  = xValue
                bandDataPoint.yValues = [SChartBandKeyHigh: self.highThreshold,
                                          SChartBandKeyLow: self.lowThreshold]
                data                  = bandDataPoint
            default:
                let dataPoint    = SChartDataPoint()
                dataPoint.xValue = xValue
                dataPoint.yValue = datapoint["value"] as? Double
                data             = dataPoint
        }

        return data
    }


    // MARK: - Helper methods
    func getAttributedStringFor(value value: String, unit: String) -> NSAttributedString {
        let style           = NSMutableParagraphStyle()
        style.alignment     = .Center
        style.lineBreakMode = .ByWordWrapping
        let normalFont      = UIFont.geiaFontOfSize(15.0)
        let valueColor      = UIColor.geiaDarkGrayColor()
        let unitColor       = UIColor.geiaLightGrayColor()

        let attStr = NSMutableAttributedString(string: value, attributes: [NSFontAttributeName: normalFont,
                                                                 NSParagraphStyleAttributeName: style,
                                                                NSForegroundColorAttributeName: valueColor])
        let unitStr = NSAttributedString(string: " \(unit)", attributes: [NSFontAttributeName: normalFont,
                                                                NSParagraphStyleAttributeName: style,
                                                               NSForegroundColorAttributeName: unitColor])

        attStr.appendAttributedString(unitStr)

        return attStr
    }
}
