//
//  UITableView+Utilities.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 2/1/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//


extension UITableView {
    func tableViewScrollToBottom(animated: Bool) {

        let delay = 0.1 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))

        dispatch_after(time, dispatch_get_main_queue(), {

            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRowsInSection(numberOfSections-1)

            if numberOfRows > 0 {
                let indexPath = NSIndexPath(forRow: numberOfRows-1, inSection: (numberOfSections-1))
                self.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: animated)
            }

        })
    }
}
