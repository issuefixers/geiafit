//
//  Constants.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/4/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import Foundation

// MARK: - URLs
var SERVER_URL: String {
    #if DEBUG
        //DEV SERVER
        return "https://api.geiafit.com/api"
//        return "http://geiafit.dev.cpcs.ws/api"
    #else
        //PRODUCTION SERVER
        return "https://api.geiafit.com/api"
    #endif
}

// MARK: - Endpoints
let LOGIN_ENDPOINT                = "/user/login"
let MESSAGE_ENDPOINT              = "/messages/<userID>/<timeStamp>"
let LOGOUT_ENDPOINT               = "/profile/logout/<userID>"
let PASSWORD_RESET_ENDPOINT       = "/profile/pwdreset"
let PROFILE_ENDPOINT              = "/profile/<userID>"
let PROFILE_PIC_ENDPOINT          = "/profile/profileimage/<userID>"
let CHARACTERISTICS_ENDPOINT      = "/characteristics/<userID>"
let RISK_ANALYSIS_SURVEY_ENDPOINT = "/survey/risk_analysis"
let SURVEY_ENDPOINT               = "/survey/<userID>"
let THRESHOLDS_ENDPOINT           = "/goals/tresholds/<userID>"
let FITNESS_GOALS_ENDPOINT        = "/goals/activity/<userID>"
let NUTRITION_GOALS_ENDPOINT      = "/goals/nutrition/<userID>"
let POSTURE_ENDPOINT              = "/posture/<userID>"
let WEBEX_ENDPOINT                = "/webex/<userID>"
let LOG_ACTIVITY_ENDPOINT         = "/log/activity/<userID>"
let LOG_NUTRITION_ENDPOINT        = "/log/nutrition/<userID>"
let LOG_HEALTHPOINTS_ENDPOINT     = "/log/health_points/<userID>"
let LOG_NOTIFICATION_ENDPOINT     = "/notifications/<userID>"
let DAILY_CHALLENGE_ENDPOINT      = "/challenge/<userID>"
let DEVICE_TOKEN_ENDPOINT         = "/push_notifications"
let OUTCOME_MEASURES_ENDPOINT     = "/outcome_measures/<userID>"
let IDIDIT_ENDPOINT               = "/webex/record/<userID>/<peid>"
let HEALTHPOINTS_HELP             = "/content/health_points"
let PRESCRIPTIONS_HELP            = "/content/prescription"


//Payment
let BT_CHECK                = "/bt/check"
let BT_SUBSCRIPTION         = "/bt/subscription"
let BT_SUBSCRIBE            = "/bt/subscribe"

// MARK: - UserDefaults keys
let LOGGED_IN_KEY           = "loggedIn"
let SESSION_NAME_KEY        = "sessioName"
let SESSION_ID_KEY          = "sessioID"
let CSRF_TOKEN_KEY          = "CSRFToken"
let USER_ID_KEY             = "userID"
let LAST_HK_STEP_DATE_KEY   = "lastHKStepDate"
let LAST_CHALLENGE_DATE_KEY = "lastChallengeDate"
let LAST_EMOTION_DATE_KEY   = "lastEmotionDate"
let LAST_EXERCISE_DATE_KEY   = "exerciseDateKey"
let LAST_RX_SECTION_KEY     = "lastRXSection"
let LAST_DAILY_DATA_KEY     = "lastDailyData"
let LAST_DAILY_GOALS_KEY    = "lastDailyGoals"
let CURRENT_DATE_KEY   = "currentDate"
// MARK: - Other constants
//Unit conversion constants
let KG_PER_LB: Float   = 0.4535924
let LB_PER_KG: Float   = 2.20462247604
let CM_PER_INCH: Float = 2.54
let INCH_PER_CM: Float = 0.393700787402

//Goals string constants
let WEIGHTLOSS             = "Manage Weight"
//let EAT_HEALTHY            = "Eat Healthy"
let PAIN_RELIEF            = "Relieve Pain"
let IMPROVE_POSTURE        = "Improve Posture"
let IMPROVE_OVERALL_HEALTH = "Improve Overall Health"
let MANAGE_RECOVERY        = "Manage Recovery"


//Shinobyh Charts
let SHINOBI_CHARTS_LICENSE = "Q/i/d3MocFWCSJiMjAxNjA2Mjh0bXNAcGxzZWsudXM=q1hzTGhZ8hdRvTtMffh2A/SfDZtY0Ban8/CxINN2cXDj+3s5fjAowOy/kgSSrKTfbYRI0F78X8fWPRQUpESpYUJaUK8mIDsSVL28XDuPnQhmjgyFmY+JuwLr9gTr30JFPrGs2/nAUJ4J6j5BOF07/JzrjqMM=AXR/y+mxbZFM+Bz4HYAHkrZ/ekxdI/4Aa6DClSrE4o73czce7pcia/eHXffSfX9gssIRwBWEPX9e+kKts4mY6zZWsReM+aaVF0BL6G9Vj2249wYEThll6JQdqaKda41AwAbZXwcssavcgnaHc3rxWNBjJDOk6Cd78fr/LwdW8q7gmlj4risUXPJV0h7d21jO1gzaaFCPlp5G8l05UUe2qe7rKbarpjoddMoXrpErC9j8Lm5Oj7XKbmciqAKap+71+9DGNE2sBC+sY4V/arvEthfhk52vzLe3kmSOsvg5q+DQG/W9WbgZTmlMdWHY2B2nbgm3yZB7jFCiXH/KfzyE1A==PFJTQUtleVZhbHVlPjxNb2R1bHVzPnh6YlRrc2dYWWJvQUh5VGR6dkNzQXUrUVAxQnM5b2VrZUxxZVdacnRFbUx3OHZlWStBK3pteXg4NGpJbFkzT2hGdlNYbHZDSjlKVGZQTTF4S2ZweWZBVXBGeXgxRnVBMThOcDNETUxXR1JJbTJ6WXA3a1YyMEdYZGU3RnJyTHZjdGhIbW1BZ21PTTdwMFBsNWlSKzNVMDg5M1N4b2hCZlJ5RHdEeE9vdDNlMD08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+"

