//
//  UIColor+Utilities.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/10/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet)
        var int = UInt32()
        NSScanner(string: hex).scanHexInt(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

    class func geiaDarkBlueColor() -> UIColor {
        return UIColor(hexString: "#2D76C1")
    }
    
    class func geiaMidBlueColor() -> UIColor {
        return UIColor(hexString: "#3AACED")
    }
    
    class func geiaLightBlueColor() -> UIColor {
        return UIColor(hexString: "#85D1FE")
    }
    
    class func geiaYellowColor() -> UIColor {
        return UIColor(hexString: "#F3A81B")
    }

    class func geiaLightGrayColor() -> UIColor {
        return UIColor(hexString: "#BFBFBF")
    }

    class func geiaMidGrayColor() -> UIColor {
        return UIColor(hexString: "#7F7F7F")
    }

    class func geiaDarkGrayColor() -> UIColor {
        return UIColor(hexString: "#1D2027")
    }
    
    class func geiaDarkColor()-> UIColor{
        return UIColor(hexString: "#303A44")
    }
}
