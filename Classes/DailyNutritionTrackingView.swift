//
//  DailyNutritionTrackingView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/21/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class DailyNutritionTrackingView: UIView {

    // MARK: - Outlets
    @IBOutlet weak var calories: UILabel!
    @IBOutlet weak var calorieDelta: UILabel!
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var proteinToday: UILabel!
    @IBOutlet weak var proteinGoal: UILabel!
    @IBOutlet weak var proteinDelta: UILabel!
    @IBOutlet weak var carbsToday: UILabel!
    @IBOutlet weak var carbsGoal: UILabel!
    @IBOutlet weak var carbsDelta: UILabel!
    @IBOutlet weak var fatToday: UILabel!
    @IBOutlet weak var fatGoal: UILabel!
    @IBOutlet weak var fatDelta: UILabel!

    // MARK: Local variables
    private var keys = ["Protein", "Carbohydrates", "Fat"]
    private var datasource: [[String: AnyObject]]?

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()

    private lazy var numberFormatter: NSNumberFormatter = {
        let nf                   = NSNumberFormatter()
        nf.maximumFractionDigits = 0
        nf.usesGroupingSeparator = true

        return nf
    }()

    // MARK: - Actions
    func loadData() {
        self.loaderView.show()

        self.calories.text     = "Today: 0cal / Goal: 0cal"
        self.calorieDelta.text = "0cal To Go"
        self.proteinToday.text = "Today 0g"
        self.proteinGoal.text  = "Goal 0g"
        self.proteinDelta.text = "0g To Go"
        self.carbsToday.text   = "Today 0g"
        self.carbsGoal.text    = "Goal 0g"
        self.carbsDelta.text   = "0g To Go"
        self.fatToday.text     = "Today 0g"
        self.fatGoal.text      = "Goal 0g"
        self.fatDelta.text     = "0g To Go"

        self.goalTrackingViewModel.getNutritionDataForUser(self.appDelegate.user!, dateRange: .Day) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool

            if success {
                if let data = result["data"] as? [String : AnyObject] {
                    //debugPrint(data)

                    let values: [String:AnyObject] = [self.keys[0]: data["totalProtein"] as! Double,
                                                      self.keys[1]: data["totalCarbs"] as! Double,
                                                      self.keys[2]: data["totalFat"] as! Double]
                    let goals: [String:AnyObject]  = [self.keys[0]: data["proteinGoal"] as! Double,
                                                      self.keys[1]: data["carbsGoal"] as! Double,
                                                      self.keys[2]: data["fatGoal"] as! Double]
                    self.datasource = [values, goals]

                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.loaderView.hide(animated: true)
                        self.setupGraph()

                        //Calories
                        let totalCalories = data["totalCalories"] as! Double
                        let calorieGoal   = data["calorieGoal"] as! Double
                        let calorieDiff   = calorieGoal - totalCalories
                        let cal           = self.numberFormatter.stringFromNumber(totalCalories)!
                        let calGoal       = self.numberFormatter.stringFromNumber(calorieGoal)!
                        let calDiff       = self.numberFormatter.stringFromNumber(calorieDiff)!

                        self.calories.text = "Today: \(cal)cal / Goal: \(calGoal)cal"
                        if calorieDiff >= 0 {
                            self.calorieDelta.text = "\(calDiff)cal To Go"
                        } else {
                            self.calorieDelta.text = "Over By \(calDiff)cal"
                        }

                        //Protein
                        let totalProtein = values[self.keys[0]] as! Double
                        let proteinGoal  = goals[self.keys[0]] as! Double
                        let proteinDiff  = proteinGoal - totalProtein
                        let protein      = self.numberFormatter.stringFromNumber(totalProtein)!
                        let proGoal      = self.numberFormatter.stringFromNumber(proteinGoal)!
                        let proDiff      = self.numberFormatter.stringFromNumber(proteinDiff)!

                        self.proteinToday.text = "Today \(protein)g"
                        self.proteinGoal.text  = "Goal \(proGoal)g"
                        if proteinDiff >= 0.0 {
                            self.proteinDelta.text = "\(proDiff)g To Go"
                        } else {
                            self.proteinDelta.text = "Over By \(proDiff)g"
                        }

                        //Carbs
                        let totalCarbs = values[self.keys[1]] as! Double
                        let carbsGoal  = goals[self.keys[1]] as! Double
                        let carbsDiff  = carbsGoal - totalCarbs
                        let carbs      = self.numberFormatter.stringFromNumber(totalCarbs)!
                        let cGoal      = self.numberFormatter.stringFromNumber(carbsGoal)!
                        let cDiff      = self.numberFormatter.stringFromNumber(carbsDiff)!

                        self.carbsToday.text = "Today \(carbs)g"
                        self.carbsGoal.text  = "Goal \(cGoal)g"
                        if carbsDiff >= 0.0 {
                            self.carbsDelta.text = "\(cDiff)g To Go"
                        } else {
                            self.carbsDelta.text = "Over By \(cDiff)g"
                        }

                        //Fat
                        let totalFat = values[self.keys[2]] as! Double
                        let fatGoal  = goals[self.keys[2]] as! Double
                        let fatDiff  = fatGoal - totalFat
                        let fat      = self.numberFormatter.stringFromNumber(totalFat)!
                        let fGoal    = self.numberFormatter.stringFromNumber(fatGoal)!
                        let fDiff    = self.numberFormatter.stringFromNumber(fatDiff)!

                        self.fatToday.text = "Today \(fat)g"
                        self.fatGoal.text  = "Goal \(fGoal)g"
                        if fatDiff >= 0.0 {
                            self.fatDelta.text = "\(fDiff)g To Go"
                        } else {
                            self.fatDelta.text = "Over By \(fDiff)g"
                        }
                    })
                    
                    //Post updated data to server
                    self.goalTrackingViewModel.sendNutritionDataToServerForUser(self.appDelegate.user!, data: data, completion: { (result) -> Void in
                        //Parse response
                    })
                }
            }
        }
    }

    func setupGraph() {
          }

}
