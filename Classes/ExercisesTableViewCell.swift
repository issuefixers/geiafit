//
//  ExercisesTableViewCell.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/24/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import KSToastView

class ExercisesTableViewCell: UITableViewCell {
    @IBOutlet weak var imgHeader: UIImageView!
//    @IBOutlet weak var lblDescripcion: UILabel!
    @IBOutlet weak var lblNumRepe: UILabel!
    @IBOutlet weak var btnDidIt: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblCompleted: UILabel!
//    @IBOutlet weak var imgHeader2: UIImageView!

    @IBOutlet weak var cardView: UIView!
    
    var todayExercise:Bool = false;
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    
    var exertData:Exercise?
    func layoutContentWithData(data: Exercise) {
        
//        self.lblDescripcion.text = data.comment;
        self.imgHeader.sd_setImageWithURL(NSURL(string: data.thumbUrl1!));
//        self.imgHeader2.sd_setImageWithURL(NSURL(string: data.thumbUrl2!));

        self.lblNumRepe.text = data.reps;
        

     
        exertData = data;
        
        if exertData!.completed {
            btnDidIt.backgroundColor = UIColor.geiaMidGrayColor();
            btnDidIt.enabled = false;
            btnDidIt.setTitle("Completed!", forState: .Normal)
//            self.finished.hidden = false;
            
        }else{
            btnDidIt.backgroundColor = UIColor.geiaDarkBlueColor();
            btnDidIt.enabled = true;
            btnDidIt.setTitle("I did It", forState: .Normal)
//            self.finished.hidden = true;
        
        }
        
        if self.tag == 0 {
            self.setupView();
        }
  
        self.lblCompleted.text = "\(exertData!.realized)/\(exertData!.dailyTotal)"

    }
    
    func setupView() {
        self.cardView.clipsToBounds       = false
        self.cardView.layer.shadowColor   = UIColor.geiaDarkGrayColor().CGColor
        self.cardView.layer.shadowOffset  = CGSize(width: 0, height: 0)
        self.cardView.layer.shadowRadius  = 2.0
        self.cardView.layer.shadowOpacity = 0.48

    }

    
    @IBAction func watchVideo(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("playVid", object: NSURL(string: (exertData?.videoURL)!));
    }
    
    @IBAction func didIt(sender: UIButton) {

        exertData!.realized += 1;
        
        if exertData!.realized == exertData!.dailyTotal {
            sender.backgroundColor = UIColor.geiaMidGrayColor();
            sender.enabled = false;
            sender.setTitle("Completed!", forState: .Normal)

            exertData?.completed = true;
            NSNotificationCenter.defaultCenter().postNotificationName("completedCell", object: self);
            KSToastView.ks_showToast("Great Work! You completed the exercise");

        }else{
            KSToastView.ks_showToast("Great! Keep the work going");
        }
        self.lblCompleted.text = "\(exertData!.realized)/\(exertData!.dailyTotal)"

        self.appDelegate.saveContext();
    }
    
    
    

}
