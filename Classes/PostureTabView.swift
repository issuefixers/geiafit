//
//  PostureTabView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/24/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class PostureTabView: UIView, UIWebViewDelegate {

    // MARK: - Properties
    @IBOutlet weak var postureWebView: UIWebView!

    weak var alertViewDelegate: AlertViewDelegate?
    private let prescriptionsViewModel = PrescriptionsViewModel()

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    // MARK: - Methods
    func getContentFromServerForUser(user: User) {
        self.loaderView.show()
        self.prescriptionsViewModel.getPostureReportForUser(user) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool
            
            if success {
                //save changes
                self.appDelegate.saveContext()
                
                let postureURL = result["url"] as! String
                
                if postureURL.characters.count > 0 {
                    let url = NSURL(string: postureURL)
                    let request = NSURLRequest(URL: url!)
                    self.postureWebView.loadRequest(request)
                }
            } else {
                let message = result["errorMessage"] as! String
                let alert: UIAlertController = UIAlertController(title: "Snapshot", message: message, preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                self.alertViewDelegate?.showAlert(alert, sender: self)
            }
        }
    }
    
    //MARK: - WebView Delegate
    func webViewDidStartLoad(webView: UIWebView) {
        self.loaderView.show()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.loaderView.hide(animated: true)
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        self.loaderView.hide(animated: true)
    }

}
