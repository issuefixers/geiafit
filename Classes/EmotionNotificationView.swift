//
//  EmotionNotificationView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 2/2/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class EmotionNotificationView: UIView {
    
    @IBOutlet var box: UIView!
    @IBOutlet var textBox: UITextField!
    @IBOutlet var emotionImage: UIImageView!
    @IBOutlet var slider: UISlider!
    
    weak var delegate: EmotionNotificationViewDelegate?
    private var initialValue: Float = 0.0
    private var todaysCharacteristics: Characteristics?
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    private lazy var onboardingViewModel: OnboardingViewModel = {
        return OnboardingViewModel()
    }()
    
    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()
    
    func setupView() {
        self.box.clipsToBounds       = false
        self.box.layer.shadowColor   = UIColor.geiaDarkGrayColor().CGColor
        self.box.layer.shadowOffset  = CGSize(width: 5, height: 5)
        self.box.layer.shadowRadius  = 15.0
        self.box.layer.shadowOpacity = 0.98
        self.box.layer.cornerRadius  = 10.0
        
        self.todaysCharacteristics = self.goalTrackingViewModel.getTodaysCharacteristicsObject()
        let currentValue           = (self.todaysCharacteristics?.emotion?.floatValue)!
        self.initialValue          = currentValue > 0.0 ? currentValue : 1.0
        self.textBox.text          = "\(Int(self.initialValue * 100.0))%"
        self.slider.value          = self.initialValue
        self.emotionImage.image    = self.goalTrackingViewModel.getEmotionImageForValue(self.initialValue)
        self.slider.minimumValue = 0.1;
    }
    
    @IBAction func saveChanges(sender: UIButton) {
        let emotionVal = Double(self.slider.value);
        self.todaysCharacteristics?.emotion = emotionVal;
        //inform delegate
        self.delegate?.emotionValueDidChange()
        self.uploadVitals()

        self.hidden = true
    }
    
    
    func uploadVitals() {
        //update record date
        self.todaysCharacteristics?.recordDate = NSDate().dateAtStartOfDay()
        
        // Save Values to DB
        self.onboardingViewModel.updateCharacteristics(self.todaysCharacteristics!) { (result) -> Void in
            let success = result["success"] as! Bool
            
            if success {
                //save changes
                print("Emotion Saved");
                self.appDelegate.saveContext()
            } else {
                
                print("Emotion Couldn't be sent");
            }
        }
    }
    
    @IBAction func cancel(sender: UIButton) {
        self.textBox.text       = "\(Int(self.initialValue * 100.0))%"
        self.slider.value       = self.initialValue
        self.emotionImage.image = self.goalTrackingViewModel.getEmotionImageForValue(self.initialValue)
        self.hidden             = true
    }
    
    @IBAction func sliderDidChangeValue(sender: UISlider) {
        self.textBox.text       = "\(Int(slider.value * 100.0))%"
        self.emotionImage.image = self.goalTrackingViewModel.getEmotionImageForValue(sender.value)
    }
}
