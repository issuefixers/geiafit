//
//  HealthManager.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import HealthKit
import UIKit
import CoreData

enum DateRange: Int {
    case Day, Week,Month
}

class HealthManager {
    let healthKitStore = HKHealthStore()

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    static var data = [(user:User, value:Double, date:NSDate, duration:Int, type: FitbitDataType, moc:NSManagedObjectContext) ]()

    func authorizeHealthKit(completion: ((success: Bool, error: NSError!) -> Void)!) {
        // Setup the types of data we want to read
        let healthKitTypesToRead: Set<HKObjectType> = Set(arrayLiteral:
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!,             //Setps
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,//Distance
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,    //Active  Energy
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBasalEnergyBurned)!,     //Resting Energy
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!,             //Heart Rate
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates)!,  //Carbs
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryFatTotal)!,       //Fat
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryProtein)!,        //Protein
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!  //Calories consumed
        )
        
        let healthKitTypesToWrite: Set<HKSampleType> = Set(arrayLiteral:
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBasalEnergyBurned)!,
                                                           HKObjectType.workoutType()
            //Setps
        );
        

        

        // If the data is not available (old phone or iPad), get outta dodge
        if !HKHealthStore.isHealthDataAvailable() {
            let error = NSError(domain: "biz.wellneslinks.geia.healthkit", code: 2, userInfo: [NSLocalizedDescriptionKey: "HealthKit is not available on this device"])

            if completion != nil {
                completion(success: false, error: error)
            }

            return
        }

        // Request authorization to access HealthKit
        healthKitStore.requestAuthorizationToShareTypes(healthKitTypesToWrite, readTypes: healthKitTypesToRead) { (success, error) -> Void in
            if completion != nil {
                completion(success: success, error: error)
            }
        }
        
    }

	// avoid over-counting
    
    
    func getUserStepsCountForDate(date:NSDate, dateRange: DateRange, completion: ((data:[(sum: Double, date: NSDate)], error:NSError!) -> Void)!) {
     
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let startDate : NSDate
            let endDate = date.dateAtMidnight()
            
            if dateRange == .Day {
                
                startDate = date.dateAtStartOfDay()
            } else if dateRange == .Week {
                
                
                startDate = date.dateAtStartOfDay().dateBySubtractingDays(6)!
            } else if dateRange == .Month {
                
                startDate = date.dateAtStartOfDay().dateBySubtractingDays(date.day())!
            } else {
                
                startDate = NSDate()
            }
            
            
            fitbitManager.getStepsCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                    
                    self.receiveUserStepsCountForDate(date, dateRange: dateRange, completion: completion)
//                }
                
                
            })
            
            
        } else {
            
            receiveUserStepsCountForDate(date, dateRange: dateRange, completion: completion)
            
        }
        
    }
    
    
    private func receiveUserStepsCountForDate(date:NSDate, dateRange: DateRange, completion: ((data:[(sum: Double, date: NSDate)], error:NSError!) -> Void)!) {
        
        
		let data:[(sum: Double, date: NSDate)] = []
		
		guard let quantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount) else {
			print("Couldn't create quantity type with identifier: HKQuantityTypeIdentifierDistanceWalkingRunning")
			return
		}
		
		
		if dateRange == .Day {
			self.fetchSumOfSamplesForSteps(quantityType, date: date, unit: .countUnit(), completion: { (sum, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data:sum, error:nil)
				})
			})
		} else if dateRange == .Week {
			
			self.fetchDailySumsOfSamplesForType(quantityType, endDate: date, unit: .countUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data:sums, error:nil)
				})
			})
			
		} else if dateRange == .Month {
			
			self.fetchMonthSumsOfSamplesForType(quantityType, endDate: date, unit: .countUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data:sums, error:nil)
				})
			})
			
		}

			
		if dateRange == .Day {
//			if let dp: HKQuantitySample = data.last {
//					//debugPrint("saving last date: \(dp.endDate)")
//				let date = dp.endDate.timeIntervalSince1970
//					
//				NSUserDefaults.standardUserDefaults().setDouble(date, forKey: LAST_HK_STEP_DATE_KEY)
//					NSUserDefaults.standardUserDefaults().synchronize()
//			}
		}
			
//		if completion != nil {
//			//debugPrint("got steps, with \(data.count) datapoints")
//			dispatch_async(dispatch_get_main_queue(), { () -> Void in
//				completion(data: data, error: nil)
//			})
//		}
	}
	
    func getStepsForDate(date:NSDate, dateRange: DateRange, completion: ((data:[HKQuantitySample], NSError!) -> Void)!) {
        
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let startDate : NSDate
            let endDate = date.dateAtMidnight()
            
            if dateRange == .Day {
                
                startDate = date.dateAtStartOfDay()
            } else if dateRange == .Week {
                
                
                startDate = date.dateAtStartOfDay().dateBySubtractingDays(6)!
            } else if dateRange == .Month {
                
                startDate = date.dateAtStartOfDay().dateBySubtractingDays(date.day())!
            } else {
                
                startDate = NSDate()
            }
            
            
            fitbitManager.getStepsCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveStepsForDate(date, dateRange: dateRange, completion: completion)
//                }
            })
            
            
        } else {
            
            receiveStepsForDate(date, dateRange: dateRange, completion: completion)
            
        }
        
    }
    

	private func receiveStepsForDate(date:NSDate, dateRange: DateRange, completion: ((data:[HKQuantitySample], NSError!) -> Void)!) {
		// Initialize our return array
		var data: [HKQuantitySample] = []
		
		// Build the Predicate
		var dayBefore: NSDate?
		let	endDate = date.dateAtMidnight()
		if dateRange == .Day {
//			let lastDate =  NSUserDefaults.standardUserDefaults().doubleForKey(LAST_HK_STEP_DATE_KEY)
//			
//			if lastDate > 0 {
//				dayBefore = NSDate(timeIntervalSince1970: lastDate)
//			} else
				dayBefore = date.dateAtStartOfDay()
//			}
		} else if (dateRange == .Week){
			dayBefore = date.dateAtStartOfDay().dateBySubtractingDays(7)
		} else {
			dayBefore = date.dateAtStartOfDay().dateBySubtractingDays(date.day())
		}
		//debugPrint("startDate: \(dayBefore), endDate: \(date)")
		let predicate       = HKQuery.predicateForSamplesWithStartDate(dayBefore!, endDate:endDate, options: .None)
		let sortDescriptors = [NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: true)]
		let limit           = Int(HKObjectQueryNoLimit)
		let sampleType      = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
		
		// Build samples query
		let sampleQuery = HKSampleQuery(sampleType: sampleType!, predicate: predicate, limit: limit, sortDescriptors: sortDescriptors) { (sampleQuery, results, error ) -> Void in
			if error != nil {
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data: data, error)
				})
				return
			}
			
			data = results as! [HKQuantitySample]
            
            for var i = 0; i < data.count; i += 1 {
                
                let sample = data[i]
                
                let canAddData = self.isSampleSuitableForUse(sample)
                
                if canAddData == false {

                    data.removeAtIndex(i)
                    
                    i -= 1
                }
                
                
            }
			
			if dateRange == .Day {
				if let dp: HKQuantitySample = data.last {
					//debugPrint("saving last date: \(dp.endDate)")
					let date = dp.endDate.timeIntervalSince1970
					
					NSUserDefaults.standardUserDefaults().setDouble(date, forKey: LAST_HK_STEP_DATE_KEY)
					NSUserDefaults.standardUserDefaults().synchronize()
				}
			}
			
			if completion != nil {
				//debugPrint("got steps, with \(data.count) datapoints")
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data: data, nil)
				})
			}
		}
		
		// Execute the Query
		self.healthKitStore.executeQuery(sampleQuery)
	}
	
    
    func getStepsForDateRange(dateRange: DateRange, completion: ((data:[HKQuantitySample], NSError!) -> Void)!) {
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let startDate : NSDate
            let endDate = NSDate().dateAtMidnight()
            
            if dateRange == .Day {
                
                startDate = endDate.dateAtStartOfDay()
            } else if dateRange == .Week {
                
                
                startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(6)!
            } else if dateRange == .Month {
                
                startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(endDate.day())!
            } else {
                
                startDate = NSDate()
            }
            
            
            fitbitManager.getStepsCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveStepsForDateRange(dateRange, completion: completion)
//                }
            })
            
            
        } else {
            
            receiveStepsForDateRange(dateRange, completion: completion)
            
        }
        
    }
    
    private func receiveStepsForDateRange(dateRange: DateRange, completion: ((data:[HKQuantitySample], NSError!) -> Void)!) {
        // Initialize our return array
        var data: [HKQuantitySample] = []

        // Build the Predicate
        let date = NSDate()
        var dayBefore: NSDate?

        if dateRange == .Day {
            let lastDate =  NSUserDefaults.standardUserDefaults().doubleForKey(LAST_HK_STEP_DATE_KEY)

            if lastDate > 0 {
                dayBefore = NSDate(timeIntervalSince1970: lastDate)
            } else {
                dayBefore = date.dateAtStartOfDay()
            }
        } else if (dateRange == .Week){
            dayBefore = date.dateAtStartOfDay().dateBySubtractingDays(7)
		} else {
			dayBefore = date.dateAtStartOfDay().dateBySubtractingDays(date.day())
		}
        //debugPrint("startDate: \(dayBefore), endDate: \(date)")
        let predicate       = HKQuery.predicateForSamplesWithStartDate(dayBefore!, endDate:date, options: .None)
        let sortDescriptors = [NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: true)]
        let limit           = Int(HKObjectQueryNoLimit)
        let sampleType      = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)

        // Build samples query
        let sampleQuery = HKSampleQuery(sampleType: sampleType!, predicate: predicate, limit: limit, sortDescriptors: sortDescriptors) { (sampleQuery, results, error ) -> Void in
            if error != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(data: data, error)
                })
                return
            }

            data = results as! [HKQuantitySample]
            
            for var i = 0; i < data.count; i += 1 {
                
                let sample = data[i]
                
                let canAddData = self.isSampleSuitableForUse(sample)
                
                if canAddData == false {
                    
                    data.removeAtIndex(i)
                    
                    i -= 1
                }
                
                
            }

            if dateRange == .Day {
                if let dp: HKQuantitySample = data.last {
                    //debugPrint("saving last date: \(dp.endDate)")
                    let date = dp.endDate.timeIntervalSince1970

                    NSUserDefaults.standardUserDefaults().setDouble(date, forKey: LAST_HK_STEP_DATE_KEY)
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
            }

            if completion != nil {
                //debugPrint("got steps, with \(data.count) datapoints")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(data: data, nil)
                })
            }
        }

        // Execute the Query
        self.healthKitStore.executeQuery(sampleQuery)
    }

    /**
     Returns the total steps per day for a single day or an array of 7 days.

     - parameter dateRange:  .Day or .Week
     - parameter completion: Returns an array of tupples with the date and the total steps for that day
     */
    
    func getStepCountForDateRange(dateRange: DateRange, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let startDate : NSDate
            let endDate = NSDate().dateAtMidnight()
            
            if dateRange == .Day {
                
                startDate = endDate.dateAtStartOfDay()
            } else if dateRange == .Week {
                
                
                startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(6)!
            } else if dateRange == .Month {
                
                startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(endDate.day())!
            } else {
                
                startDate = NSDate()
            }
            
            
            fitbitManager.getStepsCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveStepCountForDateRange(dateRange, completion: completion)
//                }
            })
            
            
        } else {
            
            receiveStepCountForDateRange(dateRange, completion: completion)
            
        }
        
    }
    
    private func receiveStepCountForDateRange(dateRange: DateRange, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        var data:[(sum: Double, date: NSDate)] = []

        guard let quantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount) else {
            print("Couldn't create quantity type with identifier HKQuantityTypeIdentifierStepCount")
            return
        }

        let today = NSDate().dateAtStartOfDay()

        if dateRange == .Day {

            self.fetchSumOfSamplesForType(quantityType, date: today, unit: HKUnit.countUnit(), completion: { (sum, error) -> Void in
                guard error == nil else {
                    debugPrint(error!)
                    completion(data: data, error: error)
                    return
                }

                data.append((sum: sum, date: today))

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(data: data, error:nil)
                })
            })

        } else {
            // .Week

            self.fetchDailySumsOfSamplesForType(quantityType, endDate: today, unit: HKUnit.countUnit(), completion: { (sums, error) -> Void in
                guard error == nil else {
                    debugPrint(error!)
                    completion(data: data, error: error)
                    return
                }

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(data: sums, error:nil)
                })
            })
            
        }
    }
    
    func getStepCountForDate(date: NSDate, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let startDate = date.dateAtStartOfDay()
            let endDate = date.dateAtMidnight()
            
            
            
            fitbitManager.getStepsCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveStepCountForDate(date, completion: completion)
//                }
            })
            
            
        } else {
            
            receiveStepCountForDate(date, completion: completion)
            
        }
        
    }
    
    
    private func receiveStepCountForDate(date: NSDate, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        var data:[(sum: Double, date: NSDate)] = []
        
        guard let quantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount) else {
            print("Couldn't create quantity type with identifier HKQuantityTypeIdentifierStepCount")
            return
        }
        
        let day = date.dateAtStartOfDay()
       
        self.fetchSumOfSamplesForType(quantityType, date: day, unit: HKUnit.countUnit(), completion: { (sum, error) -> Void in
                guard error == nil else {
                    debugPrint(error!)
                    completion(data: data, error: error)
                    return
                }
                
            data.append((sum: sum, date: day))
                
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(data: data, error:nil)
            })
        })
        
    }
    

    /**
     Returns both Active and Resting calories burned sums for the specified date range

     - parameter dateRange:  .Day or .Week
     - parameter completion: Callback that receives arrays for each type of burned calorie along with the date for each sum
     */
    
    func getCaloriesForDateRange(dateRange: DateRange, completion: ((activeCalories: [(sum: Double, date: NSDate)], restingCalories: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            var startDate = NSDate().dateAtStartOfDay()
            let endDate = NSDate().dateAtMidnight()
            
            
            if dateRange == .Day {
                
                
            } else if dateRange == .Week {
                
                startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(6)!
                
            } else if dateRange == .Month {
                
                startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(endDate.day()-1)!
                
            }
            
            
            fitbitManager.getCaloriesCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveCaloriesForDateRange(dateRange, completion: completion)
//                }
            })
            
            
        } else {
            
            self.receiveCaloriesForDateRange(dateRange, completion: completion)
            
        }
        
    }
    
    
    private func receiveCaloriesForDateRange(dateRange: DateRange, completion: ((activeCalories: [(sum: Double, date: NSDate)], restingCalories: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {

        guard let activeQuantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned) else {
            print("Couldn't create quantity type with identifier HKQuantityTypeIdentifierActiveEnergyBurned")
            return
        }
        guard let restingQuantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBasalEnergyBurned) else {
            print("Couldn't create quantity type with identifier HKQuantityTypeIdentifierBasalEnergyBurned")
            return
        }

        let today = NSDate().dateAtStartOfDay()

        if dateRange == .Day {

            var activeSum  = 0.0
            var restingSum = 0.0

            self.fetchSumOfSamplesForType(activeQuantityType, date: today, unit: HKUnit.kilocalorieUnit(), completion: { (sum, error) -> Void in
                guard error == nil else {
                    debugPrint(error!)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(activeCalories: [], restingCalories: [], error: error)
                    })
                    return
                }

                activeSum = sum

                self.fetchSumOfSamplesForType(restingQuantityType, date: today, unit: HKUnit.kilocalorieUnit(), completion: { (sum, error) -> Void in
                    guard error == nil else {
                        debugPrint(error!)
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            completion(activeCalories: [], restingCalories: [], error: error)
                        })
                        return
                    }

                    restingSum = sum

                    dispatch_async(dispatch_get_main_queue(), { () -> Void in

                        let dailyTotal = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: today, moc: self.appDelegate.managedObjectContext)
                        dailyTotal?.activeCaloriesBurned  = activeSum
                        dailyTotal?.restingCaloriesBurned = restingSum

                        completion(activeCalories: [(sum: activeSum, date: today)], restingCalories: [(sum: restingSum, date: today)], error: nil)
                    })
                })// resting
            })// active

        } else if dateRange == .Week{
            // .Week

            var activeCalories: [(sum: Double, date: NSDate)]  = []
            var restingCalories: [(sum: Double, date: NSDate)] = []

            self.fetchDailySumsOfSamplesForType(activeQuantityType, endDate: today, unit: HKUnit.kilocalorieUnit(), completion: { (sums, error) -> Void in
                guard error == nil else {
                    debugPrint(error!)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(activeCalories: [], restingCalories: [], error: error)
                    })
                    return
                }

                activeCalories = sums

                self.fetchDailySumsOfSamplesForType(activeQuantityType, endDate: today, unit: HKUnit.kilocalorieUnit(), completion: { (sums, error) -> Void in
                    guard error == nil else {
                        debugPrint(error!)
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            completion(activeCalories: [], restingCalories: [], error: error)
                        })
                        return
                    }

                    restingCalories = sums

                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(activeCalories: activeCalories, restingCalories: restingCalories, error: nil)
                    })
                })// resting
            })// active
            
		} else if dateRange == .Month{
			// .Week
			
			var activeCalories: [(sum: Double, date: NSDate)]  = []
			var restingCalories: [(sum: Double, date: NSDate)] = []
			
			self.fetchMonthSumsOfSamplesForType(activeQuantityType, endDate: today, unit: HKUnit.kilocalorieUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(activeCalories: [], restingCalories: [], error: error)
					})
					return
				}
				
				activeCalories = sums
				
				self.fetchMonthSumsOfSamplesForType(activeQuantityType, endDate: today, unit: HKUnit.kilocalorieUnit(), completion: { (sums, error) -> Void in
					guard error == nil else {
						debugPrint(error!)
						dispatch_async(dispatch_get_main_queue(), { () -> Void in
							completion(activeCalories: [], restingCalories: [], error: error)
						})
						return
					}
					
					restingCalories = sums
					
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(activeCalories: activeCalories, restingCalories: restingCalories, error: nil)
					})
				})// resting
			})// active
			
		}
    }

    
    func getCaloriesForDate(date:NSDate, dateRange: DateRange, completion: ((activeCalories: [(sum: Double, date: NSDate)], restingCalories: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let startDate = date.dateAtStartOfDay()
            let endDate = date.dateAtMidnight()
            
            
            
            fitbitManager.getCaloriesCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveCaloriesForDate(date, dateRange: dateRange, completion: completion)
//                }
            })
            
            
        } else {
            
            receiveCaloriesForDate(date, dateRange: dateRange, completion: completion)
            
        }
        
    }
    
    
        private func receiveCaloriesForDate(date:NSDate, dateRange: DateRange, completion: ((activeCalories: [(sum: Double, date: NSDate)], restingCalories: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {

		
		guard let activeQuantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned) else {
			print("Couldn't create quantity type with identifier HKQuantityTypeIdentifierActiveEnergyBurned")
			return
		}
		guard let restingQuantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBasalEnergyBurned) else {
			print("Couldn't create quantity type with identifier HKQuantityTypeIdentifierBasalEnergyBurned")
			return
		}
		
		let today = date
		
		if dateRange == .Day {
			
			var activeSum  = 0.0
			var restingSum = 0.0
			
			self.fetchSumOfSamplesForType(activeQuantityType, date: today, unit: HKUnit.kilocalorieUnit(), completion: { (sum, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(activeCalories: [], restingCalories: [], error: error)
					})
					return
				}
				
				activeSum = sum
				
				self.fetchSumOfSamplesForType(restingQuantityType, date: today, unit: HKUnit.kilocalorieUnit(), completion: { (sum, error) -> Void in
					guard error == nil else {
						debugPrint(error!)
						dispatch_async(dispatch_get_main_queue(), { () -> Void in
							completion(activeCalories: [], restingCalories: [], error: error)
						})
						return
					}
					
					restingSum = sum
					
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						
						let dailyTotal = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: today, moc: self.appDelegate.managedObjectContext)
						dailyTotal?.activeCaloriesBurned  = activeSum
						dailyTotal?.restingCaloriesBurned = restingSum
						
						completion(activeCalories: [(sum: activeSum, date: today)], restingCalories: [(sum: restingSum, date: today)], error: nil)
					})
				})// resting
			})// active
			
		} else if dateRange == .Week{
			// .Week
			
			var activeCalories: [(sum: Double, date: NSDate)]  = []
			var restingCalories: [(sum: Double, date: NSDate)] = []
			
			self.fetchDailySumsOfSamplesForType(activeQuantityType, endDate: today, unit: HKUnit.kilocalorieUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(activeCalories: [], restingCalories: [], error: error)
					})
					return
				}
				
				activeCalories = sums
				
				self.fetchDailySumsOfSamplesForType(activeQuantityType, endDate: today, unit: HKUnit.kilocalorieUnit(), completion: { (sums, error) -> Void in
					guard error == nil else {
						debugPrint(error!)
						dispatch_async(dispatch_get_main_queue(), { () -> Void in
							completion(activeCalories: [], restingCalories: [], error: error)
						})
						return
					}
					
					restingCalories = sums
					
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(activeCalories: activeCalories, restingCalories: restingCalories, error: nil)
					})
				})// resting
			})// active
			
		} else if dateRange == .Month{
			// .Week
			
			var activeCalories: [(sum: Double, date: NSDate)]  = []
			var restingCalories: [(sum: Double, date: NSDate)] = []
			
			self.fetchMonthSumsOfSamplesForType(activeQuantityType, endDate: today, unit: HKUnit.kilocalorieUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(activeCalories: [], restingCalories: [], error: error)
					})
					return
				}
				
				activeCalories = sums
				
				self.fetchMonthSumsOfSamplesForType(activeQuantityType, endDate: today, unit: HKUnit.kilocalorieUnit(), completion: { (sums, error) -> Void in
					guard error == nil else {
						debugPrint(error!)
						dispatch_async(dispatch_get_main_queue(), { () -> Void in
							completion(activeCalories: [], restingCalories: [], error: error)
						})
						return
					}
					
					restingCalories = sums
					
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(activeCalories: activeCalories, restingCalories: restingCalories, error: nil)
					})
				})// resting
			})// active
			
		}
	}
    /**
     Gets the total distance walked or run by the user on the specified date

     - parameter date:       Date to be queried
     - parameter completion: Callback that receives the totalDistance Double
     */
    
    func getDistanceForDate(date: NSDate, completion: ((totalDistance: Double, error: NSError!) -> Void)!) {
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let startDate = date.dateAtStartOfDay()
            let endDate = date.dateAtMidnight()
            
            
            
            fitbitManager.getDistanceCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveDistanceForDate(date, completion: completion)
//                }
            })
            
            
        } else {
            
            self.receiveDistanceForDate(date, completion: completion)
            
        }
        
    }
    
    private func receiveDistanceForDate(date: NSDate, completion: ((totalDistance: Double, error: NSError!) -> Void)!) {
        guard let quantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning) else {
            print("Couldn't create quantity type with identifier: HKQuantityTypeIdentifierDistanceWalkingRunning")
            return
        }

        self.fetchSumOfSamplesForType(quantityType, date: date, unit: .mileUnit(), completion: { (sum, error) -> Void in
            guard error == nil else {
                debugPrint(error!)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(totalDistance: 0.0, error: error)
                })
                return
            }

            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(totalDistance: sum, error: nil)
            })
        })
    }

	
    func getMilesForDate(dateRange: DateRange, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let date = NSDate().dateAtStartOfDay()
            
            let startDate : NSDate
            let endDate = date.dateAtMidnight()
            
            if dateRange == .Day {
                
                startDate = endDate.dateAtStartOfDay()
            } else if dateRange == .Week {
                
                
                startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(6)!
            } else if dateRange == .Month {
                
                startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(date.day())!
            } else {
                
                startDate = NSDate()
            }
            
            
            fitbitManager.getDistanceCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveMilesForDate(dateRange, completion: completion)
//                }
            })
            
            
        } else {
            
            receiveMilesForDate(dateRange, completion: completion)
            
        }
        
    }
    
    
	private func receiveMilesForDate(dateRange: DateRange, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
		
		var data:[(sum: Double, date: NSDate)] = []
		
		guard let quantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning) else {
			print("Couldn't create quantity type with identifier: HKQuantityTypeIdentifierDistanceWalkingRunning")
			return
		}
		
		let today = NSDate().dateAtStartOfDay()
		
		if dateRange == .Day {
			self.fetchSumOfSamplesForType(quantityType, date: today, unit: .mileUnit(), completion: { (sum, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				data.append((sum: sum, date: today))
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data: data, error:nil)
				})
			})
		} else if dateRange == .Week {
			
			self.fetchDailySumsOfSamplesForType(quantityType, endDate: today, unit: .mileUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data:sums, error:nil)
				})
			})
			
		} else if dateRange == .Month {
			
			self.fetchMonthSumsOfSamplesForType(quantityType, endDate: today, unit: .mileUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data:sums, error:nil)
				})
			})
			
		}
	}
	
    func getUserMilesForDate(date:NSDate, dateRange: DateRange, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        let fitbitManager = FitbitManager.sharedInstance
        
        
        if fitbitManager.checkIsLogin() == true {
            
            let startDate : NSDate
            let endDate = date.dateAtMidnight()
            
            if dateRange == .Day {
                
                startDate = date.dateAtStartOfDay()
            } else if dateRange == .Week {
                
                
                startDate = date.dateAtStartOfDay().dateBySubtractingDays(6)!
            } else if dateRange == .Month {
                
                startDate = date.dateAtStartOfDay().dateBySubtractingDays(date.day())!
            } else {
                
                startDate = NSDate()
            }
            
            
            fitbitManager.getDistanceCountForDate(startDate, dateEnd:endDate , completion: { (success) in
                //let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                //dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.receiveUserMilesForDate(date, dateRange: dateRange, completion: completion)
//                }
            })
            
            
        } else {
            
            receiveUserMilesForDate(date, dateRange: dateRange, completion: completion)
            
        }
        
    }
	
	private func receiveUserMilesForDate(date:NSDate, dateRange: DateRange, completion: ((data: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
		
		 var data:[(sum: Double, date: NSDate)] = []
		
		guard let quantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning) else {
			print("Couldn't create quantity type with identifier: HKQuantityTypeIdentifierDistanceWalkingRunning")
			return
		}
		
		let today = date
		
		if dateRange == .Day {
			self.fetchSumOfSamplesForType(quantityType, date: today, unit: .mileUnit(), completion: { (sum, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				data.append((sum: sum, date: today))
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
				completion(data: data, error:nil)
				})
			})
		} else if dateRange == .Week {
		
			self.fetchDailySumsOfSamplesForType(quantityType, endDate: today, unit: .mileUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data:sums, error:nil)
				})
			})
		
		} else if dateRange == .Month {
			
			self.fetchMonthSumsOfSamplesForType(quantityType, endDate: today, unit: .mileUnit(), completion: { (sums, error) -> Void in
				guard error == nil else {
					debugPrint(error!)
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						completion(data: data, error:nil)
					})
					return
				}
				
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completion(data:sums, error:nil)
				})
			})
			
		}
	}
    /**
     Gets the Nutrition Data from HealthKit

     - parameter identifier: HealthKit Sample Type Identifier 
     - parameter dateRange:  .Day or .Week
     - parameter completion: Returns an array of tuples with the date and the total carbs (or protein, fat, dietary calories) for that day
     */
    func getNutritionDataForIdentifier(identifier: String, dateRange: DateRange, completion: ((data:[(sum: Double, date: NSDate)], error:NSError!) -> Void)!) {
        var data:[(sum: Double, date: NSDate)] = []

        guard let quantityType = HKSampleType.quantityTypeForIdentifier(identifier) else {
            print("Couldn't create quantity type with identifier: \(identifier)")
            return
        }

        let unit  = identifier == HKQuantityTypeIdentifierDietaryEnergyConsumed ? HKUnit.kilocalorieUnit() : HKUnit.gramUnit()
        let today = NSDate().dateAtStartOfDay()

        if dateRange == .Day {

            self.fetchSumOfSamplesForType(quantityType, date: today, unit: unit, completion: { (sum, error) -> Void in
                guard error == nil else {
                    debugPrint(error!)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(data: data, error: error)
                    })
                    return
                }

                data.append((sum: sum, date: today))

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(data: data, error:nil)
                })
            })

        } else {
            // .Week

            self.fetchDailySumsOfSamplesForType(quantityType, endDate: today, unit: unit, completion: { (sums, error) -> Void in
                guard error == nil else {
                    debugPrint(error!)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(data: data, error: error)
                    })
                    return
                }

                data = sums

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(data: data, error:nil)
                })
            })

        }
    }

    /**
     Gets the Nutrition Data from HealthKit and stores it in CoreData

     - parameter dateRange:  .Day or .Week
     - parameter completion: Returns a boolean indicating if the caller needs to save the ManagedObjectContext
     */
    func getNutritionForDateRange(dateRange: DateRange, completion: ((success: Bool) -> Void)!) {
        var success                                     = false
        var carbsData: [(sum: Double, date: NSDate)]    = []
        var fatData: [(sum: Double, date: NSDate)]      = []
        var proteinData: [(sum: Double, date: NSDate)]  = []
        var caloriesData: [(sum: Double, date: NSDate)] = []

        self.getNutritionDataForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates, dateRange: dateRange) { (data: [(sum: Double, date: NSDate)], error: NSError!) -> Void in
            if error == nil {
                print("CarbsData: \(data)")
                carbsData = data
            }

            self.getNutritionDataForIdentifier(HKQuantityTypeIdentifierDietaryFatTotal, dateRange: dateRange) { (data, error) -> Void in
                if error == nil {
                    print("FatData: \(data)")
                    fatData = data
                }

                self.getNutritionDataForIdentifier(HKQuantityTypeIdentifierDietaryProtein, dateRange: dateRange) { (data, error) -> Void in
                    if error == nil {
                        print("ProteinData: \(data)")
                        proteinData = data
                    }

                    self.getNutritionDataForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed, dateRange: dateRange) { (data, error) -> Void in
                        if error == nil {
                            print("CaloriesData: \(data)")
                            caloriesData = data
                        }

                        success = self.updateDailyTotals(carbsData: carbsData, fatData: fatData, proteinData: proteinData, caloriesData: caloriesData)

                        completion(success: success)
                    }//Energy Consumed
                }//Protein
            }//Fat Total
        }//Carbs
    }

    /**
     Method used by getNutritionForDateRange(_, completion) to create/update the HealthDailyTotal objects with the data pulled from HealthKit

     - parameter carbsData:    Array of tuples with the carbs total for each date
     - parameter fatData:      Array of tuples with the fat total for each date
     - parameter proteinData:  Array of tuples with the protein total for each date
     - parameter caloriesData: Array of tuples with the total calories consumed for each date

     - returns: Returns a success flag
     */
    func updateDailyTotals(carbsData carbsData: [(sum: Double, date: NSDate)], fatData: [(sum: Double, date: NSDate)], proteinData: [(sum: Double, date: NSDate)], caloriesData: [(sum: Double, date: NSDate)]) -> Bool {
        var currentHealthDailyTotal: HealthDailyTotal?
        var success = false

        for carbs in carbsData {
            currentHealthDailyTotal = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: carbs.date, moc: self.appDelegate.managedObjectContext)
            currentHealthDailyTotal?.carbs = carbs.sum
            //print("updated HDT carbs \(currentHealthDailyTotal?.carbs) on \(currentHealthDailyTotal?.date)")
            success = true
        }

        for fat in fatData {
            currentHealthDailyTotal = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: fat.date, moc: self.appDelegate.managedObjectContext)
            currentHealthDailyTotal?.fat = fat.sum
            //print("updated HDT fat \(currentHealthDailyTotal?.fat) on \(currentHealthDailyTotal?.date)")
            success = true
        }

        for protein in proteinData {
            currentHealthDailyTotal = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: protein.date, moc: self.appDelegate.managedObjectContext)
            currentHealthDailyTotal?.protein = protein.sum
            //print("updated HDT protein \(currentHealthDailyTotal?.protein) on \(currentHealthDailyTotal?.date)")
            success = true
        }

        for calories in caloriesData {
            currentHealthDailyTotal = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: calories.date, moc: self.appDelegate.managedObjectContext)
            currentHealthDailyTotal?.caloriesConsumed = calories.sum
            //print("updated HDT calories consumed \(currentHealthDailyTotal?.caloriesConsumed) on \(currentHealthDailyTotal?.date)")
            success = true
        }

        return success
    }

    
    func isSampleSuitableForUse(sample:HKQuantitySample) -> Bool {
        
        var canAddData = false
        
        if FitbitManager.sharedInstance.checkIsLogin() == true {
            
            
            if let metadata = sample.metadata {
                
                if let deviceName = metadata["HealthDataDeviceStringType"] {
                    
                    if deviceName as! String == HealthDataDeviceStringType.Fitbit.rawValue || deviceName as! String == HealthDataDeviceStringType.Manually.rawValue {
                        
                        canAddData = true
                    }
                    
                }
                
            }
            
            
        } else {
            
            canAddData = true
        }
        
        return canAddData;
    }
    
    
    /**
     Helper method that creates a Statistics query to get the sum of the specified QuantityType for the specified date

     - parameter quantityType: Type to be queried from HealthKit, it **must be a Cumulative type**
     - parameter date:         Date for the data to be queried
     - parameter unit:         HKUnit desired like .gramUnit() or .kilocalorieUnit()
     - parameter completion:   Callback method that gets sent the sum queried
     */
    
    func fetchSumOfSamplesForSteps(quantityType: HKQuantityType, date: NSDate, unit: HKUnit, completion: ((sums: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        var data:[(sum: Double, date: NSDate)] = []
        
        let startDate = date.dateAtStartOfDay()
        let endDate   = date.dateAtMidnight()
        
        
        //debugPrint("startDate: \(dayBefore), endDate: \(date)")
        let predicate       = HKQuery.predicateForSamplesWithStartDate(startDate, endDate:endDate, options: .None)
        let sortDescriptors = [NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: true)]
        let limit           = Int(HKObjectQueryNoLimit)
        let sampleType      = quantityType //HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
        
        // Build samples query
        let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: limit, sortDescriptors: sortDescriptors) { (sampleQuery, results, error ) -> Void in
            if error != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(sums: data, error: error)
                })
                return
            }
            
            
            let resultDatas = results as! [HKQuantitySample]
            
            var dateStart = date.dateAtStartOfDay().dateIgnoringSeconds()
            
            for _ in 0..<24 {
                
                var sum = 0.0
                
                for sample in resultDatas {
                    
                    let canAddData = self.isSampleSuitableForUse(sample)
                    
                    
                    if canAddData == true {
                        
                        let sampleStart = sample.startDate
                        
                        if sampleStart >= dateStart && sampleStart < dateStart.dateByAddingMinutes(60) {
                            
                            let value       = sample.quantity.doubleValueForUnit(unit)
                            
                            sum += value
                            
                        }
                    }
                    
                    
                }
                
                
                data.append((sum, dateStart))
                
                dateStart = dateStart.dateByAddingMinutes(60)!
            }
            
            if completion != nil {
                //debugPrint("got steps, with \(data.count) datapoints")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(sums: data, error: error)
                })
            }
        }
        
        // Execute the Query
        self.healthKitStore.executeQuery(sampleQuery)
        
    }
    
//	func fetchSumOfSamplesForSteps_(quantityType: HKQuantityType, date: NSDate, unit: HKUnit, completion: ((sums: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
//		
//		var data:[(sum: Double, date: NSDate)] = []
//		
//		let startDate = date.dateAtStartOfDay()
//		let endDate   = date.dateAtMidnight()
//		
//		// Your interval: sum by hour
//		let intervalComponents =  NSDateComponents();
//		intervalComponents.hour = 1;
//		
//		let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .None)
//		
//		let query = HKStatisticsCollectionQuery(quantityType: quantityType, quantitySamplePredicate: predicate, options: .CumulativeSum, anchorDate: startDate, intervalComponents: intervalComponents)
//		
//		query.initialResultsHandler = { (query: HKStatisticsCollectionQuery, results: HKStatisticsCollection?, error: NSError?) in
//			guard let statsCollection = results else {
//				debugPrint("Couldn't get stats collection. \(error!)")
//				completion(sums: data, error: error)
//				return
//			}
//			
//			statsCollection.enumerateStatisticsFromDate(startDate, toDate: endDate, withBlock: { (statistics: HKStatistics, stop) -> Void in
//				if let quantity = statistics.sumQuantity() {
//					let date = statistics.startDate
//					
//					var sum = 0.0
//					if quantity.isCompatibleWithUnit(.countUnit()) {
//						sum = quantity.doubleValueForUnit(.countUnit())
//					} else if quantity.isCompatibleWithUnit(.gramUnit()) {
//						sum = quantity.doubleValueForUnit(.gramUnit())
//					} else if quantity.isCompatibleWithUnit(.mileUnit()) {
//						sum = quantity.doubleValueForUnit(.mileUnit())
//					} else if quantity.isCompatibleWithUnit(.kilocalorieUnit()){
//						sum = quantity.doubleValueForUnit(.kilocalorieUnit())
//					}
//					
//					print("Fetched \(quantityType) stats collection. \(sum) on \(date)")
//					data.append((sum: sum, date: date))
//				} else {
//					
//					
//					let date = statistics.startDate
//					data.append((sum:0, date: date))
//					print("Fetched \(quantityType) stats collection. \(0) on \(date)")
//				}
//			})
//			
//			//print("Collection results for \(quantityType): \(data)")
//			completion(sums: data, error:nil)
//		}
//		
//		self.healthKitStore.executeQuery(query)
//	}
	
    func fetchSumOfSamplesForType(quantityType: HKQuantityType, date: NSDate, unit: HKUnit, completion: ((sum: Double, error: NSError!) -> Void)!) {
        
        var sum = 0.0
        
        let startDate = date.dateAtStartOfDay()
        let endDate   = date.dateAtMidnight()
        
        var currentDate = startDate;
        
        // Build samples query
        
        var countOfDay = 0
        
        while currentDate <  endDate{
            
            countOfDay += 1
            
            
//            self.fetchSumOfSamplesForSteps(quantityType, date: currentDate, unit: .countUnit(), completion: { (sums, error) in
            self.fetchSumOfSamplesForSteps(quantityType, date: currentDate, unit: unit, completion: { (sums, error) in
            
                var daySum = 0.0
                var dayDate: NSDate? = nil
                
                
                for (sum, date) in sums {
                    
                    daySum += sum
                    
                    if dayDate == nil {
                        
                        dayDate = date.dateAtStartOfDay()
                    }
                }
                
                sum += daySum
                
                
                countOfDay -= 1
                
                if countOfDay == 0 {

                    completion(sum: sum, error: nil)
                }
                
            })
            
            currentDate = currentDate.dateByAddingDays(1)!
            
        }
        
        
        
    }
    
	
//    func fetchSumOfSamplesForType(quantityType: HKQuantityType, date: NSDate, unit: HKUnit, completion: ((sum: Double, error: NSError!) -> Void)!) {
//        let startDate = date.dateAtStartOfDay()
//        let endDate   = date.dateAtMidnight()
//        let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .None)
//
//        let query = HKStatisticsQuery(quantityType: quantityType, quantitySamplePredicate: predicate, options: .CumulativeSum) { (query, result, error) -> Void in
//            if error != nil {
//                debugPrint(error!)
//                completion(sum: 0.0, error: error)
//                return
//            }
//
//            if (completion != nil) {
//                guard let sum = result?.sumQuantity() else {
//                    //print("Fetched \(quantityType) sum for \(date): sum = nil")
//                    completion(sum: 0.0, error: nil)
//                    return
//                }
//
//                let value = sum.doubleValueForUnit(unit)
//
//                //print("Fetched \(quantityType) sum for \(date): sum = \(sum), value = \(value)")
//                completion(sum: value, error: nil)
//            }
//        }
//
//        self.healthKitStore.executeQuery(query)
//    }

    /**
     Helper method that creates a Statistics Collection query to get the daily sums of the specified QuantityType for 7 days

     - parameter quantityType: Type to be queried from HealthKit, it **must be a Cumulative type**
     - parameter date:         End date for the data to be queried. The start date will be calculated as 7 days before the end date
     - parameter completion:   Callback method that gets sent an array of tuples of the sum queried and it's corresponding date
     */
    
    func fetchDailySumsOfSamplesForType(quantityType: HKQuantityType, endDate: NSDate, unit: HKUnit, completion: ((sums: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        var data:[(sum: Double, date: NSDate)] = []
        
        let startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(6)
        let endDate   = endDate.dateAtMidnight()
        
        var currentDate = startDate;
        
        // Build samples query
        
        var countOfDay = 0
        
        while currentDate <  endDate{
            
            countOfDay += 1
            
            
//            self.fetchSumOfSamplesForSteps(quantityType, date: currentDate!, unit: .countUnit(), completion: { (sums, error) in
            self.fetchSumOfSamplesForSteps(quantityType, date: currentDate!, unit: unit, completion: { (sums, error) in
            
                var daySum = 0.0
                var dayDate: NSDate? = nil
                
                
                for (sum, date) in sums {
                    
                    daySum += sum
                    
                    if dayDate == nil {
                        
                        dayDate = date.dateAtStartOfDay()
                    }
                }
                
                data.append((daySum, dayDate!))
                
                countOfDay -= 1
                
                if countOfDay == 0 {
                    
                    data.sortInPlace({ (value1, value2) -> Bool in
                        
                        return value1.date < value2.date
                    })
                    
                    completion(sums: data, error:nil)
                }
                
            })
            
            currentDate = currentDate?.dateByAddingDays(1)
            
        }
        
        
        
    }
    
//    func fetchDailySumsOfSamplesForType(quantityType: HKQuantityType, endDate: NSDate, completion: ((sums: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
//        var data:[(sum: Double, date: NSDate)] = []
//        let anchorDate = endDate.dateAtStartOfDay().dateBySubtractingDays(6)
//        let interval   = NSDateComponents()
//        interval.day   = 1
//
//        let query = HKStatisticsCollectionQuery(quantityType: quantityType, quantitySamplePredicate: nil, options: .CumulativeSum, anchorDate: anchorDate!, intervalComponents: interval)
//
//        query.initialResultsHandler = { (query: HKStatisticsCollectionQuery, results: HKStatisticsCollection?, error: NSError?) in
//            guard let statsCollection = results else {
//                debugPrint("Couldn't get stats collection. \(error!)")
//                completion(sums: data, error: error)
//                return
//            }
//
//            statsCollection.enumerateStatisticsFromDate(anchorDate!, toDate: endDate, withBlock: { (statistics: HKStatistics, stop) -> Void in
//                if let quantity = statistics.sumQuantity() {
//                    let date = statistics.startDate.dateAtStartOfDay()
//
//                    var sum = 0.0
//                    if quantity.isCompatibleWithUnit(.countUnit()) {
//                        sum = quantity.doubleValueForUnit(.countUnit())
//                    } else if quantity.isCompatibleWithUnit(.gramUnit()) {
//                        sum = quantity.doubleValueForUnit(.gramUnit())
//					} else if quantity.isCompatibleWithUnit(.mileUnit()) {
//						sum = quantity.doubleValueForUnit(.mileUnit())
//					} else if quantity.isCompatibleWithUnit(.kilocalorieUnit()){
//						sum = quantity.doubleValueForUnit(.kilocalorieUnit())
//					}
//
//                    //print("Fetched \(quantityType) stats collection. \(sum) on \(date)")
//                    data.append((sum: sum, date: date))
//				} else {
//				
//				
//					let date = statistics.startDate.dateAtStartOfDay()
//					data.append((sum:0, date: date))
//					
//				}
//            })
//
//            //print("Collection results for \(quantityType): \(data)")
//            completion(sums: data, error:nil)
//        }
//
//        // Execute the Query
//        self.healthKitStore.executeQuery(query)
//    }
	
    func fetchMonthSumsOfSamplesForType(quantityType: HKQuantityType, endDate: NSDate, unit: HKUnit, completion: ((sums: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
        
        var data:[(sum: Double, date: NSDate)] = []
        
        let startDate = endDate.dateAtStartOfDay().dateBySubtractingDays(endDate.day()-1)
        //        let endDate   = date.dateAtMidnight()
        
        var currentDate = startDate;
        
        // Build samples query
        
        var countOfDay = 0
        
        while currentDate <  endDate{
            
            countOfDay += 1
            
            
//            self.fetchSumOfSamplesForSteps(quantityType, date: currentDate!, unit: .countUnit(), completion: { (sums, error) in
            self.fetchSumOfSamplesForSteps(quantityType, date: currentDate!, unit: unit, completion: { (sums, error) in
            
                var daySum = 0.0
                var dayDate: NSDate? = nil
                
                
                for (sum, date) in sums {
                    
                    daySum += sum
                    
                    if dayDate == nil {
                        
                        dayDate = date.dateAtStartOfDay()
                    }
                }
                
                data.append((daySum, dayDate!))
                
                countOfDay -= 1
                
                if countOfDay == 0 {
                    
                    data.sortInPlace({ (value1, value2) -> Bool in
                        
                        return value1.date < value2.date
                    })
                    
                    completion(sums: data, error:nil)
                }
                
            })
            
            currentDate = currentDate?.dateByAddingDays(1)
            
        }

        
        
    }
    
    
//	func fetchMonthSumsOfSamplesForType(quantityType: HKQuantityType, endDate: NSDate, completion: ((sums: [(sum: Double, date: NSDate)], error: NSError!) -> Void)!) {
//		var data:[(sum: Double, date: NSDate)] = []
////		let lastDate =  Common.getFirstAndLastDateOfMonth(endDate).lastDateOfMonth
//		let anchorDate = endDate.dateAtStartOfDay().dateBySubtractingDays(endDate.day()-1)
//		let interval   = NSDateComponents()
//		interval.day   = 1
//		
//		let query = HKStatisticsCollectionQuery(quantityType: quantityType, quantitySamplePredicate: nil, options: .CumulativeSum, anchorDate: anchorDate!, intervalComponents: interval)
//		
//		query.initialResultsHandler = { (query: HKStatisticsCollectionQuery, results: HKStatisticsCollection?, error: NSError?) in
//			guard let statsCollection = results else {
//				debugPrint("Couldn't get stats collection. \(error!)")
//				completion(sums: data, error: error)
//				return
//			}
//			
//			statsCollection.enumerateStatisticsFromDate(anchorDate!, toDate: endDate, withBlock: { (statistics: HKStatistics, stop) -> Void in
//				if let quantity = statistics.sumQuantity() {
//					let date = statistics.startDate.dateAtStartOfDay()
//					
//					var sum = 0.0
//					if quantity.isCompatibleWithUnit(.countUnit()) {
//						sum = quantity.doubleValueForUnit(.countUnit())
//					} else if quantity.isCompatibleWithUnit(.gramUnit()) {
//						sum = quantity.doubleValueForUnit(.gramUnit())
//					} else if quantity.isCompatibleWithUnit(.mileUnit()) {
//						sum = quantity.doubleValueForUnit(.mileUnit())
//					} else if quantity.isCompatibleWithUnit(.kilocalorieUnit()){
//						sum = quantity.doubleValueForUnit(.kilocalorieUnit())
//					}
//					
//					
//					data.append((sum: sum, date: date))
//				} else {
//					
//					let date = statistics.startDate.dateAtStartOfDay()
//					data.append((sum:0, date: date))
//					
//				}
//			})
//			
//			print(Common.getDays(endDate))
//			if (Common.getDays(endDate) - endDate.day()>1) {
//				for dayDiff in 1 ... Common.getDays(endDate) - endDate.day() {
//					let nDays = dayDiff
//					let date  = endDate.dateByAddingDays(nDays)!
//					data.append((sum:0, date: date))
//				}
//			}
//			
//			completion(sums: data, error:nil)
//		}
//		
//		// Execute the Query
//		self.healthKitStore.executeQuery(query)
//	}
    
    class func addFitbitValue(user: User!, value:Double, date:NSDate, type: FitbitDataType, duration:Int, moc:NSManagedObjectContext, completition: (Bool, NSError?) -> Void){
        
        // 1. Create a Step  Sample
        
        var value = value
        
        let metadata = ["HealthDataDeviceStringType":HealthDataDeviceStringType.Fitbit.rawValue]
        
        var quantityType : String
        var unit : HKUnit
        var nameOfValue : String
        
        switch type {
        case FitbitDataType.Steps:
            
            quantityType = HKQuantityTypeIdentifierStepCount
            unit = HKUnit.countUnit()
            nameOfValue = "Steps"
            
            data.append((user:user , value:value, date:date, duration:duration, type:type, moc:moc))
            
            break
        case FitbitDataType.Calories:
            
            quantityType = HKQuantityTypeIdentifierActiveEnergyBurned
            unit = HKUnit.kilocalorieUnit()
            nameOfValue = "Calories"
            
            break
        case FitbitDataType.Distance:
            
            quantityType = HKQuantityTypeIdentifierDistanceWalkingRunning
            unit = HKUnit.mileUnit()
            nameOfValue = "Distance"
            
            value *= 0.621371
            
            break
        }
        
        debugPrint("add\(nameOfValue)  \(date)     \(value)    \(duration)  FitbitDataType: \(type.rawValue)")
        
        let queryType = HKQuantityType.quantityTypeForIdentifier(quantityType)
        let stepQuantity = HKQuantity(unit: unit, doubleValue: value)
        let stepSample = HKQuantitySample(type: queryType!, quantity: stepQuantity, startDate: date, endDate: date.dateByAddingMinutes(duration)!, metadata: metadata);
        
        // 2. Save the sample in the store
        HKHealthStore().saveObject(stepSample, withCompletion: { (success, error) -> Void in
            if( error != nil ) {
                print("Error saving \(nameOfValue) sample: \(error!.localizedDescription)")
            } else {
//                print("\(steps) steps sample saved successfully!")
                
                if data.count > 0 && data.first!.type == FitbitDataType.Steps{
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        let user = data.first!.user
                        let value = data.first!.value
                        let date = data.first!.date
                        let duration = data.first!.duration
                        let moc = data.first!.moc
                        
                        data.removeFirst()
                        
                        HealthDataPoint.addDataPointForUser(user, count: value, timestamp: date, type: HealthDataType.Steps, duration: Double(duration), moc: moc, deviceType: HealthDataDeviceType.Fitbit);
                        
                    })
                    
                }
            }
            
            completition(true, nil)
        })
        
        
    }
    
    //HKQuantityTypeIdentifierDistanceWalkingRunning
    //HKQuantityTypeIdentifierDistanceCycling
    //HKQuantityTypeIdentifierBasalEnergyBurned
    //HKQuantityTypeIdentifierActiveEnergyBurned
    
    
    class func removeAllFitbitData(sinceDate:NSDate, dataType: FitbitDataType, completition: (Bool, NSError?) -> Void){
        
        
        var dayBefore: NSDate?
        var	endDate: NSDate?
        
        dayBefore = sinceDate.dateAtStartOfDay()
        endDate = sinceDate.dateAtMidnight()
        
        var quantityType : String
        
        switch dataType {
        case FitbitDataType.Steps:
            
            quantityType = HKQuantityTypeIdentifierStepCount
            
            break
        case FitbitDataType.Calories:
            
            quantityType = HKQuantityTypeIdentifierActiveEnergyBurned
            
            break
        case FitbitDataType.Distance:
            
            quantityType = HKQuantityTypeIdentifierDistanceWalkingRunning
            
            break
        }
        
        
//        debugPrint("startDate: \(dayBefore), endDate: \(endDate)")
        let predicate       = HKQuery.predicateForSamplesWithStartDate(dayBefore!, endDate:endDate, options: .None)
        let sortDescriptors = [NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: true)]
        let limit           = Int(HKObjectQueryNoLimit)
        let sampleType      = HKSampleType.quantityTypeForIdentifier(quantityType)
        
        // Build samples query
        let sampleQuery = HKSampleQuery(sampleType: sampleType!, predicate: predicate, limit: limit, sortDescriptors: sortDescriptors) { (sampleQuery, results, error ) -> Void in
            if error != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completition(false, error)
                })
                return
            }
            
            var data: [HKQuantitySample] = []
            
            data = results as! [HKQuantitySample]
            
            let dataCount = data.count
            
            if dataCount > 0 {
                
                for stepSample in data {
                    
                    if let metadata = stepSample.metadata {
                        
                        if let deviceName = metadata["HealthDataDeviceStringType"] {
                            
                            if deviceName as! String == HealthDataDeviceStringType.Fitbit.rawValue {
                                
                                HKHealthStore().deleteObject(stepSample, withCompletion: { (success, error) in
                                    
                                })
                                
                            }
                            
                        }
                        
                    }
                    
                    
                }
                
            }
            
            if endDate?.isInPast() == true {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.removeAllFitbitData(endDate!, dataType: dataType, completition: completition)
                })
                
            } else {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completition(true, nil);
                })
            }
            
        }
        
        
        let healthManager = HealthManager()
        
        // Execute the Query
        healthManager.healthKitStore.executeQuery(sampleQuery)
        
        
    }
    
    
}






