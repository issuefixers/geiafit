//
//  Toolchain.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 11/25/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

enum ScreenSize: Int {
    case Unknown
    case ThreePointFiveInches
    case FourInches
    case FourPointSevenInches
    case FivePointFiveInches
}

class Toolchain: NSObject {
    class func is_iPad() -> Bool {
        return (UI_USER_INTERFACE_IDIOM() == .Pad)
    }

    class func is_iPhone() -> Bool {
        return (UI_USER_INTERFACE_IDIOM() == .Phone)
    }

    class func getScreenSize() -> ScreenSize {
        var size:ScreenSize = .Unknown

        if self.is_iPhone() {
            let height = self.getWindowSize().height

            if height < 568.0 {
                size = .ThreePointFiveInches
            } else if height == 568.0 {
                size = .FourInches
            } else if height == 667.0 {
                size = .FourPointSevenInches
            } else if height == 736.0 {
                size = .FivePointFiveInches
            }
        }

        return size
    }

    class func getWindowSize() -> CGSize {
        return UIScreen.mainScreen().bounds.size
    }

    class func getScreenDensity() -> CGFloat {
        return UIScreen.mainScreen().scale
    }

    class func stringFromBool(theBool: Bool) -> String {
        return theBool ? "YES" : "NO"
    }

    class func stringFromNSData(theData: NSData) -> String? {
        guard let theString:String = String.init(data: theData, encoding: NSUTF8StringEncoding)! else {
            print("Couldn't convert NSData to String")
            return nil
        }

        return theString
    }

    class func setSubviewOnTopOfViewController(view: UIView) {
        guard let window = UIApplication.sharedApplication().windows.last else {
            print("Couldn't get window reference")
            return
        }

        view.removeFromSuperview()
        window.addSubview(view)

        let views   = ["view":view]
        let metrics = ["width" : window.frame.size.width,
                       "height": window.frame.size.height]

        window.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[view(==width)]",  options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        window.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[view(==height)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        window.addConstraint(NSLayoutConstraint(item: view, attribute: .CenterX, relatedBy: .Equal, toItem: window, attribute: .CenterX, multiplier: 1.0, constant: 0.0))
        window.addConstraint(NSLayoutConstraint(item: view, attribute: .CenterY, relatedBy: .Equal, toItem: window, attribute: .CenterY, multiplier: 1.0, constant: 0.0))
    }
}
