//
//  AlertViewDelegate.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/24/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

protocol AlertViewDelegate: class {
    func showAlert(alert: UIAlertController, sender: UIView?)
}