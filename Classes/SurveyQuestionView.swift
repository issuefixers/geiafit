
//
//  SurveyQuestionView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/18/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class SurveyQuestionView: UIView {

    weak var delegate: SurveyQuestionViewDelegate?
    @IBOutlet var questionNumber: UILabel!
    @IBOutlet var question: UILabel!
    @IBOutlet var option0View: UIView!
    @IBOutlet var option0Text: UILabel!
    @IBOutlet var option0Button: UIButton!
    @IBOutlet var option1View: UIView!
    @IBOutlet var option1Text: UILabel!
    @IBOutlet var option1Button: UIButton!
    @IBOutlet var option2View: UIView!
    @IBOutlet var option2Text: UILabel!
    @IBOutlet var option2Button: UIButton!
    @IBOutlet var option3View: UIView!
    @IBOutlet var option3Text: UILabel!
    @IBOutlet var option3Button: UIButton!
    @IBOutlet var option4View: UIView!
    @IBOutlet var option4Text: UILabel!
    @IBOutlet var option4Button: UIButton!
    @IBOutlet var option5View: UIView!
    @IBOutlet var option5Text: UILabel!
    @IBOutlet var option5Button: UIButton!
    @IBOutlet var option6View: UIView!
    @IBOutlet var option6Text: UILabel!
    @IBOutlet var option6Button: UIButton!

    func loadQuestion(question: Question, number: Int, total: Int) {
        self.question.text       = question.text
        self.questionNumber.text = "\(number) of \(total)"
        let answers              = question.answers?.sortedArrayUsingDescriptors([NSSortDescriptor.init(key: "points", ascending: true)]) as! [Answer]
        self.option0View.hidden = true;
        self.option1View.hidden = true
        self.option2View.hidden = true
        self.option3View.hidden = true
        self.option4View.hidden = true
        self.option5View.hidden = true
        self.option6View.hidden = true
        for answer: Answer in answers {
            switch answer.points {
                case 0:
                    self.option0Text.text       = answer.text!
                    self.option0Button.selected = false
                    self.option0View.hidden     = false
                case 1:
                    self.option1Text.text       = answer.text!
                    self.option1Button.selected = false
                    self.option1View.hidden     = false
                case 2:
                    self.option2Text.text       = answer.text!
                    self.option2Button.selected = false
                    self.option2View.hidden     = false
                case 3:
                    self.option3Text.text       = answer.text!
                    self.option3Button.selected = false
                    self.option3View.hidden     = false
                case 4:
                    self.option4Text.text       = answer.text!
                    self.option4Button.selected = false
                    self.option4View.hidden     = false
                case 5:
                    self.option5Text.text       = answer.text!
                    self.option5Button.selected = false
                    self.option5View.hidden     = false
                case 6:
                    self.option6Text.text       = answer.text!
                    self.option6Button.selected = false
                    self.option6View.hidden     = false
                
                default:
                    self.option1Text.text       = answer.text!
                    self.option1Button.selected = false
                    self.option1View.hidden     = false
            }
        }

        self.layoutIfNeeded()
    }

    @IBAction func chooseOption(sender: UIButton!) {
        sender.selected = true

        if self.delegate != nil {
            self.delegate?.didChooseAnswerWithPoints(sender.tag)
        }
    }
}

protocol SurveyQuestionViewDelegate: class {
    func didChooseAnswerWithPoints(points: Int)
}