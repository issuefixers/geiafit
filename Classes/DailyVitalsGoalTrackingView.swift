//
//  DailyVitalsGoalTrackingView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/15/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

protocol VitalsGoalTrackingViewDelegate: class {
    func didBeginEditing(frame: CGRect)
    func didExpandBox(delta: CGFloat, frame: CGRect)
}

enum VitalSign: Int {
    case Weight, RHR, HR, BMI, Emotion
}

class DailyVitalsGoalTrackingView: UIView, UITextFieldDelegate {

    // MARK: - Outlets
    @IBOutlet weak var weightButton: UIButton!
    @IBOutlet weak var restingHRButton: UIButton!
    @IBOutlet weak var hrButton: UIButton!
    @IBOutlet weak var bmiButton: UIButton!
    @IBOutlet weak var emotionImage: UIImageView!
    @IBOutlet weak var emotionImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var weightHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var restingHRHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var hrHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bmiHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var emotionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var restingHR: UITextField!
    @IBOutlet weak var maxHR: UITextField!
    @IBOutlet weak var bmi: UITextField!
    @IBOutlet weak var emotion: UISlider!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var restingHRView: UIView!
    @IBOutlet weak var maxHRView: UIView!
    @IBOutlet weak var bmiView: UIView!
    @IBOutlet weak var emotionView: UIView!

    // MARK: Local variables
    weak var delegate: VitalsGoalTrackingViewDelegate?
    private let compactHeight  = CGFloat(37)
    private let expandedHeight = CGFloat(130.0)
    private var currentWeight  = 0.0
    private var currentRHR     = 0
    private var currentMaxHR   = 0
    private var currentBMI     = 0.0
    private var currentEmotion = 0.0
    private var todaysCharacteristics: Characteristics?

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()

    private lazy var onboardingViewModel: OnboardingViewModel = {
        return OnboardingViewModel()
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()

    private lazy var numberFormatter:NSNumberFormatter = {
        let nf         = NSNumberFormatter()
        nf.locale      = NSLocale.currentLocale()
        nf.numberStyle = .DecimalStyle

        return nf
    }()

    // MARK: - Actions
    func updateDataFromServer() {
        self.loaderView.show()
        self.onboardingViewModel.getCharacteristicsForUser(self.appDelegate.user!) { (result) -> Void in
            //save changes
            self.appDelegate.saveContext()

            self.loaderView.hide(animated: false)
            self.loadData()
        }
    }

    func loadData() {
        self.weight.delegate    = self
        self.restingHR.delegate = self
        self.maxHR.delegate     = self
        self.bmi.delegate       = self

        self.loaderView.show()
        self.goalTrackingViewModel.getVitalsForUser(self.appDelegate.user!, dateRange: .Day) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool

            if success {

                self.todaysCharacteristics = self.goalTrackingViewModel.getTodaysCharacteristicsObject()

                if let data = result["data"] {
                    //debugPrint(result)

                    if let weightValue = data["weight"] {
                        self.currentWeight                 = weightValue as! Double
                        self.todaysCharacteristics?.weight = self.currentWeight
                    }

                    if let rhrValue = data["rhr"] {
                        self.currentRHR                              = rhrValue as! Int
                        self.todaysCharacteristics?.restingHeartRate = self.currentRHR
                    }

                    if let maxHRValue = data["maxHR"] {
                        self.currentMaxHR                        = maxHRValue as! Int
                        self.todaysCharacteristics?.maxHeartRate = self.currentMaxHR
                    }

                    if let bmiValue = data["bmi"] {
                        self.currentBMI                 = bmiValue as! Double
                        self.todaysCharacteristics?.bmi = self.currentBMI
                    }

                    if let emotionValue = data["emotion"] {
                        self.currentEmotion = emotionValue as! Double
                    } else {
                        self.currentEmotion = 1.0
                    }
                    self.todaysCharacteristics?.emotion = self.currentEmotion

                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.weightButton.setAttributedTitle(self.getAttributtedStringForText("\(self.numberFormatter.stringFromNumber(self.currentWeight)!) LBS"), forState: .Normal)
                        self.restingHRButton.setAttributedTitle(self.getAttributtedStringForText("\(self.currentRHR) BPM"), forState: .Normal)
                        self.hrButton.setAttributedTitle(self.getAttributtedStringForText("\(self.currentMaxHR) BPM"), forState: .Normal)
                        self.bmiButton.setAttributedTitle(self.getAttributtedStringForText(self.numberFormatter.stringFromNumber(self.currentBMI)!), forState: .Normal)

                        self.weight.text        = self.numberFormatter.stringFromNumber(self.currentWeight)!
                        self.restingHR.text     = "\(self.currentRHR)"
                        self.maxHR.text         = "\(self.currentMaxHR)"
                        self.bmi.text           = self.numberFormatter.stringFromNumber(self.currentBMI)
                        self.emotion.value      = Float(self.currentEmotion)
                        self.emotionImage.image = self.goalTrackingViewModel.getEmotionImageForValue(self.emotion.value)
                    })
                }
            }
        }
    }

    @IBAction func toggleViewHeight(sender: UIButton) {
        self.endEditing(true)

        guard sender.tag >= VitalSign.Weight.rawValue && sender.tag <= VitalSign.Emotion.rawValue else {
            return
        }

        let vitalSign = VitalSign(rawValue: sender.tag)!
        UIView.animateWithDuration(0.2) { () -> Void in
            switch vitalSign {
                case .Weight:
                    self.weight.text                     = self.numberFormatter.stringFromNumber(self.currentWeight)!
                    let currentHeight                    = self.weightHeightConstraint.constant
                    self.weightHeightConstraint.constant = self.weightHeightConstraint.constant == self.compactHeight ? self.expandedHeight : self.compactHeight
                    let newHeight                        = self.weightHeightConstraint.constant
                    if newHeight > currentHeight {
                        self.delegate?.didExpandBox(newHeight - currentHeight, frame: self.weightView.frame)
                    }
                    self.delegate?.didBeginEditing(self.weight.frame)
                case .RHR:
                    self.restingHR.text                     = "\(self.currentRHR)"
                    let currentHeight                       = self.restingHRHeightConstraint.constant
                    self.restingHRHeightConstraint.constant = self.restingHRHeightConstraint.constant == self.compactHeight ? self.expandedHeight : self.compactHeight
                    let newHeight                           = self.restingHRHeightConstraint.constant
                    if newHeight > currentHeight {
                        self.delegate?.didExpandBox(newHeight - currentHeight, frame: self.restingHRView.frame)
                    }
                    self.delegate?.didBeginEditing(self.restingHR.frame)
                case .HR:
                    self.maxHR.text                  = "\(self.currentMaxHR)"
                    let currentHeight                = self.hrHeightConstraint.constant
                    self.hrHeightConstraint.constant = self.hrHeightConstraint.constant == self.compactHeight ? self.expandedHeight : self.compactHeight
                    let newHeight                    = self.hrHeightConstraint.constant
                    if newHeight > currentHeight {
                        self.delegate?.didExpandBox(newHeight - currentHeight, frame: self.maxHRView.frame)
                    }
                    self.delegate?.didBeginEditing(self.maxHR.frame)
                case .BMI:
                    self.bmi.text                     = self.numberFormatter.stringFromNumber(self.currentBMI)
                    let currentHeight                 = self.bmiHeightConstraint.constant
                    self.bmiHeightConstraint.constant = self.bmiHeightConstraint.constant == self.compactHeight ? self.expandedHeight : self.compactHeight
                    let newHeight                     = self.bmiHeightConstraint.constant
                    if newHeight > currentHeight {
                        self.delegate?.didExpandBox(newHeight - currentHeight, frame: self.bmiView.frame)
                    }
                    self.delegate?.didBeginEditing(self.bmi.frame)
                case .Emotion:
                    self.emotion.value                    = Float(self.currentEmotion)
                    self.emotionImage.image               = self.goalTrackingViewModel.getEmotionImageForValue(self.emotion.value)
                    let currentHeight                     = self.emotionHeightConstraint.constant
                    self.emotionHeightConstraint.constant = self.emotionHeightConstraint.constant == self.compactHeight ? self.expandedHeight : self.compactHeight
                    let newHeight                         = self.emotionHeightConstraint.constant
                    if newHeight > currentHeight {
                        self.delegate?.didExpandBox(newHeight - currentHeight, frame: self.emotionView.frame)
                        self.emotionImageHeightConstraint.constant *= 2.0
                    } else {
                        self.emotionImageHeightConstraint.constant /= 2.0
                    }
                    self.delegate?.didBeginEditing(self.emotion.frame)
            }

            self.layoutIfNeeded()
        }
    }

    @IBAction func updateValue(sender: UIButton) {
        guard sender.tag >= VitalSign.Weight.rawValue && sender.tag <= VitalSign.Emotion.rawValue else {
            return
        }

        let vitalSign = VitalSign(rawValue: sender.tag)!
        switch vitalSign {
            case .Weight:
                // TODO: Validate values
                self.currentWeight                 = self.numberFormatter.numberFromString(self.weight.text!) as! Double
                self.todaysCharacteristics?.weight = self.currentWeight
                self.weightButton.setAttributedTitle(self.getAttributtedStringForText("\(self.numberFormatter.stringFromNumber(self.currentWeight)!) LBS"), forState: .Normal)
            case .RHR:
                self.currentRHR                              = self.numberFormatter.numberFromString(self.restingHR.text!) as! Int
                self.todaysCharacteristics?.restingHeartRate = self.currentRHR
                self.restingHRButton.setAttributedTitle(self.getAttributtedStringForText("\(self.currentRHR) BPM"), forState: .Normal)
            case .HR:
                self.currentMaxHR                        = self.numberFormatter.numberFromString(self.maxHR.text!) as! Int
                self.todaysCharacteristics?.maxHeartRate = self.currentMaxHR
                self.hrButton.setAttributedTitle(self.getAttributtedStringForText("\(self.currentMaxHR) BPM"), forState: .Normal)
            case .BMI:
                self.currentBMI                 = self.numberFormatter.numberFromString(self.bmi.text!) as! Double
                self.todaysCharacteristics?.bmi = self.currentBMI
                self.bmiButton.setAttributedTitle(self.getAttributtedStringForText(self.numberFormatter.stringFromNumber(self.currentBMI)!), forState: .Normal)
            case .Emotion:
                self.currentEmotion                 = Double(self.emotion.value)
                self.todaysCharacteristics?.emotion = self.currentEmotion
                self.emotionImage.image             = self.goalTrackingViewModel.getEmotionImageForValue(self.emotion.value)
        }

        // Save Values to DB
        self.appDelegate.saveContext()

        self.uploadVitals()

        //collapse the view
        self.toggleViewHeight(sender)
    }

    func uploadVitals() {
        //update record date
        self.todaysCharacteristics?.recordDate = NSDate().dateAtStartOfDay()

        // Save Values to DB
        self.appDelegate.saveContext()

        //upload new data
        self.loaderView.show()
        self.onboardingViewModel.updateCharacteristics(self.todaysCharacteristics!) { (result) -> Void in
            self.loaderView.hide(animated: true)
        }
    }

    @IBAction func emotionValueDidChange(sender: UISlider) {
        self.emotionImage.image = self.goalTrackingViewModel.getEmotionImageForValue(sender.value)
    }

    func getAttributtedStringForText(text: String) -> NSAttributedString {
        let style           = NSMutableParagraphStyle()
        style.alignment     = .Left
        style.lineBreakMode = .ByWordWrapping
        let normalFont      = UIFont.geiaFontOfSize(15.0)
        let theColor        = UIColor.geiaMidBlueColor()

        let str = NSAttributedString(string: text, attributes: [NSFontAttributeName: normalFont, NSParagraphStyleAttributeName: style, NSForegroundColorAttributeName: theColor, NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue])

        return str
    }

    // MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField) {
        self.delegate?.didBeginEditing(textField.frame)
    }

}
