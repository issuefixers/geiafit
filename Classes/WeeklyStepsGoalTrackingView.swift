//
//  WeeklyStepsGoalTrackingView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class WeeklyStepsGoalTrackingView: UIView, SChartDatasource {

    // MARK: - Outlets
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var dataPoint: UIView!

    // MARK: Local variables
    private var datasource: [[String: AnyObject]] = []
    private var goals: [ActivityGoals] = []
    private var chart: ShinobiChart?

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()

    // MARK: - Actions
    func updateWeeklySteps() {
        self.goalTrackingViewModel.updateStepsFromHealthKitForDateRange(.Week) { (success) -> Void in
            if success {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //save changes
                    self.appDelegate.saveContext()

                    self.loadData()
                })
            }
        }
    }
    
    func loadData() {
        //make datapoint round
        self.dataPoint.convertToCircle()
        
        self.loaderView.show()
        self.goalTrackingViewModel.getStepsForUser(self.appDelegate.user!, dateRange: .Week) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool

            if success {
                //debugPrint(result)
                if let data = result["stepData"] {
                    self.datasource = data as! [[String: AnyObject]]
                }

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.setupGraph()
                })
            }
        }
    }

    func setupGraph() {
        let padding = CGFloat(10.0)
        let frame   = CGRect(x: 0, y: padding, width: self.graphView.bounds.width - padding, height: self.graphView.bounds.height - padding)
        self.chart                             = ShinobiChart(frame: frame)
        self.chart!.clearsContextBeforeDrawing = true
        self.chart!.autoresizingMask           = .None
        self.chart!.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.chart!.backgroundColor            = .clearColor()
        self.chart!.canvasAreaBackgroundColor  = .clearColor()
        self.chart!.plotAreaBackgroundColor    = .whiteColor()
        self.chart!.plotAreaBorderThickness    = 1.0
        self.chart!.plotAreaBorderColor        = .lightGrayColor()
        self.chart!.borderThickness            = 0.0
        self.chart!.titleLabel.font            = .geiaFontOfSize(15.0)

        //Axes
        let xAxis = SChartDateTimeAxis()
        xAxis.majorTickFrequency             = SChartDateFrequency(day: 1)
        xAxis.minorTickFrequency             = SChartDateFrequency(hour: 12)
        xAxis.rangePaddingHigh               = SChartDateFrequency(hour: 12)
        xAxis.rangePaddingLow                = SChartDateFrequency(hour: 12)
        xAxis.enableGesturePanning           = false
        xAxis.enableGestureZooming           = false
        xAxis.enableMomentumPanning          = false
        xAxis.enableMomentumZooming          = false
        xAxis.axisPositionValue              = 0
        xAxis.style.lineColor                = .clearColor()
        xAxis.style.majorTickStyle.labelFont = UIFont.systemFontOfSize(12.0)
        xAxis.style.majorTickStyle.showTicks = false
        xAxis.style.majorGridLineStyle.showMajorGridLines = true

        let yAxis = SChartNumberAxis()
        yAxis.title                           = "STEPS"
        yAxis.rangePaddingHigh                = 500.0
        yAxis.rangePaddingLow                 = 100.0
        yAxis.enableGesturePanning            = false
        yAxis.enableGestureZooming            = false
        yAxis.enableMomentumPanning           = false
        yAxis.enableMomentumZooming           = false
        yAxis.axisPositionValue               = 0
        yAxis.style.lineColor                 = .clearColor()
        yAxis.style.majorTickStyle.showTicks  = true
        yAxis.style.majorTickStyle.showLabels = true
        yAxis.style.majorGridLineStyle.showMajorGridLines = true

        self.chart!.xAxis = xAxis
        self.chart!.yAxis = yAxis

        self.graphView.addSubview(self.chart!)
        self.chart!.datasource = self
    }

    // MARK: - SChart Datasource
    func numberOfSeriesInSChart(chart: ShinobiChart!) -> Int {
        return 2
    }

    func sChart(chart: ShinobiChart!, seriesAtIndex index: Int) -> SChartSeries! {
        if index < 1 {
            let lineSeries               = SChartLineSeries()
            lineSeries.style().lineColor = .geiaDarkBlueColor()
            lineSeries.style().lineWidth = 2.0

            return lineSeries
        } else {
            let scatterSeries = SChartScatterSeries()
            scatterSeries.style().useSeriesSymbols        = true
            scatterSeries.crosshairEnabled                = true
            scatterSeries.style().pointStyle().color      = .geiaLightBlueColor()
            scatterSeries.style().pointStyle().innerColor = .geiaLightBlueColor()
            scatterSeries.style().pointStyle().radius      = 10.0
            scatterSeries.style().pointStyle().innerRadius = 1.0

            return scatterSeries
        }
    }

    func sChart(chart: ShinobiChart!, numberOfDataPointsForSeriesAtIndex seriesIndex: Int) -> Int {
        return self.datasource.count
    }

    func sChart(chart: ShinobiChart!, dataPointAtIndex dataIndex: Int, forSeriesAtIndex seriesIndex: Int) -> SChartData! {
        let scDatapoint = SChartDataPoint()
        let datapoint   = self.datasource[dataIndex]

        let xValue = datapoint["timestamp"] as! NSDate
        scDatapoint.xValue = xValue

        var yValue: Double?
        switch seriesIndex {
        case 0:
            yValue = datapoint["goal"] as? Double
        default:
            yValue = datapoint["value"] as? Double
        }
        
        scDatapoint.yValue = yValue
        
        return scDatapoint
    }
}
