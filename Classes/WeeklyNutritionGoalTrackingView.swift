//
//  WeeklyNutritionGoalTrackingView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 2/1/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class WeeklyNutritionGoalTrackingView: UIView {

    // MARK: - Outlets
    @IBOutlet weak var proteinGraphView: UIView!
    @IBOutlet weak var carbsGraphView: UIView!
    @IBOutlet weak var fatGraphView: UIView!
    @IBOutlet weak var dataPoint: UIView!

    // MARK: Local variables
    private var proteinDatasource: [[String: AnyObject]] = []
    private var carbsDatasource: [[String: AnyObject]]   = []
    private var fatDatasource: [[String: AnyObject]]     = []
    private var goals: [ActivityGoals] = []
   
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()

    // MARK: - Actions
    func updateNutrition() {
        self.goalTrackingViewModel.updateNutritionDataFromHealthKitForDateRange(.Week) { (success) -> Void in
            if success {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //save changes
                    self.appDelegate.saveContext()

                    self.loadData()
                })
            }
        }
    }

    func loadData() {
        //make datapoint round
        self.dataPoint.convertToCircle()

        self.loaderView.show()
        self.goalTrackingViewModel.getNutritionDataForUser(self.appDelegate.user!, dateRange: .Week) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool

            if success {
                //debugPrint(result)
                if let proteinData = result["proteinData"] {
                    self.proteinDatasource = proteinData as! [[String: AnyObject]]
                }
                if let carbsData = result["carbsData"] {
                    self.carbsDatasource = carbsData as! [[String: AnyObject]]
                }
                if let fatData = result["fatData"] {
                    self.fatDatasource = fatData as! [[String: AnyObject]]
                }

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.setupGraphs()
                })
            }
        }
    }

    func setupGraphs() {
    }

}