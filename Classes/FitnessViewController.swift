//
//  FitnessTabView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/24/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class FitnessViewController: UITableViewController {

    // MARK: - Properties
    @IBOutlet weak var steps:        UILabel!
    @IBOutlet weak var activeHigh:   UILabel!
    @IBOutlet weak var activeMedium: UILabel!
    @IBOutlet weak var activeLow:    UILabel!
    @IBOutlet weak var instructions: UILabel!
    var prescriptionHelp = "";

    
    private let prescriptionsViewModel = PrescriptionsViewModel()
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    private lazy var dateFormatter:NSDateFormatter = {
        let df       = NSDateFormatter()
        df.locale    = NSLocale.currentLocale()
        df.timeStyle = .NoStyle
        df.dateStyle = .MediumStyle
        
        return df
    }()
    
    private lazy var numberFormatter:NSNumberFormatter = {
        let nf         = NSNumberFormatter()
        nf.locale      = NSLocale.currentLocale()
        nf.numberStyle = .DecimalStyle
        
        return nf
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(FitnessViewController.dismissViewController));
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Help", style: .Plain, target: self, action: #selector(FitnessViewController.showHelp));

        self.title = "Activity Levels"
        
        let lblTotal = UILabel(frame: CGRectMake(0,0,CGRectGetWidth(self.tableView!.frame),33));
        lblTotal.text = "Today / " + NSDate().stringDateWithFormat("MMM dd, YYYY");
        lblTotal.textAlignment = .Center;
        lblTotal.backgroundColor = UIColor.blackColor();
        lblTotal.textColor = UIColor.whiteColor();
        lblTotal.font = UIFont.geiaFontOfSize(14.5);
        self.tableView!.tableHeaderView = lblTotal;
        
        prescriptionsViewModel.getPrescriptionHelp { (result) in
            self.prescriptionHelp = result;
        }
        self.getContentFromServerForUser(self.appDelegate.user!) { (result) in
            //
        }
    }

    // MARK: - Methods
    func getContentFromServerForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        self.loaderView.show()
        self.prescriptionsViewModel.getFitnessGoalsForUser(user) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool
            
            if success {
                //add to the DB
                let data = result["data"] as! [[String:AnyObject]]
                for fitnessData in data {
                    ActivityGoals.addGoalsForUser(self.appDelegate.user!, data: fitnessData, moc: self.appDelegate.managedObjectContext)
                }

                //save changes
                self.appDelegate.saveContext()

                //grab the latest one
                if let goals = ActivityGoals.getLatestGoalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext) {
                    self.steps.text        = "\(goals.totalSteps) steps"
                    self.activeHigh.text   = "\(goals.vigorousTime) minutes"
                    self.activeMedium.text = "\(goals.moderateTime) minutes"
                    self.activeLow.text    = "\(goals.lightTime) minutes"
                    self.instructions.text = goals.instructions

                    let formattedDate = self.dateFormatter.stringFromDate(goals.date)
                    completion(result: ["success":true, "date":formattedDate])
                }
            } else {
                let message = result["errorMessage"] as! String
                let alert: UIAlertController = UIAlertController(title: "Posture", message: message, preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
//                self.alertViewDelegate?.showAlert(alert, sender: self.view)
            }
        }
    }
    func dismissViewController(){
        self.navigationController!.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func showHelp(){
        self.performSegueWithIdentifier("goToHelp", sender: self);
    }
    
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return !prescriptionHelp.isEmpty;
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destinationController = (segue.destinationViewController as! UINavigationController).viewControllers.first as! HealthHelpViewController;
        
        destinationController.htmlStringHelp = prescriptionHelp;
        
        
    }

}
