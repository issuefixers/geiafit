//
//  WeeklyActivityGoalTrackingView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/13/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class WeeklyActivityGoalTrackingView: UIView, SChartDatasource {

    // MARK: - Outlets
    @IBOutlet weak var lightGraphView: UIView!
    @IBOutlet weak var moderateGraphView: UIView!
    @IBOutlet weak var vigorousGraphView: UIView!
    @IBOutlet weak var dataPoint: UIView!

    // MARK: Local variables
    private var lightDatasource: [[String: AnyObject]]    = []
    private var moderateDatasource: [[String: AnyObject]] = []
    private var vigorousDatasource: [[String: AnyObject]] = []
    private var goals: [ActivityGoals] = []
    private var lightChart: ShinobiChart?
    private var moderateChart: ShinobiChart?
    private var vigorousChart: ShinobiChart?

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()

    // MARK: - Actions
    func updateSteps() {
        self.goalTrackingViewModel.updateStepsFromHealthKitForDateRange(.Week) { (success) -> Void in
            if success {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //save changes
                    self.appDelegate.saveContext()

                    self.loadData()
                })
            }
        }
    }

    func loadData() {
        //make datapoint round
        self.dataPoint.convertToCircle()

        self.loaderView.show()
        self.goalTrackingViewModel.getStepsForUser(self.appDelegate.user!, dateRange: .Week) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool

            if success {
                //debugPrint(result)
                if let lightData = result["lightData"] {
                    self.lightDatasource = lightData as! [[String: AnyObject]]
                }
                if let moderateData = result["moderateData"] {
                    self.moderateDatasource = moderateData as! [[String: AnyObject]]
                }
                if let vigorousData = result["vigorousData"] {
                    self.vigorousDatasource = vigorousData as! [[String: AnyObject]]
                }

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.setupGraphs()
                })
            }
        }
    }

    func setupGraphs() {
        let padding = CGFloat(10.0)
        let frame   = CGRect(x: padding, y: padding, width: self.lightGraphView.bounds.width - 2 * padding, height: self.lightGraphView.bounds.height - 2 * padding)
        self.lightChart                             = ShinobiChart(frame: frame)
        self.lightChart?.clearsContextBeforeDrawing = true
        self.lightChart?.title                      = "Light"
        self.lightChart?.titleLabel.font            = .geiaFontOfSize(15.0)
        self.lightChart?.autoresizingMask           = .None
        self.lightChart?.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.lightChart?.backgroundColor            = .clearColor()
        self.lightChart?.canvasAreaBackgroundColor  = .clearColor()
        self.lightChart?.plotAreaBackgroundColor    = .whiteColor()
        self.lightChart?.plotAreaBorderThickness    = 1.0
        self.lightChart?.plotAreaBorderColor        = .lightGrayColor()

        self.moderateChart                             = ShinobiChart(frame: frame)
        self.moderateChart?.clearsContextBeforeDrawing = true
        self.moderateChart?.title                      = "Moderate"
        self.moderateChart?.titleLabel.font            = .geiaFontOfSize(15.0)
        self.moderateChart?.autoresizingMask           = .None
        self.moderateChart?.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.moderateChart?.backgroundColor            = .clearColor()
        self.moderateChart?.canvasAreaBackgroundColor  = .clearColor()
        self.moderateChart?.plotAreaBackgroundColor    = .whiteColor()
        self.moderateChart?.plotAreaBorderThickness    = 1.0
        self.moderateChart?.plotAreaBorderColor        = .lightGrayColor()

        self.vigorousChart                             = ShinobiChart(frame: frame)
        self.vigorousChart?.clearsContextBeforeDrawing = true
        self.vigorousChart?.title                      = "Vigorous"
        self.vigorousChart?.titleLabel.font            = .geiaFontOfSize(15.0)
        self.vigorousChart?.autoresizingMask           = .None
        self.vigorousChart?.licenseKey                 = SHINOBI_CHARTS_LICENSE
        self.vigorousChart?.backgroundColor            = .clearColor()
        self.vigorousChart?.canvasAreaBackgroundColor  = .clearColor()
        self.vigorousChart?.plotAreaBackgroundColor    = .whiteColor()
        self.vigorousChart?.plotAreaBorderThickness    = 1.0
        self.vigorousChart?.plotAreaBorderColor        = .lightGrayColor()

        //Axes
        self.lightChart?.xAxis    = self.setupXAxis()
        self.moderateChart?.xAxis = self.setupXAxis()
        self.vigorousChart?.xAxis = self.setupXAxis()

        self.lightChart?.yAxis    = self.setupYAxis()
        self.moderateChart?.yAxis = self.setupYAxis()
        self.vigorousChart?.yAxis = self.setupYAxis()

        self.lightGraphView.addSubview(self.lightChart!)
        self.moderateGraphView.addSubview(self.moderateChart!)
        self.vigorousGraphView.addSubview(self.vigorousChart!)

        self.lightChart?.datasource    = self
        self.moderateChart?.datasource = self
        self.vigorousChart?.datasource = self
    }

    func setupXAxis() -> SChartDateTimeAxis {
        let xAxis = SChartDateTimeAxis()
        xAxis.majorTickFrequency             = SChartDateFrequency(day: 1)
        xAxis.minorTickFrequency             = SChartDateFrequency(hour: 12)
        xAxis.rangePaddingHigh               = SChartDateFrequency(hour: 12)
        xAxis.rangePaddingLow                = SChartDateFrequency(hour: 12)
        xAxis.enableGesturePanning           = false
        xAxis.enableGestureZooming           = false
        xAxis.enableMomentumPanning          = false
        xAxis.enableMomentumZooming          = false
        xAxis.axisPositionValue              = 0
        xAxis.style.lineColor                = .clearColor()
        xAxis.style.majorTickStyle.labelFont = UIFont.geiaFontOfSize(8.0)
        xAxis.style.majorTickStyle.showTicks = false
        xAxis.style.majorGridLineStyle.showMajorGridLines = true

        let df                       = NSDateFormatter()
        df.dateFormat                = "MM/dd"
        let tickLabelFormatter       = SChartTickLabelFormatter()
        tickLabelFormatter.formatter = df
        xAxis.labelFormatter         = tickLabelFormatter

        return xAxis
    }

    func setupYAxis() -> SChartNumberAxis {
        let yAxis = SChartNumberAxis()
        yAxis.rangePaddingHigh                = 10.0
        yAxis.rangePaddingLow                 = 10.0
        yAxis.enableGesturePanning            = false
        yAxis.enableGestureZooming            = false
        yAxis.enableMomentumPanning           = false
        yAxis.enableMomentumZooming           = false
        yAxis.axisPositionValue               = 0
        yAxis.style.lineColor                 = .clearColor()
        yAxis.style.majorTickStyle.showTicks  = true
        yAxis.style.majorTickStyle.showLabels = true
        yAxis.style.majorGridLineStyle.showMajorGridLines = true

        return yAxis
    }

    // MARK: - SChart Datasource
    func numberOfSeriesInSChart(chart: ShinobiChart!) -> Int {
        return 2
    }

    func sChart(chart: ShinobiChart!, seriesAtIndex index: Int) -> SChartSeries! {
        if index < 1 {
            let lineSeries               = SChartLineSeries()
            lineSeries.style().lineColor = .geiaDarkBlueColor()
            lineSeries.style().lineWidth = 2.0

            return lineSeries
        } else {
            let scatterSeries = SChartScatterSeries()
            scatterSeries.style().useSeriesSymbols         = true
            scatterSeries.crosshairEnabled                 = true
            scatterSeries.style().pointStyle().color       = .geiaLightBlueColor()
            scatterSeries.style().pointStyle().innerColor  = .geiaLightBlueColor()
            scatterSeries.style().pointStyle().radius      = 10.0
            scatterSeries.style().pointStyle().innerRadius = 1.0

            return scatterSeries
        }
    }

    func sChart(chart: ShinobiChart!, numberOfDataPointsForSeriesAtIndex seriesIndex: Int) -> Int {
        var count = 0

        if chart == self.lightChart {
            count = self.lightDatasource.count
        } else if chart == self.moderateChart {
            count = self.moderateDatasource.count
        } else {
            count = self.vigorousDatasource.count
        }

        return count
    }

    func sChart(chart: ShinobiChart!, dataPointAtIndex dataIndex: Int, forSeriesAtIndex seriesIndex: Int) -> SChartData! {
        let scDatapoint = SChartDataPoint()
        let datapoint   = chart == self.lightChart ? self.lightDatasource[dataIndex] : chart == self.moderateChart ? self.moderateDatasource[dataIndex] : self.vigorousDatasource[dataIndex]

        let xValue = datapoint["timestamp"] as! NSDate
        scDatapoint.xValue = xValue

        var yValue: Double?
        switch seriesIndex {
        case 0:
            yValue = datapoint["goal"] as? Double
        default:
            yValue = datapoint["value"] as? Double
        }
        
        scDatapoint.yValue = yValue
        
        return scDatapoint
    }
}
