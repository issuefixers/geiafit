//
//  DailyStepsGoalTrackingView.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class DailyStepsGoalTrackingView: UIView, SChartDatasource {

    // MARK: - Outlets
    @IBOutlet weak var progressView: CircleProgressView!
    @IBOutlet weak var totalDistance: UILabel!
    @IBOutlet weak var totalSteps: UILabel!
    @IBOutlet weak var stepGoal: UILabel!
    @IBOutlet weak var totalCalories: UILabel!
    @IBOutlet weak var graphView: UIView!

    // MARK: Local variables
    private var datasource: [[String: AnyObject]] = []

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()

    // MARK: - Actions
    func updateSteps() {
        self.goalTrackingViewModel.updateStepsFromHealthKitForDateRange(.Day) { (success) -> Void in
            if success {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //save changes
                    self.appDelegate.saveContext()

                    self.loadData()
                })
            }
        }
    }
    
    func loadData() {
        self.loaderView.show()
        //Steps
        self.goalTrackingViewModel.getStepsForUser(self.appDelegate.user!, dateRange: .Day) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool

            if success {
                if let data = result["data"] {
                    //debugPrint(result)

                    // group step counts per hour
                    let datasource = data as! [[String: AnyObject]]
                    var groupedData: [[String: AnyObject]] = []

                    var currentHour      = -1
                    var currentHourCount = 0.0
                    var lastTimestamp: NSDate?

                    for obj in datasource {
                        let timestamp = obj["timestamp"] as! NSDate
                        let value     = obj["value"] as! Double

                        if currentHour < 0 {
                            currentHour = timestamp.hour()
                        } else if timestamp.hour() != currentHour {
                            // New hour
                            //1) Save stuff
                            let newObj = ["timestamp": lastTimestamp!, "value": currentHourCount]
                            groupedData.append(newObj)

                            //2) Reset Currents
                            currentHour      = timestamp.hour()
                            currentHourCount = 0.0
                        }

                        lastTimestamp = timestamp
                        currentHourCount += value
                    }

                    //3) After the loop, save the last stuff
                    if lastTimestamp != nil {
                        let newObj = ["timestamp": lastTimestamp!, "value": currentHourCount]
                        groupedData.append(newObj)
                    }

                    self.datasource = groupedData
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.setupGraph()
                    })
                }


                let dailyTotals = HealthDailyTotal.getTodaysTotalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext)
                let totalSteps  = (dailyTotals?.steps)!
                let stepGoal    = result["stepGoal"] as! Int
                let progress    = Double(totalSteps) / Double(stepGoal)

                //Update UI
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let nf                   = NSNumberFormatter()
                    nf.maximumFractionDigits = 0
                    nf.usesGroupingSeparator = true

                    self.totalSteps.text = (nf.stringFromNumber(totalSteps))!
                    self.stepGoal.text   = "of \(nf.stringFromNumber(stepGoal)!)"
                    self.progressView.setProgress(progress, animated: true)
                })
            }
        }

        //Calories
        let healthManager = HealthManager()
        healthManager.getCaloriesForDateRange(.Day, completion: { (activeCalories, restingCalories, error) -> Void in
            if error != nil {
                print("Error reading HealthKit Store: \(error.localizedDescription), \(error.code)")
                return
            }

            var totalCalories = 0.0

            if let totalsForToday = HealthDailyTotal.getTodaysTotalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext) {
                totalCalories = totalsForToday.getTotalCaloriesBurned()
            }

            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let nf                   = NSNumberFormatter()
                nf.maximumFractionDigits = 0
                nf.usesGroupingSeparator = true
                self.totalCalories.text = nf.stringFromNumber(totalCalories)!
                
                //commit new data
                self.appDelegate.saveContext()
            })
        })

        //Distance
        healthManager.getDistanceForDate(NSDate(), completion: { (totalDistance, error) -> Void in
            if error != nil {
                print("Error reading HealthKit Store: \(error.localizedDescription)")
                return
            }

            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let nf                   = NSNumberFormatter()
                nf.maximumFractionDigits = 2

                let distance            = nf.stringFromNumber(totalDistance)!
                self.totalDistance.text = "\(distance)"
            })
        })
    }

    func setupGraph() {
        self.loaderView.hide(animated: true)
        let padding = CGFloat(10.0)
        let frame   = CGRect(x: 0, y: 0, width: self.graphView.bounds.width - padding, height: self.graphView.bounds.height - padding)
        let chart                        = ShinobiChart(frame: frame)
        chart.clearsContextBeforeDrawing = true
        chart.autoresizingMask           = .None
        chart.licenseKey                 = SHINOBI_CHARTS_LICENSE
        chart.backgroundColor            = .clearColor()
        chart.canvasAreaBackgroundColor  = .clearColor()
        chart.plotAreaBackgroundColor    = .whiteColor()
        chart.plotAreaBorderThickness    = 0.0
        chart.plotAreaBorderColor        = .clearColor()

        //Axes
        let xAxis = SChartCategoryAxis()
        xAxis.enableGesturePanning  = false
        xAxis.enableGestureZooming  = false
        xAxis.enableMomentumPanning = false
        xAxis.enableMomentumZooming = false
        xAxis.axisPositionValue     = 0
        xAxis.style.lineColor       = .lightGrayColor()

        xAxis.style.majorTickStyle.showLabels             = false
        xAxis.style.majorGridLineStyle.showMajorGridLines = true

        let yAxis = SChartNumberAxis()
        yAxis.rangePaddingHigh                = 100.0
        yAxis.enableGesturePanning            = false
        yAxis.enableGestureZooming            = false
        yAxis.enableMomentumPanning           = false
        yAxis.enableMomentumZooming           = false
        yAxis.axisPositionValue               = 0
        yAxis.style.lineColor                 = .clearColor()
        yAxis.style.majorTickStyle.showTicks  = true
        yAxis.style.majorTickStyle.showLabels = true

        chart.xAxis = xAxis
        chart.yAxis = yAxis

        self.graphView.addSubview(chart)

        var max = 0.0
        for datapoint in self.datasource {
            let value = datapoint["value"] as! Double

            max = value > max ? value : max
        }

        chart.yAxis.defaultRange = nil
        chart.yAxis.setRange(SChartRange(minimum: Double(0), andMaximum: Double(max)), withAnimation: false)

        chart.datasource = self
    }


    // MARK: - SChart Datasource
    func numberOfSeriesInSChart(chart: ShinobiChart!) -> Int {
        return 1
    }

    func sChart(chart: ShinobiChart!, seriesAtIndex index: Int) -> SChartSeries! {
        let series = SChartColumnSeries()

        series.orientation       = SChartSeriesOrientationHorizontal
        series.style().lineColor = .geiaLightBlueColor()
        series.style().areaColor = .geiaLightBlueColor()
        series.style().showArea  = true
        series.style().lineWidth = 2
        series.crosshairEnabled  = true
        series.style().showAreaWithGradient = false

        return series
    }

    func sChart(chart: ShinobiChart!, numberOfDataPointsForSeriesAtIndex seriesIndex: Int) -> Int {
        return self.datasource.count
    }

    func sChart(chart: ShinobiChart!, dataPointAtIndex dataIndex: Int, forSeriesAtIndex seriesIndex: Int) -> SChartData! {
        let scDatapoint = SChartDataPoint()
        let datapoint   = self.datasource[dataIndex]

        let df        = NSDateFormatter()
        df.dateFormat = "h a"
        let xValue    = datapoint["timestamp"] as! NSDate
        let category  = df.stringFromDate(xValue)
        scDatapoint.xValue = category

        let yValue = datapoint["value"] as? Double
        scDatapoint.yValue = yValue!

        return scDatapoint
    }

}
