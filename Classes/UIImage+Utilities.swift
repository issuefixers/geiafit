//
//  UIImage+Utilities.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/15/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

extension UIImage {

    //
    //Colorizes an image with the given UIColor
    //
    func imageWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()
        CGContextTranslateCTM(context!, 0, self.size.height)
        CGContextScaleCTM(context!, 1.0, -1.0)
        CGContextSetBlendMode(context!, CGBlendMode.Normal)
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        CGContextClipToMask(context!, rect, self.CGImage!)
        color.setFill()
        CGContextFillRect(context!, rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }

    //
    // Processes images resizing them to a given size and cropping them
    // to the correct ratio
    //
    func cropAndResizeTo(newSize: CGSize) -> UIImage {
        var newPic = self

        //get height and width
        let originalSize  = self.size
        let originalRatio = originalSize.width / originalSize.height
        let newRatio      = newSize.width / newSize.height

        if originalRatio != newRatio {
            //we need to crop it
            let smallestSide = originalSize.width < originalSize.height ? originalSize.width : originalSize.height
            let sideSize     = ceil(fabs(smallestSide / newRatio))
            let croppedSize  = CGSize(width: sideSize, height: sideSize)
            newPic           = self.cropImageToSize(croppedSize)
        }

        //Now that it's cropped, lets resize it
        newPic = newPic.resizeTo(newSize)

        return newPic
    }

    //
    // Crops the image to a given size maintaining the same image rotation
    //
    func cropImageToSize(newSize: CGSize) -> UIImage {
        let imageSize = self.size
        let diff      = ceil(imageSize.height - newSize.height)
        let cropRect  = CGRect(x: 0, y: ceil(diff/2.0), width: newSize.width, height: newSize.height)

        var rectTransform:CGAffineTransform

        switch self.imageOrientation {
            case .Left:
                rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(CGFloat(M_PI_2)), 0, -imageSize.height)
            case .Right:
                rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(CGFloat(-M_PI_2)), -imageSize.width, 0)
            case .Down:
                rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(CGFloat(-M_PI)), -imageSize.width, -imageSize.height)
            default:
                rectTransform = CGAffineTransformIdentity
        }
        rectTransform = CGAffineTransformScale(rectTransform, self.scale, self.scale)

        let imageRef = CGImageCreateWithImageInRect(self.CGImage!, CGRectApplyAffineTransform(cropRect, rectTransform))
        let newImage = UIImage(CGImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)

        return newImage
    }

    //
    // Resizes the image to a given size
    //
    func resizeTo(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        self.drawInRect(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }

    //
    // Crops a specific rect out of the image
    //
    func cropRect(rect: CGRect) -> UIImage {
        let image        = self.CGImage
        let croppedRef   = CGImageCreateWithImageInRect(image!, rect)
        let croppedImage = UIImage(CGImage: croppedRef!)

        return croppedImage
    }
    
    class func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context!, color.CGColor)
        CGContextFillRect(context!, rect)
            
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
            
        return image!
    }
    

}
