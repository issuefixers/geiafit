//
//  UIView+Utilities.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/15/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

extension UIView {

    func setRoundedCorners(corners: UIRectCorner, radius: CGFloat) {
        let rect = self.bounds

        //create path
        let maskPath = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))

        //create the shape layer and set its path
        let maskLayer   = CAShapeLayer()
        maskLayer.frame = rect
        maskLayer.path  = maskPath.CGPath

        //set the new shape layer as the mask for the view's layer
        self.layer.mask = maskLayer
    }

    //
    //Apply circle mask to image
    //
    func convertToCircle() {
        self.clipsToBounds      = true
        self.layer.cornerRadius = floor(self.bounds.width/2.0)
    }
    public class func fromNib(nibNameOrNil: String? = nil) -> Self {
        return fromNib(nibNameOrNil, type: self)
    }
    
    public class func fromNib<T : UIView>(nibNameOrNil: String? = nil, type: T.Type) -> T {
        let v: T? = fromNib(nibNameOrNil, type: T.self)
        return v!
    }
    
    public class func fromNib<T : UIView>(nibNameOrNil: String? = nil, type: T.Type) -> T? {
        var view: T?
        let name: String
        if let nibName = nibNameOrNil {
            name = nibName
        } else {
            // Most nibs are demangled by practice, if not, just declare string explicitly
            name = nibName
        }
        let nibViews = NSBundle.mainBundle().loadNibNamed(name, owner: nil, options: nil)
        for v in nibViews! {
            if let tog = v as? T {
                view = tog
            }
        }
        return view
    }
    
    public class var nibName: String {
        let name = "\(self)".componentsSeparatedByString(".").first ?? ""
        return name
    }
    public class var nib: UINib? {
        if let _ = NSBundle.mainBundle().pathForResource(nibName, ofType: "nib") {
            return UINib(nibName: nibName, bundle: nil)
        } else {
            return nil
        }
    }
}
