//
//  NSAttributedSring+HTML.swift
//  Geia
//
//  Created by Sergio Solorzano on 10/12/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

extension String {
    var utf8Data: NSData? {
        return dataUsingEncoding(NSUTF8StringEncoding)
    }
}


extension NSData {
    var attributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options:[NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
