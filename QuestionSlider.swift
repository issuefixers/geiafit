//
//  QuestionSlider.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/15/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class QuestionSlider: QuestionView {

    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var sldQuestion: UISlider!
    @IBOutlet weak var emoteView: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    class func viewWithQuestion(question: QuestionMeasure) -> QuestionSlider{
        let view = QuestionSlider.fromNib();
        view.qMeasure = question;
        view.lblQuestion.text = question.question;
        view.sldQuestion.minimumValue = 0;//Float(question.min!);
        view.sldQuestion.maximumValue = 100;// Float(question.max!);
        view.lblAmount.text       = "\(Int(view.sldQuestion.value))%";
        view.emoteView.image = view.getEmotionImageForValue(view.sldQuestion.value)
        
        if question.icon == "emoji" {
            view.emoteView.alpha = 1;
            view.lblAmount.alpha = 0;
        }else{
            view.emoteView.alpha = 0;
            view.lblAmount.alpha = 1;
        }
        return view;
    }
    
    @IBAction func valueSliderChanged(sender: AnyObject) {
        changed = true;
        print(sldQuestion.value);
        self.lblAmount.text       = "\(Int(sldQuestion.value))%";
        self.emoteView.image = self.getEmotionImageForValue(sender.value)
        qMeasure?.value = String(sldQuestion.value);
    }
    
    
    func getEmotionImageForValue(value: Float) -> UIImage? {
        var filteredValue = value
        
        if filteredValue < 0 {
            filteredValue = 0.0
        }
        
        var imageName: String
        switch filteredValue {
        case 0 ... 10.0: imageName = "emotion-1"
        case 10 ... 20.0: imageName = "emotion-2"
        case 20 ... 30.0: imageName = "emotion-3"
        case 30 ... 40.0: imageName = "emotion-4"
        case 40 ... 50.0: imageName = "emotion-5"
        case 50 ... 60.0: imageName = "emotion-6"
        case 60 ... 70.0: imageName = "emotion-7"
        case 70 ... 80.0: imageName = "emotion-8"
        case 80 ... 90.0: imageName = "emotion-9"
        default:            imageName = "emotion-10"
        }
        
        let image = UIImage(named: imageName)
        return image
    }


}
