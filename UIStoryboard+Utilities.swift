//
//  UIStoryboard+Utilities.swift
//  Geia
//
//  Created by Sergio Solorzano on 8/18/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import UIKit



extension UIStoryboard {
    class func getPrescriptionStoryboard() -> UIStoryboard{
        
        return  UIStoryboard(name: "Prescriptions", bundle: NSBundle.mainBundle())
    }
    class func getPostureStoryboard() -> UIStoryboard{
        
        return  UIStoryboard(name: "Posture", bundle: NSBundle.mainBundle())
    }
    
    class func getDashboardStoryboard() -> UIStoryboard{
        
        return UIStoryboard(name: "Dashboard", bundle: NSBundle.mainBundle())
        
    }
    
    class func getMessagesStoryboard() -> UIStoryboard{
        return UIStoryboard(name: "Messages", bundle: NSBundle.mainBundle())
    }
    
    class func getGoalTrackingStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "GoalTracking", bundle: NSBundle.mainBundle());
    }
    class func getActivityStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Activity", bundle: NSBundle.mainBundle());
    }

    
    class func getProfileStoryboard() ->UIStoryboard{
        return  UIStoryboard(name: "Profile", bundle: NSBundle.mainBundle())
        
    }
    class func getOnboardingStoryboard() ->UIStoryboard{
        return  UIStoryboard(name: "Onboarding", bundle: NSBundle.mainBundle())
        
    }
    class func getVideoCaptureStoryboard() ->UIStoryboard{
        return  UIStoryboard(name: "VideoCapture", bundle: NSBundle.mainBundle())
        
    }
    class func getNotificationStoryboard() -> UIStoryboard{
        return UIStoryboard(name: "Notifications", bundle: NSBundle.mainBundle());
    }
    class func getMainStoryboard() -> UIStoryboard{
        return UIStoryboard(name: "Main", bundle: NSBundle.mainBundle());
    }
}
