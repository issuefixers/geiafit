//
//  UIFont+Utilities.swift
//  Geia
//
//  Created by Sergio Solorzano on 1/23/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

extension UIFont{
    class func geiaFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Regular", size: size)!
    }
    class func geiaBoldFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Bold", size: size)!
    }
    
}