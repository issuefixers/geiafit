//
//  SSInstagramSegmented.swift
//  Geia
//
//  Created by Sergio Solorzano on 8/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit


enum ComponentOrientation {
    case TopDown
    case LeftRight
}


class SSInstagramSegmented: UIControl {

    
    private var componentOrientation: ComponentOrientation = ComponentOrientation.LeftRight
    
    private var icons = [UIImageView]()
    
    private var img_icon = UIImageView()
    private var selected_img_icon = UIImageView()
    
    private var withIcon:Bool = true
    
    private func setOrientation(orientation orientation:ComponentOrientation){
        switch orientation {
        case .LeftRight:
            componentOrientation = ComponentOrientation.LeftRight
        case .TopDown :
            componentOrientation = ComponentOrientation.TopDown
        }
    }
    
    private var thumbColor: UIColor = UIColor.whiteColor() {
        didSet{
            setThumbColor()
        }
    }
    
    private func setThumbColor(){
        thumbView.backgroundColor = UIColor.clearColor();
        thumbView.layer.borderColor = thumbColor.CGColor;
        thumbView.layer.borderWidth = 2;
        
    }
    
    private var textColor: UIColor = UIColor.whiteColor()
    
    
    
    
    
    private var thumbView = UIView()
    
    
    
    private var icon:[UIImage] = []
    private var selected_icon:[UIImage] = []
    
    var selectedIndex:Int = 0 {
        didSet{
            displayNewSelectedIndex()
        }
    }
    
    
    init(frame: CGRect, items:[UIImage], backgroundColor:UIColor) {
        super.init(frame: frame)
        self.icon = items
        self.backgroundColor = backgroundColor
        self.componentOrientation = ComponentOrientation.TopDown
        self.withIcon = false
        setupView()
    }
    
    
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
    }
    
    private func getIconFrameByOrientation(orientation:ComponentOrientation, index:Int, text:String) -> CGRect {
        let width = self.bounds.width/CGFloat(icon.count)
        let height = self.bounds.height
        
        
        let iconRect = CGRectMake(width*CGFloat(index-1), 0, width, height)
        return iconRect
    }
    
    private func getTextFrameByOrintation(orientation:ComponentOrientation, text:String, index:Int) -> CGRect {
        let height = self.bounds.height
        let width = self.bounds.width/CGFloat(icon.count)
        let xPosition = CGFloat(index) * width
        let evaluateTextX = xPosition + width
        
        switch orientation {
        case .LeftRight:
            let textRect = CGRectMake(evaluateTextX, 0, width, height)
            return textRect
        case .TopDown:
            let centre = evaluateTextX - 13
            let textRect:CGRect?
            if withIcon {
                textRect = CGRectMake(centre, 18, width, 25)
            }else {
                textRect = CGRectMake(centre, 0, width, height)
            }
            return textRect!
        }
    }
    
    
    private func setupView(){
        layer.cornerRadius = 5
        setupImages()
        insertSubview(thumbView, atIndex: 99)
        displayNewSelectedIndex();
    }
    
    private func setupImages(){
        
        for index in 1...icon.count {
            
            
            let view = UIView.init(frame: CGRectZero)
            self.img_icon = UIImageView(frame: getIconFrameByOrientation(self.componentOrientation, index: index, text: ""))
            self.img_icon.contentMode = .ScaleAspectFill
            self.img_icon.image = icon[index-1]
            self.img_icon.clipsToBounds = true;
            self.img_icon.alpha = 0.4;
            view.addSubview(self.img_icon)
            
            view.addSubview(self.img_icon)
            icons.append(self.img_icon)
            
            self.addSubview(view)
            
        }
        
        
        
    }
    
    
    
    
    internal override func layoutSubviews() {
        super.layoutSubviews()
        
        var selectedFrame = self.bounds
        let newWidth = CGRectGetWidth(selectedFrame) / CGFloat(icon.count)
        selectedFrame.size.width = newWidth
        
        selectedFrame.origin.x = selectedFrame.origin.x
        selectedFrame.origin.y = selectedFrame.origin.y
        selectedFrame.size.width = selectedFrame.width
        selectedFrame.size.height = selectedFrame.height
        
        
        if selectedIndex > 0 {
            thumbView.frame = icons[selectedIndex].frame
        }else {
            thumbView.frame = selectedFrame
        }
        
        thumbView.backgroundColor = UIColor.clearColor();
        thumbView.layer.borderWidth = 2;
        thumbView.layer.borderColor = UIColor.whiteColor().CGColor;
        
        
        
    }
    
    
    
    internal override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        let location = touch.locationInView(self)
        
        
        var calculatedIndex : Int?
        for (index, item) in icons.enumerate() {
            
            
            let frame = item.frame;
            
            if frame.contains(location){
                calculatedIndex = index
            }else{
                if withIcon {
                    icons[index].image = icon[index]
                }
                
            }
        }
        
        if calculatedIndex != nil {
            selectedIndex = calculatedIndex!
            sendActionsForControlEvents(.ValueChanged)
        }
        return false
    }
    
    private func displayNewSelectedIndex(){
        
        selected_img_icon = icons[selectedIndex]
        self.deselectItems();
        
        UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: [], animations: {
            
            
            
            
            self.selected_img_icon.alpha = 1;
            
            self.thumbView.frame = self.selected_img_icon.frame
            
            }, completion: nil)
        
        
    }
    
    
    func deselectItems(){
        for image in icons{
            image.alpha = 0.45;
        }
    }
    
}
