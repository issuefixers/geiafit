/*
The MIT License (MIT)

Copyright (c) 2015 Max Konovalov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

import UIKit

@IBDesignable
class MKRingProgressGroupView: UIView {

    let  ringus:[MKRingProgressView] = [MKRingProgressView(),MKRingProgressView(),MKRingProgressView()];
//    let ring1 = MKRingProgressView()
//    let ring2 = MKRingProgressView()
//    let ring3 = MKRingProgressView()
    
    @IBInspectable var ring1StartColor: UIColor = UIColor.init(hexString: "#4299D1") {
        didSet {
            ringus[0].startColor = UIColor.init(hexString: "#4299D1")
        }
    }
    
    @IBInspectable var ring1EndColor: UIColor = UIColor.init(hexString: "#1F60A4") {
        didSet {
            ringus[0].endColor = UIColor.init(hexString: "#1F60A4")
        }
    }
    
    @IBInspectable var ring2StartColor: UIColor = UIColor.redColor() {
        didSet {
            ringus[1].startColor = ring2StartColor
        }
    }
    
    @IBInspectable var ring2EndColor: UIColor = UIColor.blueColor() {
        didSet {
            ringus[1].endColor = ring2EndColor
        }
    }
    
    @IBInspectable var ring3StartColor: UIColor = UIColor.init(hexString: "#DDF6BC") {
        didSet {
            ringus[2].startColor = UIColor.init(hexString: "#DDF6BC")
        }
    }
    
    @IBInspectable var ring3EndColor: UIColor = UIColor.init(hexString: "#B8E986") {
        didSet {
            ringus[2].endColor = UIColor.init(hexString: "#B8E986")
        }
    }
    
    @IBInspectable var ringWidth: CGFloat = 20 {
        didSet {
            ringus[0].ringWidth = ringWidth
            ringus[1].ringWidth = ringWidth
            ringus[2].ringWidth = ringWidth
            setNeedsLayout()
        }
    }
    
    @IBInspectable var ringSpacing: CGFloat = 2 {
        didSet {
            setNeedsLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        addSubview(ringus[0])
        ringus[0].startColor = UIColor.init(hexString: "#4299D1")
        ringus[0].endColor = UIColor.init(hexString: "#1F60A4")
        addSubview(ringus[1])
        addSubview(ringus[2])
        ringus[2].startColor = UIColor.init(hexString: "#DDF6BC")
        ringus[2].endColor = UIColor.init(hexString: "#B8E986")


    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        ringus[0].frame = bounds
        ringus[1].frame = bounds.insetBy(dx: ringWidth + ringSpacing, dy: ringWidth + ringSpacing)
        ringus[2].frame = bounds.insetBy(dx: 2 * ringWidth + 2 * ringSpacing, dy: 2 * ringWidth + 2 * ringSpacing)

    }
    
    
    func updateChartData(progressArray: [Double]){
        for index in 0..<progressArray.count {
            
            ringus[index].progress = progressArray[index];
            print("I setted \(progressArray[index])");
        }
    }
/*
     - (void)updateChartData:(NSArray *)rangeArr
     {
     [self setChartData:rangeArr];
     }
     
 */
}
