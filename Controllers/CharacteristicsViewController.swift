//
//  CharacteristicsViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/2/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0


class CharacteristicsViewController: GeiaViewController, UITextFieldDelegate {
    @IBOutlet weak var bodyFat: UITextField!
    var bodyFatValue:NSNumber = 0;
    @IBOutlet weak var bmi: UITextField!
    @IBOutlet weak var heartRate: UITextField!

    @IBOutlet weak var bloodPressure: UIButton!
    @IBOutlet weak var nxtButton: UIButton!

    // MARK: - Local variables
    var settedVal = false;

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var numberFormatter:NSNumberFormatter = {
        let nf         = NSNumberFormatter()
        nf.locale      = NSLocale.currentLocale()
        nf.numberStyle = .DecimalStyle

        return nf
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()



    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated: false)



        self.loadDataForUser(self.appDelegate.user!)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

     
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }




    // MARK: - Data
    private func loadDataForUser(user: User) {
        //load the user's characteristics
        if let data = Characteristics.getLatestCharacteristicsForUser(user, moc: self.appDelegate.managedObjectContext) {
            if data.bodyFat != nil && data.bodyFat != NSNull() {
                bodyFatValue = data.bodyFat!;
                self.bodyFat.text = self.numberFormatter.stringFromNumber((data.bodyFat)!)! + "%" 
            }
            if data.bmi != nil && data.bmi != NSNull() {
                self.bmi.text = self.numberFormatter.stringFromNumber((data.bmi)!)
            }
            if data.restingHeartRate != nil && data.restingHeartRate != NSNull() {
                self.heartRate.text = self.numberFormatter.stringFromNumber((data.restingHeartRate)!)
            }
            if data.bloodPressureSys?.integerValue > 0 && data.bloodPressureDia?.integerValue > 0 {
                self.bloodPressure.setTitle("\(data.bloodPressureSys!) / \(data.bloodPressureDia!)", forState: .Normal)
                self.bloodPressure.setTitleColor(UIColor.blackColor(), forState: .Normal);
                settedVal = true;
                
            }
        } else {
            self.bodyFat.text       = ""
            self.bmi.text           = ""
            self.heartRate.text     = ""
            self.nxtButton.backgroundColor = UIColor.geiaYellowColor();
            self.nxtButton.enabled = true;
            
        }
        
        //
        
        self.bodyFat.tintColor       = .geiaDarkBlueColor();
        self.bmi.tintColor       = .geiaDarkBlueColor();
        self.heartRate.tintColor       = .geiaDarkBlueColor();
        self.bloodPressure.tintColor       = .geiaDarkBlueColor();

    }

    // MARK: - Actions
    @IBAction func showNext(sender: AnyObject) {
        //update user's characteristics object for today
        let todaysData = Characteristics.getTodaysCharacteristicsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext)

        todaysData?.recordDate = NSDate().dateAtStartOfDay()
        todaysData?.height     = OnboardingViewController.sharedInstance.heightValue
        todaysData?.weight     = OnboardingViewController.sharedInstance.weightValue

        if self.bodyFat.text!.characters.count > 0 {
            todaysData?.bodyFat = bodyFatValue;
        }
        if self.bmi.text!.characters.count > 0 {
            todaysData?.bmi = self.numberFormatter.numberFromString(self.bmi.text!)
        }
        if self.heartRate.text!.characters.count > 0 {
            todaysData?.restingHeartRate = self.numberFormatter.numberFromString(self.heartRate.text!)
        }

        let bpString = self.bloodPressure.titleForState(.Normal)!
        if bpString.characters.count > 0 && bpString.containsString("/") {
            let components = bpString.componentsSeparatedByString("/")

            if components.count == 2 {
                let bpSys                    = components[0]
                let bpDia                    = components[1]
                todaysData?.bloodPressureSys = self.numberFormatter.numberFromString(bpSys)
                todaysData?.bloodPressureDia = self.numberFormatter.numberFromString(bpDia)
            }
        }

        OnboardingViewController.sharedInstance.characteristics = todaysData;
        //sync data with server
        self.performSegueWithIdentifier("showNext", sender: nil)
     
    }






    @IBAction func blodPressurePressed(sender: UIButton) {
        self.view.endEditing(true);
        
      let pressureSelected =  ActionSheetDistancePicker(title: "Blood Pressure", bigUnitString: "/", bigUnitMax: 190, selectedBigUnit: 120, smallUnitString: "", smallUnitMax: 80, selectedSmallUnit: 80, target: self, action: #selector(CharacteristicsViewController.bloodPressureSelected(_:smallUnit:element:)), origin: self.view)
        
        pressureSelected.showActionSheetPicker();
        
    }
    
    func bloodPressureSelected(bigUnit:NSNumber, smallUnit:NSNumber, element:AnyObject){
        self.bloodPressure.setTitle("\(bigUnit) / \(smallUnit)", forState: .Normal);

        if bigUnit == 0  || smallUnit == 0{
            settedVal = false;
            self.bloodPressure.setTitleColor(UIColor.redColor(), forState: .Normal);

        }else{
            settedVal = true;
            
            self.bloodPressure.setTitleColor(UIColor.blackColor(), forState: .Normal);
        }

        
    }
    
    @IBAction func bodyFatEditingEnd(sender: UITextField) {
        
        bodyFatValue = Float(sender.text!)!;
        
        if (bodyFatValue.floatValue > 100.0) {
            bodyFatValue = 100;
        }
        
        sender.text = NSString.init(format: "%.2f%@", bodyFatValue.floatValue,"%") as String;
        
    }
    

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == self.bloodPressure {
            let inverseSet = NSCharacterSet(charactersInString:"0123456789 /").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered   = components.joinWithSeparator("")
            return string  == filtered
        } else {
            return true
        }
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
