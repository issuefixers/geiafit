//
//  DashboardViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/2/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import HealthKit

class DashboardViewController: UITableViewController,EmotionNotificationViewDelegate {
    // MARK: - Outlets
    @IBOutlet weak var lightTime: UILabel!
    @IBOutlet weak var moderateTime: UILabel!
    @IBOutlet weak var vigorousTime: UILabel!

    @IBOutlet weak var challengeText: UITextView!
    @IBOutlet weak var challengeCompletedView: UIView!
    @IBOutlet weak var stepsPercentage: UILabel!
    @IBOutlet weak var stepsAmount: UILabel!
    @IBOutlet weak var exercisesPercentage: UILabel!
    @IBOutlet weak var exercisesAmount: UILabel!
    
    @IBOutlet weak var vigorousCircle: UIImageView!
    @IBOutlet weak var mediumCircle: UIImageView!
    @IBOutlet weak var lightCircle: UIImageView!
    @IBOutlet weak var stepCircleView: MKRingProgressView!
    @IBOutlet weak var exerciseCircleView: MKRingProgressView!
    @IBOutlet weak var activityCircleView: MKRingProgressGroupView!
	weak var emotionNotificationDelegate: EmotionNotificationViewDelegate?

    var lastRecordedDate:NSDate?
    var firstOpenApp: Bool = true

    private let prescriptionsViewModel = PrescriptionsViewModel()
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()

    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()

	 // MARK: - View Lifecycle
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)

        if !self.firstOpenApp {
            self.getGoalsAndThresholds()
        }
            
       
		
	}
    
    
    
    override func viewWillAppear(animated: Bool) {
        if lastRecordedDate == nil{
            self.lastRecordedDate = NSUserDefaults.standardUserDefaults().objectForKey(LAST_EXERCISE_DATE_KEY) as! NSDate!
            if lastRecordedDate == nil{
                lastRecordedDate = NSDate();
            }
        }
        
        
        
        if !lastRecordedDate!.dateByAddingMinutes(5)!.isLaterThanDate(NSDate()) {
            lastRecordedDate = NSDate();
            NSUserDefaults.standardUserDefaults().setObject(lastRecordedDate, forKey: LAST_EXERCISE_DATE_KEY);
            self.sendActivityWithDate(lastRecordedDate!)

        }
        
        
        let lastTimeEmotionNotificationWasShown = NSUserDefaults.standardUserDefaults().objectForKey(LAST_EMOTION_DATE_KEY) as! NSDate?
        if lastTimeEmotionNotificationWasShown == nil || !lastTimeEmotionNotificationWasShown!.isEqualToDateIgnoringTime(NSDate()) {
            self.emotionNotificationDelegate?.showEmotionalNotification!()
            NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: LAST_EMOTION_DATE_KEY)
            NSUserDefaults.standardUserDefaults().synchronize()
            self.clearExercises();
        }

		
		NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: CURRENT_DATE_KEY)
		NSUserDefaults.standardUserDefaults().synchronize()
		
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value:"Dashboard")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])

    }

	
    func clearExercises(){
        for exer in Exercise.getAllExercises(self.appDelegate.managedObjectContext)!{
            exer.completed = false;
            exer.realized = 0;
        }
        self.appDelegate.saveContext();
        
        
        let lastTimeEmotionNotificationWasShown = NSUserDefaults.standardUserDefaults().objectForKey(LAST_EMOTION_DATE_KEY) as! NSDate?
        if lastTimeEmotionNotificationWasShown == nil || !lastTimeEmotionNotificationWasShown!.isEqualToDateIgnoringTime(NSDate()) {
            self.emotionNotificationDelegate?.showEmotionalNotification!()
            NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: LAST_EMOTION_DATE_KEY)
            NSUserDefaults.standardUserDefaults().synchronize()
            self.clearExercises();
        }
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value:"Dashboard")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])

    }
	

	
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.firstOpenApp = false
    }
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
        self.setupView()
        self.appDelegate.blockRotation = true
        self.setupDailyChallenge()

        let refreshControl = UIRefreshControl(frame: self.tableView.frame)
        refreshControl.addTarget(self, action: #selector(DashboardViewController.refreshData(_:)), forControlEvents: .ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.authHealthKit()
        
        // Do any additional setup after loading the view.
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 87, height: 25))
        imageView.contentMode = .ScaleAspectFit
        let image = UIImage(named: "logoNavBar")
        imageView.clipsToBounds = true;
        imageView.image = image
        navigationItem.titleView = imageView
        let lblTotal = UILabel(frame: CGRectMake(0,0,CGRectGetWidth(self.tableView!.frame),38));
        lblTotal.text = "Today / " + NSDate().stringDateWithFormat("MMM dd, YYYY");
        lblTotal.textAlignment = .Center;
        lblTotal.backgroundColor = UIColor.geiaDarkGrayColor();
        lblTotal.textColor = UIColor.whiteColor();
        lblTotal.font = UIFont.geiaFontOfSize(14.5);
        self.tableView!.tableHeaderView = lblTotal;
        
        
    }
    
    func setupView() {
        
        self.lightTime.text     = "0/0 min"
        self.moderateTime.text  = "0/0 min"
        self.vigorousTime.text  = "0/0 min"
        self.lightCircle.highlighted = false;
        self.mediumCircle.highlighted = false;
        self.vigorousCircle.highlighted = false;
        self.lightTime.adjustsFontSizeToFitWidth = true
        self.moderateTime.adjustsFontSizeToFitWidth = true
        self.vigorousTime.adjustsFontSizeToFitWidth = true
        var formattedString = NSMutableAttributedString()
        formattedString.bold("0").normal(" Steps")
        self.stepsPercentage.text = "\(0)%"
        self.stepsAmount.attributedText = formattedString
        formattedString = NSMutableAttributedString()
        formattedString.bold("0").normal(" Completed")
        self.exercisesAmount.attributedText = formattedString
        self.exercisesPercentage.text = "\(0)%"
//        self.stepCircleView.updateChartView()
    }

    // MARK: - Actions
    func authHealthKit() {
        
        let healthManager = HealthManager()
        
        healthManager.authorizeHealthKit { (success, error) in
            
            if success {
                print("ActivityKit Authorized.")
                self.getGoalsAndThresholds()
            } else {
                print("ActivityKit NOT Authorized.")
                if error != nil {
                    debugPrint(error)
                }
            }
        }
        
    }
    
    func getGoalsAndThresholds() {
        
        //self.loaderView.show()
        
        let timestamp = NSUserDefaults.standardUserDefaults().objectForKey(LAST_DAILY_GOALS_KEY) as! NSDate?
        if timestamp == nil || !timestamp!.isEqualToDateIgnoringTime(NSDate()) {
            //
            // Pull the Thresholds and Activity Goals since they're needed for the graphs
            //
            let goalTrackingViewModel = GoalTrackingViewModel()
            goalTrackingViewModel.getThresholdsForUser(self.appDelegate.user!) { (result) -> Void in
                let success = result["success"] as! Bool
                
                if success {
                    let data = result["data"] as! [String: AnyObject]
                    UserThresholds.addThresholdsForUser(self.appDelegate.user!, data: data, moc: self.appDelegate.managedObjectContext)
                    
                    let prescriptionsViewModel = PrescriptionsViewModel()
                    prescriptionsViewModel.getFitnessGoalsForUser(self.appDelegate.user!) { (result) -> Void in
                        let success = result["success"] as! Bool
                        
                        if success {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                //add to the DB
                                let data = result["data"] as! [[String:AnyObject]]
                                for fitnessData in data {
                                    ActivityGoals.addGoalsForUser(self.appDelegate.user!, data: fitnessData, moc: self.appDelegate.managedObjectContext)
                                }
                                
                                //save changes
                                self.appDelegate.saveContext()
                                
                                //Update UI
                                self.updateUserData()
                                
                                //set last updated date
                                NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: LAST_DAILY_GOALS_KEY)
                                NSUserDefaults.standardUserDefaults().synchronize()
                            })
                        } else {
                        
                            //self.loaderView.hide(animated: true)
                        }
                    }// get Fitness Goals
                } else {
                
                    //self.loaderView.hide(animated: true)
                }
            }// get Thresholds
        } else {
            self.updateUserData()
        }
    }
    
    func refreshData(refreshControll: UIRefreshControl) {
        refreshControll.endRefreshing()
        self.updateUserData()
    }
    
     func updateUserData() {

        HealthDataPoint.purgeTodayDataPointsInContext(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext, type: .Steps)
        
        self.goalTrackingViewModel.updateStepsFromHealthKitForDayOrWeek(NSDate(), dateRange: .Day) { (success) -> Void in
            //self.loaderView.hide(animated: true)
            if success {
                    //save changes
                self.appDelegate.saveContext()
                self.loadActivityData()
            }
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1.0*Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
            () -> Void in
        
        self.prescriptionsViewModel.getExercisesForUser(self.appDelegate.user!) { (result) -> Void in
           
            if (result.count != 0) {
            
                let todayExercises = Exercise.getTodayAllExercises(self.appDelegate.managedObjectContext)! as [Exercise]
                
                var progress = 0.0
                if (todayExercises.count > 0) {
                    
                    var didCount: Int = 0
                    var totalCount: Int = 0
                    for data:Exercise in todayExercises {
                        
                        didCount += data.realized
                        totalCount += data.dailyTotal
                    }
                    
                    if (totalCount > 0) {
                        progress = Double(didCount) / Double(totalCount)
                        if (progress == 1 || progress > 1) {
//                            self.exerciseCircleView.aboveColor = UIColor(red: 243.0/255.0, green: 172.0/255.0, blue: 3.0/255.0, alpha: 1.0)
                        } else {
//                            self.exerciseCircleView.aboveColor = UIColor(red: 0, green: 163.0/255.0, blue: 222.0/255.0, alpha: 1.0)
                        }
                    }
                    let formattedString = NSMutableAttributedString()
                    formattedString.bold("\(didCount)").normal(" Completed")
                    self.exercisesAmount.attributedText = formattedString
                }

                self.exercisesPercentage.text = Common.getStrFromProgress(progress)
                self.exerciseCircleView.progress = progress
//                self.exerciseCircleView.updateChartView()
            }
        }
            
        })
     }
     
    func loadActivityData() {

        self.loaderView.show()
        self.goalTrackingViewModel.getStepsForUserByDay(NSDate(), user: self.appDelegate.user!) {
            (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool
     
            if success {
            
                let lightMin         = result["lightMin"] as! Int
                let lightMinGoal     = result["lightMinGoal"] as! Int
                let moderateMin      = result["moderateMin"] as! Int
                let moderateMinGoal  = result["moderateMinGoal"] as! Int
                let vigorousMin      = result["vigorousMin"] as! Int
                let vigorousMinGoal  = result["vigorousMinGoal"] as! Int
                let lightProgress    = result["lightProgress"] as! Double
                let moderateProgress = result["moderateProgress"] as! Double
                let vigorousProgress = result["vigorousProgress"] as! Double
                let stepGoal         = result["stepGoal"] as! Int
                
                var totalSteps: Int = 0
                if let datasource = result["dataAct"] as? [[String: AnyObject]] {
                    
                    for obj in datasource {
                        
                        let value     = obj["value"] as! Double
                        totalSteps += Int(value)
                    }
                }
                
                let theDate        = NSDate().dateAtStartOfDay()
                let dailyTotals    = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: theDate, moc: self.appDelegate.managedObjectContext)
                dailyTotals?.steps = Double(totalSteps)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //Update UI
                    self.lightTime.text    = "\(lightMin)/\(lightMinGoal) min"
                    self.moderateTime.text = "\(moderateMin)/\(moderateMinGoal) min"
                    self.vigorousTime.text = "\(vigorousMin)/\(vigorousMinGoal) min"
                    //progress circle
                    
                    if lightMin >= lightMinGoal{
                        self.lightCircle.highlighted = true;
                    }
                    
                    if moderateMin >= moderateMinGoal{
                        self.mediumCircle.highlighted = true;
                    }
                    
                    if vigorousMin >= vigorousMinGoal{
                        self.vigorousCircle.highlighted = true;
                    }
                    
                    self.activityCircleView.updateChartData([vigorousProgress, moderateProgress,lightProgress])
     
                    //add totalSteps to result dictionary
                    let progress = (stepGoal != 0) ? Double(totalSteps) / Double(stepGoal) : 0
                
                    if (progress == 1 || progress > 1) {
//                        self.stepCircleView.aboveColor = UIColor(red: 243.0/255.0, green: 172.0/255.0, blue: 03.0/255.0, alpha: 1.0)
                    } else {
//                        self.stepCircleView.aboveColor = UIColor(red: 0, green: 163.0/255.0, blue: 222.0/255.0, alpha: 1.0)
                    }
            
                    let formattedString = NSMutableAttributedString()
                    formattedString.bold("\(totalSteps)").normal(" Steps")
                    self.stepsAmount.attributedText = formattedString
                    self.stepsPercentage.text = Common.getStrFromProgress(progress)
                    
                    self.stepCircleView.progress = progress
//                    self.stepCircleView.updateChartView()
                
                })
                
                var data           = result
                data["totalSteps"] = totalSteps
                
                //Post updated data to server
                self.goalTrackingViewModel.sendActivityDataToServerForUser(NSDate(), user: self.appDelegate.user!, data: data, completion: { (result) -> Void in
               //Parse response
               })
            } else {
                let noGoals = result["noGoals"] as! Bool
                
                if noGoals {
                    let prescriptionsViewModel = PrescriptionsViewModel()
                    prescriptionsViewModel.getFitnessGoalsForUser(self.appDelegate.user!) { (result) -> Void in
                        let success = result["success"] as! Bool
                        
                        if success {
                            //add to the DB
                            let data = result["data"] as! [[String:AnyObject]]
                            for fitnessData in data {
                                ActivityGoals.addGoalsForUser(self.appDelegate.user!, data: fitnessData, moc: self.appDelegate.managedObjectContext)
                            }
                            
                            //save changes
                            self.appDelegate.saveContext()
                            print("added goals")
                        } else {
                            let message = result["errorMessage"] as! String
                            let alert: UIAlertController = UIAlertController(title: "Setup Data", message: message, preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    func setupDailyChallenge() {
        let timestamp = NSUserDefaults.standardUserDefaults().doubleForKey(LAST_CHALLENGE_DATE_KEY)
        let date      = NSDate(timeIntervalSince1970: timestamp)
        
        if date.isToday() {
            //Show challenge completed
            self.challengeCompletedView.hidden = false
        } else {
            //get challenge for today
            let viewModel = GemsViewModel()
            viewModel.getDailyChallengeFromServer(self.appDelegate.user!) { (result) -> Void in
                let success = result["success"] as! Bool
            
                    if success {
                        let challenge = result["challenge"] as! String
                        self.challengeText.text = challenge
                        self.challengeCompletedView.hidden = true
                    }
            }
        }
    }
    
    @IBAction func challengeCompletedWithSender(sender: AnyObject) {
        self.challengeCompletedView.hidden = false
        
        //Add gem
        self.appDelegate.user?.gems += 1
        self.appDelegate.saveContext()
        //self.rootNavbar?.setGemNumber((self.appDelegate.user?.gems)!)
        
        //Save timestamp
        let timestamp = NSDate().timeIntervalSince1970
        NSUserDefaults.standardUserDefaults().setDouble(timestamp, forKey: LAST_CHALLENGE_DATE_KEY)
        NSUserDefaults.standardUserDefaults().setInteger(1, forKey: LAST_CHALLENGE_DATE_KEY + NSDate().stringForDate());

        NSUserDefaults.standardUserDefaults().synchronize()
        
        //Push user profile
        let viewModel = GemsViewModel()
        viewModel.updateGemsForUser(self.appDelegate.user!) { (result) -> Void in
            // TODO: Handle possible sync failure
        }
		
		let tracker = GAI.sharedInstance().defaultTracker
		tracker.set(kGAIScreenName, value:"I Did It Click")
		
		let builder = GAIDictionaryBuilder.createScreenView()
		tracker.send(builder.build() as [NSObject : AnyObject])
    }
	
    func sendActivityWithDate(date: NSDate) {
        
        self.goalTrackingViewModel.getStepsForUserByDay(date, user: appDelegate.user!, completion:  { (result) -> Void in
            
            let success = result["success"] as! Bool
            
            if success {
                
                
                //                let dailyTotals = HealthDailyTotal.getTodaysTotalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext)
                let dailyTotals = HealthDailyTotal.getTotalsForUserByDate(date, user: self.appDelegate.user!, moc: self.appDelegate.managedObjectContext);
                let totalSteps  = Int((dailyTotals?.steps)!)
                
                var data           = result
                data["totalSteps"] = totalSteps
                
                //Post updated data to server
                self.goalTrackingViewModel.sendActivityDataToServerForUser(date, user: self.appDelegate.user!, data: data, completion: { (result) -> Void in
                    //Parse response
                })
            }
        })
    }

    
    
    
	// MARK: - Emotion Notification View Delegate
	func emotionValueDidChange() {
	}

	
    //MARK: - Go To Activity
    @IBAction func goToActivity() {
        NSNotificationCenter.defaultCenter().postNotificationName("goToScreen", object: 30);
    }
    
    
    @IBAction func goToSteps() {
        NSNotificationCenter.defaultCenter().postNotificationName("goToScreen", object: 31);
    }
    
    //cassie - not implemented
    @IBAction func goToExercises() {
        NSNotificationCenter.defaultCenter().postNotificationName("goToScreen", object: 41);
    }

}
