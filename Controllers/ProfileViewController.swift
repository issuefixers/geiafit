

class ProfileViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var newpassword: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var weightloss: UIButton!
    @IBOutlet weak var painRelief: UIButton!
    @IBOutlet weak var improvePosture: UIButton!
    @IBOutlet weak var improveOverallHealth: UIButton!
    @IBOutlet weak var manageRecovery: UIButton!
    
    @IBOutlet weak var saveButton: UIButton!
    
    var passwordHeight:CGFloat = 43;
    
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    private lazy var onbardingViewModel: OnboardingViewModel = {
        return OnboardingViewModel()
    }()
    
    
    
    
    override func viewDidLoad() {
        getDataFromServer();
        self.saveButton.enabled = false;
        self.saveButton.backgroundColor = UIColor.geiaMidGrayColor();
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(ProfileViewController.dismissViewController));
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.loaderView.hide(animated: true)

    }
    
     func getDataFromServer() {
        self.onbardingViewModel.getProfileForUser(appDelegate.user!) { (result) -> Void in
            
            let success = result["success"] as! Bool
            self.loaderView.hide(animated: false)
            if success {
                self.appDelegate.saveContext()
                self.firstName.text = self.appDelegate.user!.firstName
                self.lastName.text  = self.appDelegate.user!.lastName
                self.email.text = self.appDelegate.user!.email
                print(self.appDelegate.user!.pictureURL);
                
                self.avatar.sd_setImageWithURL(NSURL(string: self.appDelegate.user!.pictureURL!), completed: { (image, error, cache, url) in
                    if(image != nil){
                        self.appDelegate.user!.picture = UIImagePNGRepresentation(image)!
                    }
                })
                
                //load the user's goals
                let goals: UserGoals = UserGoals.init(rawValue: self.appDelegate.user!.goals)
                
                if goals.contains(.Weightloss) {
                    self.weightloss.selected = true
                }
                
                if goals.contains(.PainRelief) {
                    self.painRelief.selected = true
                }
                
                if goals.contains(.ImprovePosture) {
                    self.improvePosture.selected = true
                }
                
                if goals.contains(.ImproveOverallHealth) {
                    self.improveOverallHealth.selected = true
                }
                
                if goals.contains(.ManageRecovery) {
                    self.manageRecovery.selected = true
                }
                
                
            } else {
                let alert: UIAlertController = UIAlertController(title: "Connection Error", message: "Couldn't connect to the server, please check your connection and try again", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
                switch (indexPath.section, indexPath.row) {
                case (0,0):
                    return 10;
                case (1,0):
                    return 143;
                case (1,1):
                    return passwordHeight;
                case (2,0):
                    return 247;
                case (3,0):
                    return 54;
                default:
                    return 0;
                }
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4;
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0;
        case 1:
            return 2;
        default:
            return 1;
        }
    }
    
    @IBAction func updatePasswordToggle(sender: UIButton) {
        passwordHeight = 135;
        sender.hidden = true;
        self.tableView.reloadData();
        self.newpassword.becomeFirstResponder();
    }
    
    @IBAction func toggleGoalsButton(sender: UIButton) {
        sender.selected = !sender.selected
        self.enableSaveButton();
    }
    
    @IBAction func selectPicture(sender: AnyObject) {
        let hasCamera             = UIImagePickerController.isSourceTypeAvailable(.Camera)
        let hasLibrary            = UIImagePickerController.isSourceTypeAvailable(.SavedPhotosAlbum)
        let imagePicker           = UIImagePickerController()
        imagePicker.delegate      = self
        imagePicker.allowsEditing = true
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .ActionSheet)
        
        if hasCamera {
            alertController.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { (action) -> Void in
                //open camera
                imagePicker.sourceType = .Camera
                
                alertController.dismissViewControllerAnimated(true, completion: nil)
                self.navigationController!.presentViewController(imagePicker, animated: true, completion: nil)
            }))
        }
        
        if hasLibrary {
            alertController.addAction(UIAlertAction(title: "Library", style: .Default, handler: { (action) -> Void in
                //open library
                imagePicker.sourceType = .SavedPhotosAlbum
                
                alertController.dismissViewControllerAnimated(true, completion: nil)
                self.navigationController!.presentViewController(imagePicker, animated: true, completion: nil)
            }))
        }

        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive, handler: { (action) in
            alertController.dismissViewControllerAnimated(true, completion: nil)

        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Save & Sync changes
    @IBAction func saveChanges(sender: AnyObject) {
        self.view.endEditing(true);
        
        debugPrint("Syncing to 4 endpoints")
        //1) Send the profile
        self.sendProfile { (success) -> Void in
            if success {
                debugPrint("✅☑️☑️☑️")
                //2) Send the new image
                self.sendImage { (success) -> Void in
                    if success {
                        debugPrint("✅✅☑️☑️")
                        //3) Send the characteristics
                        //                        self.sendCharacteristics { (success) -> Void in
                        //                            if success {
                        //                                debugPrint("✅✅✅☑️")
                        //                                //4) Send the goals
                        self.sendGoals { (success) -> Void in
                            if success {
                                debugPrint("✅✅✅✅")
                                let alert: UIAlertController = UIAlertController(title: "Data Synced", message: "Your profile has been synced successfuly", preferredStyle: .Alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                                self.presentViewController(alert, animated: true, completion: nil)
                            } else {
                                debugPrint("✅✅✅❌ GOALS FAILED")
                            }
                        }//send goals
                        //                            } else {
                        //                                debugPrint("✅✅❌☑️ CHARACTERISTICS FAILED")
                        //                            }
                        //                        }//send characteristics
                    } else {
                        debugPrint("✅❌☑️☑️ AVATAR FAILED")
                    }
                }//send image
            } else {
                debugPrint("❌☑️☑️☑️ PROFILE FAILED")
            }
        }// send profile
    }
    
    func sendImage(completion:(success: Bool) -> Void) {
        let defaultImage = UIImage(named: "avatar")
        let newPic       = self.avatar.image!
        
        if defaultImage != newPic {
            //save image to Core Data
            let picData                    = UIImagePNGRepresentation(newPic)!
            self.appDelegate.user?.picture = picData
            
            //send to server
            self.loaderView.show()
            self.onbardingViewModel.updateProfileImageForUser(self.appDelegate.user!, imageData: picData, completion: { (result) -> Void in
                self.loaderView.hide(animated: true)
                let success = result["success"] as! Bool
                
                if success {
                    completion(success: true)
                } else {
                    print(result["errorMessage"])
                    let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your profile picture to our servers, please try again", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                    self.presentViewController(alert, animated: true, completion: nil)
                    completion(success: false)
                }
            })
        } else {
            completion(success: true)
        }
    }
    
    func sendProfile(completion:(success: Bool) -> Void)  {
        //make sure both passwords match
        var newPassword: String = ""
        if self.newpassword.text != self.confirmPassword.text {
            let alert: UIAlertController = UIAlertController(title: "Passwords doesn't match", message: "Please make sure that your password is typed exactly the same in both fields and try again", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
            self.presentViewController(alert, animated: true, completion: nil)
            completion(success: false)
            return
        } else if self.newpassword.text?.characters.count > 0 {
            newPassword = self.newpassword.text!
        }
        
        if self.email.text?.isValidEmail() == false {
            let alert: UIAlertController = UIAlertController(title: "Email address is not valid", message: "Please make sure that the email you typed is a valid address", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
            self.presentViewController(alert, animated: true, completion: nil)
            completion(success: false)
            return
        }
        
        //make sure all fields are filled
        if self.firstName.text?.characters.count == 0 ||
            self.lastName.text?.characters.count == 0 ||
            self.email.text?.characters.count == 0 {
            let alert: UIAlertController = UIAlertController(title: "Fields missing", message: "Please make sure that all the fields are filled and try again", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
            self.presentViewController(alert, animated: true, completion: nil)
            completion(success: false)
            return;
        }
        
        //update user object
        self.appDelegate.user?.email     = self.email.text!
        self.appDelegate.user?.firstName = self.firstName.text!
        self.appDelegate.user?.lastName  = self.lastName.text!
        //sync data with server
        self.loaderView.show()
        self.onbardingViewModel.updateUserInformationForUser(self.appDelegate.user!, password: newPassword) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool
            
            if success {
                //save changes
                self.appDelegate.saveContext()
                
                completion(success: true)
            } else {
                print(result["errorMessage"])
                let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your data to our servers, please try again", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                self.presentViewController(alert, animated: true, completion: nil)
                completion(success: false)
            }
        }
    }
    
    // MARK: - UIImagePickerController Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        //crop it to a square
        
        //set picked image in the image view
        self.avatar.image              = image
        let picData                    = UIImagePNGRepresentation(image)!
        self.appDelegate.user?.picture = picData
        self.appDelegate.saveContext()
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func sendGoals(completion:(success: Bool) -> Void) {
        //determine which buttons are selected
        var goalsValue      = UserGoals.None.rawValue
        var goals: [String] = []
        
        if self.weightloss.selected {
            goalsValue += UserGoals.Weightloss.rawValue
            goals.append(WEIGHTLOSS)
        }
        
//        if self.eatHelthy.selected {
//            goalsValue += UserGoals.EatHealthy.rawValue
//            goals.append(EAT_HEALTHY)
//        }
        
        if self.painRelief.selected {
            goalsValue += UserGoals.PainRelief.rawValue
            goals.append(PAIN_RELIEF)
        }
        
        if self.improvePosture.selected {
            goalsValue += UserGoals.ImprovePosture.rawValue
            goals.append(IMPROVE_POSTURE)
        }
        
        if self.improveOverallHealth.selected {
            goalsValue += UserGoals.ImproveOverallHealth.rawValue
            goals.append(IMPROVE_OVERALL_HEALTH)
        }
        
        if self.manageRecovery.selected {
            goalsValue += UserGoals.ManageRecovery.rawValue
            goals.append(MANAGE_RECOVERY)
        }
        
        //set the value in core data
        self.appDelegate.user?.goals = goalsValue
        
        if goals.count > 0 {
            //send to server
            self.loaderView.show()
            self.onbardingViewModel.updateGoalsForUser(self.appDelegate.user!, goals: goals, completion: { (result) -> Void in
                self.loaderView.hide(animated: true)
                let success = result["success"] as! Bool
                
                if success {
                    //save changes
                    self.appDelegate.saveContext()
                    
                    completion(success: true)
                } else {
                    print(result["errorMessage"])
                    let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your goals to our servers, please try again", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                    self.presentViewController(alert, animated: true, completion: nil)
                    completion(success: false)
                }
            })
        }
    }
    
    
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        switch (indexPath.section, indexPath.row) {
        case (1,1):
            if cell.tag != 99 && passwordHeight == 135 {
                cell.viewWithTag(90)?.hidden = false;
                cell.viewWithTag(91)?.hidden = false;
                cell.viewWithTag(89)?.hidden = false;
            }
        default:
            break;
        }
    }
    @IBAction func userInfoChanged(sender: AnyObject) {
        self.enableSaveButton();
    }
    
    @IBAction func trimStringFromtextField(sender: UITextField) {
        
        sender.text = sender.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        if sender == email {
            if email.text?.isValidEmail() == false{
                sender.textColor = UIColor.redColor();
            }else{
                sender.textColor = UIColor.blackColor();
            }
        }
    }
    
    
    func enableSaveButton(){
        if email.text?.isValidEmail() == true{
            self.saveButton.enabled = true;
            self.saveButton.backgroundColor = UIColor.geiaDarkBlueColor();
        }else{
            self.saveButton.enabled = false;
            self.saveButton.backgroundColor = UIColor.geiaMidGrayColor();
        }

    }
    
    
    func dismissViewController(){
        self.navigationController!.dismissViewControllerAnimated(true, completion: nil);
    }
    
    
}
