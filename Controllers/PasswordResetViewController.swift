//
//  PasswordResetViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 2/3/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class PasswordResetViewController: GeiaViewController, UITextFieldDelegate {

    // MARK: - Outlets and local vars
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!

    private var scrollViewHeightNormal: CGFloat        = 0.0
    private var scrollViewHeightKeyboardShown: CGFloat = 0.0
    var email: String?

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.scrollViewHeightNormal = Toolchain.getWindowSize().height

        self.emailField.delegate  = self
        self.emailField.tintColor = .whiteColor()

        let placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        self.emailField.attributedPlaceholder = NSAttributedString(string: "EMAIL ADDRESS", attributes: [NSForegroundColorAttributeName: placeholderColor])

        if self.email?.characters.count > 0 {
            self.emailField.text = self.email!
        }

        }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .Portrait {
            self.scrollViewHeightNormal = Toolchain.getWindowSize().height
        } else if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.scrollViewHeightNormal = Toolchain.getWindowSize().height
        }
    }

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.scrollViewHeightConstraint.constant = self.scrollViewHeightNormal
        self.scrollView.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }

    // MARK: - Actions
    @IBAction func resetPassword(sender: AnyObject) {
        let emailAddress: String = self.emailField.text!

        if emailAddress.characters.count > 0 {
            let loaderView = RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
            let viewModel  = LoginViewModel()

            loaderView.show()
            viewModel.requestPasswordResetForEmail(emailAddress) { (result) -> Void in
                loaderView.hide(animated: true)
                let success = result["success"] as! Bool

                if success {
                    let message = result["message"] as! String

                    let alert: UIAlertController = UIAlertController(title: "Password Reset", message: message, preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    let alert: UIAlertController = UIAlertController(title: "Password Reset", message: "The request failed, check that the email address is the correct one and try again", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        } else {
            let alert: UIAlertController = UIAlertController(title: "Missing Credentials", message: "Your email address is missing, please check and try again", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    @IBAction func cancelReset(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - TextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.resetPassword(textField)

        return true
    }

}