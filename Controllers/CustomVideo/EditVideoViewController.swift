//
//  EditVideoViewController.swift
//  Geia
//
//  Created by Cassie on 5/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import MediaPlayer

protocol RefreshFrequencyCellDelegate: class {
    func refreshCell(textStrArr: [String])
}

class EditVideoViewController: UITableViewController, UIAlertViewDelegate, RefreshFrequencyCellDelegate, UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate {
    
    let navHeight: CGFloat = 44.0
    let descrptionText: String = "Type the Description"
    
    var videoURL: NSURL!
    var videoThumbnailImage: UIImage!
    var videoLastThumbnailImage: UIImage!
    var frequencyStr: String = "Please select the day"
    var mpPlayerViewController: MPMoviePlayerViewController?
    var canUpload: Bool = false
    var videoInfo = VideoInfo()
    var rightBarBtn: UIButton!
    var keyboardControls: BSKeyboardControls?
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var repetField: UITextField!
    @IBOutlet weak var setsField: UITextField!
    @IBOutlet weak var timesField: UITextField!
    
     // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Close"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(leftBtnClicked))
        
        //get video thumbnail
        videoInfo.localURL = self.videoURL
        let asset = AVURLAsset(URL: self.videoURL, options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        
        var cgImage: CGImage?
        var cgImage2: CGImage?
        
        do {
            cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
        } catch {
            cgImage = nil
            print("Error generate image")
            return
        }
        
        let duration = Int64(CMTimeGetSeconds(asset.duration))
        do {
            cgImage2 = try imgGenerator.copyCGImageAtTime(CMTimeMake(duration, 1), actualTime: nil)
        } catch {
            cgImage2 = nil
            print("Error generate image2")
            return
        }
        
        self.videoThumbnailImage = UIImage(CGImage: cgImage!)
        self.videoLastThumbnailImage = UIImage(CGImage: cgImage2!)
        
        let objects = [self.titleField, self.descTextView, self.repetField, self.setsField, self.timesField]
        self.keyboardControls = BSKeyboardControls(fields: objects)
        self.keyboardControls?.delegate = self

    }
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.rightBarBtn = UIButton(type: .Custom)
        self.rightBarBtn.frame = CGRectMake(self.view.frame.size.width - navHeight - 5.0 ,0, navHeight,navHeight)
        let imageName = self.canUpload ? "Upload" : "UploadDisable"
        self.rightBarBtn.setImage(UIImage(named: imageName), forState: .Normal)
        self.rightBarBtn.addTarget(self, action: #selector(rightBtnClicked), forControlEvents: .TouchUpInside)
        self.navigationController!.navigationBar.addSubview(self.rightBarBtn)
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController!.navigationBarHidden = false
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 34.0/255.0, green: 34.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        let interfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        
        if interfaceOrientation == .LandscapeLeft || interfaceOrientation == .LandscapeRight {
        
           UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
        }
        self.appDelegate.blockRotation = true
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.rightBarBtn.removeFromSuperview()
    }
    
//    override func shouldAutorotate() -> Bool {
//        return false
//    }
//    
//    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
//        return UIInterfaceOrientationMask.Portrait
//    }
//    
//    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
//        return .Portrait
//    }
//    
//    override func shouldAutorotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation) -> Bool {
//        return toInterfaceOrientation == .Portrait
//    }
    
    // MARK: - Custom methods
    func checkIfcanUpload() {
    
        if videoInfo.title != nil && videoInfo.repetitions != nil && videoInfo.sets != nil && videoInfo.timesPerDay != nil && frequencyStr != "Please select the day" {
            self.canUpload = true
            self.rightBarBtn.setImage(UIImage(named: "Upload"), forState: .Normal)
        } else {
            self.canUpload = false
            self.rightBarBtn.setImage(UIImage(named: "UploadDisable"), forState: .Normal)
        }
    }
    
    func makeupChoices() -> [String] {
        
        var days: [String] = [String]()
        
        if frequencyStr == "Please select the day" {
        }
        else if frequencyStr == "Every Day" {
            days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        }
//        else if frequencyStr == "Workdays" {
//            days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
//        }
//        else if frequencyStr == "Weekends" {
//            days = ["Sunday", "Saturday"]
//        }
        else {
            
            let strArr = frequencyStr.componentsSeparatedByString(" ")
            for str in strArr {
            
                if str == "M" {
                   days.append("Monday")
                }
                else if str == "T" {
                   days.append("Tuesday")
                }
                else if str == "W" {
                   days.append("Wednesday")
                }
                else if str == "R" {
                   days.append("Thursday")
                }
                else if str == "F" {
                   days.append("Friday")
                }
                else if str == "Sa" {
                   days.append("Saturday")
                }
                else {
                   days.append("Sunday")
                }
            }
        }
        
        return days
    }
    
    // MARK: - User Interaction Action
    @IBAction func leftBtnClicked() {

        
        let alertController:UIAlertController = UIAlertController(title: "Do you want to discard this video?", message: "", preferredStyle: .Alert);
        
        alertController.addAction(UIAlertAction(title: "Yes, Delete it", style: .Destructive, handler: { (action) in
            self.view.endEditing(true)
            
            CircularProgressBar.removeFileAtUrl(self.videoURL)
            
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }));
        
        alertController.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action) in
            alertController.dismissViewControllerAnimated(true, completion: nil);
        }));
        
        self.presentViewController(alertController, animated: true, completion: nil);
    }
    
    @IBAction func rightBtnClicked() {
        
        if self.canUpload == false {
           return
        }
        
        if Int(videoInfo.sets!) == 0 {
            
            UIAlertView(title: "", message: "The SETS must be greater than 0", delegate: self, cancelButtonTitle: "OK").show()
            return
        }
        
        let storyboard = UIStoryboard(name: "VideoCapture", bundle: NSBundle.mainBundle())
        let uploadViewController = storyboard.instantiateViewControllerWithIdentifier("UploadVideoViewController") as! UploadVideoViewController
        videoInfo.imageDataStr = UIImageJPEGRepresentation(self.videoThumbnailImage, 0.5)!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        videoInfo.imageDataStr2 = UIImageJPEGRepresentation(self.videoLastThumbnailImage, 0.5)!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        uploadViewController.videoInfo = videoInfo
        self.navigationController?.pushViewController(uploadViewController, animated: true)
        
    }

    @IBAction func playBtnClicked() {
        
        mpPlayerViewController = MPMoviePlayerViewController(contentURL: self.videoURL)
        self.presentMoviePlayerViewControllerAnimated(mpPlayerViewController)
        mpPlayerViewController?.moviePlayer.play()
        
    }
    
    // MARK: - UITextFieldDelegate method
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        self.keyboardControls?.activeField = textField
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let strOrigin = (textField.text?.characters.count == 0) ? "" : textField.text
        let str = (strOrigin! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        switch textField.tag {
        case 0:
            if str.characters.count != 0 {
                videoInfo.title = str
            } else {
                videoInfo.title = nil
            }
            break
        case 1:
            if str.characters.count != 0 {
                videoInfo.repetitions = str
            } else {
                videoInfo.repetitions = nil
            }
            break
        case 2:
            if str.characters.count != 0 {
                videoInfo.sets = str
            } else {
                videoInfo.sets = nil
            }
            break
        case 3:
            if str.characters.count != 0 {
                videoInfo.timesPerDay = str
            } else {
                videoInfo.timesPerDay = nil
            }
            break
        default:
            break
        }
        
        self.checkIfcanUpload()
    
        return true
    }
    
    
    // MARK: - UITextViewDelegate method
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
    
        self.keyboardControls?.activeField = textView
        
        if textView.text == descrptionText {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        
        return true
    }
    
    func textViewDidChange(textView: UITextView) {
    
        if textView.text.characters.count == 0 {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = descrptionText
            textView.resignFirstResponder()
        } else {
            videoInfo.videoDescription = textView.text
        }
    
    }
    
    // MARK: - TableView Delegate & Datasource
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    
        var cellHeight: CGFloat
        switch indexPath.row {
            case 0:
                cellHeight = 248.0
                break
            case 1,3,5,7,9,11:
                cellHeight = 77.0
                break
            case 4:
                cellHeight = 171.0
                break
            case 13:
                cellHeight = 300.0
                break
            default:
                cellHeight = 57.0
                break
        }
        
        return cellHeight
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    
        switch (indexPath.row) {
            case 0:
                let imageView = cell.viewWithTag(1000) as! UIImageView
                imageView.image = self.videoThumbnailImage
                break
            case 12:
                let label = cell.viewWithTag(1000) as! UILabel
                label.text = frequencyStr
                
                break
            default:
                break
        }
        
        if (indexPath.row != 0 && indexPath.row != 1) {
            let seperatorLine: UILabel = UILabel(frame: CGRectMake(0, 0, cell.frame.size.width, 1.0))
            seperatorLine.backgroundColor = UIColor(red: 200.0/255.0, green: 199.0/255.0, blue: 204.0/255.0, alpha: 1.0)
            cell.addSubview(seperatorLine)
        }
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if indexPath.row == 12 {
            
            let storyboard = UIStoryboard(name: "VideoCapture", bundle: NSBundle.mainBundle())
            let repeatViewController = storyboard.instantiateViewControllerWithIdentifier("RepeatOptionViewController") as! RepeatOptionViewController
            repeatViewController.refreshDelegate = self
            repeatViewController.selectedChoice = self.makeupChoices()
            self.navigationController?.pushViewController(repeatViewController, animated: true)
        
        }
        
        if indexPath.row%2 == 1 {
           //hide keyboard
            self.view.endEditing(true)
        }
        
    }
    
    // MARK: - UIAlertView Delegate Method
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {

    
        
        let alertController:UIAlertController = UIAlertController(title: "Do you want to discard this video?", message: "", preferredStyle: .Alert);
        
        alertController.addAction(UIAlertAction(title: "Yes, Delete it", style: .Destructive, handler: { (action) in
            self.view.endEditing(true)
            
            CircularProgressBar.removeFileAtUrl(self.videoURL)
            
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }));
        
        alertController.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action) in
            alertController.dismissViewControllerAnimated(true, completion: nil);
        }));
        
        self.presentViewController(alertController, animated: true, completion: nil);
   
    }
    
    // MARK: - RefreshFrequencyCellDelegate
    func refreshCell(textStrArr: [String]) {
    
        videoInfo.daysPerWeek.removeAll()
        videoInfo.daysPerWeek = textStrArr
        
        frequencyStr = ""
        
        if textStrArr.count != 0 {
          
            if textStrArr.count == 7 {
               frequencyStr = "Every Day"
            }
//            else if (textStrArr.count == 2) {
//                
//                if textStrArr.indexOf("Sunday") != nil && textStrArr.indexOf("Saturday") != nil {
//                    frequencyStr = "Weekends"
//                } else {
//                    self.makeupString(textStrArr)
//                }
//            }
//            else if (textStrArr.count == 5) {
//                
//                if textStrArr.indexOf("Sunday") == nil && textStrArr.indexOf("Saturday") == nil {
//                   frequencyStr = "Workdays"
//                } else {
//                   self.makeupString(textStrArr)
//                }
//                
//            }
            else {
            
               self.makeupString(textStrArr)
            }
        } else {
        
            frequencyStr = "Please select the day"
        }
        
        let indexPath = NSIndexPath(forRow: 12, inSection: 0)
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
        
        self.checkIfcanUpload()
    }
    
    func makeupString(textStrArr: [String]) {
    
        if textStrArr.indexOf("Sunday") != nil {
            self.makeupFrequency("Su")
        }
        
        if textStrArr.indexOf("Monday") != nil {
            self.makeupFrequency("M")
        }
        
        if textStrArr.indexOf("Tuesday") != nil {
            self.makeupFrequency("T")
        }
        
        if textStrArr.indexOf("Wednesday") != nil {
            self.makeupFrequency("W")
        }
        
        if textStrArr.indexOf("Thursday") != nil {
            self.makeupFrequency("R")
        }
        
        if textStrArr.indexOf("Friday") != nil {
            self.makeupFrequency("F")
        }
        
        if textStrArr.indexOf("Saturday") != nil {
            self.makeupFrequency("Sa")
        }
        
    }
    
    func makeupFrequency(preStr: NSString) {
        
        if frequencyStr.characters.count == 0 {
            frequencyStr = "\(preStr)"
        } else {
            frequencyStr = "\(frequencyStr) \(preStr)"
        }
    }
    
    // MARK: - Keyboard Controls Delegate Method
    func keyboardControls(keyboardControls: BSKeyboardControls!, selectedField field: UIView!, inDirection direction: BSKeyboardControlsDirection) {
        
        let view = field.superview?.superview?.superview
        self.tableView.scrollRectToVisible(view!.frame, animated: true)
        
    }
    
    func keyboardControlsDonePressed(keyboardControls: BSKeyboardControls!) {
        self.view.endEditing(true)
    }

}
