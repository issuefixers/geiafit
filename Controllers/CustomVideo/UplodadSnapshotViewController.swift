//
//  UplodadSnapshotViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 8/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import MediaPlayer
import Alamofire
import Photos
import JTProgressHUD

class UplodadSnapshotViewController: UITableViewController,UITextViewDelegate,NSURLSessionDelegate{

    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
  
    lazy var numberFormatter: NSNumberFormatter = {
        let nf          = NSNumberFormatter()
        nf.allowsFloats = false
        return nf
    }()
    
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descTextView: UITextView!
    var videoURL: NSURL!
    var videoThumbnailImage: UIImage!
    var mpPlayerViewController: MPMoviePlayerViewController?
    var canSave: Bool = false
    


    override func viewDidLoad() {
        self.title = "UPLOAD VIDEO";
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Close"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(leftBtnClicked))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Upload"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(rightBtnClicked))
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
   
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController!.navigationBarHidden = false
        self.imgPreview.image = self.videoThumbnailImage;
        
    }
    
    
    
    
    
    // MARK: - User Interaction Action
    @IBAction func leftBtnClicked() {
        
        
        let alertController:UIAlertController = UIAlertController(title: "Do you want to discard this video?", message: "", preferredStyle: .Alert);
        
        alertController.addAction(UIAlertAction(title: "Yes, Delete it", style: .Destructive, handler: { (action) in
            self.view.endEditing(true)
            
            CircularProgressBar.removeFileAtUrl(self.videoURL)
            
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }));
        
        alertController.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action) in
            alertController.dismissViewControllerAnimated(true, completion: nil);
        }));
        
        self.presentViewController(alertController, animated: true, completion: nil);
    }
    
    @IBAction func rightBtnClicked() {
        
        if !(titleField.text?.isEmpty)! && !descTextView.text.isEmpty{
            JTProgressHUD.show();
            self.uploadVideoSnapshot();
        }else{
            let alertController = UIAlertController(title: "Missing Video Information", message: "Your video has to have a title anddescription", preferredStyle: .Alert);
            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                alertController.dismissViewControllerAnimated(true, completion: nil);
            }));
            
            self.presentViewController(alertController, animated: true, completion: nil);
        }
        
    }
    
    @IBAction func playBtnClicked() {
        
        mpPlayerViewController = MPMoviePlayerViewController(contentURL: self.videoURL)
        self.presentMoviePlayerViewControllerAnimated(mpPlayerViewController)
        mpPlayerViewController?.moviePlayer.play()
        
    }
    
    @IBAction func titleDidBegingEditing(sender: UITextField) {
        
        if sender.tag != 99 {
            sender.text = "";
            sender.tag = 99;
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.tag != 99{
            textView.text =  "";
            textView.tag = 99;
        }
    }
    
    @IBAction func saveToggleChanged(sender: UISwitch) {
        canSave = sender.on;
    }
    
    func uploadVideoSnapshot(){
     
        self.view.endEditing(true);
        
        if canSave == true {
            PHPhotoLibrary.sharedPhotoLibrary().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideoAtFileURL(self.videoURL)
            }) { completed, error in
                if completed {
                    print("Video is saved!")
                }
            }
        }
        
        
        let imagePrewiew:String = UIImageJPEGRepresentation(self.videoThumbnailImage, 0.5)!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
            
        let videoStringData:String = NSData(contentsOfURL: self.videoURL)!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: self.appDelegate.user!.userID))!
        let endpoint: String = (POSTURE_ENDPOINT + "/video").stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let fileName = "SNAPSHOT_" + NSDate().epochTimestamp();
        let params : [String:AnyObject] = ["video_name":fileName + ".mp4",
                                           "video_data": videoStringData,
                                           "video_image_name":fileName+".jpg",
                                           "video_image":imagePrewiew,
                                           "comments":self.descTextView.text,
                                           "name":self.titleField.text!];
        
        let url: String = "\(SERVER_URL)\(endpoint)";
        
        
        
        //print(params)
        
        let URLRequest = NSMutableURLRequest(URL: NSURL(string: url)!)
        URLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        
        let sessionID: String   = NSUserDefaults.standardUserDefaults().stringForKey(SESSION_ID_KEY)!
        let token: String       = NSUserDefaults.standardUserDefaults().stringForKey(CSRF_TOKEN_KEY)!
        
        URLRequest.addValue(sessionID, forHTTPHeaderField:"Cookie")
        URLRequest.addValue(token, forHTTPHeaderField:"X-CSRF-Token")
        
        URLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let data : NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions(rawValue: 0))
        URLRequest.HTTPBody = data
        
        let session: NSURLSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        
        let task = session.dataTaskWithRequest(URLRequest, completionHandler: { data, response, error -> Void in
            JTProgressHUD.hide();
            if error != nil{
                print("An error has occured completing the request")
                let alertController:UIAlertController = UIAlertController(title: "An error has ocurred", message: "We're having trouble completing the upload", preferredStyle: .Alert);
                
           
                alertController.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { (action) in
                    alertController.dismissViewControllerAnimated(true, completion: nil);
                    self.uploadVideoSnapshot();
                }));
                
                alertController.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler: { (action) in
                    self.view.endEditing(true)
                    
                    CircularProgressBar.removeFileAtUrl(self.videoURL)
                    
                    self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
                }));
                
                
                
                self.presentViewController(alertController, animated: true, completion: nil);
            }
            else{
                print("succes");
                let alertController:UIAlertController = UIAlertController(title: "Success!", message: "Your video snapshot has been sent to your PT", preferredStyle: .Alert);
                
                
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                    alertController.dismissViewControllerAnimated(true, completion: nil);
                    self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
                }));
                
                self.presentViewController(alertController, animated: true, completion: nil);

                
            }
            
        })
        
        task.resume()
        
        
        
    }

}

