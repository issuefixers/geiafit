//
//  UploadSuccessViewController.swift
//  Geia
//
//  Created by Cassie on 5/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

class UploadSuccessViewController: UIViewController {
    
    @IBOutlet weak var newCameraBtn: UIButton!

    var videoInfo: VideoInfo!
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Success"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(rightBarBtnClicked))
//        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)], forState: .Normal)
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        self.appDelegate.blockRotation = true
    }
    
    // MARK: - Custom Methods
    func rightBarBtnClicked() {
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func newCameraBtnClicked() {
    
        CircularProgressBar.removeFileAtUrl(self.videoInfo.localURL)
        CircularProgressBar.removeFileAtUrl(self.videoInfo.videoCompressedURL)
        
        NSNotificationCenter.defaultCenter().postNotificationName("CaptureViewClear", object: nil)
        
        self.navigationController?.popToRootViewControllerAnimated(false)
    
    }
    
}