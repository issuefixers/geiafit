//
//  CaptureVideoViewController.swift
//  Geia
//
//  Created by Cassie on 5/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import AVFoundation

enum CaptureBtnStatus: Int {
    case Normal, Recording, Play, Pause
}

enum CameraMode: Int {
    case CustomVideo, Snapshot
}

class CaptureVideoViewController: UIViewController, AVCaptureFileOutputRecordingDelegate, UIAlertViewDelegate, CircularProgressDelegate {

    let radius: CGFloat = 37.0
    let duration: Int = 45
    
    private var captureSession: AVCaptureSession?
    private var videoDevice: AVCaptureDevice?
    private var audioDevice: AVCaptureDevice?
    private var videoInput: AVCaptureDeviceInput?
    private var audioInput: AVCaptureDeviceInput?
    private var movieOutput: AVCaptureMovieFileOutput?
    private var captureVideoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    private var temporaryOutputPath: String = ""
    private var captureBtnStatus: CaptureBtnStatus = .Normal
    private var link: CADisplayLink?
    private var player: AVPlayer?
    private var playerLayer: AVPlayerLayer?
    private var canSave: Bool = false
    private var circularProgressBar: CircularProgressBar!
    
    @IBOutlet weak var captureBtn: UIButton!
    @IBOutlet weak var retakeBtn: UIButton!
    @IBOutlet weak var finishBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var flashBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var secondsLabel: UILabel!
    @IBOutlet weak var recordingImageView: UIImageView!
    
    var cameraMode:CameraMode = .CustomVideo ;
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(restartView), name: "CaptureViewClear", object: nil)
        
        self.initUI()
        self.getAuthorization()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarHidden = true
        self.navigationController!.navigationBarHidden = true
        
        self.appDelegate.blockRotation = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.appDelegate.blockRotation = true
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
       
        self.circularProgressBar.frame = CGRectMake(self.view.frame.size.width/2.0-radius, self.bottomView.frame.size.height/2.0-radius, radius*2, radius*2)
    }
    
    override func prefersStatusBarHidden() -> Bool {
       return true
    }
    
    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        
        self.circularProgressBar.frame = CGRectMake(self.view.frame.size.width/2.0-radius, self.bottomView.frame.size.height/2.0-radius, radius*2, radius*2)
        if (self.playerLayer != nil) {
            self.playerLayer!.frame = self.view.frame
        }
        if (self.captureVideoPreviewLayer != nil) {
           self.captureVideoPreviewLayer!.frame = self.view.layer.bounds
            
           let captureConnection = self.captureVideoPreviewLayer!.connection as AVCaptureConnection
           if captureConnection.supportsVideoOrientation {
            
             switch toInterfaceOrientation {
                case .Portrait:
                    captureConnection.videoOrientation = .Portrait
                    break
                case .LandscapeLeft:
                    captureConnection.videoOrientation = .LandscapeLeft
                    break
                case .LandscapeRight:
                    captureConnection.videoOrientation = .LandscapeRight
                    break
                default:
                    captureConnection.videoOrientation = .Portrait
                    break
             }
            
           }
        }
        
    }
    
    func initUI() {
        
        self.circularProgressBar = CircularProgressBar(frame: CGRectMake(self.view.frame.size.width/2.0-radius, self.bottomView.frame.size.height/2.0-radius, radius*2, radius*2))
        self.circularProgressBar.delegate = self
        self.circularProgressBar.setTotalSecondTime(CGFloat(duration))
        self.bottomView.insertSubview(self.circularProgressBar, atIndex: 0)

        self.cancelBtn.hidden = false
    
        self.captureBtnStatus = .Normal
        self.updateCaptureBtnUI()
    
        self.secondsLabel.text = "\(duration)\""

    }
    
    func getAuthorization() {
    
        let authorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) as AVAuthorizationStatus
        
        switch (authorizationStatus) {
            case AVAuthorizationStatus.Authorized:
                //print("camera authorized")
                self.setupAVCaptureInfo()
                break
            case AVAuthorizationStatus.NotDetermined:
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                    
                    if granted == true {
                        self.setupAVCaptureInfo()
                        
                    } else {
                        self.showAlertMsg("Error", content: "User refused")
                    }
                    
                    return
                })
                break
           default:
            self.showAlertMsg("Error", content: "Rejected authorization")
            break
        }
    }

    func updateCaptureBtnUI() {
    
        switch (self.captureBtnStatus) {
    
            case .Normal:
                self.captureBtn.setImage(UIImage(named: "Record"), forState: .Normal)
                self.retakeBtn.hidden = true
                self.finishBtn.hidden = true
                self.secondsLabel.hidden = false
                self.recordingImageView.hidden = true
                self.flashBtn.hidden = false
                self.circularProgressBar.hidden = false
                break
            case .Recording:
                self.captureBtn.setImage(UIImage(named: "Stop"), forState: .Normal)
                self.retakeBtn.hidden = true
                self.finishBtn.hidden = true
                self.secondsLabel.hidden = false
                self.recordingImageView.hidden = false
                self.flashBtn.hidden = false
                self.circularProgressBar.hidden = false
                break
            case .Play:
                self.captureBtn.setImage(UIImage(named: "Play1"), forState: .Normal)
                self.retakeBtn.hidden = false
                self.finishBtn.hidden = false
                self.secondsLabel.hidden = true
                self.recordingImageView.hidden = true
                self.flashBtn.hidden = true
                self.circularProgressBar.hidden = true
                break
            case .Pause:
                self.captureBtn.setImage(UIImage(named: "Pause"), forState: .Normal)
                self.retakeBtn.hidden = true
                self.finishBtn.hidden = true
                self.secondsLabel.hidden = true
                self.recordingImageView.hidden = true
                self.flashBtn.hidden = true
                self.circularProgressBar.hidden = true
                break
        }
    }
    
    func clearCaptureInfo() {
    
        self.captureSession = nil
        self.videoDevice = nil
        self.audioDevice = nil
        self.videoInput = nil
        self.audioInput = nil
        self.movieOutput = nil
        self.captureVideoPreviewLayer = nil
    
    }

    func setupAVCaptureInfo() {
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            self.addSession()
        
            self.captureSession?.beginConfiguration()
            self.addVideo()
            self.addAudio()
            self.addPreviewLayer()
            self.captureSession?.commitConfiguration()
        
            //start session (!= start recording)
            self.captureSession?.startRunning()
        })
    }

    func addSession() {
    
        self.captureSession = AVCaptureSession()
    
        if ((self.captureSession?.canSetSessionPreset(AVAssetExportPresetHighestQuality)) != nil) {
            self.captureSession?.canSetSessionPreset(AVAssetExportPresetHighestQuality)
        }
    }
    
    func addVideo() {
        
       self.videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
       //self.deviceWithMediaType(AVMediaTypeVideo, position: AVCaptureDevicePosition.Back)
       self.addVideoInput()
       self.addMovieOutput()

    }
    
    func addVideoInput() {
    
        do {
        
            self.videoInput = try AVCaptureDeviceInput(device: self.videoDevice)
            
            if (self.captureSession?.canAddInput(self.videoInput) != nil) {
               self.captureSession?.addInput(self.videoInput)
            }
            
        } catch let error as NSError {
           print("----videoInput----- \(error)")
        }
    }
    
    func addMovieOutput() {
        
        self.movieOutput = AVCaptureMovieFileOutput()
        
        if (self.captureSession?.canAddOutput(self.movieOutput) != nil) {
        
            self.captureSession?.addOutput(self.movieOutput)
            let captureConnection = self.movieOutput!.connectionWithMediaType(AVMediaTypeVideo) as AVCaptureConnection
            
//            if (captureConnection.supportsVideoOrientation) {
//                   captureConnection.videoOrientation = .Portrait
//            }
            
            if (captureConnection.supportsVideoStabilization) {
            
               captureConnection.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.Auto
            }
            
            captureConnection.videoScaleAndCropFactor = captureConnection.videoMaxScaleAndCropFactor
            
        }
    }
    
    func addAudio() {
        
        self.audioDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeAudio)
        
        do {
            
            self.audioInput = try AVCaptureDeviceInput(device: self.audioDevice)
            
            if (self.captureSession?.canAddInput(self.audioInput) != nil) {
                self.captureSession?.addInput(self.audioInput)
            }
            
        } catch let error as NSError {
            print("---audioInput---- \(error)")
        }
    }
    
    func addPreviewLayer() {
        
        self.captureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.captureVideoPreviewLayer!.frame = self.view.layer.bounds
        self.captureVideoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
    
        self.view.layer.masksToBounds = true
        
        self.view.layer.insertSublayer(self.captureVideoPreviewLayer!, atIndex: 0)
        self.view.layoutIfNeeded()
        
    }

    // MARK: - Videoing Methods
    func deviceWithMediaType(mediaType: String, position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        
        let devices = AVCaptureDevice.devicesWithMediaType(mediaType)
        var captureDevice: AVCaptureDevice?
        
        for device in devices  {
            let device = device as! AVCaptureDevice
            if ( device.position == position ) {
                captureDevice = device
                break
            }
        }
        
        return captureDevice
    }
    
    func outPutFileURL() -> NSURL {
    
        return NSURL.fileURLWithPath("\(NSTemporaryDirectory())outPut\(NSUUID().UUIDString).mov")
    }
    
    func startRecord() {
        self.movieOutput?.startRecordingToOutputFileURL(self.outPutFileURL(), recordingDelegate: self)
    }
    
    func stopRecord() {
        self.movieOutput?.stopRecording()
    }
    
    func recordComplete() {
        self.canSave = true
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        //print("---- Start Recording ----")
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!) {
        //print("---- Finish Recording ----")
        
        self.temporaryOutputPath = outputFileURL.absoluteString!
        
        if (self.temporaryOutputPath.characters.count == 0 && captureOutput.outputFileURL.absoluteString!.characters.count == 0 ) {
            self.showAlertMsg("Error", content: "Error of saving video path");
            return;
        }
        
        if (self.canSave) {
            self.startToPlay(outputFileURL)
            self.canSave = false;
        }
        
    }
    
    func startToPlay(url: NSURL) {
    
        self.captureVideoPreviewLayer?.removeFromSuperlayer()

        if (self.player != nil) {
            self.player = nil;
            NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
        }
    
        if (self.playerLayer != nil) {
            self.playerLayer = nil;
        }
    
        self.player = AVPlayer(playerItem: AVPlayerItem(URL: url))
        self.playerLayer = AVPlayerLayer(player: self.player)
        self.playerLayer!.frame = self.view.frame
        self.playerLayer!.videoGravity=AVLayerVideoGravityResizeAspectFill;
        self.view.layer.insertSublayer(self.playerLayer!, atIndex: 0)
    
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(playEnd(_:)), name: AVPlayerItemDidPlayToEndTimeNotification, object: self.player!.currentItem)
    }
    
    func playEnd(notification: NSNotification?) {
    
        if (notification != nil) {
            let playerItem = notification!.object as! AVPlayerItem
            playerItem.seekToTime(kCMTimeZero)
        }
    
        self.captureBtnStatus = .Play
        self.updateCaptureBtnUI()
    
    }
    
    // MARK: - Interaction Action
    @IBAction func captureBtnClicked() {
    
        self.cancelBtn.hidden = true
    
        switch (self.captureBtnStatus) {
            case .Normal:
                self.startAnimation()
                self.captureBtnStatus = .Recording
                break
            case .Recording:
                self.recordComplete()
                self.stopAnimation()
                self.captureBtnStatus = .Play
                break
            case .Play:
                self.player?.play()
                self.captureBtnStatus = .Pause
                break
            case .Pause:
                self.player?.pause()
                self.captureBtnStatus = .Play
                break
        }
    
        self.updateCaptureBtnUI()
    }
    
    @IBAction func retakeBtnClicked() {
    
        self.restartView()
        
        //delete the video just recorded
        if (NSFileManager.defaultManager().fileExistsAtPath(self.temporaryOutputPath)) {
        
            do {
               try NSFileManager.defaultManager().removeItemAtPath(self.temporaryOutputPath)
            } catch {
               print("Failed to remove temporary file directory at \(self.temporaryOutputPath)")
            }
        }
    }
    
    @IBAction func finishBtnClicked() {

        
        if cameraMode == CameraMode.CustomVideo {
            let storyboard = UIStoryboard(name: "VideoCapture", bundle: NSBundle.mainBundle())
            let editVideoViewController = storyboard.instantiateViewControllerWithIdentifier("EditVideoViewController") as! EditVideoViewController
            editVideoViewController.videoURL = NSURL(string: self.temporaryOutputPath)
            self.clearView()
            self.navigationController?.pushViewController(editVideoViewController, animated: true)
        }else{
            let editVideoViewController:TrimVideoViewController = UIStoryboard.getVideoCaptureStoryboard().instantiateViewControllerWithIdentifier("TrimVideoViewController") as! TrimVideoViewController;
            editVideoViewController.assetURL = NSURL(string: self.temporaryOutputPath)
            self.clearView()
            self.navigationController?.pushViewController(editVideoViewController , animated: true)
        }

    
    }
    
    @IBAction func cancelBtnClicked() {
    
      self.clearView()
      
      UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
      self.setNeedsStatusBarAppearanceUpdate()
        
      self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func changeFlashlight(sender: UIButton) {

        if (self.videoDevice?.hasTorch == true && self.videoDevice?.hasFlash == true ) {
            
            self.changeDevicePropertySafety({ (captureDevice) in
                
                if self.videoDevice?.flashMode == AVCaptureFlashMode.On {
                    self.flashBtn.setImage(UIImage(named: "FlashOff"), forState: .Normal)
                    self.videoDevice?.flashMode = AVCaptureFlashMode.Off
                    self.videoDevice?.torchMode = AVCaptureTorchMode.Off
                } else if self.videoDevice?.flashMode == AVCaptureFlashMode.Off {
                    self.flashBtn.setImage(UIImage(named: "FlashOn"), forState: .Normal)
                    self.videoDevice?.flashMode = AVCaptureFlashMode.On
                    self.videoDevice?.torchMode = AVCaptureTorchMode.On
                } else {
                    self.flashBtn.setImage(UIImage(named: "FlashAuto"), forState: .Normal)
                    self.videoDevice?.flashMode = AVCaptureFlashMode.Auto
                    self.videoDevice?.torchMode = AVCaptureTorchMode.Auto
                }
            })
            
            sender.selected = !sender.selected
        } else {
        
            print("Fail to turn on flash/torch")
        }
    }
    
    //更改设备属性前一定要锁上
    func changeDevicePropertySafety(propertyChange:((captureDevice: AVCaptureDevice) -> Void)) {

        let captureDevice = self.videoInput!.device

        do {
            try captureDevice.lockForConfiguration()
        } catch {
            print("Failed to lock device")
            return
        }
        
        self.captureSession?.beginConfiguration()
        propertyChange(captureDevice: captureDevice)
        captureDevice.unlockForConfiguration()
        self.captureSession?.commitConfiguration()
    }
    
    func getLink() {

        if (self.link == nil) {
            self.link = CADisplayLink(target: self, selector: #selector(refresh))
            self.startRecord()
        }

    }
    
    func stopLink() {
        
        if (self.link != nil) {
        
            self.link!.paused = true
            self.link!.invalidate()
            self.link = nil
            
        }
    }
    
    func refresh() {
        
        let text = self.secondsLabel.text! as NSString
        let length: Int = text.length - 1
        let secondStr = text.substringWithRange(NSMakeRange(0, length)) as NSString
        let seconds: Int = secondStr.integerValue
        
        if seconds <= 0 {
            self.recordComplete()
            self.stopLink()
            self.stopRecord()
        }
    }
    
    // MARK: - Custom Animation
    func startAnimation() {
        
        self.circularProgressBar.startTimer()
        self.stopLink()
        
        self.getLink()
        self.link!.addToRunLoop(NSRunLoop.mainRunLoop(), forMode: NSRunLoopCommonModes)
        
    }
    
    func stopAnimation() {
    
        self.circularProgressBar.stopTimer()
        self.stopLink()
        self.stopRecord()
    
    }

    // MARK: - progress bar delegate
    func circularProgressEnd() {
    
        self.playEnd(nil)
        self.circularProgressBar.stopTimer()
        
    }
    
    func updateDisplaySeconds() {
        
        let text = self.secondsLabel.text! as NSString
        let length: Int = text.length - 1
        let secondStr = text.substringWithRange(NSMakeRange(0, length)) as NSString
        var seconds: Int = secondStr.integerValue
        seconds -= 1
        
        self.secondsLabel.text = "\(seconds)\""
        
    }
    
    // MARK: - UIAlertView Delegate Method
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        self.cancelBtnClicked()
    }

    // MARK: - System Methods
    func showAlertMsg(title: String, content: String) {
    
        UIAlertView(title: title, message: content, delegate: self, cancelButtonTitle: "OK").show()
    
    }
    
    // MARK: - notification method
    func restartView() {
    
        self.clearView()
        self.setupAVCaptureInfo()
    }
    
    func clearView() {
        
        self.playerLayer?.removeFromSuperlayer()
        self.cancelBtn.hidden = false
        self.clearCaptureInfo()
        self.secondsLabel.text = "\(duration)\""
        
        self.stopLink()
        self.stopRecord()
        self.canSave = false
        
        self.captureBtnStatus = .Normal
        self.updateCaptureBtnUI()
        
    }
    
}
