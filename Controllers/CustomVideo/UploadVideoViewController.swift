//
//  UploadVideoViewController.swift
//  Geia
//
//  Created by Cassie on 5/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import Alamofire

class UploadVideoViewController: UIViewController, NSURLSessionDelegate, NSURLSessionTaskDelegate {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var retryBtn: UIButton!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    @IBOutlet weak var progressLineWidthContraints: NSLayoutConstraint!
    
    var videoInfo: VideoInfo!
    var task: NSURLSessionDataTask?
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    lazy var numberFormatter: NSNumberFormatter = {
        let nf          = NSNumberFormatter()
        nf.allowsFloats = false
        return nf
    }()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bottomView.hidden = true
        self.errorView.hidden = true
        self.hintLabel.hidden = false
        self.progressLineWidthContraints.constant = 0
        self.titleLabel.text = self.videoInfo.title
        self.descriptionLabel.text = self.videoInfo.videoDescription
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.appDelegate.blockRotation = true
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style:
            UIBarButtonItemStyle.Plain, target: self, action: #selector(leftBarBtnClicked))
        
        self.performSelector(#selector(compressVideo), withObject: self, afterDelay: 0.2)
    }
    
    func leftBarBtnClicked() {
        if #available(iOS 9.0, *) {
            Manager.sharedInstance.session.getAllTasksWithCompletionHandler { tasks in
                tasks.forEach {  $0.cancel() }
            }
        } else {
            // Fallback on earlier versions
            Manager.sharedInstance.session.getTasksWithCompletionHandler { dataTasks,uploadTasks,downloadTasks in
                dataTasks.forEach {  $0.cancel() }
                uploadTasks.forEach {  $0.cancel() }
                downloadTasks.forEach {  $0.cancel() }
            }
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func compressVideo() {
        
        let fileUrl = CircularProgressBar.videoCompress(self.videoInfo.localURL)
        self.videoInfo.videoCompressedURL = fileUrl
        self.videoInfo.videoDataStr = NSData(contentsOfURL: fileUrl)!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        self.startUploading()
    }
    
    func startUploading() {
        
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: self.appDelegate.user!.userID))!
        let endpoint: String = WEBEX_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
        let url: String      = "\(SERVER_URL)\(endpoint)"
        let params: [String : AnyObject] = self.videoInfo.makeJson()
        
        //print(params)
        
        let URLRequest = NSMutableURLRequest(URL: NSURL(string: url)!)
        URLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        
        let sessionID: String   = NSUserDefaults.standardUserDefaults().stringForKey(SESSION_ID_KEY)!
        let token: String       = NSUserDefaults.standardUserDefaults().stringForKey(CSRF_TOKEN_KEY)!
        
        URLRequest.addValue(sessionID, forHTTPHeaderField:"Cookie")
        URLRequest.addValue(token, forHTTPHeaderField:"X-CSRF-Token")
        
        URLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let data : NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions(rawValue: 0))
        URLRequest.HTTPBody = data
        
        let session: NSURLSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        
        task = session.dataTaskWithRequest(URLRequest, completionHandler: { data, response, error -> Void in
            if error != nil{
                print("An error has occured completing the request")
                self.progressLineWidthContraints.constant = 0
                self.errorView.hidden = false
                self.bottomView.hidden = false
                self.hintLabel.hidden = true
            }
            else{
                self.goSuccessPage()
            }
            
        })
        
        task!.resume()
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        self.progressLineWidthContraints.constant = (self.view.frame.size.width - 24.0*2.0)*CGFloat(totalBytesSent)/CGFloat(totalBytesExpectedToSend)
    }
    
    @IBAction func retryBtnClicked() {
        
        self.errorView.hidden = true
        self.bottomView.hidden = true
        self.hintLabel.hidden = false
        
        self.startUploading()
        
    }
    
    @IBAction func cancelBtnClicked() {
 
        self.task!.cancel()
        
        CircularProgressBar.removeFileAtUrl(self.videoInfo.localURL)
        CircularProgressBar.removeFileAtUrl(self.videoInfo.videoCompressedURL)
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func goSuccessPage() {
    
        let storyboard = UIStoryboard(name: "VideoCapture", bundle: NSBundle.mainBundle())
        let uploadSuccessViewController = storyboard.instantiateViewControllerWithIdentifier("UploadSuccessViewController") as! UploadSuccessViewController
        uploadSuccessViewController.videoInfo = self.videoInfo
        self.navigationController?.pushViewController(uploadSuccessViewController, animated: true)
        
    }
    
}