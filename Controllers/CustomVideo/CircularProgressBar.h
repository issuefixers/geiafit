//
//  CircularProgressBar.h
//  CircularProgressBar
//
//  Created by du on 10/8/15.
//  Copyright © 2015 du. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CircularProgressDelegate

- (void)circularProgressEnd;
- (void)updateDisplaySeconds;

@end


@interface CircularProgressBar : UIView

@property(nonatomic, assign) id<CircularProgressDelegate> delegate;
@property(nonatomic) CGFloat time_left;

- (void)setTotalSecondTime:(CGFloat)time;
- (void)setTotalMinuteTime:(CGFloat)time;

- (void)startTimer;
- (void)stopTimer;
- (void)pauseTimer;

+ (NSURL *)videoCompress:(NSURL *)url;
+ (void)removeFileAtUrl:(NSURL *)url;

@end
