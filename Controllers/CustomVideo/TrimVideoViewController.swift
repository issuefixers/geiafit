//
//  TrimVideoViewController.swift
//  Geia
//
//  Created by Cindy Mora on 8/7/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import AVFoundation
import ICGVideoTrimmer
import Alamofire
import JTProgressHUD

enum TrimMode: Int {
    case CustomVideo, Snapshot
}


class TrimVideoViewController: UIViewController,ICGVideoTrimmerDelegate{

    var isPlaying:Bool = false;
    var player:AVPlayer?;
    var playerItem:AVPlayerItem?;
    var playerLayer:AVPlayerLayer?;
    var playbackTimeCheckerTimer:NSTimer?;
    var videoPlaybackPosition:CGFloat = 0.0;
    var videoStringData:String = "" ;
    var assetURL:NSURL?;
    var segmentedControl:SSInstagramSegmented?
    
    @IBOutlet weak var segmentedThumbs: UISegmentedControl!
    
    @IBOutlet weak var thumbnailView: UIView!
    @IBOutlet var trimmerView:ICGVideoTrimmerView?;
    @IBOutlet var videoPlayer:UIView?;
    @IBOutlet var videoLayer:VideoContainerView?;
    
    private var tempVideoPath:NSURL?;
    private var exportSession:AVAssetExportSession?;
    private var asset:AVAsset?;
    private var startTime:CGFloat = 0.0;
    private var stopTime:CGFloat = 0.0;
    private var images:[UIImage]?
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    lazy var numberFormatter: NSNumberFormatter = {
        let nf          = NSNumberFormatter()
        nf.allowsFloats = false
        return nf
    }()
    
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad();
        self.tempVideoPath = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("tmpMov.mov")

        self.asset = AVAsset(URL: assetURL!)
        let item = AVPlayerItem(asset: self.asset!);
        self.player = AVPlayer(playerItem: item);
        self.playerLayer = AVPlayerLayer(player: self.player);
        
        self.playerLayer?.contentsGravity = AVLayerVideoGravityResizeAspect;
        self.player?.actionAtItemEnd = AVPlayerActionAtItemEnd.None;
        self.videoLayer?.layer.addSublayer(self.playerLayer!);
        self.videoLayer?.playerLayer = self.playerLayer;

        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TrimVideoViewController.tapOnVideoLayer(_:)));
        self.videoLayer?.addGestureRecognizer(tap);
        
        self.videoPlaybackPosition = 0;
        self.tapOnVideoLayer(tap);
        
        // trimmer view properties;
       
        
        self.trimmerView?.themeColor = UIColor.lightGrayColor();
        self.trimmerView?.asset = self.asset;
        self.trimmerView?.showsRulerView = true;
        self.trimmerView?.trackerColor = UIColor.cyanColor();
        self.trimmerView?.delegate = self;

        if self.asset?.duration.seconds > 45.0 {
            self.trimmerView?.maxLength = 45.0;
        }else{
            self.trimmerView?.maxLength = CGFloat((self.asset?.duration.seconds)!);
        }
        
        self.trimmerView?.minLength = 1;

        self.title = "UPLOAD VIDEO";
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Close"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(leftBtnClicked))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: #selector(rightBtnClicked(_:)));
        self.navigationController!.navigationBarHidden = false
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 34.0/255.0, green: 34.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]

        
        images = [self.previewImageForLocalVideo(0.25)!,self.previewImageForLocalVideo(0.50)!,self.previewImageForLocalVideo(0.75)!,self.previewImageForLocalVideo(0.99)!]
       
    
    }
    
    

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.trimmerView?.resetSubviews();
        self.playerLayer?.frame = CGRectMake(0, 0, CGRectGetWidth(self.videoLayer!.frame), CGRectGetHeight(self.videoLayer!.frame));
        UIApplication.sharedApplication().statusBarHidden = true;
        self.navigationController!.navigationBarHidden = false;
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        UIApplication.sharedApplication().statusBarHidden = false;
        self.navigationController!.navigationBarHidden = false;
    }
    
    override func viewDidAppear(animated:Bool){
        super.viewDidAppear(animated);
        self.trimmerView?.resetSubviews();
        segmentedControl = SSInstagramSegmented.init(frame: self.segmentedThumbs.frame, items: self.images!, backgroundColor: UIColor.geiaDarkGrayColor());
        
        self.segmentedThumbs.removeFromSuperview();
        self.thumbnailView.addSubview(segmentedControl!);

    }
    // MARK: - User Interaction Action
    
 
    
    func rightBtnClicked(sender: UIBarButtonItem){
        JTProgressHUD.show()
        self.exportSessionToVideo();
   
        
    }
    
    func leftBtnClicked() {
        
        
        let alertController:UIAlertController = UIAlertController(title: "Do you want to discard this video?", message: "", preferredStyle: .Alert);
        
        alertController.addAction(UIAlertAction(title: "Yes, Delete it", style: .Destructive, handler: { (action) in
            self.view.endEditing(true)
            
            CircularProgressBar.removeFileAtUrl(self.assetURL)
            self.player?.pause();
            self.stopPlaybackTimeChecker();
            
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }));
        
        alertController.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action) in
            alertController.dismissViewControllerAnimated(true, completion: nil);
        }));
        
        self.presentViewController(alertController, animated: true, completion: nil);
    }
    
    
    //MARK: - IGVideoTrimmerDelegate;
    
    func exportSessionToVideo(){
        self.deleteTempFile();
        
        let compatiblePreset = AVAssetExportSession.exportPresetsCompatibleWithAsset(self.asset!);
        
        if compatiblePreset.contains(AVAssetExportPresetMediumQuality) {
            self.exportSession = AVAssetExportSession(asset: self.asset!, presetName:AVAssetExportPresetMediumQuality);
            
//            let furl = NSURL(fileURLWithPath: self.tempVideoPath);
            self.exportSession?.outputURL = self.tempVideoPath;
            self.exportSession?.outputFileType = AVFileTypeQuickTimeMovie;
            
            let start = CMTimeMakeWithSeconds(Double(self.startTime), (self.asset?.duration.timescale)!);
            let duration = CMTimeMakeWithSeconds(Double(self.stopTime), (self.asset?.duration.timescale)!);
            let range = CMTimeRangeMake(start, duration);
            self.exportSession!.timeRange = range;

            self.exportSession?.exportAsynchronouslyWithCompletionHandler({
                switch(self.exportSession!.status){
                case .Failed:
                    print("export failed");
                    print(self.exportSession?.error?.localizedDescription);
                    break
                    
                    
                
                case .Cancelled:
                    print("cancelled");
                    break;
                default:
                    self.uploadVideoSnapshot();
                    
                    break;
            }
                
                }
                
                );
            
        }
    }
    
    @IBAction func changeTool(sender: UISegmentedControl) {
    
        if sender.selectedSegmentIndex == 1 {
            self.trimmerView?.alpha = 0;
            UIView.animateWithDuration(0.25, animations: { 
                self.thumbnailView?.alpha = 1;
            });
        }else{
            self.thumbnailView?.alpha = 0;
            UIView.animateWithDuration(0.25, animations: {
                self.trimmerView?.alpha = 1;
            });
        }
    
    }

    
    func uploadVideoSnapshot(){
//        loaderView.hide(animated: true);

        let ctrlUpload = UIStoryboard.getVideoCaptureStoryboard().instantiateViewControllerWithIdentifier("UplodadSnapshotViewController") as?UplodadSnapshotViewController;
        
        
        
        ctrlUpload!.videoURL = self.tempVideoPath;
        ctrlUpload!.videoThumbnailImage = images![(self.segmentedControl?.selectedIndex)!];
        self.player?.pause();
        self.stopPlaybackTimeChecker();
        
        dispatch_async(dispatch_get_main_queue()) {
            JTProgressHUD.hide()

            self.navigationController?.pushViewController(ctrlUpload!, animated: true);
        };
        
        
    }
    
    
    func trimmerView(trimmerView: ICGVideoTrimmerView!, didChangeLeftPosition startTime: CGFloat, rightPosition endTime: CGFloat) {
        if startTime != self.startTime {
            self.seekVideoToPos(position: startTime);
        }
        self.startTime = startTime;
        self.stopTime = endTime;
    }
    
    func deleteTempFile(){
        let url:NSURL = self.tempVideoPath!;
        let fm:NSFileManager = NSFileManager.defaultManager();
        if fm.fileExistsAtPath(url.path!){
            do{
               try fm.removeItemAtURL(url)
            }catch _{
                
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.playerLayer?.frame = CGRectMake(0, 0, (self.videoLayer?.frame.size.width)!, (self.videoLayer?.frame.size.height)!);
    }
    
    func tapOnVideoLayer(tapGesture: UITapGestureRecognizer){
        if self.isPlaying {
            self.player?.pause();
            self.stopPlaybackTimeChecker();
        }else{
            self.player?.play();
            self.startPlaybackTimeChecker();
        }
        
        self.isPlaying = !self.isPlaying;
        self.trimmerView?.hideTracker(!self.isPlaying);
    }
    
    
    func startPlaybackTimeChecker(){
        self.stopPlaybackTimeChecker();
        self.playbackTimeCheckerTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self,
                                                                               selector: #selector(TrimVideoViewController.onPlaybackTimeCheckerTimer), userInfo: nil, repeats: true);
    }
    
    func stopPlaybackTimeChecker(){
        if (self.playbackTimeCheckerTimer != nil) {
            self.playbackTimeCheckerTimer?.invalidate();
            self.playbackTimeCheckerTimer = nil;
        }
    }
    
    func onPlaybackTimeCheckerTimer(){
        
        self.videoPlaybackPosition = CGFloat(CMTimeGetSeconds(self.player!.currentTime()))
        self.trimmerView?.seekToTime(self.videoPlaybackPosition)
        
    }
    
    func seekVideoToPos(position pos:CGFloat) {
        self.videoPlaybackPosition = pos;
        let time:CMTime = CMTimeMakeWithSeconds(Double(self.videoPlaybackPosition), (self.player?.currentTime().timescale)!);
        self.player?.seekToTime(time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero);
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    func previewImageForLocalVideo(position: Float64) -> UIImage?
    {
        let imageGenerator = AVAssetImageGenerator(asset: self.asset!)
        imageGenerator.appliesPreferredTrackTransform = true
        
        var time = self.asset!.duration
        
        time = CMTimeMultiplyByFloat64(time, position)
        
        do {
            let imageRef = try imageGenerator.copyCGImageAtTime(time, actualTime: nil)
            return UIImage(CGImage: imageRef)
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            return nil
        }
    }
}
