//
//  RepeatOptionViewController.swift
//  Geia
//
//  Created by Cassie on 5/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

class RepeatOptionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    private var datasource: [NSString]!
    
    let cellHeight: CGFloat = 44.0
    
    var selectedChoice: [String]!
    weak var refreshDelegate: RefreshFrequencyCellDelegate?
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.datasource = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Select All", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(rightBarBtnClicked))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(leftBarBtnClicked))
        
        self.tableViewHeightConstraint.constant = CGFloat(self.datasource.count+1) * cellHeight
        
        self.appDelegate.blockRotation = true
    }
    
    // MARK: - Custom Methods
    func rightBarBtnClicked() {
    
        self.selectedChoice.removeAll()
        self.selectedChoice = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        self.tableview.reloadData()
       
    }
    
    func leftBarBtnClicked() {
        
        if self.refreshDelegate != nil {
           self.refreshDelegate?.refreshCell(self.selectedChoice)
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func updateSelectedChoice(indexPath: NSIndexPath) {
    
        let cell = self.tableview.cellForRowAtIndexPath(indexPath)
        let dayStr = (cell?.textLabel?.text)! as String
        
        if self.selectedChoice.indexOf(dayStr) != nil {
        
            let index: Int = self.selectedChoice.indexOf(dayStr)!
            self.selectedChoice.removeAtIndex(index)
        
        } else {
        
            self.selectedChoice.append(dayStr)
        
        }
    
    }
    
    // MARK: - TableView Delegate & Datasource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count+1
    }
        
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        if indexPath.row == self.datasource.count {
        
            let cell =  tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)
            cell.textLabel?.text = "Select All Days"
            cell.textLabel?.textColor = UIColor(red: 0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
            
            return cell
        }
        
        let cell =  tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        let dayStr = self.datasource[indexPath.row] as String
        cell.textLabel?.text = dayStr
        
        let imageView = cell.viewWithTag(1000) as! UIImageView
        if self.selectedChoice.indexOf(dayStr) != nil {
            imageView.image = UIImage(named: "CheckBox1")
        } else {
            imageView.image = UIImage(named: "CheckBox2")
        }
    
        return cell
        
    }
        
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if indexPath.row == self.datasource.count {
            
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.rightBarBtnClicked()
            
        } else {
            
            self.updateSelectedChoice(indexPath)
            tableView.reloadData()
            
        }
        
    }

}