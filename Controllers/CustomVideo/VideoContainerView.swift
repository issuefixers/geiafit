//
//  VideoContainerView.swift
//  Geia
//
//  Created by Sergio Solorzano on 10/6/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class VideoContainerView: UIView {
    var playerLayer: CALayer?
    override func layoutSublayersOfLayer(layer: CALayer) {
        super.layoutSublayersOfLayer(layer)
        playerLayer?.frame = self.bounds
    }
}
