//
//  CircularProgressBar.m
//  CircularProgressBar
//
//  Created by du on 10/8/15.
//  Copyright © 2015 du. All rights reserved.
//

#define CIRCLE_WIDTH 6
#define PROGRESS_WIDTH 6
#define TIMER_INTERVAL 0.1

#import "CircularProgressBar.h"
#import <AVFoundation/AVFoundation.h>

@interface CircularProgressBar () {
    CGFloat startAngle;
    CGFloat endAngle;
    int     totalTime;
    
    NSTimer *m_timer;
    bool b_timerRunning;
    CGFloat lastTime_left;
}
@end

@implementation CircularProgressBar

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
       
        self.backgroundColor = [UIColor clearColor];
        [self initData];
     
    }
    return self;
    
}

- (void)initData {
    
    startAngle = -0.5 * M_PI;
    endAngle = startAngle;
    
    totalTime = 0;
    lastTime_left = self.time_left;
    
    b_timerRunning = NO;

}

- (void)drawRect:(CGRect)rect {
    
    if (totalTime == 0) 
        endAngle = startAngle;
    else
        endAngle = (1 - self.time_left / totalTime) * 2 * M_PI + startAngle;
    
    float radius = (rect.size.width / 2) < (rect.size.height / 2) ? (rect.size.width / 2) : (rect.size.height / 2);
    UIBezierPath *circle = [UIBezierPath bezierPath];
    [circle addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:radius - CIRCLE_WIDTH
                      startAngle:0
                        endAngle:2 * M_PI
                       clockwise:YES];
    circle.lineWidth = CIRCLE_WIDTH;
    [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.3] setStroke];
    [circle stroke];

    
    UIBezierPath *progress = [UIBezierPath bezierPath];
    [progress addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:radius - CIRCLE_WIDTH
                      startAngle:startAngle
                        endAngle:endAngle
                       clockwise:YES];
    progress.lineWidth = PROGRESS_WIDTH;
//    [[UIColor redColor] setStroke];
    [[UIColor colorWithRed:243.0/255.0 green:168.0/255.0 blue:27.0/255.0 alpha:1.0] set];
    [progress stroke];
    
}

- (void)setTotalSecondTime:(CGFloat)time {
    totalTime = time;
    self.time_left = totalTime;
}

- (void)setTotalMinuteTime:(CGFloat)time {
    totalTime = time * 60;
    self.time_left = totalTime;
}

- (void)startTimer {
    if (!b_timerRunning) {
        m_timer = [NSTimer scheduledTimerWithTimeInterval:TIMER_INTERVAL target:self selector:@selector(setProgress) userInfo:nil repeats:YES];
        b_timerRunning = YES;
    }
}

- (void)pauseTimer {
    if (b_timerRunning) {
        [m_timer invalidate];
        m_timer = nil;
        b_timerRunning = NO;
    }
}

- (void)setProgress {
    if (self.time_left > 0) {
        self.time_left -= TIMER_INTERVAL;
        [self setNeedsDisplay];
        
        if (delegate && (self.time_left < lastTime_left)) {
            [delegate updateDisplaySeconds];
        }
        
        lastTime_left = floor(self.time_left);
        
    } else {
        [self pauseTimer];
        
        if (delegate) {
            [delegate circularProgressEnd];
        }
    }
}

- (void)stopTimer {
    [self pauseTimer];
    
    startAngle = -0.5 * M_PI;
    endAngle = startAngle;
    self.time_left = totalTime;
    [self setNeedsDisplay];

}

+ (NSURL *)videoCompress:(NSURL *)url {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *fileCompressedLocalPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"compressed-%@",url.lastPathComponent]];
    NSURL *outputURL = [NSURL fileURLWithPath:fileCompressedLocalPath];

    NSString *aPath = [outputURL.absoluteString substringFromIndex:7];
    if (![[NSFileManager defaultManager] fileExistsAtPath:aPath]) {
        
        __block BOOL finished = NO;
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        AVAsset *avAsset = [AVAsset assetWithURL:url];
        AVAssetExportSession *exportSession= [[AVAssetExportSession alloc] initWithAsset:avAsset     presetName:AVAssetExportPresetMediumQuality];
        exportSession.outputURL = outputURL;
        exportSession.outputFileType = AVFileTypeMPEG4;
        [exportSession exportAsynchronouslyWithCompletionHandler:^(void) {
            switch (exportSession.status)
            {
                case AVAssetExportSessionStatusUnknown:
                    break;
                case AVAssetExportSessionStatusWaiting:
                    break;
                case AVAssetExportSessionStatusExporting:
                    break;
                case AVAssetExportSessionStatusCompleted: {
                    finished = YES;
                    dispatch_semaphore_signal(sema);
                    break;
                }
                case AVAssetExportSessionStatusFailed: {
                    finished = NO;
                    dispatch_semaphore_signal(sema);
                    break;
                }
                case AVAssetExportSessionStatusCancelled: {
                    finished = NO;
                    dispatch_semaphore_signal(sema);
                    break;
                }
            }
        }];

        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);

        if (finished) {
            return outputURL;
        }
    
        return url;
    }
    
    return outputURL;
}

+ (void)removeFileAtUrl:(NSURL *)url {
    
    NSString *aPath = url.absoluteString;
    if ([aPath hasPrefix:@"file://"]) {
       aPath = [aPath substringFromIndex:7];
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:aPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:aPath error:nil];
    }
    
}

@end
