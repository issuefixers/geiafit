//
//  VideoInfo.swift
//  Geia
//
//  Created by Cassie on 5/17/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

class VideoInfo: NSObject {

    var title: String?
    var videoName: String?
    var imageName: String?
    var imageDataStr: String?
    var imageDataStr2: String?
    var videoDescription: String?
    var repetitions: String?
    var sets: String?
    var timesPerDay: String?
    var daysPerWeek: [String] = [String]()
    var localURL: NSURL?
    var videoDataStr: String?
    var videoCompressedURL: NSURL?

    func makeJson() -> [String : AnyObject] {
    
       return ["name": self.title!,
               "video_name": self.title!,
               "video_image_name": "\(NSUUID().UUIDString)",
               "video_image_name2": "\(NSUUID().UUIDString)",
               "video_image": self.imageDataStr!,
               "video_image2": self.imageDataStr2!,
               "video_data": self.videoDataStr!,
               "comments": ((self.videoDescription != nil) ? self.videoDescription : "")!,
               "reps": self.repetitions!,
               "sets": self.sets!,
               "daily": self.timesPerDay!,
               "week_days": self.makeWeekDays()
       ]
    
    }
    
    func makeWeekDays() -> [[String : String]] {
    
        var sunday: [String : String]
        if ((self.daysPerWeek.contains("Sunday")) == true) {
            sunday = ["day":"0", "on":"1"];
        } else {
            sunday = ["day":"0", "on":"0"];
        }
        
        var monday: [String : String]
        if ((self.daysPerWeek.contains("Monday")) == true) {
            monday = ["day":"1", "on":"1"];
        } else {
            monday = ["day":"1", "on":"0"];
        }
        
        var tuesday: [String : String]
        if ((self.daysPerWeek.contains("Tuesday")) == true) {
            tuesday = ["day":"2", "on":"1"];
        } else {
            tuesday = ["day":"2", "on":"0"];
        }
        
        var wednesday: [String : String]
        if ((self.daysPerWeek.contains("Wednesday")) == true) {
            wednesday = ["day":"3", "on":"1"];
        } else {
            wednesday = ["day":"3", "on":"0"];
        }
        
        var thursday: [String : String]
        if ((self.daysPerWeek.contains("Thursday")) == true) {
            thursday = ["day":"4", "on":"1"];
        } else {
            thursday = ["day":"4", "on":"0"];
        }
        
        var friday: [String : String]
        if ((self.daysPerWeek.contains("Friday")) == true) {
            friday = ["day":"5", "on":"1"];
        } else {
            friday = ["day":"5", "on":"0"];
        }
        
        var saturday: [String : String]
        if ((self.daysPerWeek.contains("Saturday")) == true) {
            saturday = ["day":"6", "on":"1"];
        } else {
            saturday = ["day":"6", "on":"0"];
        }
        
        return [sunday, monday, tuesday, wednesday, thursday, friday, saturday]
    }
    
}