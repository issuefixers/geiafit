//
//  StartViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 11/24/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class StartViewController: GeiaViewController, UITextFieldDelegate {
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var password2: UITextField!

    var user: User?

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    private lazy var onbardingViewModel: OnboardingViewModel = {
        return OnboardingViewModel()
    }()



    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.firstName.delegate = self
        self.lastName.delegate  = self
        self.email.delegate     = self
        self.password.delegate  = self
        self.password2.delegate = self

        self.firstName.tintColor = .geiaDarkBlueColor()
        self.lastName.tintColor  = .geiaDarkBlueColor()
        self.email.tintColor     = .geiaDarkBlueColor()
        self.password.tintColor  = .geiaDarkBlueColor()
        self.password2.tintColor = .geiaDarkBlueColor()
        
        let placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        self.firstName.attributedPlaceholder = NSAttributedString(string: "* FIRST NAME", attributes: [NSForegroundColorAttributeName: placeholderColor])
        self.lastName.attributedPlaceholder  = NSAttributedString(string: "* LAST NAME", attributes: [NSForegroundColorAttributeName: placeholderColor])
        self.email.attributedPlaceholder     = NSAttributedString(string: "* EMAIL (This will be your username)", attributes: [NSForegroundColorAttributeName: placeholderColor])
        self.password.attributedPlaceholder  = NSAttributedString(string: "* PASSWORD", attributes: [NSForegroundColorAttributeName: placeholderColor])
        self.password2.attributedPlaceholder = NSAttributedString(string: "* REPEAT PASSWORD", attributes: [NSForegroundColorAttributeName: placeholderColor])

        //Prefill fields
        self.email.text = self.user?.email
        
        //Hide NavigationBar
//        self.navigationController?.navigationBarHidden = true;
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.getDataFromServer()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
      

        self.view.endEditing(true)
    }

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.view.layoutIfNeeded()
    }

    // MARK: - Data
    private func getDataFromServer() {
        self.loaderView.show()
        self.onbardingViewModel.getProfileForUser(self.user!) { (result) -> Void in

            let success = result["success"] as! Bool
            self.loaderView.hide(animated: false)
                if success {
                  self.appDelegate.saveContext()
                  self.firstName.text = self.user!.firstName
                  self.lastName.text  = self.user!.lastName
                  self.user!.downloadPicture({ () -> Void in
                   self.appDelegate.saveContext()
            })
                } else {
                    let alert: UIAlertController = UIAlertController(title: "Connection Error", message: "Couldn't connect to the server, please check your connection and try again", preferredStyle: .Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
        }
    }

    // MARK: - Actions
    @IBAction func showNext(sender: AnyObject) {
        //make sure both passwords match
        if self.password.text != self.password2.text {
            let alert: UIAlertController = UIAlertController(title: "Passwords doesn't match", message: "Please make sure that your password is typed exactly the same in both fields and try again", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }

        //make sure all fields are filled
        if self.firstName.text?.characters.count == 0 ||
            self.lastName.text?.characters.count == 0 ||
            self.email.text?.characters.count == 0 ||
            self.password.text?.characters.count == 0 {
                let alert: UIAlertController = UIAlertController(title: "Fields missing", message: "Please make sure that all the fields are filled and try again", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                self.presentViewController(alert, animated: true, completion: nil)
                return
        }

        self.performSegueWithIdentifier("showNext", sender: nil)
    }

    func hideKeyboard() {
        self.view.endEditing(true)
    }

  

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.firstName {
            self.lastName.becomeFirstResponder()
        } else if textField == self.lastName {
            self.email.becomeFirstResponder()
        } else if textField == self.email {
            self.password.becomeFirstResponder()
        } else if textField == self.password {
            self.password2.becomeFirstResponder()
        } else {
            self.hideKeyboard()
            self.showNext(textField)
        }

        return true
    }

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        OnboardingViewController.sharedInstance.firstName = self.firstName.text
        OnboardingViewController.sharedInstance.lastName  = self.lastName.text
        OnboardingViewController.sharedInstance.email     = self.email.text
        OnboardingViewController.sharedInstance.password  = self.password.text
    }

}
