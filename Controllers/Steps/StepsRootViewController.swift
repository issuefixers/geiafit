//
//  StepsRootViewController.swift
//  Geia
//
//  Created by haley on 6/3/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Foundation
class StepsRootViewController: UIViewController,ECSegmentedControlDelegate {
	
	@IBOutlet var contentView : UIView!
	@IBOutlet var sideSegmentedControl : ECSegmentedControl!
	var  curSegCtrlIndex : UInt!
	var  currentViewController : UIViewController!
	var  currentClassName : NSString!
	private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
		return GoalTrackingViewModel()
	}()
	
	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	class func instanceFromStoryBord() -> StepsRootViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Steps", bundle: NSBundle.mainBundle())
		let stepsRootViewController : StepsRootViewController = storyboard.instantiateViewControllerWithIdentifier("StepsRootViewController") as!StepsRootViewController
		return stepsRootViewController
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupSgeBar()
		self.curSegCtrlIndex = 0
		
		
		let vc = self.viewControllerForSegmentIndex(self.curSegCtrlIndex)
		
		vc.view.frame = self.contentView.bounds;
		self.addChildViewController(vc)
		self.contentView.addSubview(vc.view)
		self.currentViewController = vc;
		
		
	}
	
	
//	func prepareDataSource (){
//	
//		
//		var date = NSDate()
//		for i in 0..<6 {
//			
//			for _ in 0..<i {
//				date = Common.getPreviousMonth(date)
//				
//				date = Common.getFirstAndLastDateOfMonth(date).lastDateOfMonth
//			}
//			self.goalTrackingViewModel.updateStepsFromHealthKitForDate(date,dateRange: .Month) { (success) -> Void in
//				if success {
//					
//					dispatch_async(dispatch_get_main_queue(), { () -> Void in
//						//save changes
//						self.appDelegate.saveContext()
//					})
//				}
//
//		
//		}
//	}
//	}
	
	
	func setupSgeBar() {
		let segment1 : ECSegmentedControlSegment = ECSegmentedControlSegment.init()
		segment1.backgroundImage = UIImage.init(named: "DayNormal")
		segment1.highlightedImage = UIImage.init(named: "Day")
		segment1.size = UInt(UIScreen.mainScreen().bounds.size.width-45)/3
		
		let segment2 : ECSegmentedControlSegment = ECSegmentedControlSegment.init()
		segment2.backgroundImage = UIImage.init(named: "WeekNormal")
		segment2.highlightedImage = UIImage.init(named: "Week")
		segment2.size = UInt(UIScreen.mainScreen().bounds.size.width-45)/3
		
		let segment3 : ECSegmentedControlSegment = ECSegmentedControlSegment.init()
		segment3.backgroundImage = UIImage.init(named: "MonthNormal")
		segment3.highlightedImage = UIImage.init(named: "Month")
		segment3.size = UInt(UIScreen.mainScreen().bounds.size.width-45)/3
		self.sideSegmentedControl.setupWithSegmentArray([segment1,segment2,segment3],selectedSegment:0,controlSize:31,dividerImage:nil,verticalOrientation:false)
		self.sideSegmentedControl.delegate = self
		
	}
	// MARK:- Segmented Value Changed
	func selectedSegmentChanged(sender: AnyObject!, withIndex index: UInt) {
		
		self.curSegCtrlIndex = index
		let vc : UIViewController =  self.viewControllerForSegmentIndex(index)
		self.addChildViewController(vc)
		self.transitionFromViewController(self.currentViewController, toViewController: vc, duration: 0.5, options: UIViewAnimationOptions.CurveLinear, animations: {
			
			self.currentViewController.view.removeFromSuperview()
			self.contentView.subviews.forEach { $0.removeFromSuperview() }
			self.contentView.addSubview(vc.view)
			
		}) { (Bool) in
			vc.didMoveToParentViewController(self)
			
			self.currentViewController.removeFromParentViewController()
			self.currentViewController = vc;
			self.refrshData()
		}
	}
	
	func viewControllerForSegmentIndex(index:UInt) -> UIViewController {
		var  vc : UIViewController = UIViewController.init()
		if index == 0 {
			let dayController : DayStepsViewController = DayStepsViewController.instanceFromStoryBord()
			vc = dayController
			self.currentClassName = "Daily Steps"
		} else if (index == 1) {
			
			let weekController : WeekStepsViewController = WeekStepsViewController.instanceFromStoryBord()
			vc = weekController
			self.currentClassName = "Week Steps"
		} else if (index == 2) {
			let monthController : MonthStepsViewController = MonthStepsViewController.instanceFromStoryBord()
			vc = monthController
			self.currentClassName = "Month Steps"
		}
		vc.view.frame = self.contentView.bounds;
		
		
		let tracker = GAI.sharedInstance().defaultTracker
		tracker.set(kGAIScreenName, value:String(self.currentClassName))
		
		let builder = GAIDictionaryBuilder.createScreenView()
		tracker.send(builder.build() as [NSObject : AnyObject])
		
		
		
		return vc
	}
	func refrshData() {
		if self.currentViewController.isKindOfClass(DayStepsViewController) {
			(self.currentViewController as! DayStepsViewController).refreshUI()
		}
	}
}