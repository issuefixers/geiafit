//
//  MonthStepsViewController.swift
//  Geia
//
//  Created by haley on 6/6/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts
class MonthStepsViewController: UIViewController {
	
	private var stepsDatasource:[(sum:Double,date:NSDate)] = []
	private var activeCalories:[(sum:Double,date:NSDate)] = []
	private var restingCalories:[(sum:Double,date:NSDate)] = []
	private var milesDatasource:[(sum:Double,date:NSDate)] = []
	
	var customData: BarChartData!
	
	var xVals : [String] = [String]()
	var yVals : [ChartDataEntry] = [ChartDataEntry]()
	
	var caloriesXVals : [String] = [String]()
	var caloriesYVals : [ChartDataEntry] = [ChartDataEntry]()
	
	var milesXVals : [String] = [String]()
	var milesYVals : [ChartDataEntry] = [ChartDataEntry]()
	
	var maxYValue :Double!
	
	var currentDate : NSDate!
	
	var totalCalories :Double!
	var totalSteps :Double!
	var totalMiles :Double!
	
	var chartView : BarChartBaseView!
	var milesChart: BarChartBaseView!
	var caloriesChart: BarChartBaseView!
	
	@IBOutlet var averageSteps : UILabel!
	@IBOutlet var averageCalories : UILabel!
	@IBOutlet var averageMiles : UILabel!
	
    @IBOutlet weak var vernierLight: UIImageView!
    @IBOutlet weak var vernierModerate: UIImageView!
    @IBOutlet weak var vernierVigorous: UIImageView!
    @IBOutlet weak var vernierLeftConstraint: NSLayoutConstraint!
	@IBOutlet weak var milesVernierLeftConstraint: NSLayoutConstraint!
	@IBOutlet weak var calVernierLeftConstraint: NSLayoutConstraint!
	
	@IBOutlet var dayLabel : UILabel!
	@IBOutlet weak var scrollView: UIScrollView!
	
	
	 // MARK: - Local variables
	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
		return GoalTrackingViewModel()
	}()
	
	private lazy var dateFormatter:NSDateFormatter = {
		let df       = NSDateFormatter()
		df.locale    = NSLocale.currentLocale()
		df.timeStyle = .NoStyle
		df.dateStyle = .MediumStyle
		df.dateFormat = "MMM,yyyy"
		return df
	}()
	
	private lazy var loaderView: RPLoadingAnimationView = {
		return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
	}()
	
	// MARK: - View Lifecycle
	
	class func instanceFromStoryBord() -> MonthStepsViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Steps", bundle: NSBundle.mainBundle())
		let monthStepsViewController : MonthStepsViewController = storyboard.instantiateViewControllerWithIdentifier("MonthStepsViewController") as!MonthStepsViewController
		return monthStepsViewController
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.showLoadingView()
		self.totalMiles = 0
		self.totalCalories = 0
		self.totalSteps = 0
		self.maxYValue = 0
		self.currentDate = Common.getFirstAndLastDateOfMonth(NSDate()).lastDateOfMonth
		
        self.hideVernier(false)
		
		let refreshControl = UIRefreshControl(frame: self.scrollView.frame)
		refreshControl.addTarget(self, action: #selector(MonthStepsViewController.refreshData(_:)), forControlEvents: .ValueChanged)
		self.scrollView.addSubview(refreshControl)
		
		self.customData = BarChartBaseView.setupDataByDate(self.currentDate,dateRange:.Month)
		
		self.setupGraph()
		self.setupCaloriesGraph()
		self.setupMilesGraph()
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showStr(_:)), name: "RectStartXandWidth", object: nil)
		self.updateUserData()
	}
	
	
	func showStr(notification: NSNotification) {
		
		let strs = notification.object as! [CGFloat]
		
		dispatch_async(dispatch_get_main_queue(), { () -> Void in
			
			let components = NSCalendar.currentCalendar().components([.Day , .Month , .Year], fromDate: NSDate())
			let startx = strs[0] as CGFloat
			let barWidth = strs[1] as CGFloat
			self.vernierLeftConstraint.constant = startx + barWidth*(CGFloat(components.day - 1)/0.85+0.5) - 3.5
			
			self.milesVernierLeftConstraint.constant = startx + barWidth*(CGFloat(components.day - 1)/0.85+0.5) - 3.5
			
			self.calVernierLeftConstraint.constant = startx + barWidth*(CGFloat(components.day - 1)/0.85+0.5) - 3.5
			self.hideVernier(false)
			
		})
		
		NSNotificationCenter.defaultCenter().removeObserver(self, name: "RectStartXandWidth", object: nil)
	}
	// MARK: - Actions
	
	@IBAction func previousButtonClicked() {
		
		self.showLoadingView()
		let date = Common.getPreviousMonth(self.currentDate)
		
		self.currentDate = date
		
		self.updateUserData()
		
	}
	
	@IBAction func nextButtonClicked() {
		
		self.showLoadingView()
		let date =  Common.getNextMonth(self.currentDate)
		
		self.currentDate = date
		
		self.updateUserData()
	}
	
	// MARK: - Clear
	func clearChartValue() {
		self.chartView.data?.clearValues()
		self.caloriesChart.data?.clearValues()
		self.milesChart.data?.clearValues()
	}
	
	func clear() {
		
		self.clearChartValue()
		
		self.chartView.leftAxis.removeAllLimitLines()
		
		self.totalMiles = 0
		self.totalCalories = 0
		self.totalSteps = 0
		self.maxYValue = 0
		
		self.activeCalories.removeAll()
		self.restingCalories.removeAll()
		self.milesDatasource.removeAll()
		self.stepsDatasource.removeAll()
		
		self.xVals.removeAll()
		self.yVals.removeAll()
		
		self.caloriesXVals.removeAll()
		self.caloriesYVals .removeAll()
		
		self.milesXVals.removeAll()
		self.milesYVals.removeAll()
		
		self.averageSteps.text = "Daily Average: N/A"
		self.averageMiles.text = "Daily Average: N/A"
		self.averageCalories.text = "Daily Average: N/A"
	}
	
    func compareDate() {
    
        if (Common.isEqualThisMonth(self.currentDate)) {
            self.hideVernier(false)
        } else {
            self.hideVernier(true)
        }
    }
	
	// MARK: - Animation
    func hideVernier(isHide: Bool) {
        
        self.vernierLight.hidden = isHide
        self.vernierModerate.hidden = isHide
        self.vernierVigorous.hidden = isHide
        
    }
    
    func showLoadingView() {
        self.loaderView.show()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func hideLoadingView() {
        self.loaderView.hide(animated: true)
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
	// MARK:- Init UI
	func setupGraph() {
		self.chartView.setupBarChartView()
		self.chartView.drawBarShadowEnabled = false;
		let xAxis = self.chartView.xAxis
		
		xAxis.setLabelsToSkip(13)

		let leftAxis : ChartYAxis = self.chartView.leftAxis;
		leftAxis.gridColor = UIColor.clearColor()
		leftAxis.labelTextColor = UIColor.clearColor()
		self.chartView.data = self.customData
	}
	
	func setupCaloriesGraph() {
		self.caloriesChart.setupBarChartView()
		self.caloriesChart.drawBarShadowEnabled = true;
		let xAxis = self.caloriesChart.xAxis
		xAxis.setLabelsToSkip(13)
		let leftAxis : ChartYAxis = self.caloriesChart.leftAxis;
		leftAxis.gridColor = UIColor.clearColor()
		leftAxis.labelCount = 1;
		leftAxis.showOnlyMaxEnabled = true
		self.caloriesChart.data = self.customData
	}
	
	func setupMilesGraph() {
		self.milesChart.setupBarChartView()
		self.milesChart.drawBarShadowEnabled = true;
		let xAxis = self.milesChart.xAxis
		xAxis.setLabelsToSkip(13)
		let leftAxis : ChartYAxis = self.milesChart.leftAxis;
		leftAxis.gridColor = UIColor.clearColor()
		leftAxis.labelCount = 1;
		leftAxis.showOnlyMaxEnabled = true

		self.milesChart.data = self.customData
	}
	
	
	// MARK:- Get data source
	
	
	func updateUserData() {
	
		let formattedDate = self.dateFormatter.stringFromDate(self.currentDate)
		
		self.dayLabel.text = formattedDate
		
		self.compareDate()
		
		if Common.isEarlyThisMonth(self.currentDate) || Common.isEqualThisMonth(self.currentDate){
			self.updateMonthSteps()
		}
		else {
			self.customData = BarChartBaseView.setupDataByDate(self.currentDate,dateRange:.Month)
			
			self.clear()
			self.setupGraph()
			self.setupCaloriesGraph()
			self.setupMilesGraph()
			self.hideLoadingView()
		}
	}
	
	func updateMonthSteps() {
		self.clear()
		self.loadData()
	}
	
	func loadData() {
		let healthManager = HealthManager()
		
        healthManager.getUserStepsCountForDate(self.currentDate, dateRange: .Month) { (data, error) in
            self.stepsDatasource = data
            
            
            healthManager.getCaloriesForDate(self.currentDate,dateRange:.Month, completion: { (activeCalories, restingCalories, error) -> Void in
                if error != nil {
                    print("Error reading HealthKit Store: \(error.localizedDescription), \(error.code)")
                    return
                }
                
                self.activeCalories = activeCalories
                self.restingCalories = restingCalories
                
                
                
                //---------------------------------
                
                healthManager.getUserMilesForDate(self.currentDate,dateRange: .Month, completion:{(data, error) ->  Void in
                    
                    if error != nil {
                        print("Error reading HealthKit Store: \(error.localizedDescription), \(error.code)")
                        return
                    }
                    
                    if data.count != 0 {
                        for dataIndex in 0...(data.count-1) {
                            let datapoint   = data[dataIndex]
                            self.milesDatasource.append(datapoint)
                        }
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        self.setupStepsDataSource()
                        
                        //---------
                        
                        self.setupCaloriesDataSource()
                        
                        //---------
                        
                        self.setupMilesDataSource()
                    })
                    
                    
                    self.hideLoadingView()
                })

                
            })
            
            
        }
		
	}
	
	func setupStepsDataSource() {
		
        if self.stepsDatasource.count == 0 {
           return
        }
	
		var stepGoal : Double = 0

		
		if let goals = ActivityGoals.getLatestGoalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext) {
			
			stepGoal = Double(goals.totalSteps)
		}
		
		
		for dataIndex in 0...self.stepsDatasource.count-1 {
			let datapoint   = self.stepsDatasource[dataIndex]
			
			let df        = NSDateFormatter()
			df.dateFormat = "d"
			let xValue    = datapoint.date
			let category  = df.stringFromDate(xValue)
			
            var value1: Double = 0.0
			var value2: Double = 0.0
			var value3: Double = 0.0
			
			let yValue = datapoint.sum
			let limitValue = stepGoal
			if (yValue < limitValue || yValue == limitValue) {
				value1 = yValue
				value2 = limitValue - yValue
				value3 = 0.0
			} else {
				value1 = limitValue
				value2 = 0.0
				value3 = yValue - limitValue
			}
			
			self.yVals.append(BarChartDataEntry(values:[value1,value2,value3], xIndex:dataIndex))
			
			if yValue > self.maxYValue {
				self.maxYValue = yValue
			}
			
			self.xVals.append(("\(category)"))
			self.totalSteps = self.totalSteps + yValue
		}
		
		
		if stepGoal > self.maxYValue {
			self.maxYValue = stepGoal
		}
		
        if (stepGoal > 0) {
            let ll1 : ChartLimitLine = ChartLimitLine.init(limit: stepGoal, label: "Goal");
            ll1.lineWidth = 1.0;
            if self.maxYValue > stepGoal {
                ll1.lineColor = UIColor(red:245.0/255, green: 184.0/255, blue: 39.0/255, alpha: 1.0)
            }
            else {
                ll1.lineColor = UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)
            }
		
            ll1.label2 = "\(stepGoal)"
            ll1.labelPosition = .LeftTop;
            ll1.valueFont = UIFont.systemFontOfSize(10);
            ll1.valueTextColor = UIColor.init(red:152.0/255, green: 152.0/255, blue: 152.0/255, alpha: 1.0)
            ll1.valueTextColor2 = UIColor.whiteColor()
            self.chartView.leftAxis.removeAllLimitLines()
			
            self.chartView.leftAxis.addLimitLine(ll1)
		}
		
		self.chartView.leftAxis.axisMaxValue = self.maxYValue+1000
		
		var set1 :BarChartDataSet
		if self.chartView.data?.dataSetCount > 0 {
			set1 = self.chartView.data?.dataSets[0] as! BarChartDataSet
			set1.yVals = yVals
			self.chartView.data!.xValsObjc = xVals
			self.chartView.notifyDataSetChanged()
		}
		else {
			
			set1 = BarChartDataSet(yVals: yVals, label: "DataSet")
			set1.drawValuesEnabled = false
            if (stepGoal > 0) {
                set1.colors = [UIColor(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0), UIColor(red:74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 0.5), UIColor(red:245.0/255, green: 184.0/255, blue: 39.0/255, alpha: 1.0)]
            } else {
                set1.colors = [UIColor(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
            }
			var dataSets = [IChartDataSet]()
			dataSets.append(set1)
			let data = BarChartData(xVals: xVals, dataSets: dataSets)
			self.chartView.data = data;
		}
		let nf                   = NSNumberFormatter()
		nf.maximumFractionDigits = 0
		nf.usesGroupingSeparator = true
		let days = Common.getDays(self.currentDate)
		print(days)
		if(self.totalSteps == 0) {
			self.averageSteps.text = "Daily Average: N/A"
		} else {
			let average = self.totalSteps/Double(Common.getDays(self.currentDate))
			
			self.averageSteps.text = "Daily Average: \((nf.stringFromNumber(average))!)"
		}
		
	}
	
	func setupMilesDataSource() {
		
        if self.milesDatasource.count == 0 {
           return
        }
        
		for dataIndex in 0...self.milesDatasource.count-1 {
			let datapoint   = self.milesDatasource[dataIndex]
			
			let df        = NSDateFormatter()
			df.dateFormat = "d"
			let xValue    = datapoint.date
			let category  = df.stringFromDate(xValue)
			
			
			var yValue: Double?
			yValue = datapoint.sum
			self.milesYVals.append(BarChartDataEntry(value:yValue!, xIndex:dataIndex))
			self.milesXVals.append(("\(category)"))
			self.totalMiles = self.totalMiles + yValue!
		}
		
		var set1 :BarChartDataSet
		if self.milesChart.data?.dataSetCount > 0 {
			set1 = self.milesChart.data?.dataSets[0] as! BarChartDataSet
			set1.yVals = self.milesYVals
			self.chartView.data!.xValsObjc = self.milesXVals
			self.chartView.notifyDataSetChanged()
		}
		else {
			
			set1 = BarChartDataSet(yVals: self.milesYVals, label: "DataSet")
			set1.drawValuesEnabled = false;
			set1.barShadowColor = NSUIColor(red:74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 0.5)
			set1.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			var dataSets = [IChartDataSet]()
			dataSets.append(set1)
			let data = BarChartData(xVals: self.milesXVals, dataSets: dataSets)
			self.milesChart.data = data;
		}
		let nf                   = NSNumberFormatter()
		nf.maximumFractionDigits = 1
		nf.usesGroupingSeparator = true
		
		if(self.totalMiles == 0) {
			self.averageMiles.text = "Daily Average: N/A"
		} else {
			let average = self.totalMiles/Double(Common.getDays(self.currentDate))
			self.averageMiles.text = "Daily Average: \((nf.stringFromNumber(average))!)"
			
		}
		
	}
	
	func setupCaloriesDataSource() {
		
        if self.activeCalories.count == 0 {
            return
        }
        
		for dataIndex in 0...self.activeCalories.count-1 {
			let datapoint   = self.activeCalories[dataIndex]
			
			let df        = NSDateFormatter()
			df.dateFormat = "d"
			let xValue    = datapoint.date
			let category  = df.stringFromDate(xValue)
			var yValue: Double?
			yValue = datapoint.sum
			
            if self.restingCalories.count != 0 {
                
                for index in 0...self.restingCalories.count-1 {
				
                    let restingDatapoint   = self.restingCalories[index]
				
                    let restingXValue    = restingDatapoint.date
				
                    let restingCategory  = df.stringFromDate(restingXValue)
                    if  category == restingCategory{
                        yValue = yValue! + restingDatapoint.sum
                        break
                    }
                }
             }
			
			self.caloriesYVals.append(BarChartDataEntry(value:yValue!, xIndex:dataIndex))
			self.caloriesXVals.append(("\(category)"))
			self.totalCalories = self.totalCalories + yValue!
		}
		
		var set1 :BarChartDataSet
		if self.caloriesChart.data?.dataSetCount > 0 {
			set1 = self.caloriesChart.data?.dataSets[0] as! BarChartDataSet
			set1.yVals = self.caloriesYVals
			self.chartView.data!.xValsObjc = self.caloriesXVals
			self.chartView.notifyDataSetChanged()
		}
		else {
			
			set1 = BarChartDataSet(yVals: self.caloriesYVals, label: "DataSet")
			set1.drawValuesEnabled = false;
			set1.barShadowColor = NSUIColor(red:74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 0.5)
			set1.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			
			
			var dataSets = [IChartDataSet]()
			dataSets.append(set1)
			let data = BarChartData(xVals: self.caloriesXVals, dataSets: dataSets)
			self.caloriesChart.data = data;
		}
		let nf                   = NSNumberFormatter()
		nf.maximumFractionDigits = 1
		nf.usesGroupingSeparator = true
		
		if(self.totalCalories == 0) {
			self.averageCalories.text = "Daily Average: N/A"
		} else {
			let average = self.totalCalories/Double(Common.getDays(self.currentDate))
			self.averageCalories.text = "Daily Average: \((nf.stringFromNumber(average))!)"
			
		}
		
	}
	
	// MARK:- Refresh control
	func refreshData(refreshControll: UIRefreshControl) {
		refreshControll.endRefreshing()
		self.updateUserData()
	}
}
