//
//  WeekStepsViewController.swift
//  Geia
//
//  Created by haley on 6/6/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts

class WeekStepsViewController: UIViewController {
	
    @IBOutlet weak var vernierLight: UIImageView!
    @IBOutlet weak var vernierModerate: UIImageView!
    @IBOutlet weak var vernierVigorous: UIImageView!
	
    @IBOutlet weak var stepsVernierLeftConstraint: NSLayoutConstraint!
	@IBOutlet weak var mailsVernierLeftConstraint: NSLayoutConstraint!
	@IBOutlet weak var calVernierLeftConstraint: NSLayoutConstraint!
	
	private var stepsDatasource:[(sum:Double,date:NSDate)] = []
	private var datasource: [[String: AnyObject]] = []
	private var activeCalories:[(sum:Double,date:NSDate)] = []
	private var restingCalories:[(sum:Double,date:NSDate)] = []
	private var milesDatasource:[(sum:Double,date:NSDate)] = []
	private var goals: [ActivityGoals] = []
	
	var xVals : [String] = [String]()
	var yVals : [ChartDataEntry] = [ChartDataEntry]()
	
	var maxYValue :Double!
	var caloriesXVals : [String] = [String]()
	var caloriesYVals : [ChartDataEntry] = [ChartDataEntry]()
	
	var currentDate : NSDate!
	
	var milesXVals : [String] = [String]()
	var milesYVals : [ChartDataEntry] = [ChartDataEntry]()
	
	var totalCalories :Double!
	var totalSteps :Double!
	var totalMiles :Double!
	
	var customData: BarChartData!
	
	var chartView : BarChartBaseView!
	var milesChart: BarChartBaseView!
	var caloriesChart: BarChartBaseView!
	
	@IBOutlet var averageSteps : UILabel!
	@IBOutlet var averageCalories : UILabel!
	@IBOutlet var averageMiles : UILabel!
	
	@IBOutlet weak var scrollView: UIScrollView!
	
	// MARK: - Local variables
	
	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	private lazy var loaderView: RPLoadingAnimationView = {
		return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
	}()
	
	
	private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
		return GoalTrackingViewModel()
	}()
	
	// MARK: - View Lifecycle
	
	
	class func instanceFromStoryBord() -> WeekStepsViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Steps", bundle: NSBundle.mainBundle())
		let weekStepsViewController : WeekStepsViewController = storyboard.instantiateViewControllerWithIdentifier("WeekStepsViewController") as!WeekStepsViewController
		return weekStepsViewController
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.currentDate = NSDate()
		self.maxYValue = 0
		self.totalMiles = 0
		self.totalCalories = 0
		self.totalSteps = 0
		
		self.hideVernier(false)
		
		self.customData = BarChartBaseView.setupDataByDate(NSDate(),dateRange:.Week)
		
		
		let refreshControl = UIRefreshControl(frame: self.scrollView.frame)
		refreshControl.addTarget(self, action: #selector(WeekStepsViewController.refreshData(_:)), forControlEvents: .ValueChanged)
		self.scrollView.addSubview(refreshControl)
		
		self.setupGraph()
		self.setupCaloriesGraph()
		self.setupMilesGraph()
		
		self.updateWeeklySteps()
		
	}
    
    func hideVernier(isHide: Bool) {
        
        self.vernierLight.hidden = isHide
        self.vernierModerate.hidden = isHide
        self.vernierVigorous.hidden = isHide
        
    }
	
	// MARK:- Init UI
	
	func setupGraph() {
		self.chartView.setupBarChartView()
		
		let leftAxis : ChartYAxis = self.chartView.leftAxis;
		leftAxis.gridColor = UIColor.clearColor()
		leftAxis.labelCount = 2;
		leftAxis.labelTextColor = UIColor.clearColor()
		self.chartView.data = self.customData
	}

	func setupCaloriesGraph() {
		self.caloriesChart.setupBarChartView()
		let leftAxis : ChartYAxis = self.caloriesChart.leftAxis;
		leftAxis.showOnlyMinMaxEnabled = true
		leftAxis.labelCount = 1;
		leftAxis.gridColor = UIColor.clearColor()
		leftAxis.showOnlyMaxEnabled = true
		self.caloriesChart.data = self.customData
	}
	
	func setupMilesGraph() {
		self.milesChart.setupBarChartView()
		
		let leftAxis : ChartYAxis = self.milesChart.leftAxis;
		leftAxis.showOnlyMinMaxEnabled = true
		leftAxis.gridColor = UIColor.clearColor()
		leftAxis.labelCount = 1;
		leftAxis.showOnlyMaxEnabled = true
		self.milesChart.data = self.customData
	}
	
	// MARK: - Clear data source
	
	func clearChartValue() {
		self.maxYValue = 0
		self.totalMiles = 0
		self.totalCalories = 0
		self.totalSteps = 0
		
		self.stepsDatasource.removeAll()
		self.milesDatasource.removeAll()
		self.restingCalories.removeAll()
		self.activeCalories.removeAll()
		
		self.caloriesXVals.removeAll()
		self.caloriesYVals.removeAll()
		
		self.milesXVals.removeAll()
		self.milesYVals.removeAll()
		
		self.xVals.removeAll()
		self.yVals.removeAll()

		
		self.chartView.positions.removeAll()
		self.chartView.data?.clearValues()
		self.caloriesChart.data?.clearValues()
		self.caloriesChart.positions.removeAll()
		self.milesChart.data?.clearValues()
		self.milesChart.positions.removeAll()
		
		
	}
	
	// MARK:- Get data source
	
	func updateWeeklySteps() {
		self.loaderView.show()
		self.clearChartValue()
		self.loadData()
	}
	
	func loadData() {
		
		let healthManager = HealthManager()
		
        healthManager.getUserStepsCountForDate(self.currentDate, dateRange: .Week) { (data, error) in
            
            self.stepsDatasource = data
            
            
            
            healthManager.getCaloriesForDateRange(.Week, completion: { (activeCalories, restingCalories, error) -> Void in
                if error != nil {
                    print("Error reading HealthKit Store: \(error.localizedDescription), \(error.code)")
                    return
                }
                
                self.activeCalories = activeCalories
                self.restingCalories = restingCalories
                
                
                
                
                //---------------------
                
                healthManager.getMilesForDate(.Week, completion:{(data, error) ->  Void in
                    
                    if error != nil {
                        print("Error reading HealthKit Store: \(error.localizedDescription), \(error.code)")
                        return
                    }
                    
                    if data.count != 0 {
                        for dataIndex in 0...(data.count-1) {
                            let datapoint   = data[dataIndex]
                            self.milesDatasource.append(datapoint)
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        self.setupStepsDataSource()
                        
                        //------------
                        
                        self.setupCaloriesDataSource()
                        
                        //------------
                        
                        self.setupMilesDataSource()
                    })
                    
                    self.loaderView.hide(animated: false)
                })
                
            })
            
        }
		
	}
	
	func setupStepsDataSource() {
        
        if self.stepsDatasource.count == 0 {
           return
        }
		
		
		var stepGoal : Double = 0
		
		
		if let goals = ActivityGoals.getLatestGoalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext) {
			
			stepGoal = Double(goals.totalSteps)
		}
		
		for dataIndex in 0...self.stepsDatasource.count-1 {
			let datapoint   = self.stepsDatasource[dataIndex]
			
			let df        = NSDateFormatter()

			if dataIndex == 0 {
				df.dateFormat = "MMM d"
			} else {
				df.dateFormat = "d"
			}
			
			let xValue    = datapoint.date
			
			
			let category  = df.stringFromDate(xValue)
			
			var value1: Double = 0.0
			var value2: Double = 0.0
			var value3: Double = 0.0
			
			let yValue = datapoint.sum
			let limitValue = stepGoal
			if (yValue < limitValue || yValue == limitValue) {
				value1 = yValue
				value2 = limitValue - yValue
				value3 = 0.0
			} else {
				value1 = limitValue
				value2 = 0.0
				value3 = yValue - limitValue
			}
			
			self.yVals.append(BarChartDataEntry(values:[value1,value2,value3], xIndex:dataIndex))
			
			if yValue > self.maxYValue {
				self.maxYValue = yValue + 1000
			}
			
			self.xVals.append(("\(category)"))
			self.totalSteps = self.totalSteps + yValue
		}
		
		
		if stepGoal > self.maxYValue {
			self.maxYValue = stepGoal+1000
		}
		
        if (stepGoal > 0) {
            
            let ll1 : ChartLimitLine = ChartLimitLine.init(limit: stepGoal, label: "Goal");
            ll1.lineWidth = 1.0;
            if self.maxYValue > stepGoal {
                ll1.lineColor = UIColor(red:245.0/255, green: 184.0/255, blue: 39.0/255, alpha: 1.0)
            }
            else {
                ll1.lineColor = UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)
            }
		
            ll1.valueFont = UIFont.systemFontOfSize(10);
            ll1.label2 = "\(stepGoal)"
            ll1.labelPosition = .LeftTop;
            ll1.valueTextColor = UIColor.init(red:152.0/255, green: 152.0/255, blue: 152.0/255, alpha: 1.0)
            ll1.valueTextColor2 = UIColor.whiteColor()
            self.chartView.leftAxis.axisMaxValue = self.maxYValue
            self.chartView.leftAxis.addLimitLine(ll1)
            
        }
		
		var set1 :BarChartDataSet
		if self.chartView.data?.dataSetCount > 0 {
			set1 = self.chartView.data?.dataSets[0] as! BarChartDataSet
			set1.yVals = self.yVals
			self.chartView.data!.xValsObjc = self.xVals
			self.chartView.notifyDataSetChanged()
		}
		else {
			
			set1 = BarChartDataSet(yVals: self.yVals, label: "DataSet")
			set1.drawValuesEnabled = false;
            if (stepGoal > 0) {
                set1.colors = [UIColor(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0), UIColor(red:74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0), UIColor(red:245.0/255, green: 184.0/255, blue: 39.0/255, alpha: 1.0)]
            } else {
                set1.colors = [UIColor(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
            }
			var dataSets = [IChartDataSet]()
			dataSets.append(set1)
			
			let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
			self.chartView.data = data;
		}
		let nf = NSNumberFormatter()
		nf.maximumFractionDigits = 0
		nf.usesGroupingSeparator = true

		if (self.totalSteps == 0) {
			self.averageSteps.text = "Daily Average: N/A"
		} else {
			self.averageSteps.text = "Daily Average: \((nf.stringFromNumber(self.totalSteps/7))!)"
		}
		
	}
	
	func setupMilesDataSource() {
        
        if self.milesDatasource.count == 0 {
           return
        }
        
		for dataIndex in 0...self.milesDatasource.count-1 {
			let datapoint   = self.milesDatasource[dataIndex]
			
            let df        = NSDateFormatter()
            let xValue    = datapoint.date
            
            if dataIndex == 0 {
                df.dateFormat = "MMM d"
            } else {
                df.dateFormat = "d"
            }
            
			let category  = df.stringFromDate(xValue)
			
			var yValue: Double?
			yValue = datapoint.sum
			self.milesYVals.append(BarChartDataEntry(value:yValue!, xIndex:dataIndex))
			self.milesXVals.append(("\(category)"))
			self.totalMiles = self.totalMiles + yValue!
		}
		
		var set1 :BarChartDataSet
		if self.milesChart.data?.dataSetCount > 0 {
			set1 = self.milesChart.data?.dataSets[0] as! BarChartDataSet
			set1.yVals = self.milesYVals
			self.milesChart.data!.xValsObjc = self.milesXVals
			self.milesChart.notifyDataSetChanged()
		}
		else {
			
			set1 = BarChartDataSet(yVals: self.milesYVals, label: "DataSet")
			set1.drawValuesEnabled = false;
			
			set1.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			var dataSets = [IChartDataSet]()
			dataSets.append(set1)
			let data = BarChartData(xVals: self.milesXVals, dataSets: dataSets)
			self.milesChart.data = data;
		}
		let nf                   = NSNumberFormatter()
		nf.maximumFractionDigits = 1
		nf.usesGroupingSeparator = true
		if (self.totalMiles == 0) {
			self.averageMiles.text = "Daily Average: N/A"
		} else {
			self.averageMiles.text = "Daily Average: \((nf.stringFromNumber(self.totalMiles/7))!)"
		}
		
	}
	
	func setupCaloriesDataSource() {
        
        if self.activeCalories.count == 0 {
           return
        }
        
		for dataIndex in 0...self.activeCalories.count-1 {
			let datapoint   = self.activeCalories[dataIndex]
			
            let df        = NSDateFormatter()
            let xValue    = datapoint.date
            
            if dataIndex == 0 {
                df.dateFormat = "MMM d"
            } else {
                df.dateFormat = "d"
            }
            
			let category  = df.stringFromDate(xValue)
			var yValue: Double?
			yValue = datapoint.sum
			
            if self.restingCalories.count != 0 {
			  for index in 0...self.restingCalories.count-1 {
			
				let restingDatapoint   = self.restingCalories[index]
				
				let restingXValue    = restingDatapoint.date
				
				let restingCategory  = df.stringFromDate(restingXValue)
				if  category == restingCategory{
					yValue = yValue! + restingDatapoint.sum
					break
				}
			  }
            }
			
			self.caloriesYVals.append(BarChartDataEntry(value:yValue!, xIndex:dataIndex))
			self.caloriesXVals.append(("\(category)"))
			self.totalCalories = self.totalCalories + yValue!
		}
		
		var set1 :BarChartDataSet
		if self.caloriesChart.data?.dataSetCount > 0 {
			set1 = self.caloriesChart.data?.dataSets[0] as! BarChartDataSet
			set1.yVals = self.caloriesYVals
			self.caloriesChart.data!.xValsObjc = self.caloriesXVals
			self.caloriesChart.notifyDataSetChanged()
		}
		else {
			
			set1 = BarChartDataSet(yVals: self.caloriesYVals, label: "DataSet")
			set1.drawValuesEnabled = false;
			
			set1.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			var dataSets = [IChartDataSet]()
			dataSets.append(set1)
			let data = BarChartData(xVals: self.caloriesXVals, dataSets: dataSets)
			self.caloriesChart.data = data;
		}
		let nf                   = NSNumberFormatter()
		nf.maximumFractionDigits = 1
		nf.usesGroupingSeparator = true
		if(self.totalCalories == 0){
			self.averageCalories.text = "Daily Average: N/A"
		} else {
			self.averageCalories.text = "Daily Average: \((nf.stringFromNumber(self.totalCalories/7))!)"
		}
		
	}
	
	// MARK:- Refresh controll
	func refreshData(refreshControll: UIRefreshControl) {
		refreshControll.endRefreshing()
		self.updateWeeklySteps()
	}
}

