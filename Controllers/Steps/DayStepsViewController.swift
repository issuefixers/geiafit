//
//  DayStepsViewController.swift
//  Geia
//
//  Created by haley on 6/6/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts
import Foundation
class DayStepsViewController: UIViewController {
	
	@IBOutlet weak var scrollView: UIScrollView!
	
	@IBOutlet var dayLabel : UILabel!
    var chartView : BarChartBaseView!
	@IBOutlet var mailsLabel : UILabel!
	@IBOutlet var caloriesLabel : UILabel!
	@IBOutlet var nowStepLabel : UILabel!
	@IBOutlet var goalStepLabel : UILabel!
	@IBOutlet var togoStepLabel : UILabel!
	@IBOutlet var togoLabel : UILabel!
	@IBOutlet var symbolLabel : UILabel!
	@IBOutlet var labelWidth : NSLayoutConstraint!
	@IBOutlet var progressView :MKRingProgressView!
	@IBOutlet var nowLabel : UILabel!
	@IBOutlet var goalLabel : UILabel!
	@IBOutlet var stepsLabel :UILabel!
	@IBOutlet var stepsValueLabel : UILabel!
    @IBOutlet weak var lblStepPercentage: UILabel!
    
	var customData: BarChartData!
	var currentDate : NSDate!
	
	var xVals : [String] = [String]()
	var yVals : [ChartDataEntry] = [ChartDataEntry]()
	var stepGoal : Double = 0
	var dailyTotals: Double = 0
	
	private var stepsDatasource:[(sum:Double,date:NSDate)] = []
	
	 // MARK: - Local variables
	
	private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
		return GoalTrackingViewModel()
	}()
	
	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	private lazy var dateFormatter:NSDateFormatter = {
		let df       = NSDateFormatter()
		df.locale    = NSLocale.currentLocale()
		df.timeStyle = .NoStyle
		df.dateStyle = .MediumStyle
		
		return df
	}()

	
	private lazy var loaderView: RPLoadingAnimationView = {
		return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
	}()
	
	// MARK: - View Lifecycle
	
	func checkDate() {
		
		let date = NSUserDefaults.standardUserDefaults().objectForKey(CURRENT_DATE_KEY) as! NSDate?
		if date != nil {
		
			if !self.currentDate.isEqualToDate(date!) {
				self.currentDate = date
			}
			
		} else {
		
			NSUserDefaults.standardUserDefaults().setObject(self.currentDate, forKey: CURRENT_DATE_KEY)
			NSUserDefaults.standardUserDefaults().synchronize()
		}
	}
	
	class func instanceFromStoryBord() -> DayStepsViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Steps", bundle: NSBundle.mainBundle())
		let dayStepsViewController : DayStepsViewController = storyboard.instantiateViewControllerWithIdentifier("DayStepsViewController") as!DayStepsViewController
		return dayStepsViewController
	}
	
	func refreshUI() {
		
		self.showLoadingView()
		self.currentDate = NSDate()
		
		self.checkDate()
		
		self.setupView()
		
		self.stepsLabel.hidden = true
		self.stepsValueLabel.hidden = true
		self.updateUserData()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let refreshControl = UIRefreshControl(frame: self.scrollView.frame)
		refreshControl.addTarget(self, action: #selector(DayStepsViewController.refreshData(_:)), forControlEvents: .ValueChanged)
		self.scrollView.addSubview(refreshControl)
	}
	
	
	func setupView() {
		
        self.progressView.progress = 0;
        
		
		self.customData = BarChartBaseView.setupDataByDate(self.currentDate,dateRange:.Day)
		self.chartView.setupBarChartView()
		self.chartView.leftAxis.showMinDisabled = true
		let xAxis = self.chartView.xAxis
		xAxis.setLabelsToSkip(11)
		self.chartView.data = self.customData
		
		self.caloriesLabel.text = "0"
		self.mailsLabel.text = "0"
		self.nowStepLabel.text = "0"
		self.goalStepLabel.text = "0"
		self.togoStepLabel.text = "0"
		
		self.hideLoadingView()
	}
	
	// MARK: - Action
	@IBAction func previousButtonClicked() {
		self.showLoadingView()
		let date =  self.currentDate.dateByAddingDays(-1)
		self.currentDate = date!
		
		NSUserDefaults.standardUserDefaults().setObject(self.currentDate, forKey: CURRENT_DATE_KEY)
		NSUserDefaults.standardUserDefaults().synchronize()
		
		self.updateUserData()
	}
	
	@IBAction func nextButtonClicked() {
		self.showLoadingView()
		let date =  self.currentDate.dateByAddingDays(1)
		
		self.currentDate = date!
		
		NSUserDefaults.standardUserDefaults().setObject(self.currentDate, forKey: CURRENT_DATE_KEY)
		NSUserDefaults.standardUserDefaults().synchronize()
		
		self.updateUserData()
	}
	
	// MARK: - Animation
	
    func showLoadingView() {
        self.loaderView.show()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func hideLoadingView() {
        self.loaderView.hide(animated: true)
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
	
	// MARK: - Clear
	func clearChartValue() {
		
		self.xVals.removeAll()
		self.yVals.removeAll()
		self.chartView.data?.clearValues()
		self.dailyTotals = 0
	}

	// MARK: - Data
	
	func updateUserData() {
		
		let formattedDate = self.dateFormatter.stringFromDate(self.currentDate)
		
		self.dayLabel.text = formattedDate
		self.clearChartValue()
		
		if Common.isLargerThanToday(self.currentDate) {
			self.setupView()
		} else {
			self.updateSteps()
		}
	}
	
	func updateSteps() {
		self.loadData()
	}
	
	func loadData() {
		
		//Steps
		let healthManager = HealthManager()
		
        
        healthManager.getUserStepsCountForDate(self.currentDate, dateRange: .Day) { (data, error) in
            self.stepsDatasource = data
            
            
            //-----------------------------
            
            //Calories
            healthManager.getCaloriesForDate(self.currentDate,dateRange:.Day, completion: { (activeCalories, restingCalories, error) -> Void in
                if error != nil {
                    print("Error reading HealthKit Store: \(error.localizedDescription), \(error.code)")
                    return
                }
                
                var totalCalories = 0.0
                
                for item in activeCalories {
                    
                    totalCalories = item.sum + totalCalories
                }
                
                for item in restingCalories {
                    totalCalories = item.sum + totalCalories
                }
                

                //-----------------------------
                
                
                //Distance
                healthManager.getDistanceForDate(self.currentDate, completion: { (totalDistance, error) -> Void in
                    if error != nil {
                        print("Error reading HealthKit Store: \(error.localizedDescription)")
                        return
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        // --------------
                        //Steps
                        
                        self.setupStepsDataSource()
                        
                        // --------------
                        //Calories
                        
                        var nf                   = NSNumberFormatter()
                        nf.maximumFractionDigits = 0
                        nf.usesGroupingSeparator = true
                        if totalCalories == 0 {
                            self.caloriesLabel.text = "N/A"
                        } else {
                            self.caloriesLabel.text = nf.stringFromNumber(totalCalories)!
                        }
                        
                        self.appDelegate.saveContext()
                        
                        // --------------
                        //Distance
                        
                        nf                   = NSNumberFormatter()
                        nf.maximumFractionDigits = 2
                        
                        let distance = nf.stringFromNumber(totalDistance)!
                        
                        self.mailsLabel.text = distance
                    })
                    
                    
                    self.hideLoadingView()
                })
                
            })
            
        }
		
	}
	
	
	func setupStepsDataSource() {
		
		if self.stepsDatasource.count == 0 {
			return
		}
		
		if let goals = ActivityGoals.getLatestGoalsForUser(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext) {
			
			self.stepGoal = Double(goals.totalSteps)
		}
		
		for dataIndex in 0...self.stepsDatasource.count-1 {
			let datapoint   = self.stepsDatasource[dataIndex]
			
			let df        = NSDateFormatter()
			df.dateFormat = "h"
			let xValue    = datapoint.date
			let category  = df.stringFromDate(xValue)
		
			let yValue = datapoint.sum
			
			self.yVals.append(BarChartDataEntry(value:yValue, xIndex:dataIndex))
			
			self.xVals.append(("\(category)"))
			self.dailyTotals = dailyTotals + yValue
		}
		
		let datapoint   = self.stepsDatasource[0]
		
		let df        = NSDateFormatter()
		df.dateFormat = "h"
		let xValue    = datapoint.date
		let category  = df.stringFromDate(xValue)
		
		self.yVals.append(BarChartDataEntry(value:0, xIndex:24))
		
		self.xVals.append(("\(category)"))
		
		var set1 :BarChartDataSet
		if self.chartView.data?.dataSetCount > 0 {
			set1 = self.chartView.data?.dataSets[0] as! BarChartDataSet
			set1.yVals = yVals
			self.chartView.data!.xValsObjc = xVals
			self.chartView.notifyDataSetChanged()
		}
		else {
			
			set1 = BarChartDataSet(yVals: yVals, label: "DataSet")
			set1.drawValuesEnabled = false
			set1.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			var dataSets = [IChartDataSet]()
			dataSets.append(set1)
			let data = BarChartData(xVals: xVals, dataSets: dataSets)
			self.chartView.data = data;
		}
		
		self.updateUI()
		
	}
	
	func updateUI() {
		var togo : Double = 0
		if ( self.stepGoal - self.dailyTotals > 0){
			togo =  self.stepGoal - self.dailyTotals
		}
		
		
		var progress = self.dailyTotals / self.stepGoal
		
		//Update UI
		dispatch_async(dispatch_get_main_queue(), { () -> Void in
			let nf                   = NSNumberFormatter()
			nf.maximumFractionDigits = 0
			nf.usesGroupingSeparator = true
			
			if self.stepGoal == 0 {
				
				progress = 1
				
				self.nowStepLabel.hidden = true
				self.goalStepLabel.hidden = true
				self.togoStepLabel.hidden = true
				self.symbolLabel.hidden = true
				self.togoLabel.hidden = true
				self.nowLabel.hidden = true
				self.goalLabel.hidden = true
				self.stepsLabel.hidden = false
				self.stepsValueLabel.hidden = false
				
				self.stepsValueLabel.text = (nf.stringFromNumber(self.dailyTotals))!
				
				
			} else {
				
				self.stepsLabel.hidden = true
				self.stepsValueLabel.hidden = true
				self.nowLabel.hidden = false
				self.goalLabel.hidden = false
				self.nowStepLabel.hidden = false
				self.goalStepLabel.hidden = false
				self.togoStepLabel.hidden = false
				self.symbolLabel.hidden = false
				self.togoLabel.hidden = false
				
				let rect : CGRect = Common.getTextRectSize(nf.stringFromNumber(self.dailyTotals)!, font: UIFont.systemFontOfSize(14), size:CGSizeMake(300, 21))
				self.labelWidth.constant = rect.width + 2
				self.nowStepLabel.text = (nf.stringFromNumber(self.dailyTotals))!
				self.goalStepLabel.text = (nf.stringFromNumber(self.stepGoal))!
				self.togoStepLabel.text = (nf.stringFromNumber(togo))!
			}
			
			let val = progress * 100
			
			if ((progress == 1 || progress > 1) && self.stepGoal != 0) {
				self.progressView.startColor = UIColor(red: 243.0/255.0, green: 172.0/255.0, blue: 03.0/255.0, alpha: 1.0)
			} else {
				self.progressView.endColor = UIColor(red: 0, green: 163.0/255.0, blue: 222.0/255.0, alpha: 1.0)
			}
			
			if self.stepGoal == 0 {
				self.lblStepPercentage.text = "0%"
			} else {
				self.lblStepPercentage.text = "\((nf.stringFromNumber(val))!)%"
			}
			
			self.progressView.progress = progress
		})
	}
	
	// MARK:- Refresh control
	func refreshData(refreshControll: UIRefreshControl) {
		refreshControll.endRefreshing()
		self.updateUserData()
	}
}
