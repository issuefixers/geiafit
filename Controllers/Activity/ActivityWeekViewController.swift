//
//  ActivityWeekViewController.swift
//  Geia
//
//  Created by Cassie on 6/8/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts

class ActivityWeekViewController: UIViewController {

	var customData: BarChartData!
	
    @IBOutlet weak var scrollView: UIScrollView!
    
    weak var lightChartView: BarChartBaseView!
    weak var moderateChartView: BarChartBaseView!
    weak var vigorousChartView: BarChartBaseView!
    
    @IBOutlet weak var lightAverTime: UILabel!
    @IBOutlet weak var moderateAverTime: UILabel!
    @IBOutlet weak var vigorousAverTime: UILabel!
    
    @IBOutlet weak var vernierLight: UIImageView!
    @IBOutlet weak var vernierModerate: UIImageView!
    @IBOutlet weak var vernierVigorous: UIImageView!
    
    @IBOutlet weak var vernierLeftConstraint: NSLayoutConstraint!
    
    var xVals: [String] = [String]()
    var yVals: [ChartDataEntry] = [ChartDataEntry]()
    let orangeColor = UIColor(red:245.0/255, green: 184.0/255, blue: 39.0/255, alpha: 1.0)
    let bgBarColor = UIColor(red:74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
    let bgBlueColor = UIColor(red:0, green: 159.0/255, blue: 219.0/255, alpha: 1.0)
    
    class func instanceFromStoryBord() -> ActivityWeekViewController {
        let  storyboard = UIStoryboard(name:"Activity", bundle: NSBundle.mainBundle())
        let weekActivityViewController = storyboard.instantiateViewControllerWithIdentifier("ActivityWeekViewController") as!ActivityWeekViewController
        return weekActivityViewController
    }
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
        self.hideVernier(true)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showStr(_:)), name: "RectStartXandWidth", object: nil)
        
        let refreshControl = UIRefreshControl(frame: self.scrollView.frame)
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), forControlEvents: .ValueChanged)
        self.scrollView.addSubview(refreshControl)
        
		self.customData = BarChartBaseView.setupDataByDate(NSDate(),dateRange: .Week)
		self.setupGraph()
        self.updateUserData()
    }
    
    func refreshData(refreshControll: UIRefreshControl) {
        refreshControll.endRefreshing()
        self.updateUserData()
    }
    
    func showStr(notification: NSNotification) {
        
        let strs = notification.object as! [CGFloat]
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
        
            let startx = strs[0] as CGFloat
            let barWidth = strs[1] as CGFloat
            self.vernierLeftConstraint.constant = startx + barWidth*(6.0/0.85+0.5) - 10.0
            self.hideVernier(false)
            
        })
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "RectStartXandWidth", object: nil)
    }
	
    func hideVernier(isHide: Bool) {
        
        self.vernierLight.hidden = isHide
        self.vernierModerate.hidden = isHide
        self.vernierVigorous.hidden = isHide
        
    }
	
	func setupGraph() {
		self.lightChartView.setupActWeekBarChartView()
		self.moderateChartView.setupActWeekBarChartView()
		self.vigorousChartView.setupActWeekBarChartView()
		
		self.lightChartView.data = self.customData
		self.moderateChartView.data = self.customData
		self.vigorousChartView.data = self.customData
        
        let text = "Daily Average: N/A"
        self.lightAverTime.text = text
        self.moderateAverTime.text = text
        self.vigorousAverTime.text = text
	}
	
    func updateUserData() {
		
		self.loaderView.show()
        
        HealthDataPoint.purgeTodayDataPointsInContext(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext, type: .Steps)
        self.goalTrackingViewModel.updateStepsFromHealthKitForDayOrWeek(NSDate(), dateRange: .Week) { (success) -> Void in
            if success {
				
				self.clearChartValue()
				self.loaderView.hide(animated: true)
                self.appDelegate.saveContext()
                
                self.loadActivityData()
            }
        }
        
    }
	
	func clearChartValue() {
		self.lightChartView.data?.clearValues()
		self.moderateChartView.data?.clearValues()
		self.vigorousChartView.data?.clearValues()
	}
	
    func loadActivityData() {
        
        self.loaderView.show()
        self.goalTrackingViewModel.getStepsForUserByWeek(NSDate(), user:self.appDelegate.user!) { (result) -> Void in
            self.loaderView.hide(animated: true)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in

                if let lightDatasource = result["lightData"] as? [[String: AnyObject]] {
                    self.setupDataSource(lightDatasource, chartView: self.lightChartView)
                }
                
                if let moderateDatasource = result["moderateData"] as? [[String: AnyObject]] {
                    self.setupDataSource(moderateDatasource, chartView: self.moderateChartView)
                }
                
                if let vigorousDatasource = result["vigorousData"] as? [[String: AnyObject]] {
                    self.setupDataSource(vigorousDatasource, chartView: self.vigorousChartView)
                }
            })
        }
        
    }
    
    func setupDataSource(dataSource: [[String: AnyObject]], chartView: BarChartBaseView) {
        
        if dataSource.count == 0 {
           self.setupGraph()
           return
        }
        
        self.xVals.removeAll()
        self.yVals.removeAll()
        
        var totalTime: Double = 0.0
        var isBlue: Bool = true
        let limitTime = dataSource[dataSource.count-1]["goal"] as! Double
        for dataIndex in 0...dataSource.count-1 {
            let datapoint   = dataSource[dataIndex]
            
            let df        = NSDateFormatter()
            let xValue    = datapoint["timestamp"] as! NSDate
            
            if dataIndex == 0 {
               df.dateFormat = "MMM d"
            } else {
               df.dateFormat = "d"
            }
            
            let category  = df.stringFromDate(xValue)
            
            var value1: Double = 0.0
            var value2: Double = 0.0
            var value3: Double = 0.0
            
            let yValue = datapoint["value"] as! Double
			
            if (yValue < limitTime || yValue == limitTime) {
                value1 = yValue
                value2 = limitTime - yValue
                value3 = 0.0
            } else {
                value1 = limitTime
                value2 = 0.0
                value3 = yValue - limitTime
            }
            
            self.yVals.append(BarChartDataEntry(values:[value1,value2,value3], xIndex:dataIndex))
            self.xVals.append(("\(category)"))
            totalTime = totalTime + yValue
            
            if (yValue > limitTime) {
                isBlue = false
            }
        }
        
		
        
        if (limitTime > 0) {
            let line = ChartLimitLine(limit: limitTime, label: "Goal")
            line.lineWidth = 1.0
            line.lineColor = (isBlue == false) ? self.orangeColor : self.bgBlueColor
            line.labelPosition = .LeftTop
            line.valueFont = NSUIFont.systemFontOfSize(10.0)
            line.label2 = "\(Int(limitTime)) min"
            line.valueTextColor2 = UIColor.whiteColor()
            line.valueTextColor = UIColor(red:143.0/255, green: 143.0/255, blue: 143.0/255, alpha: 1.0)
            chartView.leftAxis.addLimitLine(line)
        }
            
        var set: BarChartDataSet
        if chartView.data?.dataSetCount > 0 {
            set = chartView.data?.dataSets[0] as! BarChartDataSet
            set.yVals = self.yVals
            chartView.data!.xValsObjc = self.xVals
            chartView.notifyDataSetChanged()
        }
        else {
            
            set = BarChartDataSet(yVals: self.yVals, label: "DataSet")
            set.drawValuesEnabled = false;
            
            var mainColor: UIColor
            switch (chartView.tag) {
             case 0:
                mainColor = UIColor(red:179.0/255, green: 231.0/255, blue: 1.0, alpha: 1.0)
                break
             case 1:
                mainColor = self.bgBlueColor
                break
             case 2:
                mainColor = UIColor(red:21.0/255, green: 91.0/255, blue: 160.0/255, alpha: 1.0)
                break
             default:
                mainColor = UIColor.blueColor()
                break
            }
            
            if (limitTime > 0) {
                set.colors = [mainColor, self.bgBarColor, self.orangeColor]
            } else {
                set.colors = [mainColor]
            }
            
            var dataSets = [IChartDataSet]()
            dataSets.append(set)
            let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
            chartView.data = data;
        }
        
        if (chartView.tag == 0) {
            self.lightAverTime.text = "Daily Average: \((Int(totalTime)/7))"
        } else if (chartView.tag == 1) {
            self.moderateAverTime.text = "Daily Average: \((Int(totalTime)/7))"
        } else {
            self.vigorousAverTime.text = "Daily Average: \((Int(totalTime)/7))"
        }
    }
    
}
