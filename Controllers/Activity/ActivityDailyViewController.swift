//
//  ActivityDailyViewController.swift
//  Geia
//
//  Created by Cassie on 6/8/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts
import Foundation

class ActivityDailyViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
//    weak var lightCircleView: PieChartBaseView!
//    weak var moderateCircleView: PieChartBaseView!
//    weak var vigorousCircleView: PieChartBaseView!
//    weak var chartView: BarChartBaseView!

    @IBOutlet weak var lightCircleView: MKRingProgressView!
    @IBOutlet weak var lblLightProgress: UILabel!
    @IBOutlet weak var moderateCircleView: MKRingProgressView!
    @IBOutlet weak var lblModerateProgress: UILabel!
    @IBOutlet weak var vigorousCircleView: MKRingProgressView!
    @IBOutlet weak var lblVigorousProgress: UILabel!
    @IBOutlet weak var chartView: BarChartBaseView!

    @IBOutlet weak var lightTime: UILabel!
    @IBOutlet weak var moderateTime: UILabel!
    @IBOutlet weak var vigorousTime: UILabel!
    @IBOutlet weak var lightSubTime: UILabel!
    @IBOutlet weak var moderateSubTime: UILabel!
    @IBOutlet weak var vigorousSubTime: UILabel!
    var dailyTotals: Int = 0
    var refreshing = false;
    @IBOutlet weak var stepCountLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var calorieLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var checkLight: UIImageView!
    @IBOutlet weak var checkModerate: UIImageView!
    @IBOutlet weak var checkVigorous: UIImageView!
	
	var customData: BarChartData!
    private var currentDate: NSDate!
    
    var xVals: [String] = [String]()
    var yVals: [ChartDataEntry] = [ChartDataEntry]()
    var zVals: [Int] = [Int]()
    var dateArray: [NSDate] = [NSDate]()
    
    var lightBlue = UIColor(red: 134.0/255.0, green: 210.0/255.0, blue: 253.0/255.0, alpha: 1.0)
    var moderateBlue = UIColor(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)
    var vigorousBlue = UIColor(red: 32.0/255.0, green: 98.0/255.0, blue: 165.0/255.0, alpha: 1.0)
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var dateFormatter:NSDateFormatter = {
        let df       = NSDateFormatter()
        df.locale    = NSLocale.currentLocale()
        df.timeStyle = .NoStyle
        df.dateStyle = .MediumStyle
        
        return df
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()
    
    class func instanceFromStoryBord() -> ActivityDailyViewController {
        let  storyboard = UIStoryboard(name:"Activity", bundle: NSBundle.mainBundle())
        let dayActivityViewController = storyboard.instantiateViewControllerWithIdentifier("ActivityDailyViewController") as!ActivityDailyViewController
        return dayActivityViewController
    }
	
	func checkDate() {
		
		let date = NSUserDefaults.standardUserDefaults().objectForKey(CURRENT_DATE_KEY) as! NSDate?
		if date != nil {
			
			if !self.currentDate.isEqualToDate(date!) {
				self.currentDate = date
			}
			
		} else {
			
			NSUserDefaults.standardUserDefaults().setObject(self.currentDate, forKey: CURRENT_DATE_KEY)
			NSUserDefaults.standardUserDefaults().synchronize()
		}
	}
	
    func addCustomDataForSteps(customSteps: Int){
        let theDate        = self.currentDate.dateAtStartOfDay()
        let dailyTotalObject    = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: theDate, moc: self.appDelegate.managedObjectContext)
        dailyTotalObject?.customSteps = Double(customSteps)
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            self.stepCountLabel.text = "\(self.dailyTotals + customSteps)"
            self.clearChartValue();
            if (self.xVals.count != 0) {
                self.refreshUI();

                self.xVals.removeAll()
                self.yVals.removeAll()
                self.zVals.removeAll()
                self.dateArray.removeAll()
                self.clearChartValue();
                self.resetData(self.currentDate.dateByAddingDays(-1)!)

            }
            
        })
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	func refreshUI() {
		
//		self.showLoadingView()
	
        if self.scrollView.tag != 99 {
            self.scrollView.tag = 99;
            self.currentDate = NSDate()
            self.checkDate()
            self.updateDayLabel()
            
            self.xVals.removeAll()
            self.yVals.removeAll()
            self.zVals.removeAll()
            self.dateArray.removeAll()
            
            self.setupGraph()
            self.updateUserData()
            
            let refreshControl = UIRefreshControl(frame: self.scrollView.frame)
            refreshControl.addTarget(self, action: #selector(refreshData(_:)), forControlEvents: .ValueChanged)
            refreshControl.tag = 10;
            dispatch_async(dispatch_get_main_queue()) {
                self.scrollView.addSubview(refreshControl)
            }
        }else{
//            self.xVals.removeAll()
//            self.yVals.removeAll()
//            self.zVals.removeAll()
//            self.dateArray.removeAll()
//            
//            self.resetData(self.currentDate);
            if refreshing == false{
                self.refreshing = true;
                self.displayViewByDate()
            }

        }
	}
	
    func refreshData(refreshControll: UIRefreshControl) {
        refreshControll.endRefreshing()
        if refreshing == false{
            self.refreshing = true;
            self.displayViewByDate()
        }
    }
    
	
    @IBAction func previousButtonClicked() {
        
        self.showLoadingView()
        self.resetData(self.currentDate.dateByAddingDays(-1)!)
		
    }
    
    @IBAction func nextButtonClicked() {
        
        self.showLoadingView()
        self.resetData(self.currentDate.dateByAddingDays(1)!)
		
    }
    
    func resetData(date: NSDate) {
        
        self.currentDate = date
        self.updateDayLabel()
		
		NSUserDefaults.standardUserDefaults().setObject(self.currentDate, forKey: CURRENT_DATE_KEY)
		NSUserDefaults.standardUserDefaults().synchronize()
		
        self.displayViewByDate()
    }
    
    func displayViewByDate() {
        
        self.xVals.removeAll()
        self.yVals.removeAll()
        self.zVals.removeAll()
        self.dateArray.removeAll()
        
        if self.currentDate.isEqualToDateIgnoringTime(NSDate()) {
            
            HealthDataPoint.purgeTodayDataPointsInContext(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext, type: .Steps)
            self.updateUserData()
            
        }
        else if (Common.isLargerThanToday(self.currentDate)) {
            self.setupGraph()
            self.resetView()
            self.hideLoadingView()
        }
        else {
            
            let healthDataPointObjects: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(self.appDelegate.user!, date: self.currentDate, type: .Steps, moc: self.appDelegate.managedObjectContext)
            if healthDataPointObjects.count != 0 {
                self.loadActivityData()
            } else {
                self.updateUserData()
            }
            
        }
        
    }
    
    func updateDayLabel() {
        
        let formattedDate = self.dateFormatter.stringFromDate(self.currentDate)
        self.dayLabel.text = formattedDate
        
    }
    
    func showLoadingView() {
        self.loaderView.show()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func hideLoadingView() {
        self.loaderView.hide(animated: true)
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        self.refreshing = false;
    }
    
    func updateUserData() {
        
        self.goalTrackingViewModel.updateStepsFromHealthKitForDayOrWeek(self.currentDate, dateRange: .Day) { (success) -> Void in
        
            if success {
                //save changes
                self.appDelegate.saveContext()
            
                self.loadActivityData()
            }
            else {
                self.hideLoadingView()
            }
            self.refreshing = false;
        }
        
    }
    
    func loadActivityData() {
        
        self.goalTrackingViewModel.getStepsForUserByDay(self.currentDate, user: self.appDelegate.user!) {
            (result) -> Void in

            let success = result["success"] as! Bool
			
            if success {
				self.clearChartValue()
				let lightMin         = result["lightMin"] as! Int
                let lightMinGoal     = result["lightMinGoal"] as! Int
                let moderateMin      = result["moderateMin"] as! Int
                let moderateMinGoal  = result["moderateMinGoal"] as! Int
                let vigorousMin      = result["vigorousMin"] as! Int
                let vigorousMinGoal  = result["vigorousMinGoal"] as! Int
                let lightProgress    = result["lightProgress"] as! Double
                let moderateProgress = result["moderateProgress"] as! Double
                let vigorousProgress = result["vigorousProgress"] as! Double
                
                //Update UI
                self.lightTime.text    = "\(lightMin) / \(lightMinGoal) min"
                self.moderateTime.text = "\(moderateMin) / \(moderateMinGoal) min"
                self.vigorousTime.text = "\(vigorousMin) / \(vigorousMinGoal) min"
                
                if lightProgress > 1 {
                   self.lightSubTime.text = "Complete"
                } else {
                    let distance = (lightMinGoal>lightMin) ? lightMinGoal-lightMin : 0
                   self.lightSubTime.attributedText = self.decorateStr("\(distance) min To Go")
                }
                
                if moderateProgress > 1 {
                    self.moderateSubTime.text = "Complete"
                } else {
                    let distance = (moderateMinGoal>moderateMin) ? moderateMinGoal-moderateMin : 0
                    self.moderateSubTime.attributedText = self.decorateStr("\(distance) min To Go")
                }
                
                if vigorousProgress > 1 {
                    self.vigorousSubTime.text = "Complete"
                } else {
                    let distance = (vigorousMinGoal>vigorousMin) ? vigorousMinGoal-vigorousMin : 0
                    self.vigorousSubTime.attributedText = self.decorateStr("\(distance) min To Go")
                }
                
 //               self.lightCircleView.startColor = self.lightBlue
                self.lblLightProgress.text = Common.getStrFromProgress(lightProgress)
                self.lightCircleView.progress = lightProgress
                self.checkLight.hidden = (lightProgress < 1.0) ? true : false
                
                self.lblModerateProgress.text = Common.getStrFromProgress(moderateProgress)
                self.moderateCircleView.progress = moderateProgress
//                self.moderateCircleView.startColor = self.vigorousBlue

                self.checkModerate.hidden = (moderateProgress < 1.0) ? true : false;
                
//                self.vigorousCircleView.startColor = self.vigorousBlue
                self.lblVigorousProgress.text = Common.getStrFromProgress(vigorousProgress)
                self.vigorousCircleView.progress = vigorousProgress
                self.checkVigorous.hidden = (vigorousProgress < 1.0) ? true : false
    
                self.totalTimeLabel.text = "\(lightMin + moderateMin + vigorousMin)"
            }
            
            //Bar Chart View
            //Bar CustomTotals 
            var customTotals:Int = 0;
            self.dailyTotals = 0;
            if let datasource = result["dataAct"] as? [[String: AnyObject]] {
                
                for obj in datasource {
                    let value     = obj["value"] as! Double
                    self.dailyTotals += Int(value)
                }
            }
            
            let df        = NSDateFormatter()
            df.dateFormat = "h a"
            
            let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
            let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: self.currentDate.dateAtStartOfDay())
            components.year = self.currentDate.year()
            components.month = self.currentDate.month()
            components.day   = self.currentDate.day()
            
            for index in 0...144 {
                
                components.hour     = Int(index/6)
                components.minute   = (index%6)*10
                components.second   = 0
                
                let tempDay: NSDate =  NSCalendar.currentCalendar().dateFromComponents(components)!
                self.zVals.append(1)
                self.yVals.append(BarChartDataEntry(value: 0, xIndex: index))
                self.xVals.append(("\(df.stringFromDate(tempDay))"))
                self.dateArray.append(tempDay)
            }
            
            if let datasource = result["dataAct"] as? [[String: AnyObject]] {
              for charsObj in datasource {
                let timestamp = charsObj["timestamp"] as! NSDate
                let value     = charsObj["value"] as! Double
                let level     = charsObj["level"] as! Int
                let duration  = charsObj["duration"] as! Double //second
                let avgStepsPerMinute = value / duration * 60.0
                
                var i: Int = 0
                for tDate in self.dateArray {
                    
                    let timeDiff = timestamp.timeIntervalSince1970 - tDate.timeIntervalSince1970
                    
                    if (timeDiff <= 10*60) && (timeDiff > 0) {
                        let range :Range = Range(i...i)
                        self.yVals.replaceRange(range, with: [BarChartDataEntry(value: avgStepsPerMinute, xIndex: i)])
                        self.zVals.replaceRange(range, with: [level])
                        break
                    }
                    
                    i = i+1
                }
              }
            }
            
            let theDate        = self.currentDate.dateAtStartOfDay()
            let dailyTotalObject    = HealthDailyTotal.addDailyTotalsForUser(self.appDelegate.user!, date: theDate, moc: self.appDelegate.managedObjectContext)
            dailyTotalObject?.steps = Double(self.dailyTotals)
            customTotals =  Int((dailyTotalObject?.customSteps)!);
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.stepCountLabel.text = "\(self.dailyTotals + customTotals)"
                
                if (self.xVals.count != 0) {
                    self.setDataSource()
                }
            })
            
            self.refreshing = false;

        }
        
        
     
        
        
        //Calories
        let healthManager = HealthManager()
        healthManager.getCaloriesForDate(self.currentDate,  dateRange:.Day, completion: { (activeCalories, restingCalories, error) -> Void in
            if error != nil {
                print("Error reading HealthKit Store: \(error.localizedDescription), \(error.code)")
                return
            }
 
            var totalCalories = 0.0
            
            for item in activeCalories {
                totalCalories = item.sum + totalCalories
            }
            
            for item in restingCalories {
                totalCalories = item.sum + totalCalories
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.appDelegate.saveContext()
            
                if totalCalories == 0 {
					self.calorieLabel.text = "N/A"
				}
				else {
					self.calorieLabel.text = "\(Int(totalCalories))"
				}
				
            })
        })
		
		self.hideLoadingView()
    }
	
	func clearChartValue() {
		self.chartView.data?.clearValues()
	}
	
    func decorateStr(str: String) -> NSMutableAttributedString {
        
        let attributedText = NSMutableAttributedString(string: str)
        
        let range = (str as NSString).rangeOfString("To")
        let preRange = NSMakeRange(0, range.location)
        //let laRange = NSMakeRange(0, range.location + 1, centerText.length - range.location - 1)
        attributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 0, green: 158.0/255.0, blue: 220.0/255.0, alpha: 1.0), range: preRange)
        
        return attributedText
    }
    
    func setDataSource() {
        
        var set1: BarChartDataSet
        if self.chartView.data?.dataSetCount > 0 {
            set1 = self.chartView.data?.dataSets[0] as! BarChartDataSet
            set1.yVals = self.yVals
            self.chartView.data!.xValsObjc = self.xVals
            self.chartView.notifyDataSetChanged()
        }
        else {
            
            set1 = BarChartDataSet(yVals: self.yVals, label: "DataSet")
            set1.drawValuesEnabled = false;
            
            var colors = [UIColor]()
            for level:Int in self.zVals {
                if level == 2 {
                   colors.append(self.moderateBlue)
                } else if (level == 3) {
                   colors.append(self.vigorousBlue)
                } else {
                   colors.append(self.lightBlue)
                }
            }
            set1.colors = colors
            
            var dataSets = [IChartDataSet]()
            dataSets.append(set1)
            let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
            self.chartView.data = data;
        }
    }
	
	func setupGraph() {
        
        self.checkLight.hidden = true
        self.checkModerate.hidden = true
        self.checkVigorous.hidden = true
        
		self.customData = BarChartBaseView.setupDataByDate(self.currentDate,dateRange:.Day)
		self.chartView.setupActDayBarChartView()
        self.chartView.leftAxis.showMinDisabled = true
		self.chartView.data = self.customData

	}

    func resetView() {

        self.lblLightProgress.text = "0%"
        self.lblModerateProgress.text = "0%"
        self.lblVigorousProgress.text = "0%"
        
        self.lightCircleView.progress = 0;
        self.moderateCircleView.progress = 0;
        self.vigorousCircleView.progress = 0;

        let attributedText = self.decorateStr("0 min To Go")
        let text = "0 / 0 min"
        self.lightTime.text = text
        self.lightSubTime.attributedText = attributedText
        self.moderateTime.text = text
        self.moderateSubTime.attributedText = attributedText
        self.vigorousTime.text = text
        self.vigorousSubTime.attributedText = attributedText
        
        self.calorieLabel.text = "N/A"
        self.stepCountLabel.text = "0"
        self.totalTimeLabel.text = "0"
    }
}
