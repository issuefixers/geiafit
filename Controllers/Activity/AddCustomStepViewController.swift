//
//  AddCustomStepViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/24/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import HealthKit

class AddCustomStepViewController: GeiaViewController {

    
    
    @IBOutlet weak var btnDuration: UIButton!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var btnDay: UIButton!
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    var durationTime:Double = 0 ;
    var selectedDate:NSDate = NSDate();
    
    @IBOutlet weak var segmentedEffort: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.tintColor = UIColor.geiaYellowColor();
        
        let items: UINavigationItem = UINavigationItem(title: "Add Custom Value")
        items.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(AddCustomStepViewController.dissmisView))
        self.navigationBar.setItems([items], animated: true);
        // Do any additional setup after loading the view.
    }

    
    func dissmisView(){
        self.dismissViewControllerAnimated(true, completion: nil);

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func durationButtonPressed(sender: UIButton) {
        let alertController = UIAlertController(title: "How long did you did it?", message: "Tell us in minutes how long did you exercise", preferredStyle: .Alert);
        alertController.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.keyboardType = .NumberPad;
            textfield.keyboardAppearance = .Dark
            textfield.placeholder = "Ex. 5 min"
            
        }
        
        alertController.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) in
            let textfield = alertController.textFields![0] ;
            if(textfield.text!.isEmpty){
                textfield.text = "5"
            }else{
                self.durationTime = Double(textfield.text!)!;
                self.btnDuration.setTitle("\(Int(self.durationTime)) minutes", forState: .Normal);
            }
        }));
        self.presentViewController(alertController, animated: true, completion: nil);
    }


    @IBAction func saveCustoms(sender: AnyObject) {
        
        if (self.durationTime == 0) {
            let alertController = UIAlertController(title: "Error", message: "You need to tell us how long you worked out", preferredStyle: .Alert);
            
            alertController.addAction(UIAlertAction(title: "Accept", style: .Default, handler: { (action) in
                alertController.dismissViewControllerAnimated(true, completion: nil);
            }));
            return;
        }
        
        if(self.btnDay.tag != 999){
            let alertController = UIAlertController(title: "Error", message: "You need to tell us when you started your work out session", preferredStyle: .Alert);
            
            alertController.addAction(UIAlertAction(title: "Accept", style: .Default, handler: { (action) in
                alertController.dismissViewControllerAnimated(true, completion: nil);
            }));
            return;
            
        }
        var thresholds = 10.0;
        switch segmentedEffort.selectedSegmentIndex {
        case 0:
            thresholds =  thresholds + (self.appDelegate.user?.thresholds?.stepsMin)!
            break;
        case 1:
            thresholds = thresholds + (self.appDelegate.user?.thresholds?.stepsLow)!
            break;
        case 2:
            thresholds = thresholds + (self.appDelegate.user?.thresholds?.stepsHigh)!
            break;
        default:
            break;
        }
        
        let steps:Double = durationTime * thresholds
        

        self.saveSteps(steps, date: self.selectedDate, duration: Int(self.durationTime));
       
        
        self.dismissViewControllerAnimated(true, completion: nil);

        
    }
    
    
    func saveSteps(steps:Double, date:NSDate,duration:Int ) {
        
        // 1. Create a BMI Sample
        
        for i in 1...duration{
            
            let metadata = ["HealthDataDeviceStringType":HealthDataDeviceStringType.Manually.rawValue]
            
            let queryType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
            let stepQuantity = HKQuantity(unit: HKUnit.countUnit(), doubleValue: steps/Double(duration))
            let stepSample = HKQuantitySample(type: queryType!, quantity: stepQuantity, startDate: date.dateByAddingMinutes(i)!, endDate: date.dateByAddingMinutes(i + 1)!, metadata: metadata);
            
            // 2. Save the sample in the store
            HKHealthStore().saveObject(stepSample, withCompletion: { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving Steps sample: \(error!.localizedDescription)")
                } else {
                    print("\(steps) steps sample saved successfully!")
                    
                    HealthDataPoint.addDataPointForUser(self.appDelegate.user!, count: steps/Double(duration), timestamp: self.selectedDate.dateByAddingMinutes(i)! , type: HealthDataType.Steps, duration: 1, moc: self.appDelegate.managedObjectContext, deviceType: HealthDataDeviceType.Manually);
                }
            })
        }
        NSNotificationCenter.defaultCenter().postNotificationName("updateActivityData", object: nil);

    }
    
    @IBAction func selectDate(sender: AnyObject) {
       let datePicker =  ActionSheetDatePicker(title: "When did you start?", datePickerMode: UIDatePickerMode.DateAndTime, selectedDate: selectedDate, doneBlock: { (picker, index, value) in
            self.selectedDate = index as! NSDate;
            self.btnDay.setTitle(self.selectedDate.stringDateWithFormat("MM/dd HH:mm:ss"), forState: .Normal);
            self.btnDay.tag = 999;
            }, cancelBlock: { (picker) in
            }, origin: self.view)
        
        datePicker.maximumDate = NSDate.dateTomorrow();
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: .Done, target: nil, action: nil);
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .Done, target: nil, action: nil);
        doneButton.tintColor = UIColor.geiaYellowColor();
        cancelButton.tintColor = UIColor.geiaYellowColor();
        datePicker.setDoneButton(doneButton);
        datePicker.setCancelButton(cancelButton);
        datePicker.showActionSheetPicker();
        
    }

}
