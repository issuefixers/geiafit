//
//  ActivityMonthViewController.swift
//  Geia
//
//  Created by Cassie on 6/8/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts

class ActivityMonthViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    weak var lightChartView: BarChartBaseView!
    weak var moderateChartView: BarChartBaseView!
    weak var vigorousChartView: BarChartBaseView!
    
    @IBOutlet weak var lightAverTime: UILabel!
    @IBOutlet weak var moderateAverTime: UILabel!
    @IBOutlet weak var vigorousAverTime: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var vernierLight: UIImageView!
    @IBOutlet weak var vernierModerate: UIImageView!
    @IBOutlet weak var vernierVigorous: UIImageView!
    
    @IBOutlet weak var vernierLeftConstraint: NSLayoutConstraint!
	
	var customData: BarChartData!
	
    private var currentDate: NSDate!
    
    var xVals: [String] = [String]()
    var yVals: [ChartDataEntry] = [ChartDataEntry]()
    var dateArray: [NSDate] = [NSDate]()
    let orangeColor = UIColor(red:245.0/255, green: 184.0/255, blue: 39.0/255, alpha: 1.0)
    let bgBarColor = UIColor(red:74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
    let bgBlueColor = UIColor(red:0, green: 159.0/255, blue: 219.0/255, alpha: 1.0)
    
    class func instanceFromStoryBord() -> ActivityMonthViewController {
        let  storyboard = UIStoryboard(name:"Activity", bundle: NSBundle.mainBundle())
        let monthActivityViewController = storyboard.instantiateViewControllerWithIdentifier("ActivityMonthViewController") as!ActivityMonthViewController
        return monthActivityViewController
    }
    
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()
    
    private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
        return GoalTrackingViewModel()
    }()
    
    private lazy var dateFormatter:NSDateFormatter = {
        let df       = NSDateFormatter()
        df.locale    = NSLocale.currentLocale()
        df.timeStyle = .NoStyle
        df.dateStyle = .MediumStyle
        df.dateFormat = "MMM,yyyy"
        return df
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLoadingView()
        self.hideVernier(true)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showStr(_:)), name: "RectStartXandWidth", object: nil)
        
        let refreshControl = UIRefreshControl(frame: self.scrollView.frame)
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), forControlEvents: .ValueChanged)
        self.scrollView.addSubview(refreshControl)
        
        self.currentDate = NSDate()
        self.updateDayLabel()
        self.setupGraph()
        self.updateUserData()
    }
    
    func refreshData(refreshControll: UIRefreshControl) {
        refreshControll.endRefreshing()
        self.displayViewByDate()
    }
    
    func showStr(notification: NSNotification) {
    
        let strs = notification.object as! [CGFloat]
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
        
            let components = NSCalendar.currentCalendar().components([.Day , .Month , .Year], fromDate: NSDate())
            let startx = strs[0] as CGFloat
            let barWidth = strs[1] as CGFloat
            self.vernierLeftConstraint.constant = startx + barWidth*(CGFloat(components.day - 1)/0.85+0.5) - 3.5
            self.hideVernier(false)
        
        })
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "RectStartXandWidth", object: nil)
    }
	
	func setupGraph() {
		self.customData = BarChartBaseView.setupDataByDate(self.currentDate,dateRange:.Month)
		self.lightChartView.setupActMonthBarChartView()
		self.lightChartView.data = self.customData
		self.moderateChartView.setupActMonthBarChartView()
		self.moderateChartView.data = self.customData
		self.vigorousChartView.setupActMonthBarChartView()
		self.vigorousChartView.data = self.customData
        
        let text = "Daily Average: N/A"
        self.lightAverTime.text = text
        self.moderateAverTime.text = text
        self.vigorousAverTime.text = text
	}
	
    @IBAction func previousButtonClicked() {
        
        self.showLoadingView()
        self.resetData(Common.getPreviousMonth(self.currentDate))
        
    }
    
    @IBAction func nextButtonClicked() {
        
        self.showLoadingView()
        self.resetData(Common.getNextMonth(self.currentDate))
        
    }
    
    func resetData(date: NSDate) {
        
        self.currentDate = date
        self.updateDayLabel()
        self.compareDate()
        
        self.displayViewByDate()
    }
    
    func displayViewByDate() {
        
        if Common.isLargerThanMonth(self.currentDate) {
            
            self.setupGraph()
            self.hideLoadingView()
            
        }
        else {
            self.updateUserData()
        }
        
    }
    
    func compareDate() {
        
        if (Common.isEqualThisMonth(self.currentDate)) {
            self.hideVernier(false)
        } else {
            self.hideVernier(true)
        }
    }
    
    func updateDayLabel() {
        
        let formattedDate = self.dateFormatter.stringFromDate(self.currentDate)
        self.dayLabel.text = formattedDate
    }
    
    func hideVernier(isHide: Bool) {
    
        self.vernierLight.hidden = isHide
        self.vernierModerate.hidden = isHide
        self.vernierVigorous.hidden = isHide
        
    }
    
    func showLoadingView() {
        self.loaderView.show()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func hideLoadingView() {
        self.loaderView.hide(animated: true)
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    func updateUserData() {
        
        self.updateFromAppHealth()
        
//        let firstAndLastDay = Common.getFirstAndLastDateOfMonth(self.currentDate)
//        let healthDataPointObjects1: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(self.appDelegate.user!, date: firstAndLastDay.fistDateOfMonth, type: .Steps, moc: self.appDelegate.managedObjectContext)
//        
//        if (Common.isEqualThisMonth(self.currentDate)) {
//            HealthDataPoint.purgeTodayDataPointsInContext(self.appDelegate.user!, moc: self.appDelegate.managedObjectContext, type: .Steps)
//            if healthDataPointObjects1.count != 0 {
//                
//                self.goalTrackingViewModel.updateStepsFromHealthKitForDayOrWeek(NSDate(), dateRange: .Day) { (success) -> Void in
//                    if success {
//                        self.appDelegate.saveContext()
//                        self.loadActivityData(nil)
//                    }
//                    else {
//                        self.hideLoadingView()
//                    }
//                }
//            } else {
//                self.updateFromAppHealth()
//            }
//            
//        } else {
//        
//            if healthDataPointObjects1.count != 0 {
//                let healthDataPointObjects2: [HealthDataPoint]  = HealthDataPoint.getDataPointsForUser(self.appDelegate.user!, date: firstAndLastDay.lastDateOfMonth, type: .Steps, moc: self.appDelegate.managedObjectContext)
//                if healthDataPointObjects2.count != 0 {
//                    self.loadActivityData(nil)
//                } else {
//                    self.updateFromAppHealth()
//                }
//            } else {
//                self.updateFromAppHealth()
//            }
//            
//        }
    }
    
    func updateFromAppHealth() {
        
        self.goalTrackingViewModel.updateStepsFromHealthKitForMonth(self.currentDate) { (success, healthPoints) -> Void in
            if success {
                
                //self.appDelegate.saveContext()
                self.loadActivityData(healthPoints)
            }
            else {
                self.hideLoadingView()
            }
        }
        
    }
	
	func clearChartValue() {
		self.lightChartView.data?.clearValues()
		self.moderateChartView.data?.clearValues()
		self.vigorousChartView.data?.clearValues()
	}
	
    func loadActivityData(healthDicts: [Int : [HealthViewPoint]]?) {
        
        self.goalTrackingViewModel.getStepsForUserByMonth(self.currentDate, user:self.appDelegate.user!, healthDicts: healthDicts) { (result) -> Void in
            
            self.clearChartValue()
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                if let lightDatasource = result["lightData"] as? [[String: AnyObject]] {
                    self.setupDataSource(lightDatasource, chartView: self.lightChartView)
                }
                
                if let moderateDatasource = result["moderateData"] as? [[String: AnyObject]] {
                    self.setupDataSource(moderateDatasource, chartView: self.moderateChartView)
                }
                
                if let vigorousDatasource = result["vigorousData"] as? [[String: AnyObject]] {
                    self.setupDataSource(vigorousDatasource, chartView: self.vigorousChartView)
                }
            })
        }
		
		self.hideLoadingView()
        
    }
    
    func setupDataSource(dataSource: [[String: AnyObject]], chartView: BarChartBaseView) {
        
        if dataSource.count == 0 {
            self.setupGraph()
            return
        }
        
        self.xVals.removeAll()
        self.yVals.removeAll()
        self.dateArray.removeAll()
    
        let df        = NSDateFormatter()
        df.dateFormat = "d"
        
        let componentFlags: NSCalendarUnit = [.Year, .Day, .Month, .Hour, .Minute, .Second]
        let components    = NSCalendar.currentCalendar().components(componentFlags, fromDate: self.currentDate.dateAtStartOfDay())
        components.year = self.currentDate.year()
        components.month = self.currentDate.month()
        
        let calender: NSCalendar = NSCalendar.currentCalendar()
        let range: NSRange = calender.rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: self.currentDate)
        
        for index in 1...range.length {

            components.day   = index
            let tempDay: NSDate =  NSCalendar.currentCalendar().dateFromComponents(components)!
            self.yVals.append(BarChartDataEntry(value: 0, xIndex: index))
            self.xVals.append(("\(df.stringFromDate(tempDay))"))
            self.dateArray.append(tempDay)
        }
        
        var totalTime: Double = 0.0
        var isBlue: Bool = true
        let limitTime = dataSource[dataSource.count-1]["goal"] as! Double
        for charsObj in dataSource {
            let timestamp = charsObj["timestamp"] as! NSDate
            let yValue     = charsObj["value"] as! Double
            totalTime = totalTime + yValue
            
            var i: Int = 0
            for tDate in self.dateArray {
                
                if tDate.month() == timestamp.month() && tDate.day() == timestamp.day() {
                    
                    var value1: Double = 0.0
                    var value2: Double = 0.0
                    var value3: Double = 0.0
                    
                    if (yValue < limitTime || yValue == limitTime) {
                        value1 = yValue
                        value2 = limitTime - yValue
                        value3 = 0.0
                    } else {
                        value1 = limitTime
                        value2 = 0.0
                        value3 = yValue - limitTime
                    }
                    
                    let range :Range = Range(i...i)
                    self.yVals.replaceRange(range, with: [BarChartDataEntry(values:[value1,value2,value3], xIndex:i)])
                    break
                }
                
                i = i+1
            }
            
            if (yValue > limitTime) {
                isBlue = false
            }
        }
		
        if (limitTime > 0) {
            let line = ChartLimitLine(limit: limitTime, label: "Goal")
            line.lineColor = (isBlue == false) ? self.orangeColor : self.bgBlueColor
            line.lineWidth = 1.0
            line.labelPosition = .LeftTop
            line.valueFont = NSUIFont.systemFontOfSize(10.0)
            line.label2 = "\(Int(limitTime)) min"
            line.valueTextColor2 = UIColor.whiteColor()
            line.valueTextColor = UIColor(red:143.0/255, green: 143.0/255, blue: 143.0/255, alpha: 1.0)
            chartView.leftAxis.addLimitLine(line)
        } else {
            chartView.leftAxis.removeAllLimitLines()
        }
        
        var set: BarChartDataSet
        if chartView.data?.dataSetCount > 0 {
            set = chartView.data?.dataSets[0] as! BarChartDataSet
            set.yVals = self.yVals
            chartView.data!.xValsObjc = self.xVals
            chartView.notifyDataSetChanged()
        }
        else {
            
            set = BarChartDataSet(yVals: self.yVals, label: "DataSet")
            set.drawValuesEnabled = false;
            
            var mainColor: UIColor
            switch (chartView.tag) {
            case 0:
                mainColor = UIColor(red:179.0/255, green: 231.0/255, blue: 1.0, alpha: 1.0)
                break
            case 1:
                mainColor = self.bgBlueColor
                break
            case 2:
                mainColor = UIColor(red:21.0/255, green: 91.0/255, blue: 160.0/255, alpha: 1.0)
                break
            default:
                mainColor = UIColor.blueColor()
                break
            }
            
            if (limitTime > 0) {
                set.colors = [mainColor, self.bgBarColor, self.orangeColor]
            } else {
                set.colors = [mainColor]
            }
            
            var dataSets = [IChartDataSet]()
            dataSets.append(set)
            let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
            chartView.data = data;
        }
        
        if (chartView.tag == 0) {
            self.lightAverTime.text = "Daily Average: \((Int(totalTime)/30))"
        } else if (chartView.tag == 1) {
            self.moderateAverTime.text = "Daily Average: \((Int(totalTime)/30))"
        } else {
            self.vigorousAverTime.text = "Daily Average: \((Int(totalTime)/30))"
        }
    }

}
