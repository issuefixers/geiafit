//
//  ActivityRootViewController.swift
//  Geia
//
//  Created by Cassie on 6/8/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class ActivityRootViewController: UIViewController, ECSegmentedControlDelegate {

    @IBOutlet var contentView: UIView!
    @IBOutlet var sideSegmentedControl: ECSegmentedControl!
    @IBOutlet var titleView: UIView!
    
    var  curSegCtrlIndex: UInt!
    var  currentViewController: UIViewController!
    var  currentClassName : NSString!
	
    class func instanceFromStoryBord() -> ActivityRootViewController {
        let  storyboard: UIStoryboard = UIStoryboard.init(name:"Activity", bundle: NSBundle.mainBundle())
        let actRootViewController = storyboard.instantiateViewControllerWithIdentifier("ActivityRootViewController") as!ActivityRootViewController
        return actRootViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSgeBar()
        self.curSegCtrlIndex = 0
        
        let vc = self.viewControllerForSegmentIndex(self.curSegCtrlIndex)
        
        vc.view.frame = self.contentView.bounds;
        self.addChildViewController(vc)
        self.contentView.addSubview(vc.view)
        self.currentViewController = vc;
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "updateActivityData", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ActivityRootViewController.refrshData), name: "updateActivityData", object: nil);
    }
    
	func setupSgeBar() {
		let segment1 : ECSegmentedControlSegment = ECSegmentedControlSegment.init()
		segment1.backgroundImage = UIImage.init(named: "DayNormal")
		segment1.highlightedImage = UIImage.init(named: "Day")
		segment1.size = UInt(UIScreen.mainScreen().bounds.size.width-45)/3
		
		let segment2 : ECSegmentedControlSegment = ECSegmentedControlSegment.init()
		segment2.backgroundImage = UIImage.init(named: "WeekNormal")
		segment2.highlightedImage = UIImage.init(named: "Week")
		segment2.size = UInt(UIScreen.mainScreen().bounds.size.width-45)/3
		
		let segment3 : ECSegmentedControlSegment = ECSegmentedControlSegment.init()
		segment3.backgroundImage = UIImage.init(named: "MonthNormal")
		segment3.highlightedImage = UIImage.init(named: "Month")
		segment3.size = UInt(UIScreen.mainScreen().bounds.size.width-45)/3
		self.sideSegmentedControl.setupWithSegmentArray([segment1,segment2,segment3],selectedSegment:0,controlSize:31,dividerImage:nil,verticalOrientation:false)
		self.sideSegmentedControl.delegate = self
		
	}
    // MARK:- Segmented Value Changed
    func selectedSegmentChanged(sender: AnyObject!, withIndex index: UInt) {
        
        self.curSegCtrlIndex = index
        let vc : UIViewController =  self.viewControllerForSegmentIndex(index)
        self.addChildViewController(vc)
        self.transitionFromViewController(self.currentViewController, toViewController: vc, duration: 0.5, options: UIViewAnimationOptions.CurveLinear, animations: {
            
            self.currentViewController.view.removeFromSuperview()
            self.contentView.subviews.forEach { $0.removeFromSuperview() }
            self.contentView.addSubview(vc.view)
            
        }) { (Bool) in
            vc.didMoveToParentViewController(self)
            
            self.currentViewController.removeFromParentViewController()
            self.currentViewController = vc;
			self.refrshData()
        }
    }
    
    func viewControllerForSegmentIndex(index:UInt) -> UIViewController {
        var  vc : UIViewController = UIViewController.init()
        if index == 0 {
            let dayController = ActivityDailyViewController.instanceFromStoryBord() as ActivityDailyViewController
            vc = dayController
			self.currentClassName = "Daily Activity"
        } else if (index == 1) {
            let weekController = ActivityWeekViewController.instanceFromStoryBord() as ActivityWeekViewController
            vc = weekController
			self.currentClassName = "Week Activity"
        } else if (index == 2) {
            let monthController = ActivityMonthViewController.instanceFromStoryBord() as ActivityMonthViewController
            vc = monthController
			self.currentClassName = "Month Activity"
            
        }
		
		let tracker = GAI.sharedInstance().defaultTracker
		tracker.set(kGAIScreenName, value:String(self.currentClassName))
		
		let builder = GAIDictionaryBuilder.createScreenView()
		tracker.send(builder.build() as [NSObject : AnyObject])
		
        vc.view.frame = self.contentView.bounds;
		
		
        return vc
    }
	func refrshData() {
		if self.currentViewController.isKindOfClass(ActivityDailyViewController) {
            (self.currentViewController as! ActivityDailyViewController).refreshUI();
		}
	}
    
    func addCustomData(customData: Int){
        if self.currentViewController.isKindOfClass(ActivityDailyViewController) {
            (self.currentViewController as! ActivityDailyViewController).addCustomDataForSteps(customData);
        }
    }
}
