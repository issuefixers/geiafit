//
//  YourInfoViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 11/24/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import JTProgressHUD
import ActionSheetPicker_3_0


class YourInfoViewController: GeiaViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var male: UIButton!
    @IBOutlet weak var female: UIButton!
    @IBOutlet weak var birthday: UIButton!
    @IBOutlet weak var weight: UIButton!
    @IBOutlet weak var height: UIButton!
    @IBOutlet weak var nxtButton: UIButton!
    
    
    
    
    // MARK: - Local variables
    private let duration                            = 0.35
    private var scrollViewHeightNormal:CGFloat      = 0.0
    private var scrollViewHeightPickerShown:CGFloat = 0.0
    private var currentButtonRect:CGRect            = CGRect.zero
    private var pickerHeight:CGFloat                = 0.0
    private let locale                              = NSLocale.currentLocale()
    private var picUpdated = false;
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    let doneButton = UIBarButtonItem(title: "Done", style: .Done, target: nil, action: nil);
    let cancelButton = UIBarButtonItem(title: "Cancel", style: .Done, target: nil, action: nil);
    
    private lazy var usesMetric:Bool = {
        let locale = NSLocale.currentLocale()
        let x      = locale.objectForKey(NSLocaleUsesMetricSystem) as! Bool
        
        return x
    }()
    
    private lazy var dateFormatter:NSDateFormatter = {
        let df       = NSDateFormatter()
        df.locale    = NSLocale.currentLocale()
        df.timeStyle = .NoStyle
        df.dateStyle = .MediumStyle
        
        return df
    }()
    
    private lazy var numberFormatter:NSNumberFormatter = {
        let nf         = NSNumberFormatter()
        nf.locale      = NSLocale.currentLocale()
        nf.numberStyle = .DecimalStyle
        
        return nf
    }()
    
    
    
    
    
    
    
    
    private lazy var maxAvatarSize: CGSize = {
        let density     = Toolchain.getScreenDensity()
        let avatarWidth = (2 * density) * 104 //104 = self.avatar.bounds.width
        let avatarSize  = CGSize(width: avatarWidth, height: avatarWidth)
        
        return avatarSize
    }()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.scrollViewHeightNormal      = Toolchain.getWindowSize().height - 64
        self.scrollViewHeightPickerShown = self.scrollViewHeightNormal - self.pickerHeight
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        
        //default values to somewhere in the middle
        doneButton.tintColor = UIColor.geiaYellowColor();
        cancelButton.tintColor = UIColor.geiaYellowColor();

        self.loadDataForUser(self.appDelegate.user!)
        self.getDataFromServer()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //make the avatar a circle
        self.avatar.convertToCircle()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .Portrait {
            self.scrollViewHeightNormal = Toolchain.getWindowSize().height - 64
        } else if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.scrollViewHeightNormal = Toolchain.getWindowSize().height - 20
        }
        self.scrollViewHeightPickerShown = self.scrollViewHeightNormal - self.pickerHeight
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.scrollViewHeightConstraint.constant = self.scrollViewHeightNormal
        self.scrollView.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Data
    private func loadDataForUser(user: User) {
        //profile image
        if let pic = user.pictureAsImage() {
            self.avatar.image = pic
        }
        
        //load gender
        if user.gender == Gender.Male.rawValue {
            self.toggleGenderButtons(self.male)
        } else if user.gender == Gender.Female.rawValue {
            self.toggleGenderButtons(self.female)
        }
        
        //birthday
        let dob              = NSDate(timeIntervalSince1970: user.birthday)
        self.didPickBirthday(dob)
        
        //weight
        let weight = user.getWeight()
        if weight > 0.0 {
            
            self.didUpdateWeight(weight);
        }else{
            self.nxtButton.backgroundColor = UIColor.geiaMidGrayColor();
            self.nxtButton.enabled = false;
        }
        
        //height
        let height = user.getHeight()
        if height > 0.0 {
            self.didUpdateHeight(height)
        }else{
            self.nxtButton.backgroundColor = UIColor.geiaMidGrayColor();
            self.nxtButton.enabled = false;
        }
    }
    
    private func getDataFromServer() {
        JTProgressHUD.show();
        OnboardingViewController.sharedInstance.getCharacteristicsForUser(self.appDelegate.user!) { (result) -> Void in
            //save changes
            self.appDelegate.saveContext()
            self.loadDataForUser(self.appDelegate.user!)
            JTProgressHUD.hide();
        }
    }
    
    // MARK: - Actions
    @IBAction func chooseProfilePic(sender: AnyObject) {
        let hasCamera             = UIImagePickerController.isSourceTypeAvailable(.Camera)
        let hasLibrary            = UIImagePickerController.isSourceTypeAvailable(.SavedPhotosAlbum)
        let imagePicker           = UIImagePickerController()
        imagePicker.delegate      = self
        imagePicker.allowsEditing = true
        
        if hasCamera && hasLibrary {
            let alertController = UIAlertController(title: "Image Source", message: nil, preferredStyle: .ActionSheet)
            alertController.addAction(UIAlertAction(title: "Library", style: .Default, handler: { (action) -> Void in
                //open library
                imagePicker.sourceType = .SavedPhotosAlbum
                
                alertController.dismissViewControllerAnimated(true, completion: nil)
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }))
            alertController.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { (action) -> Void in
                //open camera
                imagePicker.sourceType = .Camera
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }))
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler: { (action) -> Void in
                //open camera
                alertController.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        } else if hasLibrary {
            //open library
            imagePicker.sourceType = .SavedPhotosAlbum
            self.presentViewController(imagePicker, animated: true, completion: nil)
        } else if hasCamera {
            //open library
            imagePicker.sourceType = .Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func toggleGenderButtons(sender: UIButton) {
        if sender == self.male {
            self.male.selected   = true
            self.female.selected = false
        } else {
            self.male.selected   = false
            self.female.selected = true
        }
    }
    
    @IBAction func pickBirthday(sender: AnyObject) {

        let datePicker = ActionSheetDatePicker(title: "Birthday:", datePickerMode: UIDatePickerMode.Date, selectedDate: NSDate().dateBySubtractingDays(1825), doneBlock: {
            picker, value, index in
            
            self.didPickBirthday(value as! NSDate);
            return
            }, cancelBlock: { ActionStringCancelBlock in return },origin: self.view);
        
        
        let secondsInWeek: NSTimeInterval = 7 * 24 * 60 * 60;
        datePicker.maximumDate = NSDate(timeInterval: secondsInWeek, sinceDate: NSDate())
      

   

        datePicker.setDoneButton(doneButton);
        datePicker.setCancelButton(cancelButton);
        
        
        
        
        datePicker.setDoneButton(doneButton);
        datePicker.setCancelButton(cancelButton);
        
        datePicker.showActionSheetPicker()
        
        
        //show Date picker
    }
    
    func didPickBirthday(sender:NSDate) {
        OnboardingViewController.sharedInstance.dobValue = sender
        self.birthday.setTitle(self.dateFormatter.stringFromDate(OnboardingViewController.sharedInstance.dobValue!), forState: .Normal)
    }
    
    @IBAction func showWeights(){
        let weightPicker:ActionSheetDistancePicker?
        if self.usesMetric {

             weightPicker = ActionSheetDistancePicker(title: "Weight", bigUnitString: ".", bigUnitMax: 180, selectedBigUnit: 70, smallUnitString: "kg", smallUnitMax: 9, selectedSmallUnit: 0, target: self, action: #selector(YourInfoViewController.weightWasSelected(_:smallUnit:element:)), origin: self.view)
          
        }else{
             weightPicker = ActionSheetDistancePicker(title: "Weight", bigUnitString: ".", bigUnitMax: 350, selectedBigUnit: 160, smallUnitString: "lb", smallUnitMax: 9, selectedSmallUnit: 0, target: self, action: #selector(YourInfoViewController.weightWasSelected(_:smallUnit:element:)), origin: self.view)
			 
            
        }
        
        weightPicker!.setDoneButton(doneButton);
        weightPicker!.setCancelButton(cancelButton);
        weightPicker!.showActionSheetPicker();
        
        
    }
    
    @IBAction func showHeights(){
        
        let heightPicker:ActionSheetDistancePicker?


        if self.usesMetric {
             heightPicker = ActionSheetDistancePicker(title: "Height", bigUnitString: ".", bigUnitMax: 220, selectedBigUnit: 170, smallUnitString: "cm", smallUnitMax: 10, selectedSmallUnit: 0, target: self, action: #selector(YourInfoViewController.heightWasSelected(_:smallUnit:element:)), origin: self.view)
            

        }else{
            heightPicker = ActionSheetDistancePicker(title: "Height", bigUnitString: "'", bigUnitMax: 9, selectedBigUnit: 5, smallUnitString: "\"", smallUnitMax: 10, selectedSmallUnit: 0, target: self, action: #selector(YourInfoViewController.heightWasSelected(_:smallUnit:element:)), origin: self.view)
            
        }
        heightPicker!.setDoneButton(doneButton);
        heightPicker!.setCancelButton(cancelButton);
        heightPicker!.showActionSheetPicker();

        
        
        
    }
    
    func weightWasSelected(bigUnit:NSNumber, smallUnit:NSNumber, element:AnyObject)
    {
        
        if self.usesMetric {
            self.didUpdateWeight(Float( "\(bigUnit).\(smallUnit)")! * LB_PER_KG)
            
        }else{
            self.didUpdateWeight(Float( "\(bigUnit).\(smallUnit)")!)
        }
        
        
        
    }
    
    func heightWasSelected(bigUnit:NSNumber, smallUnit:NSNumber, element:AnyObject)
    {
        if self.usesMetric {
            self.didUpdateHeight(Float(bigUnit.intValue +  (smallUnit.intValue/10)) * INCH_PER_CM);
            
        }else{
            self.didUpdateHeight(Float(bigUnit.intValue * 12 +  smallUnit.intValue));
            
        }
    }
    
    
    
    
    
    @IBAction func showNext(sender: AnyObject) {
        
        //update user object
        
        print("Going with:\(OnboardingViewController.sharedInstance.weightValue) and \(OnboardingViewController.sharedInstance.heightValue) " )
        
        
        self.appDelegate.user?.gender    = self.male.selected ? Gender.Male.rawValue : self.female.selected ? Gender.Female.rawValue : Gender.NotSet.rawValue
        
        self.appDelegate.saveContext();
        
        
        self.sendImage();
        
        //sync data with server
        
    }
    
    func sendImage() {
        let newPic       = self.avatar.image!
        
        if self.picUpdated == true{
            //save image to Core Data
            let picData                    = UIImagePNGRepresentation(newPic)!
            self.appDelegate.user?.picture = picData
            
            //send to server
            JTProgressHUD.show();
            
            OnboardingViewController.sharedInstance.updateProfileImageForUser(self.appDelegate.user!, imageData: picData, completion: { (result) -> Void in
                JTProgressHUD.hide();
                let success = result["success"] as! Bool
                
                if success {
                    //save changes
                    self.appDelegate.saveContext()
                    
                    //send user to next screen
                    self.performSegueWithIdentifier("showNext", sender: nil)
                } else {
                    print(result["errorMessage"])
                    let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your profile picture to our servers, please try again", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        } else {
            //send user to next screen
            JTProgressHUD.hide();
            self.performSegueWithIdentifier("showNext", sender: nil)
        }
    }
    
    
    
    
    // MARK: - UIImagePickerController Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismissViewControllerAnimated(true) { () -> Void in
            
            //crop it to a square
            let croppedImage = image.cropAndResizeTo(self.maxAvatarSize)
            self.picUpdated = true;
            //set picked image in the image view
            self.avatar.image = croppedImage
        }
    }
    
    
    
    func didUpdateWeight(weight: Float) {
        //the weight comming in should always be in pounds
        OnboardingViewController.sharedInstance.weightValue = weight
        
        
        if self.usesMetric {
            let newWeight = weight * KG_PER_LB;
            let units     = " kgs"
            let title     = self.numberFormatter.stringFromNumber(newWeight)! + units
            self.weight.setTitle(title, forState: .Normal)
            
        }else{
            //convert to kg for the UI, if necessary
            let newWeight = weight ;
            let units     = " lbs"
            let title     = self.numberFormatter.stringFromNumber(newWeight)! + units
            
            self.weight.setTitle(title, forState: .Normal)
        }
        self.validateNextButton();
    }
    
    func didUpdateHeight(height: Float) {
        //the height should always come in inches
        OnboardingViewController.sharedInstance.heightValue = height;
        
        
        if self.usesMetric {
            //convert to feet and inches for UI
            let meters = height * CM_PER_INCH;
            let title = String(format: "%.2f cm", meters);
            self.height.setTitle(title, forState: .Normal)
        }else{
            //convert to feet and inches for UI
            let inch = height % 12;
            let feet = (height - inch)/12
            
            let title = "\(self.numberFormatter.stringFromNumber(feet)!)' \(self.numberFormatter.stringFromNumber(inch)!)\""
            self.height.setTitle(title, forState: .Normal)
        }
        
        self.validateNextButton();
    }
    
    func validateNextButton(){
        
        if OnboardingViewController.sharedInstance.heightValue > 0 && OnboardingViewController.sharedInstance.weightValue > 0 {
            self.nxtButton.backgroundColor = UIColor.geiaYellowColor();
            self.nxtButton.enabled = true;
        }
    }
    
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    }
}