//
//  VitailsWeekViewController.swift
//  Geia
//
//  Created by haley on 6/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Charts
class VitalsWeekViewController: UIViewController {
	
	weak var emotionLineView: BarChartBaseView!
	weak var restingHeartRateLineView: BarChartBaseView!
	weak var maxHeartRateLineView: BarChartBaseView!
	weak var weightLineView: BarChartBaseView!
	weak var bodyMassIndexLineView: BarChartBaseView!
	
	@IBOutlet weak var vernierLeftConstraint: NSLayoutConstraint!
	
	@IBOutlet var averageEmotion : UILabel!
	@IBOutlet var averageRHR : UILabel!
	@IBOutlet var averageMHR : UILabel!
	@IBOutlet var averageWeight : UILabel!
	@IBOutlet var averageBMI : UILabel!
	
	var totalEmotion :Double!
	var totalRHR :Double!
	var totalMHR :Double!
	var totalWeight :Double!
	var totalBMI :Double!
	
	var totalEmotionDay :Double!
	var totalRHRDay :Double!
	var totalMHRDay :Double!
	var totalWeightDay :Double!
	var totalBMIDay :Double!
	
	var customData: BarChartData!
	
	var maxRHR :Double!
	var maxMHR :Double!
	var maxWeight :Double!
	var maxBMI :Double!
	
	var xVals: [String] = [String]()
	var emotionVals: [ChartDataEntry] = [ChartDataEntry]()
	var restingHeartRateVals : [ChartDataEntry] = [ChartDataEntry]()
	var maxHeartRateVals : [ChartDataEntry] = [ChartDataEntry]()
	var weightVals  : [ChartDataEntry] = [ChartDataEntry]()
	var bodyMassIndexVals  : [ChartDataEntry] = [ChartDataEntry]()
	var color :UIColor = UIColor(red:22.0/255,green:154.0/255,blue:218.0/255,alpha:1.0)
	var holdColor :UIColor = UIColor(red:35.0/255,green:40.0/255,blue:44.0/255,alpha:1.0)
	
	
	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	private lazy var loaderView: RPLoadingAnimationView = {
		return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
	}()
	
	private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
		return GoalTrackingViewModel()
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.totalBMI = 0
		self.totalMHR = 0
		self.totalWeight = 0
		self.totalEmotion = 0
		self.totalRHR = 0
		
		self.totalBMIDay = 0
		self.totalMHRDay = 0
		self.totalRHRDay = 0
		self.totalWeightDay = 0
		self.totalEmotionDay = 0
		
		self.setupGraph()
		self.updateUserData()
	}
	
	class func instanceFromStoryBord() -> VitalsWeekViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Vitals", bundle: NSBundle.mainBundle())
		let vitalsWeekViewController = storyboard.instantiateViewControllerWithIdentifier("VitalsWeekViewController") as!VitalsWeekViewController
		return vitalsWeekViewController
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	func updateUserData() {
		self.goalTrackingViewModel.updateNutritionDataFromHealthKitForDateRange(.Week) { (success) -> Void in
			if success {
				//save changes
				self.appDelegate.saveContext()
				
				self.loadActivityData()
			}
		}
		
	}
	
	func loadActivityData() {
		self.loaderView.show()
		self.goalTrackingViewModel.getVitalsForUserByDate(NSDate(),user:  self.appDelegate.user!, dateRange: .Week) { (result) -> Void in
			self.loaderView.hide(animated: true)
			self.clearChartValue()
			self.setupDataSource(result["data"] as! [Dictionary<String,AnyObject>])
		}
		
	}

	
	func clearChartValue() {
		self.bodyMassIndexLineView.data?.clearValues()
		self.emotionLineView.data?.clearValues()
		self.maxHeartRateLineView.data?.clearValues()
		self.restingHeartRateLineView.clearValues()
		self.maxHeartRateLineView.clearValues()
	}
	
	func setupDataSource(dataPoints:[Dictionary<String,AnyObject>]) {
		
		for i in 0 ... dataPoints.count-1 {
			
			let diff = dataPoints.count-1 - i
			
			let dic : Dictionary<String,AnyObject> = dataPoints[diff]
			let emotion   = dic["emotion"]
			let rhr = dic["rhr"]
			let maxHR = dic["maxHR"]
			let bmi = dic["bmi"]
			let weight = dic["weight"]
			
			if bmi?.doubleValue != -1 {
				self.totalBMI = self.totalBMI + (bmi?.doubleValue)!
				self.totalBMIDay =  self.totalBMIDay + 1
			}
			
			if maxHR?.doubleValue != -1 {
				self.totalMHR = self.totalMHR + (maxHR?.doubleValue)!
				self.totalMHRDay =  self.totalMHRDay + 1
			}
			
			if weight?.doubleValue != -1 {
				self.totalWeight = self.totalWeight + (weight?.doubleValue)!
				self.totalWeightDay = self.totalWeightDay + 1
			}
			
			if emotion?.doubleValue != -1 {
				self.totalEmotion = self.totalEmotion+(emotion?.doubleValue)!
				self.totalEmotionDay = self.totalEmotionDay + 1
			}
			
			if rhr?.doubleValue != -1 {
			
				self.totalRHR = self.totalRHR+(rhr?.doubleValue)!
				self.totalRHRDay = self.totalRHRDay + 1
			}
			
			
			
            let df        = NSDateFormatter()
            let xValue    = dic["timestamp"] as! NSDate
            
            if i == 0 {
                df.dateFormat = "MMM d"
            } else {
                df.dateFormat = "d"
            }
            
			let category  = df.stringFromDate(xValue)
			self.xVals.append(("\(category)"))
			
			
			if rhr?.doubleValue > self.maxRHR {
				self.maxRHR = rhr?.doubleValue
			}
			
			if weight?.doubleValue > self.maxWeight {
				self.maxWeight = weight?.doubleValue
			}
			
			if bmi?.doubleValue > self.maxBMI{
    
				self.maxBMI = bmi?.doubleValue
			}
			
			if maxHR?.doubleValue > self.maxMHR{
    
				self.maxMHR = maxHR?.doubleValue
			}
			
			self.emotionVals.append(BarChartDataEntry(value: (emotion?.doubleValue)!, xIndex: i))
			self.restingHeartRateVals.append(BarChartDataEntry(value: (rhr?.doubleValue)!, xIndex: i))
			self.maxHeartRateVals.append(BarChartDataEntry(value: (maxHR?.doubleValue)!, xIndex: i))
			self.bodyMassIndexVals.append(BarChartDataEntry(value: (bmi?.doubleValue)!, xIndex: i))
			self.weightVals.append(BarChartDataEntry(value: (weight?.doubleValue)!, xIndex: i))
		}
		
		if self.maxRHR < 90 {
			self.maxRHR = 100
		} else {
			
			self.maxRHR = self.maxRHR + 10
		}
		
		if self.maxMHR < 160 {
			self.maxMHR = 170
		} else {
			
			self.maxMHR = self.maxMHR + 10
		}
		
		if self.maxWeight < 180 {
			self.maxWeight = 190
		} else {
			
			self.maxWeight = self.maxWeight + 10
		}
		
		if self.maxBMI < 24 {
			self.maxBMI = 34
		} else {
		
			self.maxBMI = self.maxBMI + 10
		}
		
		
		self.restingHeartRateLineView.leftAxis.axisMaxValue = self.maxRHR
		self.weightLineView.leftAxis.axisMaxValue = self.maxWeight
		self.emotionLineView.leftAxis.axisMaxValue = 1.0
		self.bodyMassIndexLineView.leftAxis.axisMaxValue = self.maxBMI
		self.maxHeartRateLineView.leftAxis.axisMaxValue = self.maxMHR
		
		
		var emotionSet :BarChartDataSet
		if self.emotionLineView.data?.dataSetCount > 0 {
			emotionSet = self.emotionLineView.data?.dataSets[0] as! BarChartDataSet
			emotionSet.yVals = self.emotionVals
			self.emotionLineView.data!.xValsObjc = self.xVals
			self.emotionLineView.notifyDataSetChanged()
		}
		else {
			
			emotionSet = BarChartDataSet(yVals: self.emotionVals, label: "DataSet")
			emotionSet.drawValuesEnabled = false;
			
			emotionSet.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			var dataSets = [IChartDataSet]()
			dataSets.append(emotionSet)
			let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
			self.emotionLineView.data = data;
		}
		
		
		var rhrSet :BarChartDataSet
		if self.restingHeartRateLineView.data?.dataSetCount > 0 {
			rhrSet = self.restingHeartRateLineView.data?.dataSets[0] as! BarChartDataSet
			rhrSet.yVals = self.restingHeartRateVals
			self.restingHeartRateLineView.data!.xValsObjc = self.xVals
			self.restingHeartRateLineView.notifyDataSetChanged()
		}
		else {
			
			rhrSet = BarChartDataSet(yVals: self.restingHeartRateVals, label: "DataSet")
			rhrSet.drawValuesEnabled = false;
			
			rhrSet.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			var dataSets = [IChartDataSet]()
			dataSets.append(rhrSet)
			let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
			self.restingHeartRateLineView.data = data;
		}

		
		
		var weightSet :BarChartDataSet
		if self.weightLineView.data?.dataSetCount > 0 {
			weightSet = self.weightLineView.data?.dataSets[0] as! BarChartDataSet
			weightSet.yVals = self.weightVals
			self.weightLineView.data!.xValsObjc = self.xVals
			self.weightLineView.notifyDataSetChanged()
		}
		else {
			
			weightSet = BarChartDataSet(yVals: self.weightVals, label: "DataSet")
			weightSet.drawValuesEnabled = false;
			
			weightSet.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			var dataSets = [IChartDataSet]()
			dataSets.append(weightSet)
			let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
			self.weightLineView.data = data;
		}
		
		
		
		var bodySet :BarChartDataSet
		if self.bodyMassIndexLineView.data?.dataSetCount > 0 {
			bodySet = self.bodyMassIndexLineView.data?.dataSets[0] as! BarChartDataSet
			bodySet.yVals = self.bodyMassIndexVals
			self.bodyMassIndexLineView.data!.xValsObjc = self.xVals
			self.bodyMassIndexLineView.notifyDataSetChanged()
		}
		else {
			
			bodySet = BarChartDataSet(yVals: self.bodyMassIndexVals, label: "DataSet")
			bodySet.drawValuesEnabled = false;
			
			bodySet.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			var dataSets = [IChartDataSet]()
			dataSets.append(bodySet)
			let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
			self.bodyMassIndexLineView.data = data;
		}

		
		
		var maxSet :BarChartDataSet
		if self.maxHeartRateLineView.data?.dataSetCount > 0 {
			maxSet = self.maxHeartRateLineView.data?.dataSets[0] as! BarChartDataSet
			maxSet.yVals = self.maxHeartRateVals
			self.maxHeartRateLineView.data!.xValsObjc = self.xVals
			self.maxHeartRateLineView.notifyDataSetChanged()
		}
		else {
			
			maxSet = BarChartDataSet(yVals: self.maxHeartRateVals, label: "DataSet")
			maxSet.drawValuesEnabled = false;
			
			maxSet.colors = [UIColor.init(red:0, green: 169.0/255, blue: 225.0/255, alpha: 1.0)]
			
			var dataSets = [IChartDataSet]()
			dataSets.append(maxSet)
			let data = BarChartData(xVals: self.xVals, dataSets: dataSets)
			self.maxHeartRateLineView.data = data;
		}
		
		
		let nf = NSNumberFormatter()
		nf.maximumFractionDigits = 1
		nf.usesGroupingSeparator = true
		
		if self.totalRHRDay == 0 {
			self.averageRHR.text = "Daily Average: N/A"
		} else {
			
			self.averageRHR.text = "Daily Average: \((nf.stringFromNumber(self.totalRHR/self.totalRHRDay))!)"
		}
		
		if self.totalBMIDay == 0 {
			self.averageBMI.text = "Daily Average: N/A"
		} else {
			self.averageBMI.text = "Daily Average: \((nf.stringFromNumber(self.totalBMI/self.totalBMIDay))!)"
		}
		
		if self.totalEmotionDay == 0 {
			self.averageEmotion.text = "Daily Average: N/A"
		} else {
			self.averageEmotion.text = "Daily Average: \((nf.stringFromNumber(self.totalEmotion/self.totalEmotionDay))!)"
		}
		
		
		if self.totalWeightDay == 0 {
			self.averageWeight.text = "Daily Average: N/A"
		} else {
			self.averageWeight.text = "Daily Average: \((nf.stringFromNumber(self.totalWeight/self.totalWeightDay))!)"
		}
		
		if self.totalMHRDay == 0 {
			self.averageMHR.text = "Daily Average: N/A"
		} else {
			self.averageMHR.text = "Daily Average: \((nf.stringFromNumber(self.totalMHR/self.totalMHRDay))!)"
		}
		
//		self.addLimitLine(self.emotionLineView, lowValue: 60, highValue: 100)
//		
//		self.addLimitLine(self.restingHeartRateLineView, lowValue: 40, highValue: 90)
		
		
//		self.addLimitLine(self.maxHeartRateLineView, lowValue: 120, highValue: 160)
//		self.addLimitLine(self.weightLineView, lowValue: 120, highValue: 180)
//		self.addLimitLine(self.bodyMassIndexLineView, lowValue: 8, highValue: 24)
		
		if self.emotionLineView.positions.count > 0 {
			self.vernierLeftConstraint.constant = (self.emotionLineView.positions.last!.x - 10)
		}
		
		

	}
	
	func addLimitLine(chartView:LineChartBaseView,lowValue:Double,highValue:Double){
		
		
		let ll : ChartLimitLine = ChartLimitLine.init(limit: lowValue, label: "\(lowValue)");
		ll.lineWidth = 1.0;
		
		ll.lineColor = UIColor.init(red:47.0/255, green: 47.0/255, blue: 47.0/255, alpha: 1.0)
		ll.valueFont = UIFont.systemFontOfSize(10);
		ll.label2 = "\(lowValue)"
		ll.labelPosition = .Left;
		ll.valueTextColor = UIColor.whiteColor()
		ll.valueTextColor2 = UIColor.whiteColor()
		
		
		let ll2 : ChartLimitLine = ChartLimitLine.init(limit: highValue, label: "\(highValue)");
		ll2.lineWidth = 1.0;
		
		ll2.lineColor = UIColor.init(red:47.0/255, green: 47.0/255, blue: 47.0/255, alpha: 1.0)
		ll2.valueFont = UIFont.systemFontOfSize(10);
		ll2.label2 = "\(highValue)"
		ll2.labelPosition = .Left;
		ll2.valueTextColor = UIColor.whiteColor()
		ll2.valueTextColor2 = UIColor.whiteColor()
		chartView.leftAxis.addLimitLine(ll)
		chartView.leftAxis.addLimitLine(ll2)
	}
	
	func setupGraph(){
		self.customData = BarChartBaseView.setupDataByDate(NSDate(),dateRange:.Week)
		self.emotionLineView.setupBarChartView()
		self.emotionLineView.leftAxis.showOnlyMaxEnabled = true
		self.emotionLineView.data = self.customData
		self.restingHeartRateLineView.setupBarChartView()
		self.restingHeartRateLineView.leftAxis.showOnlyMaxEnabled = true
		self.restingHeartRateLineView.data = self.customData
		
		self.maxHeartRateLineView.setupBarChartView()
		self.maxHeartRateLineView.leftAxis.showOnlyMaxEnabled = true
		self.maxHeartRateLineView.data = self.customData
		
		self.bodyMassIndexLineView.setupBarChartView()
		self.bodyMassIndexLineView.leftAxis.showOnlyMaxEnabled = true
		self.bodyMassIndexLineView.data = self.customData
		
		self.weightLineView.setupBarChartView()
		self.weightLineView.leftAxis.showOnlyMaxEnabled = true
		self.weightLineView.data = self.customData
	}
}
