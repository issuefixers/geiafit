//
//  VitalsDailyViewController.swift
//  Geia
//
//  Created by haley on 6/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

protocol VitalsGoalTrackingViewDelegate: class {
	func didBeginEditing(frame: CGRect)
	func didExpandBox(delta: CGFloat, frame: CGRect)
}

enum VitalSign: Int {
	case Weight, RHR, HR, BMI, Emotion
}

class VitalsDailyViewController: UIViewController ,UITextFieldDelegate,EmotionNotificationViewDelegate{
	
	@IBOutlet weak var emotionImage: UIImageView!
	
	@IBOutlet weak var emotionNotificationView: EmotionNotificationView!
	@IBOutlet weak var vitalsPopupView:UIView!
	@IBOutlet weak var titleForPopupView:UILabel!
	
	@IBOutlet weak var emotionButton : UIButton!
	
	@IBOutlet weak var weightButton: UIButton!
	@IBOutlet weak var restingHRButton: UIButton!
	@IBOutlet weak var hrButton: UIButton!
	@IBOutlet weak var bmiButton: UIButton!
	
	@IBOutlet weak var weight: UILabel!
	@IBOutlet weak var restingHR: UILabel!
	@IBOutlet weak var maxHR: UILabel!
	@IBOutlet weak var bmi: UILabel!
	@IBOutlet weak var emotion: UISlider!
	
	@IBOutlet weak var textValue:UITextField!
	
	@IBOutlet weak var titleLabel : UILabel!
	var vitalSign:VitalSign = VitalSign.Weight
	
	weak var delegate: VitalsGoalTrackingViewDelegate?
	
	private var currentDate: NSDate!
	
	private let compactHeight  = CGFloat(37)
	private let expandedHeight = CGFloat(130.0)
	private var currentWeight  = 0.0
	private var currentRHR     = 0
	private var currentMaxHR   = 0
	private var currentBMI     = 0.0
	private var currentEmotion = 0.0
	private var todaysCharacteristics: Characteristics?
	
	weak var emotionNotificationDelegate: EmotionNotificationViewDelegate?
	
	
	private lazy var numberFormatter:NSNumberFormatter = {
		let nf         = NSNumberFormatter()
		nf.locale      = NSLocale.currentLocale()
		nf.numberStyle = .DecimalStyle
		
		return nf
	}()
	
	private lazy var dateFormatter:NSDateFormatter = {
		let df       = NSDateFormatter()
		df.locale    = NSLocale.currentLocale()
		df.timeStyle = .NoStyle
		df.dateStyle = .MediumStyle
		
		return df
	}()
	
	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	private lazy var goalTrackingViewModel: GoalTrackingViewModel = {
		return GoalTrackingViewModel()
	}()
	
	private lazy var onboardingViewModel: OnboardingViewModel = {
		return OnboardingViewModel()
	}()
	
	private lazy var loaderView: RPLoadingAnimationView = {
		return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
	}()
	
	class func instanceFromStoryBord() -> VitalsDailyViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Vitals", bundle: NSBundle.mainBundle())
		let vitalsDailyViewController = storyboard.instantiateViewControllerWithIdentifier("VitalsDailyViewController") as!VitalsDailyViewController
		return vitalsDailyViewController
	}

	// MARK: Load Data
	func updateDataFromServer() {
		
		self.onboardingViewModel.getCharacteristicsForUser(self.appDelegate.user!) { (result) -> Void in
			//save changes
			self.appDelegate.saveContext()
			self.loadData()
		}
	}
	
	
	func loadActivityDataForDay() {
		
		self.goalTrackingViewModel.getVitalsForUserByDate(self.currentDate , user: self.appDelegate.user!, dateRange: .Day) { (result) -> Void in
			self.setupDataSource(result["data"] as! [Dictionary<String,AnyObject>])
		}
		self.hideLoadingView()
	}
	
	func setupDataSource(dataPoints:[Dictionary<String,AnyObject>]) {
		self.hideLoadingView()
		if dataPoints.count>0 {
			let dic : Dictionary<String,AnyObject> = dataPoints[0]
			let df        = NSDateFormatter()
			
			df.dateFormat = "d"
			let xValue    = dic["timestamp"] as! NSDate
			if (self.currentDate.isEqualToDateIgnoringTime(xValue)) {
				
				if let weightValue = dic["weight"] {
					self.currentWeight                 = weightValue as! Double
				}
				
				if let rhrValue = dic["rhr"] {
					self.currentRHR                              = rhrValue as! Int
				}
				
				if let maxHRValue = dic["maxHR"] {
					self.currentMaxHR                        = maxHRValue as! Int
				}
				
				if let bmiValue = dic["bmi"] {
					self.currentBMI                 = bmiValue as! Double
				}
				
				if let emotionValue = dic["emotion"] {
					self.currentEmotion = emotionValue as! Double
				} else {
					self.currentEmotion = 1.0
				}
				
				
			} else {
			
				self.currentEmotion = -1
				self.currentBMI = -1
				self.currentMaxHR = -1
				self.currentRHR = -1
				self.currentWeight = -1
			}
			
			
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					
					
					if self.currentWeight == -1 {
						self.weight.text = "N/A LBS"
					} else {
						self.weight.text = "\(self.numberFormatter.stringFromNumber(self.currentWeight)!) LBS"
					}
					
					if self.currentRHR == -1 {
						self.restingHR.text = "N/A BPM"
					} else {
						self.restingHR.text = "\(self.currentRHR) BPM"
					}
					
					
					if self.currentMaxHR == -1 {
						self.maxHR.text = "N/A BPM"
					} else {
						self.maxHR.text = "\(self.currentMaxHR) BPM"
					}
					
					if self.currentBMI == -1 {
						self.bmi.text  = "N/A"
					} else {
						
						self.bmi.text  = self.numberFormatter.stringFromNumber(self.currentBMI)
					}
					
					
					
					if self.currentEmotion == -1 {
						self.emotion.value = -1
						let image = UIImage.init(named:"emotion-7")
						self.emotionImage.image = image
						self.emotionButton.setImage(image, forState:.Normal)
					} else {
						self.emotion.value = Float(self.currentEmotion)
						self.emotionImage.image = self.goalTrackingViewModel.getEmotionImageForValue(self.emotion.value)
						self.emotionButton.setImage(self.goalTrackingViewModel.getEmotionImageForValue(self.emotion.value), forState:.Normal)
					}
					
				})
			
		}
		
	
	}
	
	
	func loadData() {
		
		self.goalTrackingViewModel.getVitalsForUser(self.appDelegate.user!, dateRange: .Day) { (result) -> Void in
			let success = result["success"] as! Bool
			
			if success {
				
				self.todaysCharacteristics = self.goalTrackingViewModel.getTodaysCharacteristicsObject()
				
				if let data = result["data"] {
					//debugPrint(result)
					
					if let weightValue = data["weight"] {
						
						self.currentWeight                 = weightValue as! Double
						self.todaysCharacteristics?.weight = self.currentWeight
					}
					
					if let rhrValue = data["rhr"] {
						self.currentRHR                              = rhrValue as! Int
						self.todaysCharacteristics?.restingHeartRate = self.currentRHR
					}
					
					if let maxHRValue = data["maxHR"] {
						self.currentMaxHR                        = maxHRValue as! Int
						self.todaysCharacteristics?.maxHeartRate = self.currentMaxHR
					}
					
					if let bmiValue = data["bmi"] {
						self.currentBMI                 = bmiValue as! Double
						self.todaysCharacteristics?.bmi = self.currentBMI
					}
					
					if let emotionValue = data["emotion"] {
						self.currentEmotion = emotionValue as! Double
					} else {
						self.currentEmotion = 1.0
					}
					self.todaysCharacteristics?.emotion = self.currentEmotion
					
					dispatch_async(dispatch_get_main_queue(), { () -> Void in
						
						
						if self.currentWeight == -1 {
							self.weight.text = "N/A LBS"
						} else {
							self.weight.text = "\(self.numberFormatter.stringFromNumber(self.currentWeight)!) LBS"
						}
						
						if self.currentRHR == -1 {
							self.restingHR.text = "N/A BPM"
						} else {
							self.restingHR.text = "\(self.currentRHR) BPM"
						}
						
						
						if self.currentMaxHR == -1 {
							self.maxHR.text = "N/A BPM"
						} else {
							self.maxHR.text = "\(self.currentMaxHR) BPM"
						}
						
						if self.currentBMI == -1 {
							self.bmi.text  = "N/A"
						} else {
						
							self.bmi.text  = self.numberFormatter.stringFromNumber(self.currentBMI)
						}
						
						if self.currentEmotion == -1 {
							self.emotion.value = -1
							let image = UIImage.init(named:"emotion-7")
							self.emotionImage.image = image
							self.emotionButton.setImage(image, forState:.Normal)
						} else {
							self.emotion.value = Float(self.currentEmotion)
							self.emotionImage.image = self.goalTrackingViewModel.getEmotionImageForValue(self.emotion.value)
							self.emotionButton.setImage(self.goalTrackingViewModel.getEmotionImageForValue(self.emotion.value), forState:.Normal)
						}
						
						
						
						
					})
				}
				
				self.hideLoadingView()
				
			} else{
			
				self.hideLoadingView()
			}
		}
	}
	
	
	
	// MARK: Update the date
	@IBAction func previousButtonClicked() {
		self.showLoadingView()
		let date =  self.currentDate.dateByAddingDays(-1)
		
		self.currentDate = date!
		self.refreshData()
	}
	
	@IBAction func nextButtonClicked() {
		self.showLoadingView()
		let date =  self.currentDate.dateByAddingDays(1)
		
		self.currentDate = date!
		self.refreshData()
		
		
	}
	
	
	func  refreshData() {
		NSUserDefaults.standardUserDefaults().setObject(self.currentDate, forKey: CURRENT_DATE_KEY)
		NSUserDefaults.standardUserDefaults().synchronize()
		
		
		if(self.currentDate.isEqualToDateIgnoringTime(NSDate())) {
			self.updateDataFromServer()
			self.updateButtonStatus(true)
		}
		else {
			
			if self.currentDate.isLaterThanDate(NSDate()) {
				
				
				var dataPoints: [Dictionary<String,AnyObject>] = []
				
				let tempDay : NSDate = self.currentDate.dateAtStartOfDay()
				
				let dataPoint = ["weight": -1,        "rhr": -1,
				                 "maxHR": -1,  "bmi": -1,
				                 "emotion": -1, "timestamp":tempDay]
				print(dataPoint)
				dataPoints.append(dataPoint)
				
				self.setupDataSource(dataPoints)
				
			}
			else {
				
				self.loadActivityDataForDay()
			}
			
			self.updateButtonStatus(false)
		}
		
		
		let formattedDate = self.dateFormatter.stringFromDate(self.currentDate)
		self.titleLabel.text = formattedDate
	}
	
	// MARK: Loading Animation
    func showLoadingView() {
        self.loaderView.show()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func hideLoadingView() {
        self.loaderView.hide(animated: true)
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }

	func checkDate() {
		
		let date = NSUserDefaults.standardUserDefaults().objectForKey(CURRENT_DATE_KEY) as! NSDate?
		if date != nil {
			
			if !self.currentDate.isEqualToDate(date!) {
				self.currentDate = date
			}
			
		} else {
			
			NSUserDefaults.standardUserDefaults().setObject(self.currentDate, forKey: CURRENT_DATE_KEY)
			NSUserDefaults.standardUserDefaults().synchronize()
		}
	}
	
	func refreshUI() {
		self.currentDate = NSDate()
		self.checkDate()
		self.refreshData()
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.showLoadingView()
		self.emotionNotificationView.delegate = self
//		self.updateDataFromServer()
		self.emotionNotificationView.hidden = true
		self.vitalsPopupView.hidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

	// MARK:Action
	
	func  updateButtonStatus(canClick:Bool) {
		self.emotionButton.userInteractionEnabled = canClick
		self.weightButton.userInteractionEnabled = canClick
		self.restingHRButton.userInteractionEnabled = canClick
		self.hrButton.userInteractionEnabled = canClick
		self.bmiButton.userInteractionEnabled = canClick
	}
	
	@IBAction func emotionValueDidChange(sender: UISlider) {
		self.emotionImage.image = self.goalTrackingViewModel.getEmotionImageForValue(sender.value)
	}
	
	@IBAction func updateValue(sender: UIButton) {
        
        if (self.textValue.text?.characters.count == 0) {
            return
        }
        
		switch self.vitalSign {
		case .Weight:
			// TODO: Validate values
			self.currentWeight                 = self.numberFormatter.numberFromString(self.textValue.text!) as! Double
			self.todaysCharacteristics?.weight = self.currentWeight
			self.weight.text = "\(self.numberFormatter.stringFromNumber(self.currentWeight)!) LBS"
		case .RHR:
			self.currentRHR                              = self.numberFormatter.numberFromString(self.textValue.text!) as! Int
			self.todaysCharacteristics?.restingHeartRate = self.currentRHR
			self.restingHR.text = "\(self.currentRHR) BPM"
		case .HR:
			self.currentMaxHR                        = self.numberFormatter.numberFromString(self.textValue.text!) as! Int
			self.todaysCharacteristics?.maxHeartRate = self.currentMaxHR
			self.maxHR.text = "\(self.currentMaxHR) BPM"

		case .BMI:
			self.currentBMI                 = self.numberFormatter.numberFromString(self.textValue.text!) as! Double
			self.todaysCharacteristics?.bmi = self.currentBMI
			self.bmi.text = self.numberFormatter.stringFromNumber(self.currentBMI)!
		case .Emotion:
			self.currentEmotion                 = Double(self.emotionNotificationView.textBox.text!)!
			self.todaysCharacteristics?.emotion = self.currentEmotion
			self.emotionImage.image             = self.goalTrackingViewModel.getEmotionImageForValue(Float(self.emotionNotificationView.textBox.text!)!)
		self.emotionButton.setImage(self.goalTrackingViewModel.getEmotionImageForValue(Float(self.emotionNotificationView.textBox.text!)!), forState:.Normal)
		}
		
		// Save Values to DB
		self.appDelegate.saveContext()
		self.uploadVitals()
		//collapse the view
		self.ButtonClicked(sender)
		self.vitalsPopupView.hidden = true
		self.emotionNotificationView.hidden = true
	}
	
	
	func uploadVitals() {
		//update record date
		self.todaysCharacteristics?.recordDate = NSDate().dateAtStartOfDay()
		
		// Save Values to DB
		self.appDelegate.saveContext()
		self.onboardingViewModel.updateCharacteristics(self.todaysCharacteristics!) { (result) -> Void in
		}
	}
	
	@IBAction func cancleBtnClick(sender: UIButton) {
		self.textValue.resignFirstResponder()
		self.vitalsPopupView.hidden = true
		self.textValue.text = ""
		self.emotionNotificationView.hidden = true
	}
	
	// MARK: UITextFieldDelegate

	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		
		textField.resignFirstResponder()
		return true
	}
	
	@IBAction func emotionClick(sender:UIButton!){
		self.emotionNotificationView.setupView()
		self.emotionNotificationView.hidden = false
		self.emotionNotificationDelegate?.showEmotionalNotification!()
	}
	
	@IBAction func ButtonClicked(sender: UIButton) {
		
		self.textValue.resignFirstResponder()
		//var vitalSign:VitalSign = VitalSign.RHR
		if sender.tag == 10{
			vitalSign = .RHR
		}
		else if sender.tag == 20{
			vitalSign = .HR
		}
		else if sender.tag == 30{
			vitalSign = .Weight
		}
		else if sender.tag == 40{
			vitalSign = .BMI
		}
		
		//let VitalSigns = VitalSign(rawValue: sender.tag)!
		UIView.animateWithDuration(0.2) { () -> Void in
			switch self.vitalSign {
			case .Weight:
				self.vitalsPopupView.hidden = false
				self.titleForPopupView.text = "Weight"
				
			case .RHR:
				self.vitalsPopupView.hidden = false
				self.titleForPopupView.text = "Resting Heart Rate"
				
			case .HR:
				self.vitalsPopupView.hidden = false
				self.titleForPopupView.text = "Max Heart Rate"
			case .BMI:
				self.vitalsPopupView.hidden = false
				self.titleForPopupView.text = "Body Mass Index"
				
			case .Emotion:
				self.vitalsPopupView.hidden = false
				self.titleForPopupView.text = "Resting Heart Rate"
			}
		}
	}
	
	func emotionValueDidChange() {
		self.emotionNotificationDelegate?.emotionValueDidChange()
		uploadVitals()
		self.loadData()
	}
	
	func showEmotionalNotification() {
		self.emotionNotificationView.setupView()
		self.emotionNotificationView.hidden = false
		self.textValue.text = ""
	}

}
