//
//  GoalTrackingViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/11/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

enum GoalTrackingSection: Int {
    case Activity, Steps, Vitals
}

class GoalTrackingViewController: GeiaViewController {

    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var dailyNutritionView: DailyNutritionTrackingView!
    @IBOutlet weak var weeklyNutritionView: WeeklyNutritionGoalTrackingView!
	
    @IBOutlet weak var singleDayButton: UIButton!
    @IBOutlet weak var wholeWeekButton: UIButton!
    @IBOutlet weak var activityTab: UIButton!
    @IBOutlet weak var stepsTab: UIButton!
	
    @IBOutlet weak var vitalsTab: UIButton!

    
    var addCustoms: UIBarButtonItem!;
	
	@IBOutlet var stepsViewContent : UIView!
	@IBOutlet var vitalsViewContent : UIView!
	@IBOutlet var activityViewContent : UIView!
	
	var stepRootViewController : StepsRootViewController!
	var actRootViewController : ActivityRootViewController!
	var vitalsRootViewController : VitalsRootViewController!
	
    // MARK: - Local variables
    var currentSection: GoalTrackingSection = .Activity
    private var dayCount: DateRange = .Day
    private var scrollViewHeightNormal:CGFloat        = 0.0
    private var scrollViewHeightKeyboardShown:CGFloat = 0.0
    private var currentTextFieldRect:CGRect           = CGRect.zero

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

		self.setupStepView()
		self.setupActivityView()
		self.setupvitalsView()
        self.appDelegate.blockRotation = true
        self.addCustoms = UIBarButtonItem(title: "Add", style: .Done, target: self, action: #selector(GoalTrackingViewController.newCustomValue))
        self.navigationItem.rightBarButtonItem = self.addCustoms;
        
    }

    
    func newCustomValue(){
//        let alertController = UIAlertController(title: "Add Custom Value", message: "What do you wanna add?", preferredStyle: .ActionSheet);
//        alertController.addAction(UIAlertAction(title: "Steps", style: .Default, handler: { (action) in
//            let alertInput = UIAlertController(title: "Add Custom Value", message: "How Many Steps did you do?", preferredStyle: .Alert);
//            alertInput.addTextFieldWithConfigurationHandler({ (textfield) in
//                textfield.placeholder = "Ex 3000 Steps"
//                textfield.keyboardType = .NumberPad;
//                
//            })
//            
//            alertInput.addAction(UIAlertAction(title: "Add Steps", style: .Default, handler: { (actions) in
//                print(alertInput.textFields![0].text);
//                self.actRootViewController.addCustomData(Int(alertInput.textFields![0].text!)!);
//                alertController.dismissViewControllerAnimated(true, completion: nil);
//
//            }));
//            
//            alertInput.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler: { (action) in
//                alertController.dismissViewControllerAnimated(true, completion: nil);
//                
//            }));
//            
//            self.presentViewController(alertInput, animated: true, completion: nil);
//            
//           // alertController.dismissViewControllerAnimated(true, completion: nil);
//        }));
//        alertController.addAction(UIAlertAction(title: "Time", style: .Default, handler: { (action) in
//            alertController.dismissViewControllerAnimated(true, completion: nil);
//
//        }));
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler: { (action) in
//            alertController.dismissViewControllerAnimated(true, completion: nil);
//
//        }));

        let alertController = UIStoryboard.getActivityStoryboard().instantiateViewControllerWithIdentifier("AddCustomStepViewController") as? AddCustomStepViewController;
        alertController!.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
//        alertController!.modalTransitionStyle = .CrossDissolve;
        self.navigationController!.presentViewController(alertController!, animated: true, completion: nil);
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GoalTrackingViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GoalTrackingViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)

        //Setup the MenuView here because when viewDidLoad is executed
        //and the user is coming from the Onboarding storyboard, the menu
        //gets added to the wrong view. Adding it here ensures that it's added
        //correctly

        // Setup the tabs and section views
        let sender = self.currentSection == .Activity ? self.activityTab :
                     self.currentSection == .Steps ? self.stepsTab :
					 self.vitalsTab
        self.switchTab(sender)
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.set(kGAIScreenName, value: "Goal Tracking")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        appDelegate.blockRotation = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Actions
    @IBAction func toggleDateRange(sender: UIButton) {
        // Deselect All
        self.singleDayButton.selected = false
        self.wholeWeekButton.selected = false
        sender.selected = true

        self.dayCount = sender == self.singleDayButton ? .Day : .Week

        switch self.currentSection {
        case .Activity:
			self.activityViewContent.hidden = false
			self.actRootViewController.refrshData()

        case .Steps:
			self.stepsViewContent.hidden = false
			self.stepRootViewController.refrshData()
        case .Vitals:
			self.vitalsViewContent.hidden = false
			self.vitalsRootViewController.refrshData()
            return
        }
    }

    @IBAction func switchTab(sender: UIButton) {
       

        self.currentSection = sender == self.activityTab ? .Activity :
                              sender == self.stepsTab ? .Steps :
                               .Vitals

        
        switch(self.currentSection){
            case .Activity:
                
                self.navigationItem.rightBarButtonItem  = self.addCustoms;
                break;
            default:
                self.navigationItem.rightBarButtonItem = nil;
                break;
        }
        
        self.changeTabs();
       
    }

    
    func changeTabs()  {
        
        // Deselect All
        
        self.activityTab.selected  = false
        self.stepsTab.selected     = false
		
        self.vitalsTab.selected    = false
        // Hide all views
		
        self.dailyNutritionView.hidden  = true
        self.weeklyNutritionView.hidden = true
        
		self.stepsViewContent.hidden = true
		self.vitalsViewContent.hidden = true
		self.activityViewContent.hidden = true
		
        switch self.currentSection {
        case .Activity: self.activityTab.selected  = true
            break
        case .Vitals: self.vitalsTab.selected    = true
            break;
        case .Steps: self.stepsTab.selected     = true
            break;
     
        }
        // Show the selected one
        let sender = self.dayCount == .Day ? self.singleDayButton : self.wholeWeekButton
        self.toggleDateRange(sender)
    }

    // MARK: - Heyboard Handling
    func keyboardWillShow(notification:NSNotification) {
        self.scrollView.scrollRectToVisible(self.currentTextFieldRect, animated: true)
    }

    func keyboardWillHide(notification:NSNotification) {
        self.scrollView.contentOffset = CGPoint.init(x: 0, y: 0)
    }

    // MARK: - RootNavbar Delegate
    func showMenu() {
    }

	//MARK: - Step
	func setupStepView(){
		
		self.stepRootViewController = StepsRootViewController.instanceFromStoryBord()
		self.stepRootViewController.view.frame = self.stepsViewContent.bounds
		self.stepsViewContent.addSubview(self.stepRootViewController.view)
	}
	
	func setupActivityView() {
		
		self.actRootViewController = ActivityRootViewController.instanceFromStoryBord()
		self.actRootViewController.view.frame = self.activityViewContent.bounds
		self.activityViewContent.addSubview(self.actRootViewController.view)
		
	}
	
	func setupvitalsView(){
		
		self.vitalsRootViewController = VitalsRootViewController.instanceFromStoryBord()
		self.vitalsRootViewController.view.frame = self.vitalsViewContent.bounds
		self.vitalsViewContent.addSubview(self.vitalsRootViewController.view)
	}

    

}
