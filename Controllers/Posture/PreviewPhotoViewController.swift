//
//  PreviewPhotoViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/11/16.
//  Copyright © 2016 haley. All rights reserved.
//


protocol PreviewPhotoViewControllerDelegate: class {
	func deleteBtnClicked(postureType : NSString)
	func confirmPhoto(image:UIImage)
	
}

import UIKit
import Foundation
class PreviewPhotoViewController: UIViewController,CameraViewControllerDelegate ,UIAlertViewDelegate {
	
	@IBOutlet var deleteBtn : UIButton!
	@IBOutlet var locationPicBtn : UIButton!
	@IBOutlet var imageView:UIImageView!
	var imageData : UIImage!
	
	var postureType : NSString!
	var postureDescription : NSString!
	var cameraController : CameraViewController!
	
	var templateImageView: PictureDisplayViewController!
	
	weak var delegate: PreviewPhotoViewControllerDelegate?
	
	//MARK:---Life cycle ---
	override func viewDidLoad() {
		super.viewDidLoad()
		self.imageView.image = imageData
		self.setupNavigationBar()
		let imageName :NSString = "Camera" + (self.postureType as String)
		self.locationPicBtn.setBackgroundImage(UIImage(named:imageName as String), forState: UIControlState.Normal)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
		self.setNeedsStatusBarAppearanceUpdate()

	}
	
	func setupNavigationBar() {
	
		self.title = self.postureType as String;
		
		let leftButton : UIButton = UIButton.init(frame: CGRectMake(0, 0,20,20))
		
		leftButton.setImage(UIImage(named: "Back"), forState:UIControlState.Normal)
		leftButton.addTarget(self, action: #selector(PreviewPhotoViewController.backButtonClick), forControlEvents: UIControlEvents.TouchUpInside)
		
		let leftButtonItem = UIBarButtonItem.init(customView:leftButton)
		
		self.navigationItem.leftBarButtonItem = leftButtonItem
	}
	
	
	//MARK: private function
	
	func backButtonClick() {
		if ((self.templateImageView != nil) && (self.templateImageView.view.superview != nil)) {
			self.templateImageView.closeButtonClicked()
		}
		self.navigationController?.popViewControllerAnimated(true)
	}
	
	@IBAction func retakeButtonClick() {
		self.cameraController = CameraViewController.init()
		self.cameraController.postureType = self.postureType
		self.cameraController.postureDescription = self.postureDescription
		self.cameraController.initCustomCamera()
		self.cameraController.delegate = self
		self.cameraController.showImagePickView(self)
	}
	
	@IBAction func deleteButtonClicked() {
		
		 UIAlertView(title: "", message: "Do you want to discard the photo", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK").show()
		
	}

	func delete() {
		self.delegate!.deleteBtnClicked(self.postureType)
		self.navigationController?.popViewControllerAnimated(true)
	}
	
	func showLocationPicBtn() {
		self.locationPicBtn.hidden = false
		NSNotificationCenter.defaultCenter().removeObserver(self, name: "HideTemplateImageFinished", object: nil)
	}
	
	@IBAction func templateButtonClick() {
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showLocationPicBtn), name: "HideTemplateImageFinished", object: nil)
		
		self.templateImageView = PictureDisplayViewController.instanceFromStoryBord()
	
		self.templateImageView.postureType = self.postureType
		self.templateImageView.postureDes = self.postureDescription
		self.templateImageView.view.frame = CGRectMake(0, (self.navigationController?.navigationBar.frame.size.height)! + (self.navigationController?.navigationBar.frame.origin.y)! ,UIScreen.mainScreen().bounds.size.width , UIScreen.mainScreen().bounds.size.height - ((self.navigationController?.navigationBar.frame.size.height)! + (self.navigationController?.navigationBar.frame.origin.y)!) - 75) ;
		
		self.templateImageView.beginCenterPoint = self.locationPicBtn.center
		
		self.templateImageView.endCenterPoint = CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0,self.templateImageView.view.frame.size.height/2 + ((self.navigationController?.navigationBar.frame.size.height)! + (self.navigationController?.navigationBar.frame.origin.y)!))
		
		self.templateImageView.view.center = self.locationPicBtn.center
		
		
		let xScale : CGFloat = self.locationPicBtn.frame.size.width/UIScreen.mainScreen().bounds.size.width;
		let yScale : CGFloat = self.locationPicBtn.frame.size.height/UIScreen.mainScreen().bounds.size.height;
		self.templateImageView.xScale = xScale
		self.templateImageView.yScale = yScale
		
		let newTransform :CGAffineTransform =  CGAffineTransformScale(self.templateImageView.view.transform,xScale,yScale);
		self.templateImageView.view.transform = newTransform;
		UIApplication.sharedApplication().keyWindow!.addSubview(self.templateImageView.view);
		
		self.templateImageView.showImageViewControllerAnimation()
		
		self.locationPicBtn.hidden = true
		
	}
	
	// MARK: - UIAlertView Delegate Method
	func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
		
		if buttonIndex == 1 {
			self.delete()
		}
	}
	
	//MARK: Camera View Controller Delegate
	func confirmPhoto(image:UIImage) {
		self.delegate!.confirmPhoto(image)
		self.dismissViewControllerAnimated(false, completion: {
			self.navigationController?.popViewControllerAnimated(true)
		})
	}
	
	func cancelTakePhotoBtnClicked() {
		self.dismissViewControllerAnimated(false, completion: nil)
	}
}

