//
//  PostureReportViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/13/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PostureReportViewController : UIViewController {
	
	var reportInfo : PostureReportInfo!
	
	@IBOutlet var imageView : UICollectionView!
	@IBOutlet var textView : UITextView!
	@IBOutlet var textViewHeightConstraint : NSLayoutConstraint!
	
    @IBOutlet weak var thumbnailVideo: UIImageView?
    
    
	var presentImagescontroller : PresentImagesViewController!
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.setNeedsStatusBarAppearanceUpdate()
		
		
		let tracker = GAI.sharedInstance().defaultTracker
		tracker.set(kGAIScreenName, value:"Snapshot Detail")
		
		let builder = GAIDictionaryBuilder.createScreenView()
		tracker.send(builder.build() as [NSObject : AnyObject])
        
    
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
        if reportInfo.type == "video"{
            self.thumbnailVideo?.sd_setImageWithURL(NSURL(string: (self.reportInfo.images.objectAtIndex(0) as! PostureListImageInfo).url as String))
        }else{
            self.imageView.registerNib(UINib(nibName: "PostureReportImageCell", bundle: nil), forCellWithReuseIdentifier: "defaultCell")
        }
        

		
		self.title = self.reportInfo.report_date
		
		if (self.reportInfo.report_comments != nil) {
		
			self.textView.text = self.reportInfo.report_comments as String
			
			let height : CGFloat = PostureReportViewController.heightForString(self.textView.text, fontSize: 14, width: UIScreen.mainScreen().bounds.size.width - 30)
			if (height < UIScreen.mainScreen().bounds.size.height - self.textView.frame.origin.y - 64) {
				self.textViewHeightConstraint.constant = height
			} else {
				self.textViewHeightConstraint.constant = UIScreen.mainScreen().bounds.size.height - self.textView.frame.origin.y - 64
			}
		} else {
		
			self.textView.text = nil;
			self.textViewHeightConstraint.constant = 64
		}
		
	}
	
	
    @IBAction func playButtonPressed(sender: AnyObject) {
        let vc = AVPlayerViewController()
        vc.showsPlaybackControls = true
        vc.player                = AVPlayer(URL: NSURL(string: self.reportInfo.videoURL!)!);
        vc.player?.play()
        
        self.presentViewController(vc, animated: true, completion: nil);
    }
	
	
	// MARK:--- Collection View Delegate ---
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		//返回记录数
		return self.reportInfo.images.count;
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
	{
		let cell:PostureReportImageCell  = collectionView.dequeueReusableCellWithReuseIdentifier("defaultCell", forIndexPath: indexPath) as! PostureReportImageCell
		let image : PostureListImageInfo = self.reportInfo.images.objectAtIndex(indexPath.row) as! PostureListImageInfo
		cell.updateUI(image)
		return cell;
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		let cell:PostureReportImageCell  = collectionView.cellForItemAtIndexPath(indexPath) as! PostureReportImageCell
		
		self.presentImagescontroller = PresentImagesViewController.init()
		let arrayOne : NSMutableArray = self.reportInfo.images
		self.presentImagescontroller.datasource = arrayOne
		self.presentImagescontroller.initIndex = indexPath.row
		self.presentImagescontroller.view.frame = UIScreen.mainScreen().bounds
		self.presentImagescontroller.clickImage = ClickImage.init()
		self.presentImagescontroller.clickImage.showImage(cell.imageIcon
			, presenting:self, presented: self.presentImagescontroller)
	}
	
	class func heightForString(value: NSString ,fontSize: (CGFloat),width:(CGFloat)) -> CGFloat {
		let detailTextView : UITextView = UITextView.init(frame:CGRectMake(0, 0, width, 0))
		detailTextView.font = UIFont.systemFontOfSize(fontSize)
		detailTextView.text = value as String
		let deSize : CGSize = detailTextView.sizeThatFits(CGSizeMake(width,CGFloat.max))
		
		return deSize.height
	}
	
}
