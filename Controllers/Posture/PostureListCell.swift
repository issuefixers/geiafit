//
//  PostureListCell.swift
//  CustomCameraDemo
//
//  Created by haley on 5/13/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit

protocol PostureListCellDelegate: class {
	
	func viewPostureReport(reportInfo : PostureReportInfo)
}

class PostureListCell: UITableViewCell {
	
	@IBOutlet var  viewButton : UIButton!
	
	@IBOutlet var dateLabel: UILabel!
	
	@IBOutlet var statusLabel : UILabel!
	
	@IBOutlet var statusImage : UIImageView!
	
	@IBOutlet var imageIcon : UIImageView!
	
	var reportInfo : PostureReportInfo!
	
	weak var delegate: PostureListCellDelegate?
	
	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	func updateUI(info: PostureReportInfo) {
		self.reportInfo = info
        
        if (self.reportInfo.images.count != 0) {
			
			
            let image = self.reportInfo.images.objectAtIndex(0) as? PostureListImageInfo
            let url = NSURL.init(string: image!.url as String)
			let placeholder = UIImage(named: "DefaultImage.png")
			self.imageIcon.sd_setImageWithURL(url, placeholderImage: placeholder)
        }
        
		self.dateLabel.text = self.reportInfo.report_date
		self.statusImage.hidden = self.reportInfo.isReportRead
        
        self.statusLabel.text = info.status.capitalizedString;
	}
	
	@IBAction func viewButtonClicked() {
		
		self.reportInfo.isReportRead = true

		Posture.addPostureWithData(["userId":NSNumber.init(longLong:self.appDelegate.user!.userID),"createDate":reportInfo.report_created], moc: self.appDelegate.managedObjectContext)
		self.delegate?.viewPostureReport(reportInfo)
	}
	
}
