//
//  PresentImagesViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/16/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit
import Foundation

class PresentImagesViewController : UIViewController {

	var datasource : NSMutableArray!
	@IBOutlet var scrollView : UIScrollView!
	@IBOutlet var pageControl : UIPageControl!
	
	var endCenterPoint : CGPoint!
	
	var  initIndex : Int!
	var  currentPage :Int!
	
	var clickImage :ClickImage!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupInitView()
		UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.None)
	}
	
	func setupInitView (){
		self.scrollView.frame = UIScreen.mainScreen().bounds;
		self.scrollView.contentSize = self.contentSizeForPagingScrollView();
		self.scrollView.setContentOffset(CGPointMake(self.scrollView.bounds.size.width * CGFloat(Float(self.initIndex)), 0), animated: true)
		self.pageControl.numberOfPages = self.datasource.count
		self.loadScrollViewWithPage(initIndex, isShow: true)
		self.loadScrollViewWithPage(initIndex - 1, isShow: true)
		self.loadScrollViewWithPage(initIndex + 1, isShow: true)
		self.updateView(initIndex)
	}
	
	func updateView (page : Int) {
		self.currentPage = page
		self.pageControl.currentPage = page
	}
	
	func contentSizeForPagingScrollView () -> (CGSize) {
		return CGSizeMake(self.scrollView.bounds.size.width * CGFloat(Float(self.datasource.count)), self.scrollView.bounds.size.height);
	}
	
	func frameForPageAtIndex (index : Int ) -> (CGRect) {
		var pageFrame : CGRect = self.scrollView.bounds;
		pageFrame.origin.x = self.scrollView.bounds.size.width * CGFloat(Float(index))
		return pageFrame;
	}
	
	func loadScrollViewWithPage(page:Int, isShow:Bool ) {
		if (page < 0 || page >= self.datasource.count) {
			return
		}
		var imageView: UIImageView?
		
		if self.scrollView.viewWithTag(page+1) == nil {
			imageView = UIImageView.init(frame: self.frameForPageAtIndex(page))
			imageView?.backgroundColor = UIColor.blackColor()
			imageView?.contentMode = UIViewContentMode.ScaleAspectFit
			imageView?.tag = page + 1
			view.hidden = !isShow;
			self.scrollView.addSubview(imageView!);
		} else {
			imageView = self.scrollView.viewWithTag(page+1) as? UIImageView
			imageView!.hidden = !isShow;
		}
		let image : PostureListImageInfo = self.datasource.objectAtIndex(page) as! PostureListImageInfo
		let url = NSURL.init(string: image.url as String)
		
		let placeholder = UIImage(named: "DefaultImage.png")
		imageView!.sd_setImageWithURL(url, placeholderImage: placeholder);
		
	}
	
	func removeOtherImageView(page: Int) {
		if (page < 0 || page >= self.datasource.count) {
			return
		}
		
		var imageView: UIImageView?
		if self.scrollView.viewWithTag(page+1) != nil {
			imageView = self.scrollView.viewWithTag(page+1) as? UIImageView
			if imageView?.superview != nil {
				imageView?.removeFromSuperview()
				imageView = nil
			}
		}
		
	}
	
	func showImageViewControllerAnimation() {
		UIView.beginAnimations("imageViewBig", context: nil)
		UIView.setAnimationDuration(0.5)
		let newTransform :CGAffineTransform =  CGAffineTransformConcat(self.view.transform,  CGAffineTransformInvert(self.view.transform))
		self.view.center = self.endCenterPoint
		self.view.transform = newTransform;
		self.view.alpha = 1.0;
		UIView.commitAnimations();
	}
	
	@IBAction func closeButtonClicked (){
		if ((self.clickImage) != nil) {
			self.clickImage.dismissClickView(self);
		}
	}
	
	@IBAction func tapGestureRecognize(){
	
		if self.clickImage != nil {
			self.closeButtonClicked()
		}
	}

	//MARK: Scroll View Delegate
	func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
		self.scrollViewDidEndScrollingAnimation(scrollView);
	}
	
	func scrollViewDidEndScrollingAnimation(scrollView : UIScrollView) {
		let pageWidth : CGFloat = scrollView.frame.size.width;
		let page: Int = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth)) + 1;	// 0~~+
		self.updateView(page)
		self.removeOtherImageView(page - 2)
		self.removeOtherImageView(page + 2)
		self.loadScrollViewWithPage(page - 1, isShow: true)
		self.loadScrollViewWithPage(page, isShow: true)
		self.loadScrollViewWithPage(page + 1, isShow: true)
	}
	
}