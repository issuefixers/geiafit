//
//  UploadImagesViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/12/16.
//  Copyright © 2016 haley. All rights reserved.
//

import Foundation
import UIKit


class UploadImagesViewController : UIViewController,PostureModelDelegate {

	@IBOutlet var  uploadingLabel : UILabel!
	@IBOutlet var  uploadFaildView : UIView!
	@IBOutlet var  uploadRetryView : UIView!
	@IBOutlet var  uploadProgress : UIProgressView!
	@IBOutlet var  uploadImageView : UIImageView!
	
	private lazy var postureViewModel: PostureModel = {
		return PostureModel()
	}()

	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	var images : NSMutableArray!
	
	class func instanceFromStoryBord() -> UploadImagesViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Posture", bundle: NSBundle.mainBundle())
		let uploadImageController : UploadImagesViewController = storyboard.instantiateViewControllerWithIdentifier("UploadImagesViewController") as!UploadImagesViewController
		return uploadImageController
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Upload"
		self.uploadFaildView.hidden = true
		self.uploadRetryView.hidden = true
		let postureInfo : PostureImageInfo = self.images.objectAtIndex(0) as!PostureImageInfo
		self.updateImageView(postureInfo.originalImage!)

		self.setupNavigationBar()
		self.performSelector(#selector(UploadImagesViewController.startUpload), withObject: nil, afterDelay: 0.1)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
		self.setNeedsStatusBarAppearanceUpdate()
		
	}
	
	func setupNavigationBar() {
		
		let leftButton : UIButton = UIButton.init(frame: CGRectMake(0, 0,60,30))
		leftButton.setTitle("Cancel", forState: UIControlState.Normal)
		leftButton.addTarget(self, action: #selector(UploadImagesViewController.stopUpload), forControlEvents: UIControlEvents.TouchUpInside)
		let leftButtonItem = UIBarButtonItem.init(customView:leftButton)
		self.navigationItem.leftBarButtonItem = leftButtonItem
	}
	
	func stopUpload() {
		self.postureViewModel.stopUploadImages()
		self.navigationController?.popViewControllerAnimated(true)
	}
	
	@IBAction func retryUpload() {
		self.uploadFaildView.hidden = true
		self.uploadProgress.hidden = false
		self.uploadProgress.progress = 0.0
		self.uploadingLabel.hidden = false
		self.uploadRetryView.hidden = true
		self.startUpload()
	}
	
	@IBAction func cancelUpload(){
	
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
	}
	
	func startUpload() {
		
		self.postureViewModel.delegate = self
		self.postureViewModel.postPostureImagesForUser(self.appDelegate.user!,imagesArray: self.images) {_ in }
	}
	
	func updateImageView(image:UIImage) {
		self.uploadImageView.image = image
	}
	
	func updateProgress(progress: Float) {
	
		dispatch_after(1, dispatch_get_main_queue(), { () -> Void in
			self.uploadProgress.progress = progress;
		})
		
		if  (progress == 1) {
			self.uploadProgress.hidden = true
		}
	}
	
	func uploadDidFailed() {
		
		self.uploadFaildView.hidden = false
		self.uploadRetryView.hidden = false
		self.uploadProgress.progress = 0.0
		self.uploadingLabel.hidden = true
		self.uploadProgress.hidden = true
	}
	
	func uploadDidFinished() {
		let uploadSuccess :UploadImageSuccessViewController =  UploadImageSuccessViewController.instanceFromStoryBord()
		self.navigationController?.pushViewController(uploadSuccess, animated: true)
	}
}