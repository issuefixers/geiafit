//
//  PictureDisplayViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/9/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit

class PictureDisplayViewController: UIViewController {
	
	@IBOutlet var templateImage : UIImageView!
	@IBOutlet var desLabel : UILabel!
	var postureType: NSString!
	var postureDes : NSString!
	
	var beginCenterPoint : CGPoint!
	var endCenterPoint : CGPoint!
	var xScale : CGFloat!
	var yScale : CGFloat!
	
	
	override func prefersStatusBarHidden() -> Bool {
		return true
	}
	
	override func viewDidLoad() {
		let imageName :NSString = "Template" + (self.postureType as String)
		self.templateImage.image = UIImage(named:imageName as String)
		self.desLabel.text = self.postureDes as String
	}
	
	@IBAction func closeButtonClicked() {
		self.hideImageViewControllerAnimation()
	}
	
	class func instanceFromStoryBord() -> PictureDisplayViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Posture", bundle: NSBundle.mainBundle())
		let pictureDisplayViewController : PictureDisplayViewController = storyboard.instantiateViewControllerWithIdentifier("PictureDisplayViewController") as!PictureDisplayViewController
		return pictureDisplayViewController
	}
	
	func showImageViewControllerAnimation() {
		UIView.beginAnimations("imageViewBig", context: nil)
		UIView.setAnimationDuration(0.5)
		let newTransform :CGAffineTransform =  CGAffineTransformConcat(self.view.transform,  CGAffineTransformInvert(self.view.transform))
		self.view.center = self.endCenterPoint
		self.view.transform = newTransform;
		self.view.alpha = 1.0;
		UIView.commitAnimations();
	}
	
	func hideImageViewControllerAnimation (){
		UIView.beginAnimations("imageViewSmall", context: nil)
		UIView.setAnimationDuration(0.5)
		UIView.setAnimationDelegate(self)
		UIView.setAnimationDidStopSelector (Selector(hideAnimationDidStop()))
		let newTransform :CGAffineTransform =  CGAffineTransformScale(self.view.transform, xScale, yScale);
		self.view.transform = newTransform;
		self.view.center = beginCenterPoint;
		UIView.commitAnimations();
	}
	
	func hideAnimationDidStop(){
		self.view.removeFromSuperview()
		NSNotificationCenter.defaultCenter().postNotificationName("HideTemplateImageFinished", object: nil)
	}

}