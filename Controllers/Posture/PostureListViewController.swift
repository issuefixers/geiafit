//
//  PostureListViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/13/16.
//  Copyright © 2016 haley. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PostureListViewController : UIViewController,UITableViewDelegate {
	
	@IBOutlet var tableView: UITableView!
//	var rootNavigationController : UINavigationController!
	
	var newPostureViewController : NewPostureViewController!
	var postureList : PostureListInfo!
	
	
	private lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	private lazy var postureViewModel: PostureModel = {
		return PostureModel()
	}()
	
    
    
	
	
	override func prefersStatusBarHidden() -> Bool {
		return true
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
//		self.navigationController?.navigationBarHidden = true;
        self.tableView.reloadData();
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.postureList = PostureListInfo.init()
		self.tableView.es_startPullToRefresh()
	
		self.tableView.tableFooterView = UIView.init(frame: CGRectZero)
		let array = NSMutableArray()
		
		let imageInfo : PostureImageInfo = PostureImageInfo.init()
		imageInfo.originalImage = UIImage(named: "FlashOn")
		imageInfo.imageName = "FlashOn.png"
		array.addObject(imageInfo)
		
		self.getDataSourceFromServer()

		self.title = "Snapshots"
		self.tableView.es_addPullToRefresh {
			self.getDataSourceFromServer()
			
		}
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(PostureListViewController.dismissViewController));
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New", style: .Done, target: self, action: #selector(PostureListViewController.newPostureButtonClicked))
		self.tableView.es_addInfiniteScrolling {
			[weak self] in
			let minseconds = 3.0 * Double(NSEC_PER_SEC)
			let dtime = dispatch_time(DISPATCH_TIME_NOW, Int64(minseconds))
			dispatch_after(dtime, dispatch_get_main_queue() , {
					self?.tableView.reloadData()
					self?.tableView.es_stopLoadingMore()
			})
		}
	}
	
    
	class func instanceFromStoryBord() -> PostureListViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Posture", bundle: NSBundle.mainBundle())
		let postureListViewController : PostureListViewController = storyboard.instantiateViewControllerWithIdentifier("PostureListViewController") as!PostureListViewController
		return postureListViewController
	}
	
	@IBAction func newPostureButtonClicked(){
		
        let alertController:UIAlertController = UIAlertController(title: "New Snapshot", message: "", preferredStyle: .ActionSheet);
        
        alertController.addAction(UIAlertAction(title: "Photo", style: .Default, handler: { (action) in
            let  storyboard: UIStoryboard = UIStoryboard.init(name: "Posture", bundle: NSBundle.mainBundle())
            self.presentViewController(storyboard.instantiateViewControllerWithIdentifier("NewPostureNavigationController") as!UINavigationController, animated: true, completion: nil);
          //  self.rootNavigationController =        //     UIApplication .sharedApplication().keyWindow?.addSubview(self.rootNavigationController.view)
        }));
        
        alertController.addAction(UIAlertAction(title: "Video", style: .Default, handler: { (action) in
            let captureController: CaptureVideoViewController = UIStoryboard.getVideoCaptureStoryboard().instantiateViewControllerWithIdentifier("CaptureVideoViewController") as! CaptureVideoViewController
            captureController.cameraMode = CameraMode.Snapshot;
            let videoNavController = UINavigationController(rootViewController: captureController);
            self.presentViewController(videoNavController, animated: true, completion: nil);
        }));
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler: { (action) in
            alertController.dismissViewControllerAnimated(true, completion: nil);
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil);
        
	}
	
	func getDataSourceFromServer() {
		self.postureViewModel.getPostureReportListForUser(self.appDelegate.user!) { (result) in
			let success = result["success"] as! Bool
			if success {
				self.postureList.reports.removeAllObjects()
				let data : NSDictionary = result["data"] as! [String: AnyObject]
				self.postureList.initWithJsonDic(data)
				
				
				let sortArray = NSMutableArray();
				sortArray.addObjectsFromArray(self.sortArray(self.postureList.reports, sortkey: "report_created", asc: false) as [AnyObject])
				
				self.postureList.reports.removeAllObjects()
				self.postureList.reports.addObjectsFromArray(sortArray as [AnyObject])
				
				let array  = Posture.queryPostureData(self.appDelegate.user!.userID, moc:self.appDelegate.managedObjectContext)
				
				if (array != nil) {
				
					for reportInfo in self.postureList.reports {
						
						for tempReport in array! {
							if(((reportInfo as! PostureReportInfo).report_created as NSString).isEqualToString(((tempReport as! PostureReportInfo).report_created as NSString) as String)){
								
								(reportInfo as! PostureReportInfo).isReportRead = true
							}
						}
					}
				}
				self.tableView.reloadData()
			}
			
			self.tableView.es_stopPullToRefresh(completion: true, ignoreFooter: true)
		}
	}
	
	func sortArray(array:NSArray,sortkey:NSString,asc:Bool) -> NSArray {
		let descriptor : NSSortDescriptor = NSSortDescriptor(key:sortkey as String,ascending:asc)
		return array.sortedArrayUsingDescriptors([descriptor])
		
	}
	
	// MARK:--- Table View Delegate ---
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.postureList.reports.count;
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
		return 85;
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let report : PostureReportInfo = self.postureList.reports.objectAtIndex(indexPath.row) as! PostureReportInfo
		
		let identifier = "PostureListCell";
		let cell : PostureListCell = tableView.dequeueReusableCellWithIdentifier(identifier) as!PostureListCell
		cell.updateUI(report)
		return cell;
	}
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
	let cell : PostureListCell = tableView.dequeueReusableCellWithIdentifier("PostureListCell") as!PostureListCell
        
        let report = self.postureList.reports[indexPath.row] as! PostureReportInfo;

        cell.reportInfo = report;
        cell.viewButtonClicked();
        
        var viewIdentifier = "PostureReportViewController"
        if report.type == "video"{
            viewIdentifier = "PostureVideoReport"
        }
        let reportView : PostureReportViewController = UIStoryboard.getPostureStoryboard().instantiateViewControllerWithIdentifier(viewIdentifier) as! PostureReportViewController
        
        reportView.reportInfo = self.postureList.reports[indexPath.row] as! PostureReportInfo;
        
        self.navigationController?.pushViewController(reportView, animated: true);
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true);
    }

    
    func dismissViewController(){
        self.navigationController!.dismissViewControllerAnimated(true, completion: nil);
    }
    
 
}
