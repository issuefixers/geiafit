//
//  NewPostureViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/10/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

class NewPostureViewController: UIViewController,NewPostrueCellDelegate,CameraViewControllerDelegate,PreviewPhotoViewControllerDelegate {
	
	var cameraController : CameraViewController!
	var previewController : PreviewPhotoViewController!
	
	var titleArray: NSArray!
	var descriptionArray : NSArray!
	
	var imageDict : NSMutableDictionary!
	var postureType: NSString!
	var postureDescription : NSString!
	var imageInfoArray : NSMutableArray!
	
	@IBOutlet var tableView: UITableView!
	
	//MARK:---Life cycle ---
	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationController?.navigationBarHidden = false
		self.titleArray = ["Front","Left","Back","Right","Other"];
		self.descriptionArray = ["Front-side picture (head to toe)","Left-side picture (head to toe)","Back-side picture (head to toe)","Right-side picture (head to toe)","Seated Profile picture (head to toe)"];
		self.imageDict = NSMutableDictionary();
		self.imageInfoArray = NSMutableArray()
		self.tableView.tableFooterView = UIView.init(frame: CGRectZero)
		self.setupNavigationBar()
		self.navigationItem.rightBarButtonItem?.enabled = false
		
		 NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ClearDataSource), name: "ClearPostureData", object: nil)
	}

	func setupNavigationBar() {
		
		self.navigationController?.navigationBar.titleTextAttributes = NSDictionary(object: UIColor.whiteColor(), forKey: NSForegroundColorAttributeName) as? [String : AnyObject]
		
		let leftButton : UIButton = UIButton.init(frame: CGRectMake(0, 0,20,20))
		leftButton.setImage(UIImage.init(named:"Close.png"), forState: UIControlState.Normal)
		leftButton.addTarget(self, action: #selector(NewPostureViewController.backButtonClick), forControlEvents: UIControlEvents.TouchUpInside)
		let leftButtonItem = UIBarButtonItem.init(customView:leftButton)
		self.navigationItem.leftBarButtonItem = leftButtonItem
		
		
		let rightButton : UIButton = UIButton.init(frame: CGRectMake(0, 0,20,20))
		rightButton.setImage(UIImage.init(named:"Upload.png"), forState: UIControlState.Normal)
		rightButton.setImage(UIImage.init(named:"UploadDisable.png"), forState: UIControlState.Disabled)
		rightButton.addTarget(self, action: #selector(NewPostureViewController.sendImage), forControlEvents: UIControlEvents.TouchUpInside)
		let rightButtonItem = UIBarButtonItem.init(customView:rightButton)
		self.navigationItem.rightBarButtonItem = rightButtonItem
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.tableView.reloadData();
		
		UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
		self.setNeedsStatusBarAppearanceUpdate()
	}

	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		
//		UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
//		self.setNeedsStatusBarAppearanceUpdate()
	}
	
	@IBAction func backButtonClick() {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
    }
	
	@IBAction func sendImage() {
		
		let uploadImageController : UploadImagesViewController = UploadImagesViewController.instanceFromStoryBord()
		uploadImageController.images = self.imageInfoArray;
		
		self.navigationController?.pushViewController(uploadImageController, animated: true)
	}
	
	func ClearDataSource(){
		self.imageInfoArray.removeAllObjects()
		self.imageDict.removeAllObjects()
		self.navigationItem.rightBarButtonItem?.enabled = false
	}
	
	// MARK:--- Table View Delegate ---
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		return self.titleArray.count;
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
		return 85;
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let identifier = "NewPostureCell";
		
		let titleString : NSString = (self.titleArray[indexPath.row] as? String)!;
		
		let cell : NewPostrueCell = tableView.dequeueReusableCellWithIdentifier(identifier) as! NewPostrueCell
		cell.delegate = self;
		cell.titleLabel?.text = titleString as String
		
		let image: AnyObject? = self.imageDict[(self.titleArray[indexPath.row] as? String)!]
		let imageIcon: UIImage = UIImage.init(named:"NewPostureTakePhoto")!
		cell.takePhotoImageView.image = imageIcon
		
		if (image != nil) {
			cell.takePhotoButton.setBackgroundImage(image as? UIImage, forState: UIControlState.Normal)
			cell.takePhotoImageView.hidden = true
		} else {
			cell.takePhotoButton.setBackgroundImage(nil, forState: UIControlState.Normal)
			cell.takePhotoImageView.hidden = false
		}
		
		let imageName :NSString = "NewPosture" + (titleString as String)
		cell.postureImage.image = UIImage.init(named:imageName as String )
		cell.postureType = titleString as String
		cell.postureDescription = (self.descriptionArray[indexPath.row] as? NSString)!;
		return cell;
	}
	
	//MARK:---New Postrue Cell Delegate---
	func addPhoto(posture:NSString,postureDes:NSString) {
		
		let image: AnyObject? = self.imageDict[posture]
		self.postureType = posture;
		self.postureDescription = postureDes;
		if  image != nil {
			
			self.previewController = PreviewPhotoViewController.init()
			previewController.postureType = self.postureType
			previewController.postureDescription = self.postureDescription
			previewController.delegate = self
			previewController.imageData = image as! UIImage
			self.navigationController?.pushViewController(previewController, animated:true)

		} else {
		
			self.cameraController = CameraViewController.init()
			self.cameraController.postureType = self.postureType
			self.cameraController.postureDescription = self.postureDescription
			
			
			let authorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) as AVAuthorizationStatus
			
			switch (authorizationStatus) {
			case AVAuthorizationStatus.Authorized:
//				print("授权摄像头使用成功")
				self.cameraController.initCustomCamera()
				self.cameraController.delegate = self
				
				self.cameraController.showImagePickView(self)
				break
			case AVAuthorizationStatus.NotDetermined:
				AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
					
					if granted == true {
						self.cameraController.initCustomCamera()
						self.cameraController.delegate = self
						
						self.cameraController.showImagePickView(self)
						
					} else {
						self.showAlertMsg("Error", content: "用户拒绝授权摄像头的使用权,返回上一页.请打开\n设置-->隐私/通用等权限设置")
					}
					
					return
				})
				break
			default:
				self.showAlertMsg("Error", content: "拒绝授权,返回上一页.请检查下\n设置-->隐私/通用等权限设置")
				break
			}
		}
		
	}
	
	// MARK:---Custom Preview Delegate ---
	
	func deleteBtnClicked(postureType : NSString) {
		self.imageDict.removeObjectForKey(postureType)
		for imageInfo in self.imageInfoArray {
			if ((imageInfo as! PostureImageInfo).imageName.isEqualToString(postureType as String)) {
				self.imageInfoArray .removeObject(imageInfo)
				break
			}
		}
		if self.imageInfoArray.count > 0 {
			self.navigationItem.rightBarButtonItem?.enabled = true
		} else {
			self.navigationItem.rightBarButtonItem?.enabled = false
		}
	}
	
	// MARK:---Custom Camera View Delegate ---
	
	func cancelTakePhotoBtnClicked() {
		self.dismissViewControllerAnimated(false, completion: nil)
	}
	
	func confirmPhoto(image:UIImage) {
		self.imageDict.setObject(image, forKey:self.postureType)
		let imageInfo = PostureImageInfo.init()
		imageInfo.imageName = NSUUID().UUIDString.stringByAppendingString(".png")
		imageInfo.originalImage = image
		self.imageInfoArray.addObject(imageInfo)
		self.navigationItem.rightBarButtonItem?.enabled = true
	}
	
	func showAlertMsg(title: String, content: String) {
		
		UIAlertView(title: title, message: content, delegate: self, cancelButtonTitle: "OK").show()
		
	}
}
