//
//  ClickImage.swift
//  CustomCameraDemo
//
//  Created by haley on 5/16/16.
//  Copyright © 2016 haley. All rights reserved.
//


protocol TXClickImageDelegate: class {
	func didDismissClickImage()
}

import UIKit

class ClickImage: NSObject {

	weak var delegate: TXClickImageDelegate?

	var duration : CGFloat!
	var defaultRect : CGRect!
	var goBackRect : CGRect!
	
	var goBackgroundView : UIView!
	
	var goBackImageView : UIImageView!

	var  goBackFakeImageView : UIImageView!
	
	
	func showImage(imageview:UIImageView,presenting  presentingControl:UIViewController, presented presentedControl: UIViewController) {
		
		let image : UIImage
		if (imageview.image == nil) {
			image = UIImage.init(named:"DefaultImage")!
		} else {
			image = imageview.image!
		}
		 
		
		self.goBackgroundView = UIView.init(frame: UIScreen.mainScreen().bounds)
		
		self.goBackgroundView.backgroundColor = UIColor.blackColor()

		defaultRect = imageview.convertRect(imageview.bounds,toView:UIApplication .sharedApplication().keyWindow);
		goBackImageView = UIImageView.init(frame: defaultRect)
		goBackImageView.image = image
		
		goBackImageView.contentMode = UIViewContentMode.ScaleAspectFill
		goBackImageView.clipsToBounds = true
		
		
		goBackFakeImageView = UIImageView.init(frame: defaultRect)
		goBackFakeImageView.backgroundColor = UIColor.blackColor()
		
		goBackgroundView.addSubview(goBackFakeImageView);
		goBackgroundView.addSubview(goBackImageView);
		UIApplication.sharedApplication().keyWindow!.addSubview(goBackgroundView);
		
		UIView.animateWithDuration(0.5, delay: 0, options: .CurveLinear, animations: {
			
			self.goBackImageView.alpha = 0.0
			self.goBackImageView.frame = UIScreen.mainScreen().bounds
			self.goBackFakeImageView.frame = UIScreen.mainScreen().bounds
			
			}, completion: { (finished) in
				self.goBackRect = self.defaultRect;
				presentingControl.presentViewController(presentedControl, animated: false, completion: {
					self.goBackgroundView.alpha = 0;
				})
		})
	}
	
	func dismissClickView(presentedControl :UIViewController )  {
		
		let snapView : UIView = presentedControl.view
		
		
		UIGraphicsBeginImageContext(snapView.frame.size);
		
		snapView.layer.renderInContext(UIGraphicsGetCurrentContext()!);
		goBackFakeImageView.image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		goBackgroundView.alpha = 1;
		presentedControl.dismissViewControllerAnimated(false) { 

		}
		
		UIView.animateWithDuration(0.5, animations: {
			self.goBackImageView.alpha = 1;
			self.goBackImageView.frame = self.goBackRect;
			self.goBackFakeImageView.frame = self.goBackRect;
			}) { (finished) in
				self.goBackgroundView.removeFromSuperview();
				self.goBackFakeImageView = nil;
				self.goBackImageView = nil;
				self.goBackgroundView = nil;
				self.delegate?.didDismissClickImage()
		}
	}
	
}
