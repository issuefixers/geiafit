//
//  UploadImageSuccessViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/12/16.
//  Copyright © 2016 haley. All rights reserved.
//


import UIKit

class UploadImageSuccessViewController : UIViewController {
	
	class func instanceFromStoryBord() -> UploadImageSuccessViewController {
		let  storyboard: UIStoryboard = UIStoryboard.init(name:"Posture", bundle: NSBundle.mainBundle())
		let uploadSuccessController : UploadImageSuccessViewController = storyboard.instantiateViewControllerWithIdentifier("UploadImageSuccessViewController") as!UploadImageSuccessViewController
		return uploadSuccessController
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupNavigationBar()
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
		self.setNeedsStatusBarAppearanceUpdate()
	}
		
	func rightBarBtnClicked() {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
	}
	
	@IBAction func newPostureAnlytic(){
	
		NSNotificationCenter.defaultCenter().postNotificationName("ClearPostureData", object: nil)
		
		self.navigationController?.popToRootViewControllerAnimated(false)
		
	}
	
	func setupNavigationBar(){
		
		self.navigationItem.setHidesBackButton(true, animated: false)
		
		self.title = "Success"
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(UploadImageSuccessViewController.rightBarBtnClicked))
		self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)], forState: .Normal)
		
	}
	
	
}