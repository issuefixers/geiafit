//
//  CustomCameraView.swift
//  CustomCameraDemo
//
//  Created by haley on 5/9/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit
class CustomCameraView: UIView {
	
	@IBOutlet var finishBtn : UIButton!//完成按钮
	@IBOutlet var reTakeBtn : UIButton!//重拍
	@IBOutlet var cancelBtn : UIButton!
	@IBOutlet var locationPicBtn : UIButton!
	@IBOutlet var flashLampButton :UIButton!//闪光灯按钮
	@IBOutlet var takePhotoBtn: UIButton!//拍照按钮
	@IBOutlet var imageShowView: UIImageView! //拍照后显示图片的图片框
	
	@IBOutlet var titleLabel : UILabel!
	
	@IBOutlet var cameraTitleView : UIView!
	
	@IBOutlet var myBtnView: UIView! //相机界面的按钮界面
	var flashStatus : UIImagePickerControllerCameraFlashMode!
	
	var imageView: PictureDisplayViewController!
	var postureType : NSString!
	var postureDes : NSString!
	
	weak var delegate: CustomCameraViewDelegate?

	
	func setupUI() {
		
		self.flashStatus = UIImagePickerControllerCameraFlashMode.Auto
	self.flashLampButton.setBackgroundImage(UIImage(named:"FlashAuto"), forState: UIControlState.Normal)
		
		let imageName :NSString = "Camera" + (self.postureType as String)
		self.locationPicBtn.setBackgroundImage(UIImage(named:imageName as String), forState: UIControlState.Normal)
		self.titleLabel.text = self.postureType as String
		self.showTemplateImage()
	}
	
	
	@IBAction func takePhoto() {
		self.cancelBtn.hidden = false;
		self.takePhotoBtn.hidden = true;
		self.finishBtn.hidden = false;
		self.finishBtn.enabled = false;
		self.reTakeBtn.hidden = false;
		self.delegate?.takePhotoBtnClicked!()
	}
	
	
	@IBAction func backButtonClicked() {
		
		if ((self.imageView != nil) && (self.imageView.view.superview != nil) ){
			self.imageView.closeButtonClicked()
		}
		
		self.delegate?.cancelTakePhotoBtnClicked!()
	}
	
	
	@IBAction func useButtonClicked() {
		
		if ((self.imageView != nil) && (self.imageView.view.superview != nil) ){
			self.imageView.closeButtonClicked()
		}
		self.delegate?.confirmPhotoBtnClicked!(self.imageShowView.image!)
	}
	
	@IBAction func retakeButtonClicked() {
		
		self.finishBtn.enabled = false
		self.reTakeBtn.enabled = false
		self.finishBtn.hidden = true
		self.reTakeBtn.hidden = true
		self.takePhotoBtn.hidden = false
		self.takePhotoBtn.enabled = true
		self.imageShowView.image = nil
		self.imageShowView.hidden = true
		
		self.delegate?.retakePhotoBtnClicked!()
	}
	
	func showLocationPicBtn() {
		self.locationPicBtn.hidden = false
		NSNotificationCenter.defaultCenter().removeObserver(self, name: "HideTemplateImageFinished", object: nil)
	}
	
	func showTemplateImage(){
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showLocationPicBtn), name: "HideTemplateImageFinished", object: nil)
		
		self.imageView = PictureDisplayViewController.instanceFromStoryBord()
		self.imageView.postureType = self.postureType
		self.imageView.postureDes = self.postureDes
		self.imageView.view.frame = CGRectMake(0, self.cameraTitleView.frame.size.height,UIScreen.mainScreen().bounds.size.width , UIScreen.mainScreen().bounds.size.height - self.cameraTitleView.frame.size.height - self.myBtnView.frame.size.height);
		
		self.imageView.beginCenterPoint = self.locationPicBtn.center
		self.imageView.endCenterPoint = CGPointMake(UIScreen.mainScreen().bounds.size.width/2,self.imageView.view.frame.size.height/2 + self.cameraTitleView.frame.size.height )
		
		let xScale : CGFloat = self.locationPicBtn.frame.size.width/UIScreen.mainScreen().bounds.size.width;
		let yScale : CGFloat = self.locationPicBtn.frame.size.height/UIScreen.mainScreen().bounds.size.height;
		self.imageView.xScale = xScale
		self.imageView.yScale = yScale
		
		UIApplication.sharedApplication().keyWindow!.addSubview(self.imageView.view);
		self.locationPicBtn.hidden = true
	}
	
	@IBAction func templateButtonClick() {
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showLocationPicBtn), name: "HideTemplateImageFinished", object: nil)
		
		self.imageView = PictureDisplayViewController.instanceFromStoryBord()
		self.imageView.postureType = self.postureType
		self.imageView.postureDes = self.postureDes
		self.imageView.view.frame = CGRectMake(0, self.cameraTitleView.frame.size.height,UIScreen.mainScreen().bounds.size.width , UIScreen.mainScreen().bounds.size.height - self.cameraTitleView.frame.size.height - self.myBtnView.frame.size.height);

		self.imageView.beginCenterPoint = self.locationPicBtn.center
		self.imageView.endCenterPoint = CGPointMake(UIScreen.mainScreen().bounds.size.width/2,self.imageView.view.frame.size.height/2 + self.cameraTitleView.frame.size.height )
		self.imageView.view.center = self.locationPicBtn.center
		
		let xScale : CGFloat = self.locationPicBtn.frame.size.width/UIScreen.mainScreen().bounds.size.width;
		let yScale : CGFloat = self.locationPicBtn.frame.size.height/UIScreen.mainScreen().bounds.size.height;
		self.imageView.xScale = xScale
		self.imageView.yScale = yScale
		
		let newTransform :CGAffineTransform =  CGAffineTransformScale(self.imageView.view.transform,xScale,yScale);
		self.imageView.view.transform = newTransform;
		UIApplication.sharedApplication().keyWindow!.addSubview(self.imageView.view);
		
		self.imageView.showImageViewControllerAnimation()
		
		self.locationPicBtn.hidden = true
	}

	
	func updateImageView(info: [NSObject : AnyObject]) {
		self.imageShowView.hidden = false
		self.imageShowView.backgroundColor = UIColor.blackColor()
		self.imageShowView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
		
		self.finishBtn.enabled = true;
		self.reTakeBtn.enabled = true;

	}
	
	@IBAction func flashLampButtonClicked(sender:UIButton) {
		
		if self.flashStatus == UIImagePickerControllerCameraFlashMode.Auto {
		
			self.flashStatus = UIImagePickerControllerCameraFlashMode.On
			
			self.flashLampButton.setBackgroundImage(UIImage(named:"FlashOn"), forState: UIControlState.Normal)
		} else if self.flashStatus == UIImagePickerControllerCameraFlashMode.On {
		
			self.flashStatus = UIImagePickerControllerCameraFlashMode.Off
			self.flashLampButton.setBackgroundImage(UIImage(named:"FlashOff"), forState: UIControlState.Normal)
		} else if self.flashStatus == UIImagePickerControllerCameraFlashMode.Off {
		
			self.flashStatus = UIImagePickerControllerCameraFlashMode.Auto

			self.flashLampButton.setBackgroundImage(UIImage(named:"FlashAuto"), forState: UIControlState.Normal)
		}
		
		self.delegate?.flashLampStatusChanged!(self.flashStatus)
	}
	
}