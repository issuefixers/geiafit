//
//  NewPostrueCell.swift
//  CustomCameraDemo
//
//  Created by haley on 5/10/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit

protocol NewPostrueCellDelegate: class {
	func addPhoto(posture: NSString, postureDes:NSString)
}

class NewPostrueCell: UITableViewCell {
	
	var postureType : NSString!
	var postureDescription : NSString!
	
	@IBOutlet var titleLabel : UILabel!
	@IBOutlet var takePhotoButton : UIButton!
	@IBOutlet var postureImage: UIImageView!
	@IBOutlet var takePhotoImageView: UIImageView!
	
	weak var delegate: NewPostrueCellDelegate?
	
	@IBAction func takePhotoButtonClicked() {
		self.delegate?.addPhoto(self.postureType,postureDes:self.postureDescription)
	}
}