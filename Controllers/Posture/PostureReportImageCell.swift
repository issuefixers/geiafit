//
//  PostureReportImageCell.swift
//  CustomCameraDemo
//
//  Created by haley on 5/17/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit

class PostureReportImageCell: UICollectionViewCell {

	@IBOutlet var imageIcon : UIImageView!
	
	func updateUI(imageInfo:PostureListImageInfo) {
		
		let url = NSURL.init(string: imageInfo.url as String)
		
		let placeholder = UIImage(named: "DefaultImage.png")
		self.imageIcon.sd_setImageWithURL(url, placeholderImage: placeholder)
	}

}
