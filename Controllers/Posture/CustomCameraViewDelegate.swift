//
//  CustomCameraViewDelegate.swift
//  CustomCameraDemo
//
//  Created by haley on 5/9/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit




@objc protocol CustomCameraViewDelegate: class {
	
	optional func takePhotoBtnClicked()
	optional func retakePhotoBtnClicked()
	optional func cancelTakePhotoBtnClicked()
	optional func confirmPhotoBtnClicked(image:UIImage)
	optional func flashLampStatusChanged(status:UIImagePickerControllerCameraFlashMode)
}
