//
//  PostureModel.swift
//  Geia
//
//  Created by haley on 5/23/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData
import AVKit
import AVFoundation
import UIKit
import Foundation

protocol PostureModelDelegate: class {
	func updateProgress(progress: Float)
	func uploadDidFinished()
	func uploadDidFailed()
}

class PostureModel: NSObject,NSURLSessionDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate {
	
	weak var delegate: PostureModelDelegate?
	
	var task : NSURLSessionDataTask?
	
	lazy var appDelegate: AppDelegate = {
		return UIApplication.sharedApplication().delegate as! AppDelegate
	}()
	
	lazy var numberFormatter: NSNumberFormatter = {
		let nf          = NSNumberFormatter()
		nf.allowsFloats = false
		return nf
	}()
	
	func getPostureReportListForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void){
	
		let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
		let endpoint: String = POSTURE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
		let url: String      = "\(SERVER_URL)\(endpoint)"
		
		request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
			.validate(statusCode: 200..<300)
			.validate(contentType: ["application/json"])
			.responseJSON { (response: Response) -> Void in
				let result = response.result
				
				if result.isFailure {
					//Epic fail
					completion(result: ["success": false, "errorMessage":result.debugDescription])
					return
				}
				
				if let jsonObject: AnyObject = result.value {
					let json = JSON(jsonObject)
					debugPrint(json)
					completion(result: ["success": true, "data": result.value!])

				} else {
					completion(result: ["success": false, "errorMessage":"couldn't parse the endpoint response:\n\(result.value)"])
				}
		}
		
	}

	func postPostureImagesForUser(user: User,imagesArray:NSArray,completion: (result: Dictionary<String,AnyObject>) -> Void){
		let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
		let endpoint: String = POSTURE_ENDPOINT.stringByReplacingOccurrencesOfString("<userID>", withString: userID)
		
		var array = [AnyObject]()
		
		for postureImage in imagesArray {
			
			array.append((postureImage as! PostureImageInfo).dic())
		}
		
		let params : [String:AnyObject] = ["images": array]
		
		let url: String = "\(SERVER_URL)\(endpoint)"
		
		
		let URLRequest = NSMutableURLRequest(URL: NSURL(string: url)!)

		URLRequest.HTTPMethod = Alamofire.Method.POST.rawValue


		let sessionID: String   = NSUserDefaults.standardUserDefaults().stringForKey(SESSION_ID_KEY)!
		let token: String       = NSUserDefaults.standardUserDefaults().stringForKey(CSRF_TOKEN_KEY)!
		
		URLRequest.addValue(sessionID, forHTTPHeaderField:"Cookie")
		URLRequest.addValue(token, forHTTPHeaderField:"X-CSRF-Token")

		URLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
		let data : NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions(rawValue: 0))
		URLRequest.HTTPBody = data
		
		let session: NSURLSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: self, delegateQueue: NSOperationQueue.mainQueue())

		task = session.dataTaskWithRequest(URLRequest, completionHandler: { data, response, error -> Void in
			if error != nil{
				self.delegate?.uploadDidFailed()
				print("An error has occured completing the request")
			}else{
				let aString : NSString = NSString.init(data:data! , encoding: NSUTF8StringEncoding)!
				debugPrint(aString)
				
				self.delegate?.uploadDidFinished()
			}

		})
		
		task!.resume()
	}

	func stopUploadImages() {
		self.task!.cancel()
	}
	
	func URLSession(session: NSURLSession, task: NSURLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
		let progress = Float(totalBytesSent)/Float(totalBytesExpectedToSend)
		self.delegate?.updateProgress(progress)
	}
	
	func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
	}

	func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveResponse response: NSURLResponse, completionHandler: (NSURLSessionResponseDisposition) -> Void) {
//		completionHandler(NSURLSessionResponseDisposition.Allow)
//		let responseJSON =  JSON.init(response)
//		
//		print(responseJSON)
//		self.delegate?.uploadDidFinished()
	}

	func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
		
	}

}

