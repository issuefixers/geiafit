//
//  PostureImageInfo.swift
//  Geia
//
//  Created by haley on 5/25/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

class PostureImageInfo: NSObject {
	
	var  imageName : NSString!
	var  imageData : NSData!
	var  originalImage : UIImage!
	
	func dic() -> NSDictionary {
	
		let image = Common.fixOrientation(self.originalImage)
		self.imageData = UIImageJPEGRepresentation(image, 0.5);
		let base64String  = self.imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.init(rawValue: 0))
		
		let imageDic: [String:AnyObject] = ["image_name": self.imageName, "image_data":base64String]
		return imageDic
	}
	
}