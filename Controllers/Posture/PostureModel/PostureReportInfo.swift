//
//  PostureReportInfo.swift
//  Geia
//
//  Created by haley on 5/30/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//


import Foundation
//
//  PostureListInfo.swift
//  Geia
//
//  Created by haley on 5/30/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

class PostureReportInfo: NSObject {
	
	
	private lazy var dateFormatter:NSDateFormatter = {
		let df       = NSDateFormatter()
		df.locale    = NSLocale.currentLocale()
		df.timeStyle = .NoStyle
		df.dateStyle = .MediumStyle
		
		return df
	}()
	
	var  images : NSMutableArray!
    var  type:String = "images";
    var  videoURL:String?
	var  report_created  : NSString!
	var  report_comments : NSString!
	var  report_date  : String!
	var  isReportRead :Bool!
    var status:String!
	override  internal init() {
		self.images = NSMutableArray()
		self.isReportRead = false
	}
	
	func initWithJsonDic(dic:NSDictionary){
		
        if (dic.objectForKey("images")?.isKindOfClass(NSArray) == true) {
            let imagesArray : NSArray = dic.objectForKey("images") as! NSArray
            
            var j : NSInteger = 0
            var imageInfo : PostureListImageInfo = PostureListImageInfo.init()
            
            for item in imagesArray {
                
                if (item.isKindOfClass(NSDictionary)) {
                    let dic : NSDictionary = item as! NSDictionary
                    
                    switch j%3 {
                    case 0:
                        imageInfo.name = dic.objectForKey("name") as? NSString
                        break;
                    case 1:
                        imageInfo.url = dic.objectForKey("url") as? NSString
                        break;
                    case 2:
                        imageInfo.comments = dic.objectForKey("comments") as? NSString
                        break;
                    default:
                        return
                    }
                    
                    j = j + 1
                    
                    if (j>0 && j%3==0) {
                        self.images.addObject(imageInfo)
                        imageInfo = PostureListImageInfo.init()
                    }
                }
            }
            
            self.type = dic.objectForKey("type") as! String;
            self.status = dic.objectForKey("status") as! String;
            
            if self.type == "video" {
                self.videoURL = dic.objectForKey("videos")![1].valueForKey("url") as? String;
            }
            
            self.report_created = dic.objectForKey("report_created") as? NSString
            if (self.report_created != nil) {
                let dateValue  = self.report_created.longLongValue
                var reportDate: NSDate?
                if dateValue > 0 {
                    let aDate = Double(dateValue)
                    reportDate  = NSDate(timeIntervalSince1970: aDate)
                } else {
                    reportDate = NSDate().dateAtStartOfDay()
                }
                
                let formattedDate = self.dateFormatter.stringFromDate(reportDate!)
                
                self.report_date = formattedDate
            }
            
            self.report_comments = dic.objectForKey("report_comments") as? NSString
            
        }
    }
}
