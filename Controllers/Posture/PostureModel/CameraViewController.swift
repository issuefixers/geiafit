//
//  CameraViewController.swift
//  CustomCameraDemo
//
//  Created by haley on 5/11/16.
//  Copyright © 2016 haley. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

protocol CameraViewControllerDelegate: class {
	func confirmPhoto(image:UIImage)
}

class CameraViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CustomCameraViewDelegate {
	var imagePicker : UIImagePickerController!
	var myOverView : CustomCameraView!
	var postureType: NSString!
	
	var postureDescription : NSString!
	
	var presentViewController: UIViewController!
	
	weak var delegate: CameraViewControllerDelegate?
	
	func isCameraPermissions() -> Bool {
		
		let authStatus:AVAuthorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
		
		if (authStatus == AVAuthorizationStatus.Denied || authStatus == AVAuthorizationStatus.Restricted) {
			return false
		} else {
			return true
		}
		
	}
	
	func showImagePickView(presentingViewController:UIViewController) {
		self.presentViewController = presentingViewController
	self.presentViewController.presentViewController(self.imagePicker, animated: true, completion:{
		})
		
	}
	
	//MARK:---Init Camera View---
	func setUpCameraView() {
		
		let nibs:NSArray = NSBundle.mainBundle().loadNibNamed("CustomCameraView", owner: self, options: nil)!
		
		self.myOverView = nibs.firstObject as? CustomCameraView
		self.myOverView.delegate = self;
		self.myOverView?.frame = CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height)
		self.myOverView.postureType = self.postureType;
		self.myOverView.postureDes = self.postureDescription
		self.myOverView.setupUI()
		self.myOverView?.center = CGPointMake(UIScreen.mainScreen().bounds.width/2, UIScreen.mainScreen().bounds.height/2)
	}
	
	
	func initCustomCamera() {
		
		self.setUpCameraView()
		
		self.imagePicker = UIImagePickerController()
			self.imagePicker.delegate = self
			if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
				self.imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
				self.imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashMode.Auto
				self.imagePicker.allowsEditing = false
				self.imagePicker.showsCameraControls = false
				self.imagePicker.edgesForExtendedLayout = UIRectEdge.Bottom
				self.imagePicker.cameraDevice = UIImagePickerControllerCameraDevice.Rear
				let screenBounds : CGSize  = UIScreen.mainScreen().bounds.size
				let cameraAspectRatio : CGFloat = 4.0/3.0
				let camViewHeight : CGFloat = screenBounds.width * cameraAspectRatio;
				let scale : CGFloat = screenBounds.height / camViewHeight;
				self.imagePicker.cameraViewTransform = CGAffineTransformMakeTranslation(0, (screenBounds.height - camViewHeight) / 2.0);
				self.imagePicker.cameraViewTransform = CGAffineTransformScale(self.imagePicker.cameraViewTransform, scale, scale);
				
				self.imagePicker.edgesForExtendedLayout = UIRectEdge.All
				self.imagePicker.cameraOverlayView = self.myOverView;
			}
	}
	
	// MARK:--- UIImage Picker Controller Delegate ---
	func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
		self.myOverView.updateImageView(info)
	}
	
	// MARK:---Custom Camera View Delegate ---
	func takePhotoBtnClicked() {
		self.imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
		self.imagePicker.takePicture()
	}
	
	
	func cancelTakePhotoBtnClicked() {
		self.presentViewController.dismissViewControllerAnimated(false, completion: nil)
		self.imagePicker = nil;
		self.myOverView = nil;
	}
	
	
	func retakePhotoBtnClicked() {
		
	}
	
	func confirmPhotoBtnClicked(image:UIImage) {
		
		self.delegate?.confirmPhoto(image)
		self.presentViewController.dismissViewControllerAnimated(false, completion: nil)
		
		self.imagePicker = nil;
		self.myOverView = nil;
	}
	
	
	func imagePickerControllerDidCancel(){
		
	}
	
	func flashLampStatusChanged(status:UIImagePickerControllerCameraFlashMode) {
		self.imagePicker.cameraFlashMode = status
	}
	
	
}
