//
//  PostureListInfo.swift
//  Geia
//
//  Created by haley on 5/30/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

class PostureListInfo: NSObject {
	
	
	var  reports : NSMutableArray!
	
	override  internal init() {
	
		self.reports = NSMutableArray()
	}
	
	func initWithJsonDic(dic:NSDictionary){
		
		let reportsArray : NSArray = dic.allKeys
		
		for item in reportsArray {
			if (item .isEqualToString("success")) {
				continue
			}
			if  (dic.objectForKey(item)?.isKindOfClass(NSDictionary) == true){
				let dic : NSDictionary = dic.objectForKey(item) as!NSDictionary
				let reportInfo : PostureReportInfo = PostureReportInfo.init()
				reportInfo.initWithJsonDic(dic)
				self.reports.addObject(reportInfo)
			}
		}
	}
}