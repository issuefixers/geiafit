//
//  PostureListImageInfo.swift
//  Geia
//
//  Created by haley on 5/30/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation

class PostureListImageInfo: NSObject {
	
	var  name : NSString!
	var  url  : NSString!
	var comments : NSString!
	
}