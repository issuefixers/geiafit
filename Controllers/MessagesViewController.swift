//
//  MessagesViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 1/19/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import CoreData


class MessagesViewController: GeiaViewController,UITextViewDelegate {
    // MARK: - Outlets

    // MARK: - Local variables
    var refreshTimer: NSTimer!
    var shouldAutoScroll:Bool = true;
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    private lazy var messagesViewModel: MessagesViewModel = {
        return MessagesViewModel()
    }()

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

     
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.appDelegate.blockRotation = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //    // MARK: - RootNavbar Delegate
    func showMenu() {
    }
    
    func endSession() {
        let rate = self.storyboard?.instantiateViewControllerWithIdentifier("rate");
        rate?.modalInPopover = true
        rate?.modalPresentationStyle = .OverCurrentContext;
        rate?.modalTransitionStyle = .CrossDissolve;
//        rate?.view.backgroundColor = UIColor.clearColor()
        self.definesPresentationContext = true
        self.navigationController?.presentViewController(rate!, animated: true, completion: nil)
        
    }
    


}


