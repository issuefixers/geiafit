//
//  LoginViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 11/19/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Google

class LoginViewController: GeiaViewController, UITextFieldDelegate,UITextViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    @IBOutlet weak var txtViewTermsAndPrivacy: UITextView!
    private var scrollViewHeightNormal: CGFloat        = 0.0
    private var scrollViewHeightKeyboardShown: CGFloat = 0.0
    private var currentTextFieldRect: CGRect           = CGRect.zero
    
    // MARK: - Local variables
    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        By using this app you are agreeing to our terms of service and privacy policies.
        
        let attributedString = txtViewTermsAndPrivacy.attributedText.mutableCopy();
        
        attributedString.addAttribute(NSLinkAttributeName, value: "http://www.geiafit.com/terms-of-service.html", range: NSRange(location: 42, length: 17))
        attributedString.addAttribute(NSLinkAttributeName, value: "http://www.geiafit.com/privacy-policy.html", range: NSRange(location: 63, length: 16))
       
        txtViewTermsAndPrivacy.attributedText = attributedString as! NSAttributedString
        self.scrollViewHeightNormal = Toolchain.getWindowSize().height
        
        self.username.tintColor     = .geiaDarkBlueColor()
        self.password.tintColor     = .geiaDarkBlueColor()
        
        let placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        self.username.attributedPlaceholder = NSAttributedString(string: "EMAIL / USERNAME", attributes: [NSForegroundColorAttributeName: placeholderColor])
        self.password.attributedPlaceholder = NSAttributedString(string: "PASSWORD", attributes: [NSForegroundColorAttributeName: placeholderColor])
        
        let underlineAttribute                               = [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
        self.forgotPasswordButton.titleLabel?.attributedText = NSAttributedString(string: "Forgot Password?", attributes: underlineAttribute)
        
        
        //Already logged in?
        let isAlreadyLoggedIn = NSUserDefaults.standardUserDefaults().boolForKey(LOGGED_IN_KEY)
        
        if isAlreadyLoggedIn {
            //Load member from Model
            let userID       = NSUserDefaults.standardUserDefaults().integerForKey(USER_ID_KEY)
            let appDelegate  = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.user = User.getUserWithID(userID, moc: appDelegate.managedObjectContext)
            
            if appDelegate.user != nil {
                print("Found logged in user: \((appDelegate.user?.username)!)")
                
                //Log user into Crashlytics
                self.logUser()
//                let didCompleteSurvey = LoginViewModel.checkSurveysCompleteForUser(appDelegate.user!, moc: appDelegate.managedObjectContext)
//
                dispatch_after(1, dispatch_get_main_queue(), { () -> Void in
//                    if didCompleteSurvey == true{
                        self.performSegueWithIdentifier("login", sender: nil)
//                    } else {
//                        self.performSegueWithIdentifier("onboarding", sender: appDelegate.user)
//                    }

                })
            } else {
                print("couldn't reload user, logging out")
                NSUserDefaults.standardUserDefaults().removeObjectForKey(LOGGED_IN_KEY)
            }
        }else{
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(NSBundle.mainBundle().bundleIdentifier!);
            NSUserDefaults.standardUserDefaults().removeObjectForKey(SESSION_NAME_KEY);
            NSUserDefaults.standardUserDefaults().removeObjectForKey(SESSION_ID_KEY);
            NSUserDefaults.standardUserDefaults().removeObjectForKey(CSRF_TOKEN_KEY);
            NSUserDefaults.standardUserDefaults().removeObjectForKey(LOGGED_IN_KEY);
            NSUserDefaults.standardUserDefaults().removeObjectForKey(USER_ID_KEY);
            self.appDelegate._requestHeaders = nil;
        }
        NSUserDefaults.standardUserDefaults().synchronize()
        
        if NSProcessInfo.processInfo().environment["EBudnikDebug"] == "true" {
            username.text = "budnik1990+1@gmail.com"
            password.text = "nmko987"
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .Portrait {
            self.scrollViewHeightNormal = Toolchain.getWindowSize().height
        } else if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.scrollViewHeightNormal = Toolchain.getWindowSize().height
        }
    }
    

    // MARK: - Actions
    @IBAction func doLogin(sender: AnyObject) {
        let username: String = self.username.text!
        let password: String = self.password.text!
        
        if username.characters.count > 0 && password.characters.count > 0 {
            let loaderView = RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
            loaderView.show()
            LoginViewModel.loginToAPIWithUsername(username, password: password, completion: { (result) -> Void in
                let success = result["success"] as! Bool
                
                if success {
                    let appDelegate  = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.user = result["user"] as? User
                    appDelegate.saveContext()
                    
                    //Log user into Crashlytics
                    self.logUser()
                    
                    loaderView.hide(animated: true)
                    
                    let didCompleteSurvey = appDelegate.user?.onboarded
                    
                    if didCompleteSurvey?.intValue == 1{
                        self.performSegueWithIdentifier("login", sender: nil)
                    } else {
                        self.performSegueWithIdentifier("onboarding", sender: appDelegate.user)
                    }
                                        

                } else {
                    
                    loaderView.hide(animated: true)
                    
                    print(result["errorMessage"])
                    let alert: UIAlertController = UIAlertController(title: "Couldn't Login", message: "Username or Password were incorrect, please check and try again", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        } else {
            let alert: UIAlertController = UIAlertController(title: "Missing Credentials", message: "Either your Username or Password are missing, please check and try again", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
        
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
        //clear the password
        self.password.text = ""
    }
    
    /**
     Logs the user into Crashlytics
     */
    func logUser() {
        // You can call any combination of these three methods
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if let user = appDelegate.user {
            Crashlytics.sharedInstance().setUserEmail(user.email!)
            Crashlytics.sharedInstance().setUserIdentifier("\(user.userID)")
            
            let tracker = GAI.sharedInstance().defaultTracker
            tracker.set(kGAIUserId, value: "\(user.userID)")
            tracker.set(kGAIClientId, value: "\(user.email!)")
            tracker.set(kGAIScreenName, value:"Login")
            
            let builder = GAIDictionaryBuilder.createScreenView()
            tracker.send(builder.build() as [NSObject : AnyObject])
        }
    }
    
    //MARK - TextView Delegate
    
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        
        UIApplication.sharedApplication().openURL(URL);
        
        return false;
    }
    
    
    // MARK: - TextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField) {
        self.currentTextFieldRect = CGRect(x: textField.frame.origin.x, y: textField.frame.origin.y, width: textField.frame.width, height: textField.frame.height + 80.0)
    }
    
    
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let identifier = segue.identifier!
        
        if identifier == "onboarding" {
            let user: User = sender as! User
            let navc: UINavigationController = segue.destinationViewController as! UINavigationController
            let vc: StartViewController = navc.childViewControllers.first as! StartViewController
            vc.user = user
        } else if identifier == "passwordReset" {
            let vc: PasswordResetViewController = segue.destinationViewController as! PasswordResetViewController
            vc.email = self.username.text
        }
    }
    
}
