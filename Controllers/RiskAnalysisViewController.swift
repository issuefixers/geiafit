//
//  RiskAnalysisViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/2/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class RiskAnalysisViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var surveyTitle: UILabel!
    @IBOutlet weak var surveyDescription: UILabel!
    @IBOutlet weak var disclaimerText: UITextView!
    @IBOutlet weak var iAgreeSwitch: UISwitch!

    var isFromMenu:Bool = false;
    
    @IBOutlet weak var btnNext: UIButton!
    // MARK: - Local variables
    private var scrollViewHeight: CGFloat = 0.0
    private var userSurvey: UserSurvey?

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()



    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)

        self.scrollViewHeight = Toolchain.getWindowSize().height - 64

        self.scrollViewHeightConstraint.constant = self.scrollViewHeight
        self.scrollView.layoutIfNeeded()

        self.loadSurveyData()
        
        if isFromMenu == true {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(RiskAnalysisViewController.dismissView))
        }
        
    }

    
    func dismissView(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    // MARK: - Data
    private func loadSurveyData() {
        self.iAgreeSwitch.on = false
        self.btnNext.enabled = false;
        self.btnNext.backgroundColor = UIColor.geiaMidGrayColor();
        self.loaderView.show()
        OnboardingViewController.sharedInstance.getRiskAnalysisSurveyForUser(self.appDelegate.user!) { (result) -> Void in
            self.loaderView.hide(animated: true)
            let success = result["success"] as! Bool

            if success {
                //save changes
                self.appDelegate.saveContext()

                //load data
                self.userSurvey             = result["userSurvey"] as? UserSurvey
                let survey                  = self.userSurvey!.survey!
                self.surveyTitle.text       = survey.title!
                self.surveyDescription.text = survey.text!
                self.disclaimerText.text    = survey.disclaimer!
                self.disclaimerText.font    = .geiaFontOfSize(15.0)
            } else {
                print(result["errorMessage"])
                let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your data to our servers, please try again", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .Portrait {
            self.scrollViewHeight = Toolchain.getWindowSize().height - 64
        } else if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.scrollViewHeight = Toolchain.getWindowSize().height - 20
        }
    }

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.scrollViewHeightConstraint.constant = self.scrollViewHeight
        self.scrollView.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Actions
    @IBAction func doStartAnalysis(sender: AnyObject) {
        if self.iAgreeSwitch.on == true {
            //record timestamp on user-survey instance
            self.userSurvey!.createdDate = NSDate().timeIntervalSince1970
            self.appDelegate.saveContext()

            self.performSegueWithIdentifier("showNext", sender: nil)
        } else {
            let alert: UIAlertController = UIAlertController(title: "Warning", message: "You need to agree to the disclaimer before continuing", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    @IBAction func setAgreValue(sender: AnyObject) {
        
        if self.iAgreeSwitch.on == true {
            self.btnNext.enabled = true;
            self.btnNext.backgroundColor = UIColor.geiaYellowColor();
        }else{
            self.btnNext.enabled = false;
            self.btnNext.backgroundColor = UIColor.geiaMidGrayColor();
        }
        
    }
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showNext" {
            let vc    = segue.destinationViewController as! RiskAnalysisSurveyViewController
            vc.survey = self.userSurvey?.survey
        }
    }
}
