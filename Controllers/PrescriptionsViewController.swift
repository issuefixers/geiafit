//
//  PrescriptionsViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/23/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import CarbonKit

enum PrescriptionSection: Int {
    case Fitness = 0, Exercises = 1, Posture = 2
}

class PrescriptionsViewController: GeiaViewController,CarbonTabSwipeNavigationDelegate{
    
    
    // MARK: - Local variables
    var currentSection: PrescriptionSection = .Fitness
    var listViews:[UIViewController] = [];
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation?;
    var newSnapshotButton: UIBarButtonItem?
    
    
    @IBOutlet weak var targetView: UIView!
    var carbonIsLoaded:Bool = false;
    override func viewDidLoad() {
        super.viewDidLoad()
        let items = ["Fitness", "Exercises", "Snapshot"]
        
        let exercisesTableView = UIStoryboard.getPrescriptionStoryboard().instantiateViewControllerWithIdentifier("ExercisesTableViewController") as! ExercisesTableViewController;
        let postureView = PostureListViewController.instanceFromStoryBord();
        self.listViews = [UIStoryboard.getPrescriptionStoryboard().instantiateViewControllerWithIdentifier("FitnessViewController"),exercisesTableView,postureView];

        self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        self.carbonTabSwipeNavigation!.insertIntoRootViewController(self, andTargetView: self.targetView);
              self.newSnapshotButton = UIBarButtonItem(title: "New", style: .Done, target: postureView, action: #selector(PostureListViewController.newPostureButtonClicked))
        self.newSnapshotButton?.tintColor = UIColor.geiaYellowColor();
        self.loadSection(self.currentSection);
        
        
        

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true);
        self.carbonTabSwipeNavigation!
            .setIndicatorColor(UIColor.geiaDarkBlueColor());
        self.carbonTabSwipeNavigation!.carbonSegmentedControl?.backgroundColor = UIColor.geiaDarkGrayColor();
        self.carbonTabSwipeNavigation!.carbonTabSwipeScrollView.backgroundColor = UIColor.geiaDarkGrayColor();
        self.carbonTabSwipeNavigation!.setNormalColor(UIColor.geiaMidGrayColor(), font: UIFont.geiaFontOfSize(17));
        self.carbonTabSwipeNavigation!.setSelectedColor(UIColor.whiteColor(), font: UIFont.geiaFontOfSize(17));
        var frm = self.carbonTabSwipeNavigation!.carbonSegmentedControl!.frame;
        frm.size.width = CGRectGetWidth(self.view.frame);
        self.carbonTabSwipeNavigation!.carbonSegmentedControl!.frame = frm;
    }
    
    override func viewDidAppear(animated: Bool) {
        self.carbonIsLoaded = true;
        super.viewDidAppear(true);
        var frm = self.carbonTabSwipeNavigation!.pagesScrollView!.frame;
        frm.size.height = CGRectGetHeight(self.view.frame) + 44;
        self.carbonTabSwipeNavigation!.pagesScrollView!.frame = frm;
        self.carbonTabSwipeNavigation!.carbonSegmentedControl?.backgroundColor = UIColor.geiaDarkGrayColor();
        self.carbonTabSwipeNavigation!.carbonTabSwipeScrollView.backgroundColor = UIColor.geiaDarkGrayColor();
        self.view.backgroundColor = UIColor.clearColor();
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        // return viewController at index
        

        return listViews[Int(index)];
    }
    
    

    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAtIndex index: UInt) {
        
        if self.carbonIsLoaded{
            self.currentSection = PrescriptionSection(rawValue: Int(index))!;
   
   

            if self.currentSection != .Posture {
                self.navigationItem.rightBarButtonItem = nil;
            }else{
                self.navigationItem.rightBarButtonItem = self.newSnapshotButton!;
            }
           
        }
    }
    
    
    func loadSection(section: PrescriptionSection) {
        self.carbonTabSwipeNavigation?.setCurrentTabIndex(UInt(section.rawValue), withAnimation: true);
    }
}
