//
//  RiskAnalysisSurveyViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/18/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class RiskAnalysisSurveyViewController: GeiaViewController, UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var surveyTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    private var tries:NSInteger = 0;
    @IBOutlet weak var currentQuestionLbl: UILabel!

    @IBOutlet weak var numberedQuestionLbl: UILabel!
  

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()

    var survey: Survey?
    private var questions: [Question]?
    private var dataSource: [Answer]?
    private var scrollViewHeight: CGFloat = 0.0
    private var currentQuestion: Int      = 0
    private var totalQuestions: Int       = 0
    private let screenWidth               = Toolchain.getWindowSize().width

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)

        //        self.tableView.tableFooterView = UIView();

        self.loadSurvey()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .Portrait {
            self.scrollViewHeight = Toolchain.getWindowSize().height - 64
        } else if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.scrollViewHeight = Toolchain.getWindowSize().height - 20
        }
    }

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {

        self.view.layoutIfNeeded()
    }

    // MARK: - Data
    func loadSurvey() {
        self.surveyTitle.text = self.survey?.title
        
        self.questions      = self.survey?.getQuestions()
        self.totalQuestions = (self.questions?.count)!

        if self.questions?.count > 0 {
            self.currentQuestion = 0
            self.loadQuestion((self.questions?[self.currentQuestion])!);
        }

    }
    
    
    //MARK: - UITableView DataSource Logic
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dataSource?.count)!;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("answerCell", forIndexPath: indexPath);
        
        let ans:Answer = self.dataSource![indexPath.row];
        
        let lbl:UIButton = cell.viewWithTag(100) as! UIButton;
        
        lbl .setTitle(ans.text, forState: UIControlState.Normal);
        return cell;
        
        
    }
    
    func loadQuestion(question: Question) {
        self.tableView.beginUpdates();
        self.dataSource = question.answers?.sortedArrayUsingDescriptors([NSSortDescriptor.init(key: "points", ascending: true)]) as? [Answer]
        self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Left);
        self.tableView.endUpdates();

        self.currentQuestionLbl.text = question.text;
        self.numberedQuestionLbl.text = "\(self.currentQuestion + 1) of \(self.totalQuestions)";
        

    }

    
    
    
    //MARK: - UITableView Delegate Logic
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//
//        if((self.currentQuestion + 1) != self.totalQuestions){
//            self.currentQuestion += 1;
//            self.loadQuestion((self.questions?[self.currentQuestion])!);
//        }else{
////            self.dataSource = [AnyObject]();
//            self.tableView.reloadData();
//            print("Acabe");
//        }
        
        self.didChooseAnswerWithPoints(indexPath.row)
    }
    
   
    
    
    
//
//    func loadQuestion(questionNumber: Int) {
//        let isAdvancing: Bool       = questionNumber > self.currentQuestion
//        let isShowingLeftCard: Bool = self.questionPositionConstraint.constant == 0
//
//        if isAdvancing {
//            //Advancing
//            if !isShowingLeftCard {
//                //showing the right card, fix it then continue
//                let currentQ = self.questions![self.currentQuestion] as Question
//                self.leftQuestionCard.loadQuestion(currentQ, number: self.currentQuestion, total: self.totalQuestions)
//                self.questionPositionConstraint.constant = 0
//                self.view.layoutIfNeeded()
//            }
//
//            //Load the next question
//            let nextQ = self.questions![questionNumber]
//            self.rightQuestionCard.loadQuestion(nextQ, number: questionNumber+1, total: self.totalQuestions)
//
//            //animate
//            UIView.animateWithDuration(0.35, animations: { () -> Void in
//                self.questionPositionConstraint.constant = -self.screenWidth
//                self.scrollView.layoutIfNeeded()
//            }, completion: { (finished) -> Void in
//                self.currentQuestion = questionNumber
//            })
//        } else {
//            //Going Back
//            if isShowingLeftCard {
//                //showing the left card, fix it then continue
//                let currentQ = self.questions![self.currentQuestion] as Question
//                self.rightQuestionCard.loadQuestion(currentQ, number: self.currentQuestion, total: self.totalQuestions)
//                self.questionPositionConstraint.constant = -self.screenWidth
//                self.view.layoutIfNeeded()
//            }
//
//            //Load the previous question
//            let prevQ = self.questions![questionNumber]
//            self.leftQuestionCard.loadQuestion(prevQ, number: questionNumber, total: self.totalQuestions)
//
//            //animate
//            UIView.animateWithDuration(0.35, animations: { () -> Void in
//                self.questionPositionConstraint.constant = 0
//                self.scrollView.layoutIfNeeded()
//            }, completion: { (finished) -> Void in
//                self.currentQuestion = questionNumber
//            })
//        }
//    }

    // MARK: - Actions
    func didChooseAnswerWithPoints(points: Int) {
        //save points for question
        let answeredQuestion = self.questions![self.currentQuestion] as Question
        OnboardingViewController.sharedInstance.registerAnswerForUser(self.appDelegate.user!, question: answeredQuestion, points: points)
        self.appDelegate.saveContext()

        if self.currentQuestion+1 < self.questions?.count {
            //load next one
            self.currentQuestion += 1;
            self.loadQuestion((self.questions?[self.currentQuestion])!);
        } else {
            //survey is done
            self.didFinishSurvey()
        }
    }

    func didFinishSurvey() {
    
        if self.loaderView.hidden {
            self.loaderView.show()
            OnboardingViewController.sharedInstance.sendRiskAnalysisAnswersForUser(self.appDelegate.user!, survey: self.survey!) { (result) -> Void in
                self.loaderView.hide(animated: true)
                let success = result["success"] as! Bool
                
                if success {
                    //save changes
                    self.appDelegate.saveContext()
                    
                    self.performSegueWithIdentifier("showNext", sender: nil)
                } else {
                    print(result["errorMessage"])
                    
                    if self.tries < 2{
                        self.tries += 1;
                        self.didFinishSurvey();
                    }else{
                        let alert: UIAlertController = UIAlertController(title: "Couldn't send answers", message: "There was a problem sending your answers to our servers, please try again", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
               
                }
            }

        }
    
    }

}
