//
//  GoalsViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/2/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class GoalsViewController: GeiaViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var weightloss: UIButton!
    @IBOutlet weak var painRelief: UIButton!
    @IBOutlet weak var improvePosture: UIButton!
    @IBOutlet weak var improveOverallHealth: UIButton!
    @IBOutlet weak var manageRecovery: UIButton!
    @IBOutlet weak var btnNext: UIButton!

    // MARK: - Local variables
    private var scrollViewHeightNormal:CGFloat        = 0.0
    private var scrollViewHeightKeyboardShown:CGFloat = 0.0
    private var currentTextFieldRect:CGRect           = CGRect.zero

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    private lazy var loaderView: RPLoadingAnimationView = {
        return RPLoadingAnimationView.initWithSuperview(self.view, size: CGSize(width: 60, height: 60), color: UIColor.geiaDarkBlueColor())
    }()


    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.scrollViewHeightNormal = Toolchain.getWindowSize().height - 64

        self.scrollViewHeightConstraint.constant = self.scrollViewHeightNormal
        self.scrollView.layoutIfNeeded()

        self.loadDataForUser(self.appDelegate.user!)
        self.navigationItem.setHidesBackButton(true, animated: false)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .Portrait {
            self.scrollViewHeightNormal = Toolchain.getWindowSize().height - 64
        } else if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.scrollViewHeightNormal = Toolchain.getWindowSize().height - 20
        }
    }

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.scrollViewHeightConstraint.constant = self.scrollViewHeightNormal
        self.scrollView.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }

    // MARK: - Data
    private func loadDataForUser(user: User) {
        //load the user's goals
        let goals: UserGoals = UserGoals.init(rawValue: user.goals)
        if goals.contains(.Weightloss) {
            self.weightloss.selected = true
        }

        if goals.contains(.PainRelief) {
            self.painRelief.selected = true
        }

        if goals.contains(.ImprovePosture) {
            self.improvePosture.selected = true
        }

        if goals.contains(.ImproveOverallHealth) {
            self.improveOverallHealth.selected = true
        }

        if goals.contains(.ManageRecovery) {
            self.manageRecovery.selected = true
        }
        
        self.validateNext();

    
    }

    // MARK: - Actions
    @IBAction func toggleButton(sender: UIButton) {
        sender.selected = !sender.selected

        self.validateNext();
    }
    
    func validateNext(){
        if self.weightloss.selected || self.painRelief.selected || self.improvePosture.selected || self.improveOverallHealth.selected || self.manageRecovery.selected {
            self.btnNext.enabled = true;
            self.btnNext.backgroundColor = UIColor.geiaYellowColor();
            
        }else{
            self.btnNext.enabled = false;
            self.btnNext.backgroundColor = UIColor.geiaMidGrayColor();
        }
    }
    
  

    @IBAction func showNext(sender: AnyObject) {
        //determine which buttons are selected
        var goalsValue      = UserGoals.None.rawValue
        var goals: [String] = []

        if self.weightloss.selected {
            goalsValue += UserGoals.Weightloss.rawValue
            goals.append(WEIGHTLOSS)
        }

//        if self.eatHelthy.selected {
//            goalsValue += UserGoals.EatHealthy.rawValue
//            goals.append(EAT_HEALTHY)
//        }

        if self.painRelief.selected {
            goalsValue += UserGoals.PainRelief.rawValue
            goals.append(PAIN_RELIEF)
        }

        if self.improvePosture.selected {
            goalsValue += UserGoals.ImprovePosture.rawValue
            goals.append(IMPROVE_POSTURE)
        }

        if self.improveOverallHealth.selected {
            goalsValue += UserGoals.ImproveOverallHealth.rawValue
            goals.append(IMPROVE_OVERALL_HEALTH)
        }

        if self.manageRecovery.selected {
            goalsValue += UserGoals.ManageRecovery.rawValue
            goals.append(MANAGE_RECOVERY)
        }

        //set the value in core data
        self.appDelegate.user?.goals = goalsValue

        if goals.count > 0 {
            //send to server
            //self.loaderView.show()
            
            OnboardingViewController.sharedInstance.goals = goals;
            
            self.performSegueWithIdentifier("showNext", sender: nil)

            
          
        }
    }

}
