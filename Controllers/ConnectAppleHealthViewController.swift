//
//  ConnectAppleHealthViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/2/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit

class ConnectAppleHealthViewController: GeiaViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!

    private var scrollViewHeight:CGFloat = 0.0

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)

        self.scrollViewHeight = Toolchain.getWindowSize().height - 64

        self.scrollViewHeightConstraint.constant = self.scrollViewHeight
        self.scrollView.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .Portrait {
            self.scrollViewHeight = Toolchain.getWindowSize().height - 64
        } else if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.scrollViewHeight = Toolchain.getWindowSize().height - 20
        }
    }

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.scrollViewHeightConstraint.constant = self.scrollViewHeight
        self.scrollView.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }

    // MARK: - Actions
    @IBAction func doConnect(sender:AnyObject) {
        let healthManager = HealthManager()

        healthManager.authorizeHealthKit { (success, error) in
            
            if success {
                print("ActivityKit Authorized.")
            } else {
                print("ActivityKit NOT Authorized.")
                if error != nil {
                    debugPrint(error)
                }
            }

            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.performSegueWithIdentifier("showNext", sender: nil)
            })
        }
    }

    @IBAction func skip(sender:AnyObject) {
        self.performSegueWithIdentifier("showNext", sender: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
