//
//  YouDidItViewController.swift
//  Geia
//
//  Created by Carlos Villarreal Mora on 12/2/15.
//  Copyright © 2015 Wellness Links. All rights reserved.
//

import UIKit
import JTProgressHUD
class YouDidItViewController: GeiaViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!

    private var scrollViewHeight:CGFloat = 0.0

    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()

    
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)

        self.scrollViewHeight = Toolchain.getWindowSize().height - 64

        self.scrollViewHeightConstraint.constant = self.scrollViewHeight
        self.scrollView.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .Portrait {
            self.scrollViewHeight = Toolchain.getWindowSize().height - 64
        } else if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.scrollViewHeight = Toolchain.getWindowSize().height - 20
        }
    }

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.scrollViewHeightConstraint.constant = self.scrollViewHeight
        self.scrollView.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    
    
    @IBAction func goNext(){

        
        if NSUserDefaults.standardUserDefaults().boolForKey("isFromMenu") {
            NSNotificationCenter.defaultCenter().postNotificationName("dismissSurvey", object: nil, userInfo: nil);
        }else{
            JTProgressHUD.show()
            OnboardingViewController.sharedInstance.updateUserInformationForUser(self.appDelegate.user!) { (result) -> Void in
                let success = result["success"] as! Bool
                
                if success {
                    //save changes
                    self.appDelegate.saveContext()
                    
                    //now send image
//                    self.sendCharacteristics()
                    self.sendGoals();

                } else {
                    JTProgressHUD.hide()
                    
                    print(result["errorMessage"])
                    let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your data to our servers, please try again", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { (action) -> Void in
                        self.goNext();
                        alert.dismissViewControllerAnimated(true, completion: nil);
                        JTProgressHUD.show()
                        
                        }
                        
                        ))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    
    func sendCharacteristics()  {
        OnboardingViewController.sharedInstance.updateCharacteristics() { (result) -> Void in
            let success = result["success"] as! Bool
            
            if success {
                //save changes
                self.appDelegate.saveContext()
                self.sendGoals();
            } else {
                JTProgressHUD.hide()

                print(result["errorMessage"])
                let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your data to our servers, please try again", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { (action) -> Void in
                        self.sendCharacteristics();
                        alert.dismissViewControllerAnimated(true, completion: nil);
                    JTProgressHUD.show()

                    }
                    
                    ))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func sendGoals(){
        OnboardingViewController.sharedInstance.updateGoalsForUser(self.appDelegate.user!,completion: { (result) -> Void in
            let success = result["success"] as! Bool
            
            if success {
                JTProgressHUD.hide()

                //save changes
                self.appDelegate.saveContext()
                
                //advance
                self.performSegueWithIdentifier("showNext", sender: nil)
            } else {
                JTProgressHUD.hide()

                print(result["errorMessage"])
                let alert: UIAlertController = UIAlertController(title: "Couldn't send data", message: "There was a problem sending your goals to our servers, please try again", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { (action) -> Void in
                    self.sendGoals();
                    alert.dismissViewControllerAnimated(true, completion: nil);
                    JTProgressHUD.show()

                    
                    }
                    
                    ))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        })
    }
    // MARK: - Actions

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

    }
}
