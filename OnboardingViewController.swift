//
//  OnboardingViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 4/17/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class OnboardingViewController {

    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    var weightValue: Float?
    var heightValue: Float?
    var dobValue: NSDate?
    var goals: [String] = []
    var characteristics:Characteristics?
    
    static let sharedInstance = OnboardingViewController()
    private init() {} //This prevents others from using the default '()' initializer for this class.

    
    
    private lazy var onbardingViewModel: OnboardingViewModel = {
        return OnboardingViewModel()
    }()


    private lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    
    
    func getProfileForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {

        self.onbardingViewModel.getProfileForUser(user) { (result) in
            let success = result["success"] as! Bool
            
            if success == true {
                completion(result: result)

            }else{
                completion(result: ["success": false, "errorMessage":result.debugDescription])

            }
        }
        
    }
    

    func getCharacteristicsForUser(user: User, completion: (result: Dictionary<String, AnyObject>) -> Void) {
     self.onbardingViewModel.getCharacteristicsForUser(user) { (result) in
        let success = result["success"] as! Bool
        
        if success == true {
            completion(result: result)
            
        }else{
            completion(result: ["success": false, "errorMessage":result.debugDescription])
            
        }

        }
        
    }
    
    func updateGoalsForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        
        self.onbardingViewModel.updateGoalsForUser(user, goals: self.goals) { (result) in
            let success = result["success"] as! Bool
            
            if success == true {
                completion(result: result)
                
            }else{
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                
            }
        }
        
    }
    
    
    func updateUserInformationForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        self.appDelegate.user?.email     = self.email!
        self.appDelegate.user?.firstName = self.firstName!
        self.appDelegate.user?.lastName  = self.lastName!
        self.appDelegate.user?.birthday  = (self.dobValue?.timeIntervalSince1970)!
      
        //sync data with server
        self.onbardingViewModel.updateUserInformationForUser(self.appDelegate.user!, password: self.password!) { (result) -> Void in
            let success = result["success"] as! Bool

            if success == true {
                completion(result: result)
                
            }else{
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                
            }
            
        }
    }
    
    func updateProfileImageForUser(user: User, imageData: NSData, completion: (result: Dictionary<String, AnyObject>) -> Void) {

        self.onbardingViewModel.updateProfileImageForUser(user, imageData: imageData) { (result) in
            let success = result["success"] as! Bool
            
            if success == true {
                completion(result: result)
                
            }else{
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                
            }

        }
        
    }
    
    func updateCharacteristics(completion: (result: Dictionary<String,AnyObject>) -> Void) {

        if self.characteristics != nil {
            
        self.onbardingViewModel.updateCharacteristics(self.characteristics!) { (result) in
            let success = result["success"] as! Bool
            
            if success == true {
                completion(result: result)
                
            }else{
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                
            }
        }
        } else {
           completion(result: ["success": false, "errorMessage":"no characteristics data"])
        }
    }
 

    
    func registerAnswerForUser(user: User, question: Question, points: Int) {

        self.onbardingViewModel.registerAnswerForUser(user, question: question, points: points);
        
    }
    
    
    func sendRiskAnalysisAnswersForUser(user: User, survey: Survey, completion: (result: Dictionary<String,AnyObject>) -> Void) {

        self.onbardingViewModel.sendRiskAnalysisAnswersForUser(user, survey: survey) { (result) in
            let success = result["success"] as! Bool
            
            if success == true {
                completion(result: result)
                
            }else{
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                
            }
        }
    }
    
    
    func getRiskAnalysisSurveyForUser(user: User, completion: (result: Dictionary<String,AnyObject>) -> Void) {
        self.onbardingViewModel.getRiskAnalysisSurveyForUser(user) { (result) in
            let success = result["success"] as! Bool
            
            if success == true {
                completion(result: result)
                
            }else{
                completion(result: ["success": false, "errorMessage":result.debugDescription])
                
            }
        }
        
    }
    
  
}
