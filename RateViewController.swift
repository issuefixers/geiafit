//
//  RateViewController.swift
//  Geia
//
//  Created by Sergio Solorzano on 2/2/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit

class RateViewController: GeiaViewController {
    @IBOutlet weak var boxRate: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        self.boxRate.clipsToBounds       = false
        self.boxRate.layer.shadowColor   = UIColor.geiaDarkGrayColor().CGColor
        self.boxRate.layer.shadowOffset  = CGSize(width: 5, height: 5)
        self.boxRate.layer.shadowRadius  = 15.0
        self.boxRate.layer.shadowOpacity = 0.98
        self.boxRate.layer.cornerRadius  = 10.0
    }

    @IBAction func dismissRate(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }

    @IBAction func submitRate(sender: AnyObject) {
        //TODO: Send rating to back end

        //After sending rating, dismiss this view
        self.dismissRate(sender)
    }
}
