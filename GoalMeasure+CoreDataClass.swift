//
//  GoalMeasures+CoreDataClass.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/14/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

@objc(GoalMeasure)
public class GoalMeasure: NSManagedObject {
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
    
    
    func addGoalMeassureWithData(json:JSON){
        self.title = json["title"].string;
        self.nid = json["nid"].string;
        let jsonArray:[JSON] = json["questions"].arrayValue
//        var quest:[QuestionMeasure] = [];
        for nJson in jsonArray{
            var newQuestion = QuestionMeasure.getQuestionMeassureWithId(self.appDelegate.managedObjectContext, logId: nJson["item_id"].numberValue)
            if newQuestion == nil{
                newQuestion = NSEntityDescription.insertNewObjectForEntityForName("QuestionMeasure", inManagedObjectContext: self.appDelegate.managedObjectContext) as? QuestionMeasure;
            }
            newQuestion?.addQuestionMeasure(nJson,goal: self);
//            newQuestion?.goal = self;
//            quest.append(newQuestion!);
            
        }
    }
    
    class func getMeassureWithId(moc: NSManagedObjectContext,nId: String)-> GoalMeasure?{
        
    var measures: [GoalMeasure]?
    
    let fetchRequest        = NSFetchRequest.init(entityName: "GoalMeasure")
    
    fetchRequest.fetchLimit = 1
    fetchRequest.predicate  = NSPredicate(format: "nid == %@", nId)
    
    do {
        measures = (try moc.executeFetchRequest(fetchRequest)) as? [GoalMeasure];
        if ( measures?.count  > 0){
            return measures![0];
        }
    } catch {
        print("Error Querying")
        return nil
    }
    return nil
    }
    
}
