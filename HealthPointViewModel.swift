//
//  HealthPointViewModel.swift
//  Geia
//
//  Created by Sergio Solorzano on 9/4/16.
//  Copyright © 2016 Wellness Links. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData


class HealthPointViewModel: ViewModel {

    
    func getHealthLog(user: User, timestamp:String, completion: (result: [HealthDailyLog], dailyTotals:String) -> Void){
        let userID: String   = self.numberFormatter.stringFromNumber(NSNumber(longLong: user.userID))!
        let endpoint: String = (LOG_HEALTHPOINTS_ENDPOINT.stringByReplacingOccurrencesOfString("<timeStamp>", withString: timestamp)).stringByReplacingOccurrencesOfString("<userID>", withString: userID);
        
        let url: String      = "\(SERVER_URL)\(endpoint)"
        print(url);
        
        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                var dailyTotals = "";
                var listLogs:[HealthDailyLog] = [];
                if let jsonObject: AnyObject = result.value {
                    dailyTotals = JSON(jsonObject)["total_points"].stringValue
                    let jsonArray:[JSON] = JSON(jsonObject)["daily"].arrayValue
                    for jsonMessage:JSON in jsonArray{
                        
                        let logId = jsonMessage["id"].stringValue
                        var newLog = HealthDailyLog.getLogWithId(self.appDelegate.managedObjectContext, logId:logId )
                        
                        if(newLog == nil){
                            newLog = NSEntityDescription.insertNewObjectForEntityForName("HealthDailyLog", inManagedObjectContext: self.appDelegate.managedObjectContext) as? HealthDailyLog
                        }
                        newLog!.addDailyLogWithData(jsonMessage);
                        
                        
                        
                        listLogs.append(newLog!);
                    }
                    self.appDelegate.saveContext();
                }
                
                
                
                completion(result: listLogs, dailyTotals: dailyTotals);
        }
    }
    
    func getPrescriptionHelp(completion:(result: String)->Void){
        
        //health_points
        let url: String      = "\(SERVER_URL)\(HEALTHPOINTS_HELP)"

        request(.GET, url, encoding: .JSON, headers: self.appDelegate.requestHeaders())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { (response: Response) -> Void in
                let result = response.result
                if let jsonObject: AnyObject = result.value {

                   let  dailyTotals = JSON(jsonObject)["content"].stringValue
                    completion(result:dailyTotals);

                }
        }

    }
}
